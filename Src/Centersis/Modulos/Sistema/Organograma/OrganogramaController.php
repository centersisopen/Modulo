<?php

namespace Centersis\Modulos\Sistema\Organograma;

use Centersis\Zion2\Exception\ValidationException;
use Centersis\Sistema\Organograma\OrganogramaClass;
use Centersis\Sistema\Organograma\OrganogramaForm;
use Centersis\Ext\Core\BasicoController;

class OrganogramaController extends BasicoController {

    public function __construct() {
        parent::__construct();

        $this->class = new OrganogramaClass();
        $this->form = new OrganogramaForm($_SESSION['organograma_cod']);
    }

    protected function iniciar($nomeView = null) {
        return parent::iniciar($nomeView);
    }
    
    protected function cadastrar() {
        if ($_SESSION['usuario_cod'] <> '1') {
            throw new ValidationException('Acesso negado');
        }

        return parent::cadastrar();
    }

    protected function alterar() {

        if ($_SESSION['usuario_cod'] <> '1') {
            throw new ValidationException('Acesso negado');
        }

        return parent::alterar();
    }

    protected function remover() {

        if ($_SESSION['usuario_cod'] <> '1') {
            throw new ValidationException('Acesso negado');
        }

        return parent::remover();
    }

}
