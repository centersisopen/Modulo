<?php

namespace Centersis\Modulos\Sistema\Organograma;

use Centersis\Zion2\Banco\Conexao;
use Centersis\Ext\Core\Basico\BasicoSql;

class OrganogramaSql extends BasicoSql {

    public function __construct($con = null) {

        if (!$con) {
            $con = Conexao::conectar();
        }

        parent::__construct($con);
    }

    public function filtrarSql($objForm, $colunas) {

        $qb = $this->con->qb();

        $qb->select('organograma_cod, organograma_nome, organograma_ancestral, '
                        . 'organograma_sigla, organograma_dominio_site, '
                        . 'organograma_dominio_sistema, organograma_status, '
                        . 'organograma_hash, organograma_tipo')
                ->from('organograma', '');

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $colunas, $qb, '', null);

        return $qb;
    }

    public function getDadosSql($cod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('organograma', '')
                ->where($qb->expr()->eq('organograma_cod', ':organograma_cod'))
                ->setParameter('organograma_cod', $cod);

        return $qb;
    }

    public function getDadosOrganogramaSql($cod) {

        return $this->getDadosSql($cod);
    }

    public function getOrganogramaRefCodSql($cod = '', $acao = '') {

        $qb = $this->con->qb();

        $qb->select('a.organograma_cod AS campocod,'
                        . 'a.organograma_nome AS campodesc')
                ->from('organograma', 'a');
        if ($_SESSION['organograma_cod'] <> 1) {
            $qb->andWhere('LOCATE(' . $qb->expr()->literal('|' . $_SESSION['organograma_cod'] . '|') . ', a.organograma_ancestral)');

            $qb->andWhere($qb->expr()->neq('a.organograma_cod', ':organograma_cod'))
                    ->setParameter('organograma_cod', $_SESSION['organograma_cod'], \PDO::PARAM_INT);
        }

        if ($acao == "alterar") {

            $qb->add('where', $qb->expr()->neq('a.organograma_cod', ':organogramaCod'))
                    ->setParameter('organogramaCod', $cod, \PDO::PARAM_INT);
        }

        return $qb;
    }

}
