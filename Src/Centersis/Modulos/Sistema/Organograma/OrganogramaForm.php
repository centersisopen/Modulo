<?php

namespace Centersis\Modulos\Sistema\Organograma;

use Centersis\Ext\Form\Form;
use Centersis\Ext\Core\Basico\BasicoForm;
use Centersis\Zion2\Banco\Conexao;

class OrganogramaForm extends BasicoForm {

    protected $class;
    protected $tipos;

    public function __construct($organogramaCod, $con = null) {

        if (!$con) {
            $con = Conexao::conectar();
        }

        parent::__construct($organogramaCod, $con);

        $this->class = new OrganogramaClass();

        $this->tipos = [
            'Centersis' => 'Centersis'];
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $cod = null) {
        $form = new Form();

        $form->setAcao($acao);

        $form->config('formManu' . $cod, 'POST')
                ->setHeader('Configuração do Organograma');

        $campos[] = $form->hidden('cod')
                ->setValor($form->retornaValor('cod'));

        $campos[] = $form->chosen('organograma_ref_cod', 'Referência', false)
                ->setInicio('Opcional')
                ->setValor($form->retornaValor('organograma_ref_cod'))
                ->setMultiplo(false)
                ->setCampoCod('campocod')
                ->setOrdena(false)
                ->setSqlCompleto($this->class->getOrganogramaRefCod($cod, $acao))
                ->setCampoDesc('campodesc');

        $campos[] = $form->escolha('organograma_tipo', 'Para', true)
                ->setArray($this->tipos)
                ->setValor($form->retornaValor('organograma_tipo'));

        $campos[] = $form->texto('organograma_nome', 'Organograma', true)
                ->setValor($form->retornaValor('organograma_nome'));

        $campos[] = $form->texto('organograma_sigla', 'Sigla', true)
                ->setValor($form->retornaValor('organograma_sigla'));
        
        if($acao == 'cadastrar'){
        $campos[] = $form->senha('senha', 'Senha do Admin', false)
                ->setValor($form->retornaValor('senha'));
        }
        
        $campos[] = $form->texto('organograma_dominio_site', 'Domínio Site', false)
                ->setValor($form->retornaValor('organograma_dominio_site'));

        $campos[] = $form->texto('organograma_dominio_sistema', 'Domínio Sistema', false)
                ->setValor($form->retornaValor('organograma_dominio_sistema'));

        $campos[] = $form->escolha('organograma_status', 'Status', true)
                ->setValor($form->retornaValor('organograma_status'))
                ->setValorPadrao('A')
                ->setMultiplo(false)
                ->setExpandido(true)
                ->setArray(['A' => 'Ativo', 'I' => 'Inativo']);

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao('formManu' . $cod);

        return $form->processarForm($campos);
    }

    public function getOrganogramaTopoForm($organogramaCod) {

        $form = new Form();

        $form->config('FormOrganograma', 'GET')
                ->setNovalidate(true);

        $getDadosOrganograma = $this->class->getDadosOrganograma($organogramaCod);
        $organogramaNome = $getDadosOrganograma['organograma_sigla'];

        $campos[] = $form->suggest('organograma_cod', 'organograma', false)
                ->setUrl(SIS_URL_BASE . 'Dashboard/')
                ->setParametros(['acao' => 'getSuggest'])
                ->setClassCss('clearfix')
                ->setPlaceHolder($organogramaNome)
                ->setCampoCod('organograma_cod')
                ->setHidden(true)
                ->setOnSelect('getControllerOrganograma(\'organogramaCod\', \'organograma\', \'setOrganogramaCod\')');

        $form->processarForm($campos);
        return $form->getFormHtml('organograma_cod');
    }

}
