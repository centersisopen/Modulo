<?php

namespace Centersis\Modulos\Sistema\Organograma;

use Centersis\Zion2\Banco\Conexao;
use Centersis\Sistema\Usuario\Sistema\UsuarioClassFactory;
use Centersis\Ext\Core\Basico\BasicoClass;
use Centersis\Cadastro\Empresa\Sistema\EmpresaClassFactory;
use Centersis\Ext\Form\Form;

class OrganogramaClass extends BasicoClass {

    public function __construct($con = null, $sql = null) {

        if (!$con) {
            $con = Conexao::conectar();
        }

        if (!$sql) {
            $sql = new OrganogramaSql($con);
        }

        parent::__construct($con, $sql);

        $this->tabela = 'organograma';
        $this->chavePrimaria = 'organograma_cod';

        $this->colunasCrud = [
            'organograma_ref_cod',
            'organograma_nome',
            'organograma_sigla',
            'organograma_dominio_site',
            'organograma_dominio_sistema',
            'organograma_status',
            'organograma_tipo',
        ];

        $this->colunasGrid = [
            'organograma_nome' => ["Organograma", 'organograma_nome', false],
            'organograma_sigla' => 'Abreviação',
            'organograma_dominio_site' => 'Site',
            'organograma_dominio_sistema' => 'Sistema',
            'organograma_status' => 'Status',
            'organograma_ancestral' => 'Hierarquia',
            'organograma_tipo' => 'Para',
            'organograma_hash' => 'Hash'
        ];

        $this->filtroDinamico = [
            'organograma_nome' => '',
            'organograma_sigla' => ''
        ];
    }

    public function grid($grid) {

        $grid->substituaPor('organograma_status', ['A' => 'Ativo', 'I' => 'Inativo']);
        $grid->substituaPor('organograma_dominio_site', ['<em>não informado</em>']);
        $grid->substituaPor('organograma_dominio_sistema', ['<em>não informado</em>']);
        $grid->setQuemOrdena('organograma_ancestral');
    }

    public function getDadosOrganograma($cod) {
        return $this->con->execLinha($this->sql->getDadosOrganogramaSql($cod));
    }

    public function getResetOrganograma($organogramaCod, $usuarioCod) {

        $usuarioClass = (new UsuarioClassFactory())->instancia($organogramaCod);

        $dadosUsuario = $usuarioClass->getDadosSemOrganograma($usuarioCod);

        return ($dadosUsuario['organograma_cod'] <> $organogramaCod) ? true : false;
    }

    public function getOrganogramaRefCod($cod = '', $acao = '') {

        return $this->sql->getOrganogramaRefCodSql($cod, $acao);
    }

    public function cadastrar($objForm) {

        $this->colunasCrud[] = 'organograma_hash';

        $objForm->set('organograma_hash', bin2hex(openssl_random_pseudo_bytes(20)));

        $this->con->startTransaction();

        $organogramaCod = parent::cadastrar($objForm);

        $this->updateAncestral($organogramaCod, $objForm);

        $empresaCod = $this->cadastrarEmpresa($organogramaCod, $objForm);

        $this->criaConfiguracaoBasica($organogramaCod, $empresaCod, $objForm);

        $this->con->stopTransaction();

        return $organogramaCod;
    }

    public function alterar($objForm) {

        $this->con->startTransaction();

        $afetados = parent::alterar($objForm);

        $this->updateAncestral($objForm->get('cod'), $objForm);

        $this->con->stopTransaction();

        return $afetados;
    }

    private function cadastrarEmpresa($organogramaCod, $objForm) {
        $empresaClass = (new EmpresaClassFactory())->instancia($organogramaCod);

        $objForm->set('empresa_nome', $objForm->get('organograma_nome'));
        $objForm->set('empresa_sigla', $objForm->get('organograma_sigla'));
        $objForm->set('empresa_tratamento', 'A');

        return $empresaClass->cadastrar($objForm);
    }

    private function updateAncestral($organogramaCod, $objForm) {
        $referenciaCod = $objForm->get('organograma_ref_cod');

        $referenciaAncestral = null;

        if ($referenciaCod) {
            $referenciaAncestral = $this->getDados($referenciaCod, 'organograma_ancestral');
        }

        $objForm->set('organograma_ancestral', '|' . $organogramaCod . '|' . $referenciaAncestral);

        $this->crudUtil->update(null, $this->tabela, ['organograma_ancestral'], $objForm, [$this->chavePrimaria => $organogramaCod]);
    }

    protected function criaConfiguracaoBasica($organogramaCod, $empresaCod, $objForm) {

        $criarUsuario = $objForm->get('criar_usuario');
        $senhaAdmin = $objForm->get('senha');
        
        $contas = $this->planoContas($organogramaCod, $empresaCod);

        $this->formaPagamento($organogramaCod, $contas['banco'], $contas['caixa']);

        $this->referencia($organogramaCod);

        if ($criarUsuario <> 'N') {
            $perfilCod = $this->criaPerfilAdm($organogramaCod);
            
            $this->criaUsuarioAdm($organogramaCod, $perfilCod, $senhaAdmin);
        }
    }

    public function planoContas($organogramaCod, $empresaCod) {
        $tabela = 'fnc_conta_contabil';

        $camposTipo = [
            'fnc_conta_tipo_nome',
            'fnc_conta_tipo_opcao',
            'fnc_conta_contabil_cod'
        ];

        $campos = [
            'fnc_conta_contabil_ref_cod',
            'fnc_conta_contabil_saldo',
            'fnc_conta_contabil_nome',
            'fnc_conta_contabil_nivel',
            'fnc_conta_contabil_tipo',
            'fnc_conta_contabil_ordem'];

        $ativo = $this->crudUtil->insert($organogramaCod, $tabela, $campos, [
            'fnc_conta_contabil_ref_cod' => ['Inteiro' => null],
            'fnc_conta_contabil_saldo' => ['Float' => 0],
            'fnc_conta_contabil_nome' => ['Texto' => 'ATIVO'],
            'fnc_conta_contabil_nivel' => ['Texto' => '1'],
            'fnc_conta_contabil_tipo' => ['Texto' => 'NP'],
            'fnc_conta_contabil_ordem' => ['Texto' => '0001']]);

        $caixa = $this->crudUtil->insert($organogramaCod, $tabela, $campos, [
            'fnc_conta_contabil_ref_cod' => ['Inteiro' => $ativo],
            'fnc_conta_contabil_saldo' => ['Float' => 0],
            'fnc_conta_contabil_nome' => ['Texto' => 'Caixa'],
            'fnc_conta_contabil_nivel' => ['Texto' => '1.1'],
            'fnc_conta_contabil_tipo' => ['Texto' => 'CD'],
            'fnc_conta_contabil_ordem' => ['Texto' => '00010001']]);

        $this->crudUtil->insert($organogramaCod, 'fnc_conta_tipo', $camposTipo, [
            'fnc_conta_tipo_nome' => ['Texto' => 'Caixa'],
            'fnc_conta_tipo_opcao' => ['Texto' => 'M'],
            'fnc_conta_contabil_cod' => ['Inteiro' => $caixa],
        ]);

        $banco = $this->crudUtil->insert($organogramaCod, $tabela, $campos, [
            'fnc_conta_contabil_ref_cod' => ['Inteiro' => $ativo],
            'fnc_conta_contabil_saldo' => ['Float' => 0],
            'fnc_conta_contabil_nome' => ['Texto' => 'Banco'],
            'fnc_conta_contabil_nivel' => ['Texto' => '1.2'],
            'fnc_conta_contabil_tipo' => ['Texto' => 'CD'],
            'fnc_conta_contabil_ordem' => ['Texto' => '00010002']]);

        $this->crudUtil->insert($organogramaCod, 'fnc_conta_tipo', $camposTipo, [
            'fnc_conta_tipo_nome' => ['Texto' => 'Banco'],
            'fnc_conta_tipo_opcao' => ['Texto' => 'M'],
            'fnc_conta_contabil_cod' => ['Inteiro' => $banco],
        ]);

        $passivoCod = $this->crudUtil->insert($organogramaCod, $tabela, $campos, [
            'fnc_conta_contabil_ref_cod' => ['Inteiro' => null],
            'fnc_conta_contabil_saldo' => ['Float' => 0],
            'fnc_conta_contabil_nome' => ['Texto' => 'PASSIVO'],
            'fnc_conta_contabil_nivel' => ['Texto' => '2'],
            'fnc_conta_contabil_tipo' => ['Texto' => 'NP'],
            'fnc_conta_contabil_ordem' => ['Texto' => '0002']]);

        $aPagar = $this->crudUtil->insert($organogramaCod, $tabela, $campos, [
            'fnc_conta_contabil_ref_cod' => ['Inteiro' => $passivoCod],
            'fnc_conta_contabil_saldo' => ['Float' => 0],
            'fnc_conta_contabil_nome' => ['Texto' => 'Contas a Pagar'],
            'fnc_conta_contabil_nivel' => ['Texto' => '2.1'],
            'fnc_conta_contabil_tipo' => ['Texto' => 'CD'],
            'fnc_conta_contabil_ordem' => ['Texto' => '00020001']]);

        $aReceber = $this->crudUtil->insert($organogramaCod, $tabela, $campos, [
            'fnc_conta_contabil_ref_cod' => ['Inteiro' => $passivoCod],
            'fnc_conta_contabil_saldo' => ['Float' => 0],
            'fnc_conta_contabil_nome' => ['Texto' => 'Contas a Receber'],
            'fnc_conta_contabil_nivel' => ['Texto' => '2.2'],
            'fnc_conta_contabil_tipo' => ['Texto' => 'CD'],
            'fnc_conta_contabil_ordem' => ['Texto' => '00020002']]);

        $this->crudUtil->insert($organogramaCod, $tabela, $campos, [
            'fnc_conta_contabil_ref_cod' => ['Inteiro' => null],
            'fnc_conta_contabil_saldo' => ['Float' => 0],
            'fnc_conta_contabil_nome' => ['Texto' => 'RECEITAS'],
            'fnc_conta_contabil_nivel' => ['Texto' => '3'],
            'fnc_conta_contabil_tipo' => ['Texto' => 'NP'],
            'fnc_conta_contabil_ordem' => ['Texto' => '0003']]);

        $this->crudUtil->insert($organogramaCod, $tabela, $campos, [
            'fnc_conta_contabil_ref_cod' => ['Inteiro' => null],
            'fnc_conta_contabil_saldo' => ['Float' => 0],
            'fnc_conta_contabil_nome' => ['Texto' => 'DESPESAS'],
            'fnc_conta_contabil_nivel' => ['Texto' => '4'],
            'fnc_conta_contabil_tipo' => ['Texto' => 'NP'],
            'fnc_conta_contabil_ordem' => ['Texto' => '0004']]);

        //Configurações
        $this->crudUtil->insert($organogramaCod, 'fnc_config', [
            'empresa_cod',
            'fnc_conta_contabil_receber',
            'fnc_conta_contabil_pagar'], [
            'organogramaCod' => ['Inteiro' => $organogramaCod],
            'empresa_cod' => ['Inteiro' => $empresaCod],
            'fnc_conta_contabil_receber' => ['Inteiro' => $aReceber],
            'fnc_conta_contabil_pagar' => ['Inteiro' => $aPagar]
        ]);

        return [
            'banco' => $banco,
            'caixa' => $caixa
        ];
    }

    protected function formaPagamento($organogramaCod, $banco, $caixa) {

        $form = new Form();

        $form->set('fnc_conta_contabil_cod', $banco);
        $form->set('organogramaCod', $organogramaCod);
        $form->set('fnc_forma_nome', 'Banco');
        $form->set('fnc_forma_tipo', 'P');
        $this->crudUtil->insert($organogramaCod, 'fnc_forma', [
            'fnc_conta_contabil_cod',
            'fnc_forma_nome',
            'fnc_forma_tipo'
                ], $form);

        $form->set('fnc_conta_contabil_cod', $banco);
        $form->set('organogramaCod', $organogramaCod);
        $form->set('fnc_forma_nome', 'Banco');
        $form->set('fnc_forma_tipo', 'R');
        $this->crudUtil->insert($organogramaCod, 'fnc_forma', [
            'fnc_conta_contabil_cod',
            'fnc_forma_nome',
            'fnc_forma_tipo'
                ], $form);

        $form->set('fnc_conta_contabil_cod', $caixa);
        $form->set('organogramaCod', $organogramaCod);
        $form->set('fnc_forma_nome', 'Caixa');
        $form->set('fnc_forma_tipo', 'P');
        $this->crudUtil->insert($organogramaCod, 'fnc_forma', [
            'fnc_conta_contabil_cod',
            'fnc_forma_nome',
            'fnc_forma_tipo'
                ], $form);

        $form->set('fnc_conta_contabil_cod', $caixa);
        $form->set('organogramaCod', $organogramaCod);
        $form->set('fnc_forma_nome', 'Caixa');
        $form->set('fnc_forma_tipo', 'R');
        $this->crudUtil->insert($organogramaCod, 'fnc_forma', [
            'fnc_conta_contabil_cod',
            'fnc_forma_nome',
            'fnc_forma_tipo'
                ], $form);
    }

    protected function referencia($organogramaCod) {
        $form = new Form();
        $form->set('organogramaCod', $organogramaCod);
        $form->set('fnc_referencia_nome', 'Padrão');
        $this->crudUtil->insert($organogramaCod, 'fnc_referencia', [
            'fnc_referencia_nome',
                ], $form);
    }

    protected function criaPerfilAdm($organogramaCod) {

        $valoresPerfil = [
            'perfil_nome' => ['Texto' => 'Administrador'],
            'perfil_tipo' => ['Texto' => 'Principal'],
            'perfil_descricao' => ['Texto' => 'Administrador do Sistema'],
            'perfil_aplicacao' => ['Texto' => 'Sistema'],
        ];

        $perfilCod = $this->crudUtil->insert($organogramaCod, '_perfil', array_keys($valoresPerfil), $valoresPerfil);

        $this->permissoesAoPerfil($perfilCod);

        return $perfilCod;
    }

    protected function permissoesAoPerfil($perfilCod) {

        $modulos = $this->con->paraArray("
            SELECT a.modulo_cod FROM _modulo a INNER JOIN _grupo b ON a.grupo_cod = b.grupo_cod
            LEFT JOIN _modulo_visivel c ON a.modulo_cod = c.modulo_cod
            WHERE  a.modulo_restrito = 'N' AND b.grupo_aplicacao = 'Sistema' 
            AND c.modulo_visivel_cod IS NULL;", 'modulo_cod');

        foreach ($modulos as $moduloCod) {

            $acoes = $this->con->paraArray("
            SELECT b.acao_modulo_cod FROM _modulo a INNER JOIN _acao_modulo b ON a.modulo_cod = b.modulo_cod
            WHERE b.acao_modulo_organogramas IS NULL AND a.modulo_cod = " . $moduloCod . ";", 'acao_modulo_cod');

            foreach ($acoes as $acaoModuloCod) {

                $valores = [
                    'acao_modulo_cod' => ['Inteiro' => $acaoModuloCod],
                    'perfil_cod' => ['Inteiro' => $perfilCod, $acaoModuloCod],
                ];

                $this->crudUtil->insert(null, '_permissao', array_keys($valores), $valores);
            }
        }
    }

    protected function criaUsuarioAdm($organogramaCod, $perfilCod, $senhaAdmin) {

        $usuarioClass = (new UsuarioClassFactory())->instancia($organogramaCod);
        $form = new Form();

        $form->set('perfil_cod', $perfilCod);
        $form->set('pessoa_nome', 'Administrador');
        $form->set('usuario_login', 'admin');
        $form->set('usuario_senha', $senhaAdmin);
        $form->set('usuario_repita_senha', $senhaAdmin);
        $form->set('usuario_super', 'S');
        $form->set('usuario_status', 'A');

        $usuarioClass->cadastrar($form);
    }

}
