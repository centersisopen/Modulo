<?php

namespace Centersis\Modulos\Sistema\AcaoModulo;

use Centersis\Ext\Core\Basico\BasicoClass;
use Centersis\Zion2\Banco\Conexao;

class AcaoModuloClass extends BasicoClass {

    public function __construct($con = null, $sql = null) {

        if (!$con) {
            $con = Conexao::conectar();
        }

        if (!$sql) {
            $sql = new AcaoModuloSql($con);
        }

        parent::__construct($con, $sql);

        $this->tabela = '_acao_modulo';
        $this->chavePrimaria = 'acao_modulo_cod';

        $this->colunasCrud = [
            'acao_modulo_cod',
            'perfil_cod'
        ];
    }

    public function acoesDoModulo($moduloCod) {
        return $this->con->paraArray($this->sql->acoesDoModuloSql($moduloCod), null, 'acao_modulo_cod');
    }
}
