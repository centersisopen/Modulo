<?php

namespace Centersis\Modulos\Sistema\AcaoModulo;

use Centersis\Ext\Core\Basico\BasicoSql;

class AcaoModuloSql extends BasicoSql {

    public function __construct($con) {
        parent::__construct($con);
    }

    public function acoesDoModuloSql($moduloCod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_acao_modulo', '')
                ->where($qb->expr()->eq('modulo_cod', ':modulo_cod'))
                ->setParameter('modulo_cod', $moduloCod, \PDO::PARAM_INT)
                ->orderBy('acao_modulo_posicao', 'ASC');

        return $qb;
    }
    
}
