<?php

namespace Centersis\Modulos\Sistema\Grupo;

use Centersis\Zion2\Banco\Conexao;
use Centersis\Ext\Core\Basico\BasicoSql;

class GrupoSql extends BasicoSql {

    public function __construct($con = null) {

        if (!$con) {
            $con = Conexao::conectar();
        }

        parent::__construct($con);
    }

    public function filtrarSql($objForm, $colunas) {
        $qb = $this->con->qb();

        $qb->select('grupo_cod, grupo_nome, grupo_pacote, grupo_posicao, '
                . 'grupo_aplicacao, grupo_class')
                ->from('_grupo', '');

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $colunas, $qb, '', null);

        return $qb;
    }

    public function getDadosSql($cod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_grupo', '')
                ->where($qb->expr()->eq('grupo_cod', ':grupo_cod'))
                ->setParameter('grupo_cod', $cod);

        return $qb;
    }

    public function gruposSql($aplicacao) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_grupo', '')
                ->orderBy('grupo_posicao', 'ASC');

        if ($aplicacao) {
            $qb->where($qb->expr()->eq('grupo_aplicacao', ':grupo_aplicacao'))
                    ->setParameter('grupo_aplicacao', $aplicacao);
        }

        return $qb;
    }
}
