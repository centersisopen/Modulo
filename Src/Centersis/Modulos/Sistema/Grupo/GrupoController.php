<?php

namespace Centersis\Modulos\Sistema\Grupo;

use Centersis\Sistema\Grupo\GrupoClass;
use Centersis\Sistema\Grupo\GrupoForm;
use Centersis\Zion2\Acesso\Acesso;
use Centersis\Zion2\Exception\ValidationException;
use Centersis\Ext\Core\BasicoController;

class GrupoController extends BasicoController {

    public function __construct() {
        parent::__construct();

        $this->class = new GrupoClass();
        $this->form = new GrupoForm($this->organograma);
    }

    protected function iniciar($nomeView = 'index.html.twig') {
        return parent::iniciar($nomeView);
    }

    protected function cadastrar() {
        if ($_SESSION['usuario_cod'] <> '1') {
            throw new ValidationException('Acesso negado');
        }

        return parent::cadastrar();
    }

    protected function alterar() {

        if ($_SESSION['usuario_cod'] <> '1') {
            throw new ValidationException('Acesso negado');
        }

        return parent::alterar();
    }

    protected function remover() {

        if ($_SESSION['usuario_cod'] <> '1') {
            throw new ValidationException('Acesso negado');
        }

        return parent::remover();
    }

    protected function mudaPosicao() {
        new Acesso('alterar');

        $posicao = $this->class->mudaPosicao(filter_input(INPUT_GET, 'grupo_cod'), filter_input(INPUT_GET, 'maisMenos'));

        return parent::jsonSucesso($posicao);
    }
}
