<?php

namespace Centersis\Modulos\Sistema\Grupo;

use Centersis\Ext\Form\Form;
use Centersis\Zion2\Banco\Conexao;
use Centersis\Ext\Core\Basico\BasicoForm;

class GrupoForm extends BasicoForm {

    protected $aplicacao;


    public function __construct($organogramaCod, $con = null) {

        if (!$con) {
            $con = Conexao::conectar();
        }

        parent::__construct($organogramaCod, $con);
        
        $this->aplicacao = ['SiteGestor' => 'Gestor do Site', 'SiteRestrito' => 'Acesso Restrito do Site', 'Sistema' => 'Sistema'];
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $formCod = null) {
        $form = new Form();

        $form->setAcao($acao);

        $form->config('formManu' . $formCod, 'POST')
                ->setHeader('Grupos');

        $campos[] = $form->hidden('cod')
                ->setValor($form->retornaValor('cod'));

        $campos[] = $form->texto('grupo_nome', 'Nome', true)
                ->setMaximoCaracteres(50)
                ->setValor($form->retornaValor('grupo_nome'));

        $campos[] = $form->texto('grupo_pacote', 'Diretório', true)
                ->setMaximoCaracteres(50)
                ->setValor($form->retornaValor('grupo_pacote'));

        $campos[] = $form->numero('grupo_posicao', 'Posição', true)
                ->setValorMaximo(99)
                ->setValorMinimo(1)
                ->setValor($form->retornaValor('grupo_posicao'));

        $campos[] = $form->escolha('grupo_aplicacao', 'Aplicação', true)
                ->setArray($this->aplicacao)
                ->setValor($form->retornaValor('grupo_aplicacao'));

        $campos[] = $form->texto('grupo_class', 'Icone', true)
                ->setMaximoCaracteres(30)
                ->setToolTipMsg('Deve conter o nome da classe do repositório do Bootstrap ou Fontes Awesome')
                ->setIconFA('fa-font')
                ->setValor($form->retornaValor('grupo_class'));

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }
}
