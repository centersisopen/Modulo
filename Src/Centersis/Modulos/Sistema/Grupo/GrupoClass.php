<?php

namespace Centersis\Modulos\Sistema\Grupo;

use Centersis\Zion2\Banco\Conexao;
use Centersis\Zion2\Exception\ValidationException;
use Centersis\Ext\Core\Basico\BasicoClass;

class GrupoClass extends BasicoClass {

    public function __construct($con = null, $sql = null) {

        if (!$con) {
            $con = Conexao::conectar();
        }

        if (!$sql) {
            $sql = new GrupoSql($con);
        }

        parent::__construct($con, $sql);

        $this->tabela = '_grupo';
        $this->chavePrimaria = 'grupo_cod';

        $this->colunasCrud = [
            'grupo_nome',
            'grupo_pacote',
            'grupo_posicao',
            'grupo_aplicacao',
            'grupo_class',
        ];

        $this->colunasGrid = [
            'grupo_nome' => "Grupo",
            'grupo_pacote' => "Diretório",
            'grupo_posicao' => "Posição",
            'grupo_aplicacao'=>'Aplicação',
            'grupo_class' => "Ícone"
        ];

        $this->filtroDinamico = [
            'grupo_nome' => "",
            'grupo_pacote' => "",
            'grupo_posicao' => "",
            'grupo_class' => ""
        ];
    }

    public function grid($grid) {
        $grid->naoOrdenePor(['grupo_class']);
        $grid->converterResultado($this, 'mostraIcone', 'grupo_class', ['grupo_class']);
        $grid->converterResultado($this, 'htmlMudaPosicao', 'grupo_posicao', ['grupo_cod', 'grupo_posicao']);
        $grid->setAlinhamento(['grupo_class' => 'Centro', 'grupo_posicao' => 'Centro']);
    }

    public function mudaPosicao($grupoCod, $maisMenos) {
        $qb = $this->con->qb();

        $qb->select('grupo_posicao')
                ->from('_grupo', '')
                ->where($qb->expr()->eq('grupo_cod', ':grupo_cod'))
                ->setParameter('grupo_cod', $grupoCod, \PDO::PARAM_INT);

        $posicaoAtual = $this->con->execRLinha($qb);

        if ($maisMenos === '+') {
            $novaPosicao = $posicaoAtual + 1;

            if ($novaPosicao > 99) {
                throw new ValidationException('O valor máximo já foi alcançado!');
            }
        } else {
            $novaPosicao = $posicaoAtual - 1;

            if ($novaPosicao < 1) {
                throw new ValidationException('O valor mínimo já foi alcançado!');
            }
        }

        $update = array('grupo_posicao' => array('Inteiro' => $novaPosicao));

        $this->crudUtil->update(null, $this->tabela, ['grupo_posicao'], $update, [$this->chavePrimaria => $grupoCod]);

        return $novaPosicao;
    }

    public function htmlMudaPosicao($grupoCod, $posicao) {
        return '<button type="button" class="btn btn-xs" onclick="mudaPosicao(' . $grupoCod . ', \'-\')"><i class="fa fa-minus-square-o"></i></button>
            <button type="button" class="btn btn-xs"><span id="grupo_posicao' . $grupoCod . '"> ' . $posicao . ' </span></button>
            <button type="button" class="btn btn-xs" onclick="mudaPosicao(' . $grupoCod . ', \'+\')"><i class="fa fa-plus-square-o"></i></button>';
    }

    public function mostraIcone($grupoClass) {
        return '<i class="' . $grupoClass . '"></i>';
    }

    public function grupos($aplicacao) {
        return $this->con->paraArray($this->sql->gruposSql($aplicacao));
    }
}
