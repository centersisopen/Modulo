<?php

namespace Centersis\Modulos\Sistema\Configuracoes\Sistema\Padrao;


use Centersis\Ext\Core\Padrao\BaseClass;

class ConfiguracoesClass extends BaseClass {

    public function __construct($organogramaCod, $con, $sql) {

        parent::__construct($organogramaCod, $con, $sql);
    }
    
    public function getParametrizacoes() {

        return $this->con->paraArray($this->sql->getParametrizacoesSql());
    }

    public function setValoresFormParametrizacoes($formIntancia) {
    
        $objForm = $formIntancia->getFormParametrizacoes('alterar');

        $parametrosSql = $this->con->paraArray($this->sql->getParametrizacoesSql());

        $dados = [];

        foreach ($parametrosSql as $parametro) {
            $dados[$parametro['configuracao_nome']] = $parametro['configuracao_valor'];
        }

        $this->crudUtil->setParametrosForm($objForm, $dados, null);

        if (isset($dados['bloquear_processos_nomes'])) {
            $this->transformaEmArray($objForm, $dados['bloquear_processos_nomes']);
        }

        return $objForm;
    }

    public function alterarParametrizacoes($objForm) {
        $this->transformaEmTexto($objForm);

        $this->con->startTransaction();
        
        $this->criarCamposParametrizacao($objForm);
        
        $parametrizacoes = $this->getParametrizacoes();        

        $usuarioCod = $_SESSION['usuario_cod'];
        
        foreach ($parametrizacoes as $parametrizacao) {

            if ($parametrizacao['configuracao_restrita'] == 'S' && $usuarioCod != 1) {
                continue;
            }

            $retorno = $this->getCampoCrud($objForm, $parametrizacao['configuracao_nome']);

            $this->crudUtil->update($this->organogramaCod,
                    'configuracao',
                    $retorno['colunas'],
                    $retorno['valores'],
                    [
                        'configuracao_cod' => $parametrizacao['configuracao_cod']
                    ]
            );
        }

        $this->con->stopTransaction();

        return true;
    }

    public function criarCamposParametrizacao($objForm) {
        $campos = $objForm->getObjetos();

        foreach ($campos as $campo) {
            $parametro = $this->getConfiguracao($campo->getNome());
            if (!$parametro) {
                $this->cadastrarParametrizacao($campo, $objForm);
            }
        }
    }

    public function getConfiguracao($nome) {
        return $this->con->execLinha($this->sql->getConfiguracaoSql($nome));
    }

    public function cadastrarParametrizacao($campo, $objForm) {
        $ignorar = ['layout', 'hidden', 'button'];

        if (!in_array($campo->getTipoBase(), $ignorar) && $campo->getNome() != 'cod') {
            $retorno = $this->getCampoCrud($objForm, $campo->getNome());            
            
            $this->crudUtil->insert($this->organogramaCod, 'configuracao', $retorno['colunas'], $retorno['valores']);
        }
    }

    public function getCampoCrud($objForm, $campo) {
        $colunasCrud = [
            'configuracao_nome',
            'configuracao_valor'
        ];

        $valor = $objForm->get($campo);

        if (!$valor) {
            $valor = '';
        }

        $valores = [
            'configuracao_nome' => ['Texto' => $campo],
            'configuracao_valor' => ['Texto' => $valor]
        ];

        return [
            'colunas' => $colunasCrud,
            'valores' => $valores
        ];
    }

    protected function transformaEmTexto($objForm) {
        $transforma = $objForm->get('bloquear_processos_nomes[]');

        if (is_array($transforma) and count($transforma) > 0) {
            $objForm->set('bloquear_processos_nomes', implode(',', $transforma), "Texto");
        }

        $objForm->unsetObjeto('bloquear_processos_nomes[]');
    }

    protected function transformaEmArray($objForm, $textoTransforma) {
        $valor = $textoTransforma ? explode(',', $textoTransforma) : null;
        $objForm->set('bloquear_processos_nomes[]', $valor);
    }

}
