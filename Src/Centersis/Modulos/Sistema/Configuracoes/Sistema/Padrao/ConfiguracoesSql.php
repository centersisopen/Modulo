<?php

namespace Centersis\Modulos\Sistema\Configuracoes\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class ConfiguracoesSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function getParametrizacoesSql() {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('configuracao', '');

        $this->doOrganograma($qb);

        return $qb;
    }

    public function getConfiguracaoSql($nome) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('configuracao')
                ->where($qb->expr()->eq('configuracao_nome', ':nome'))
                ->setParameter('nome', $nome, \PDO::PARAM_STR);

        $this->doOrganograma($qb);

        return $qb;
    }

}
