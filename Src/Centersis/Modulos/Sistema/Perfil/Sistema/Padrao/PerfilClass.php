<?php

namespace Centersis\Modulos\Sistema\Perfil\Sistema\Padrao;

use Centersis\Zion2\Exception\ValidationException;
use Centersis\Ext\Core\Padrao\BaseClass;
use Centersis\Sistema\Permissao\PermissaoClass;

class PerfilClass extends BaseClass {

    public function __construct($organogramaCod, $con, $sql) {

        parent::__construct($organogramaCod, $con, $sql);

        $this->tabela = '_perfil';
        $this->chavePrimaria = 'perfil_cod';

        $this->colunasCrud = [
            'perfil_nome',
            'perfil_descricao'
        ];

        $this->colunasGrid = [
            'perfil_nome' => 'Perfil',
            'perfil_tipo' => 'Tipo de Perfil',
            'perfil_descricao' => 'Descrição'
        ];

        $this->filtroDinamico = [
            'perfil_nome' => '',
        ];
    }

    public function grid($grid) {
        $grid->setSelecaoMultipla(false);
        $grid->substituaPor('perfil_descricao', ['<i>nenhuma informação</i>']);
        $grid->substituaPor('perfil_tipo', ['<i>Padrão</i>']);
    }

    public function cadastrar($objForm) {

        $this->colunasCrud[] = 'perfil_aplicacao';
        $this->colunasCrud[] = 'cargo_cod';

        if ($_SESSION['usuario_cod'] == '1') {
            $this->colunasCrud[] = 'perfil_tipo';
        }
        
        if (!$objForm->get('perfil_aplicacao')) {
            $objForm->set('perfil_aplicacao', 'Sistema');
        }
        
        $permissao = new PermissaoClass($this->organogramaCod, $this->con);

        $this->crudUtil->startTransaction();

        $perfilCod = $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm);

        $objForm->set('perfil_cod', $perfilCod);

        $permissao->cadastrar($objForm);

        $this->crudUtil->stopTransaction();
        
        return $perfilCod;
    }

    public function alterar($objForm) {

        $permissao = new PermissaoClass($this->organogramaCod, $this->con);

        $this->crudUtil->startTransaction();

        if ($_SESSION['usuario_cod'] == '1') {
            $this->colunasCrud[] = 'perfil_tipo';
        }
        
        $alterados = $this->crudUtil->update($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm, [$this->chavePrimaria => $objForm->get('cod')]);

        $objForm->set('perfil_cod', $objForm->get('cod'));

        $permissao->alterar($objForm);

        $this->crudUtil->stopTransaction();

        return $alterados;
    }

    public function remover($cod) {
        $permissao = new PermissaoClass($this->organogramaCod, $this->con);

        if ($this->con->existe('_usuario', 'perfil_cod', $cod)) {
            throw new ValidationException('Não é possível remover este perfil pois ele possui um ou mais usuários dependentes!');
        }

        $this->crudUtil->startTransaction();

        $permissao->removerPorPerfilCod($cod);

        $removidos = $this->crudUtil->delete($this->organogramaCod, $this->tabela, [$this->chavePrimaria => $cod]);

        $this->crudUtil->stopTransaction();

        return $removidos;
    }

    public function setValoresFormManu($cod, $formIntancia, $acao = 'alterar') {
        $objForm = $formIntancia->getFormManu($acao, $cod);

        $parametrosSql = $this->con->execLinha($this->sql->getDadosSql($cod));

        $this->crudUtil->setParametrosForm($objForm, $parametrosSql, $cod);

        return $objForm;
    }

    public function getOrganogramaCod() {
        return $this->sql->getOrganogramaCodSql();
    }

    public function perfilsDaAplicacao($aplicacao) {
        return $this->con->paraArray($this->sql->perfilsDaAplicacaoSql($aplicacao));
    }

    public function perfilDoCargo($cargoCod, $aplicacao) {
        return $this->con->execLinha($this->sql->perfilDoCargoSql($cargoCod, $aplicacao));
    }

}
