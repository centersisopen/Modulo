<?php

namespace Centersis\Modulos\Sistema\Perfil\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Ext\Core\Padrao\BaseForm;
use Centersis\Sistema\Permissao\PermissaoClass;

class PerfilForm extends BaseForm {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    /**
     *
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $formCod = null) {

        $form = new Form();

        $form->setAcao($acao);

        $form->config('formManu', 'POST')
                ->setHeader('Perfil');

        $campos[] = $form->hidden('cod')
                ->setValor($form->retornaValor('cod'));

        $campos[] = $form->texto('perfil_nome', 'Perfil', true)
                ->setMaximoCaracteres(100)
                ->setMinimoCaracteres(2)
                ->setValor($form->retornaValor('perfil_nome'));

        if ($_SESSION['usuario_cod'] == '1') {
            $campos[] = $form->escolha('perfil_tipo', 'Tipo de Perfil', false)
                    ->setInicio('Padrão')
                    ->setArray(['Principal' => 'Principal'])
                    ->setValor($form->retornaValor('perfil_tipo'));
        }

        $campos[] = $form->textArea('perfil_descricao', 'Descrição', false)
                ->setMaximoCaracteres(1000)
                ->setLinhas(2)
                ->setValor($form->retornaValor('perfil_descricao'));

        $campos[] = $form->layout('linha', '<div class="form-group"><div class="col-sm-12"><hr class="panel-wide"></div></div>');

        $campos['perfil_cod'] = $form->chosen('perfil_modelo', 'Usar Modelo de Perfil', false)
                ->setMultiplo(true)
                ->setTabela('_perfil')
                ->setCampoCod('perfil_cod')
                ->setCampoDesc('perfil_nome')
                ->setInstrucoes(['perfil_aplicacao', \PDO::PARAM_STR, 'Igual', 'Sistema'])
                ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod]);

        $campos[] = $form->layout('linha2', '<div class="form-group"><div class="col-sm-12"><hr class="panel-wide"></div></div>');

        $campos[] = $form->layout('Permissoes', (new PermissaoClass($this->organogramaCod, $this->con))->layoutPermissao($acao, $formCod, $_SESSION['usuario_cod'], null));

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

}
