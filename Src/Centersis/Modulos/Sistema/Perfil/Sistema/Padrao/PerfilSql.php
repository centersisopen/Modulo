<?php

namespace Centersis\Modulos\Sistema\Perfil\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class PerfilSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function filtrarSql($objForm, $filtroDinamico = []) {
        $qb = $this->con->qb();

        $qb->select('a.perfil_cod, a.perfil_nome, a.perfil_descricao, a.perfil_tipo')
                ->from('_perfil', 'a')
                ->innerJoin('a', 'organograma', 'b', 'a.organograma_cod = b.organograma_cod')
                ->andWhere($qb->expr()->eq('perfil_aplicacao', ':perfil_aplicacao'))
                ->setParameter('perfil_aplicacao', 'Sistema', \PDO::PARAM_STR);

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $filtroDinamico, $qb, 'a', $this->organogramaCod);

        return $qb;
    }

    public function getDadosSql($cod) {

        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_perfil', '')
                ->where($qb->expr()->eq('perfil_cod', ':perfil_cod'))
                ->setParameter('perfil_cod', $cod, \PDO::PARAM_INT);

        $this->doOrganograma($qb);

        return $qb;
    }

    public function getOrganogramaCodSql() {

        $qb = $this->con->qb();

        $qb->select('a.organograma_cod, a.organograma_nome')
                ->from('organograma', 'a')
                ->where('LOCATE(' . $qb->expr()->literal('|' . $this->organogramaCod . '|') . ', a.organograma_ancestral)');

        $qb->orderBy('a.organograma_cod');

        return $qb;
    }

    public function perfilsDaAplicacaoSql($aplicacao) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_perfil', 'a')
                ->where($qb->expr()->eq('perfil_aplicacao', ':perfil_aplicacao'))
                ->setParameter('perfil_aplicacao', $aplicacao, \PDO::PARAM_STR);

        $this->doOrganograma($qb, 'a');

        return $qb;
    }    
    
    public function perfilDoCargoSql($cargoCod, $aplicacao) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_perfil', 'a')
                ->where($qb->expr()->eq('cargo_cod', ':cargo_cod'))
                ->andWhere($qb->expr()->eq('perfil_aplicacao', ':perfil_aplicacao'))
                ->setParameter('cargo_cod', $cargoCod, \PDO::PARAM_INT)
                ->setParameter('perfil_aplicacao', $aplicacao, \PDO::PARAM_STR);

        $this->doOrganograma($qb, 'a');

        return $qb;
    }    
}
