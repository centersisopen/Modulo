<?php

namespace Centersis\Modulos\Sistema\Permissao;

class PermissaoSql
{
    /**
     * @var \Zion2\Banco\Conexao
     */
    protected $con;
    protected $organogramaCod;

    public function __construct($organogramaCod, $con) {

        $this->organogramaCod = $organogramaCod;
        $this->con = $con;
    }
    
    public function permissoesPerfil($moduloCod, $perfil_cod)
    {
        $qb = $this->con->qb();

        $qb->select('d.acao_modulo_cod')
                ->from('_perfil', 'b')
                ->innerJoin('b', '_permissao', 'c', 'b.perfil_cod = c.perfil_cod')
                ->innerJoin('c', '_acao_modulo', 'd', 'c.acao_modulo_cod = d.acao_modulo_cod')
                ->innerJoin('d', '_modulo', 'e', 'd.modulo_cod = e.modulo_cod')
                ->where($qb->expr()->eq('b.perfil_cod', ':perfil_cod'))
                ->andWhere($qb->expr()->eq('e.modulo_cod', ':modulo_cod'))
                ->setParameter('perfil_cod', $perfil_cod, \PDO::PARAM_INT)
                ->setParameter('modulo_cod', $moduloCod, \PDO::PARAM_INT);

        return $qb;
    }
    
    public function permissoesPerfilModelo($perfilModeloCod)
    {
        $qb = $this->con->qb();

        $qb->select('b.acao_modulo_cod')
                ->from('perfil_modelo', 'a')
                ->innerJoin('a', 'perfil_modelo_acao', 'b', 'a.perfil_modelo_cod = b.perfil_modelo_cod')
                ->where($qb->expr()->eq('a.perfil_modelo_cod', ':perfil_modelo_cod'))
                ->setParameter('perfil_modelo_cod', $perfilModeloCod, \PDO::PARAM_INT);

        return $qb;
    }
    
    public function acoesPerfil($perfil_cod)
    {
        $qb = $this->con->qb();

        $qb->select('acao_modulo_cod')
                ->from('_permissao', '')
                ->where($qb->expr()->eq('perfil_cod', ':perfil_cod'))
                ->setParameter('perfil_cod', $perfil_cod, \PDO::PARAM_INT);

        return $qb;
    }
}
