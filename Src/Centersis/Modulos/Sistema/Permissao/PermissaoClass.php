<?php

namespace Centersis\Modulos\Sistema\Permissao;

use Centersis\Zion2\Acesso\Acesso;
use Centersis\Ext\Crud\CrudUtil;
use Centersis\Sistema\Modulo\ModuloClass;
use Centersis\Sistema\AcaoModulo\AcaoModuloClass;
use Centersis\Sistema\Grupo\GrupoClass;
use Centersis\Zion2\Pixel\Twig\Carregador;
use Centersis\Sistema\Perfil\Sistema\PerfilClassFactory;
use Centersis\Sistema\Usuario\Sistema\UsuarioClassFactory;

class PermissaoClass {

    /**
     * @var \Zion2\Banco\Conexao
     */
    protected $con;
    protected $organogramaCod;
    protected $permissaoSql;
    protected $chavePrimaria;
    protected $crudUtil;
    protected $tabela;
    protected $colunasCrud;

    public function __construct($organogramaCod, $con) {

        $this->crudUtil = new CrudUtil();
        $this->permissaoSql = new PermissaoSql($organogramaCod, $con);

        $this->organogramaCod = $organogramaCod;
        $this->con = $con;

        $this->tabela = '_permissao';
        $this->chavePrimaria = 'permissao_cod';

        $this->colunasCrud = [
            'acao_modulo_cod',
            'perfil_cod'
        ];
    }

    public function cadastrar($objForm) {
        $acaoModulo = filter_input(INPUT_POST, 'acaoModulo', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (is_array($acaoModulo)) {
            foreach ($acaoModulo as $acaoModuloCod) {
                $objForm->set('acao_modulo_cod', $acaoModuloCod, 'Inteiro');
                $this->crudUtil->insert(null, $this->tabela, $this->colunasCrud, $objForm);
            }
        }
    }

    public function alterar($objForm) {
        $this->removerPorPerfilCod($objForm->get('perfil_cod'));
        $this->cadastrar($objForm);
    }

    public function removerPorPerfilCod($cod) {
        return $this->crudUtil->delete(null, $this->tabela, ['perfil_cod' => $cod]);
    }

    public function removerPorAcaoModuloCod($acaoModuloCod) {
        return $this->crudUtil->delete(null, $this->tabela, ['acao_modulo_cod' => $acaoModuloCod]);
    }

    public function removerPorModuloCod($moduloCod) {
        $qb = $this->con->qb();
        $qb->select('acao_modulo_cod')
                ->from('_acao_modulo', '')
                ->where($qb->expr()->eq('modulo_cod', ':modulo_cod'))
                ->setParameter('modulo_cod', $moduloCod, \PDO::PARAM_INT);

        $resultados = $this->con->paraArray($qb, 'acao_modulo_cod');

        foreach ($resultados as $acaoModuloCod) {
            $this->removerPorAcaoModuloCod($acaoModuloCod);
        }
    }

    public function montaPermissao($acao, $perfilCod, $usuarioDaSession, $aplicacao) {
        $acesso = new Acesso();
        $modulo = new ModuloClass();
        $acaoModulo = new AcaoModuloClass();

        $organogramaRoot = (new UsuarioClassFactory())->instancia($this->organogramaCod)->getDados($usuarioDaSession, 'organograma_cod');

        $grupos = (new GrupoClass())->grupos($aplicacao);

        $buffer = [];

        foreach ($grupos as $dadosGrupo) {

            $modulos = $modulo->modulosDoGrupo($dadosGrupo['grupo_cod']);
            $nModulos = 0;
            foreach ($modulos as $dadosModulo) {

                $moduloCod = $dadosModulo['modulo_cod'];

                $acoes = $acaoModulo->acoesDoModulo($moduloCod);

                $acesso->setModuloNome($dadosModulo['modulo_nome']);
                $acessosPermitidos = $acesso->permissoesModulo('acao_modulo_id_permissao', 'acao_modulo_cod');
                
                if ($organogramaRoot <> 1 and $aplicacao == 'Sistema') {
                    foreach ($acoes as $codAcoes => $dAcoes) {
                        
                        $restricaoAcao = [];
                        if ($dAcoes['acao_modulo_organogramas']) {
                            $restricaoAcao = explode(',', $dAcoes['acao_modulo_organogramas']);
                        }

                        if ($restricaoAcao) {
                                                        
                            if (array_search($_SESSION['organograma_cod'], $restricaoAcao) === false) {
                                unset($acoes[$codAcoes]);
                                continue;
                            }
                        }

                        if (!array_key_exists($codAcoes, $acessosPermitidos)) {
                            unset($acoes[$codAcoes]);
                        }                                               
                    }
                }

                if (empty($acoes)) {
                    continue;
                }

                $nModulos++;

                $permissaoUsuario = [];
                if ($acao == 'alterar') {

                    $permissaoUsuario = $this->con->paraArray($this->permissaoSql->permissoesPerfil($moduloCod, $perfilCod), 'acao_modulo_cod', 'acao_modulo_cod');
                }

                $semTab = [];

                foreach ($acoes as $chaveAcoes => $dadosAcoes) {
                    $semTab[$chaveAcoes] = $dadosAcoes['acao_modulo_permissao'];
                }

                $buffer[$dadosGrupo['grupo_cod']]['permissoes'][$dadosModulo['modulo_cod']] = $permissaoUsuario;
                $buffer[$dadosGrupo['grupo_cod']]['modulo'][$dadosModulo['modulo_cod']] = $dadosGrupo['grupo_nome'] . $this->caminhoModulo($dadosModulo['modulo_cod']) . ' - ' . $dadosModulo['modulo_desc'];
                $buffer[$dadosGrupo['grupo_cod']]['acoeshtmlsemtab'][$dadosModulo['modulo_cod']] = $semTab;
            }

            if ($nModulos > 0) {
                $buffer[$dadosGrupo['grupo_cod']]['grupo_nome'] = $dadosGrupo['grupo_nome'];
            }
        }

        return $buffer;
    }

    public function caminhoModulo($moduloCod) {

        $caminho = "";

        do {
            $dadosModulo = $this->con->execLinha($this->caminhoModuloSql($moduloCod));

            $moduloReferente = $dadosModulo['modulo_cod_referente'];

            $moduloCod = $moduloReferente;

            $arrayModulo[] = $dadosModulo['modulo_nome_menu'];
        } while (!empty($moduloReferente));

        $arrayModulo = array_reverse($arrayModulo);

        foreach ($arrayModulo as $nomeMenu) {
            $caminho .= ' -> ' . $nomeMenu;
        }

        return $caminho;
    }

    public function caminhoModuloSql($moduloCod) {
        $Sql = "SELECT modulo_cod_referente, modulo_nome_menu
                FROM   _modulo 
                WHERE  modulo_cod = $moduloCod";

        return $Sql;
    }

    public function montaPermissaoModelo($acao, $perfilCod, $aplicacao) {
        $modulo = new ModuloClass();
        $acaoModulo = new AcaoModuloClass();

        $grupos = (new GrupoClass())->grupos($aplicacao);

        $buffer = [];

        foreach ($grupos as $dadosGrupo) {

            $modulos = $this->con->paraArray($modulo->modulosDoGrupoSql($dadosGrupo['grupo_cod']));
            $nModulos = 0;
            foreach ($modulos as $dadosModulo) {

                $moduloCod = $dadosModulo['modulo_cod'];

                $acoes = $acaoModulo->acoesDoModulo($moduloCod);

                if (empty($acoes)) {
                    continue;
                }

                $nModulos++;

                $permissaoUsuario = [];
                if ($acao == 'alterar') {

                    $permissaoUsuario = $this->con->paraArray($this->permissaoSql->permissoesPerfilModelo($perfilCod), 'acao_modulo_cod', 'acao_modulo_cod');
                }

                $semTab = [];

                foreach ($acoes as $chaveAcoes => $dadosAcoes) {

                    $semTab[$chaveAcoes] = $dadosAcoes['acao_modulo_permissao'];
                }


                $buffer[$dadosGrupo['grupo_cod']]['permissoes'][$dadosModulo['modulo_cod']] = $permissaoUsuario;
                $buffer[$dadosGrupo['grupo_cod']]['modulo'][$dadosModulo['modulo_cod']] = $dadosModulo['modulo_nome_menu'];
                $buffer[$dadosGrupo['grupo_cod']]['acoeshtmlsemtab'][$dadosModulo['modulo_cod']] = $semTab;
            }

            if ($nModulos > 0) {
                $buffer[$dadosGrupo['grupo_cod']]['grupo_nome'] = $dadosGrupo['grupo_nome'];
            }
        }

        return $buffer;
    }

    public function permissoesDosPerfils() {

        $perfilClass = (new PerfilClassFactory())->instancia($this->organogramaCod);

        $perfils = $perfilClass->perfilsDaAplicacao('Sistema');

        $ret = [];
        foreach ($perfils as $dadosPerfil) {
            $ret[$dadosPerfil['perfil_cod']] = $this->con->paraArray($this->permissaoSql->acoesPerfil($dadosPerfil['perfil_cod']), 'acao_modulo_cod', 'acao_modulo_cod');
        }

        return $ret;
    }

    function layoutPermissao($acao, $perfilCod, $usuarioDaSession, $aplicacao) {
        $carregador = new Carregador();

        $dadosPermissoes = $this->montaPermissao($acao, $perfilCod, $usuarioDaSession, $aplicacao);

        return $carregador->render('permissoes.html.twig', [
                    'permissoes' => $dadosPermissoes,
                    'acao' => $acao,
                    'aplicacao' => $aplicacao,
                    'perfil_acoes' => $this->permissoesDosPerfils()
        ]);
    }

    function layoutPermissaoModelo($acao, $perfilCod) {
        $carregador = new Carregador();

        $dadosPermissoes = $this->montaPermissaoModelo($acao, $perfilCod, 'Sistema');

        return $carregador->render('permissoes.html.twig', [
                    'permissoes' => $dadosPermissoes,
                    'acao' => $acao,
                    'aplicacao' => '',
                    'perfil_acoes' => $this->permissoesDosPerfils()
        ]);
    }

}
