<?php

namespace Centersis\Modulos\Sistema\Modulo;

use Centersis\Sistema\Permissao\PermissaoClass;
use Centersis\Zion2\Exception\ValidationException;
use Centersis\Zion2\Banco\Conexao;
use Centersis\Ext\Core\Basico\BasicoClass;

class ModuloClass extends BasicoClass {

    public function __construct($con = null, $sql = null) {

        if (!$con) {
            $con = Conexao::conectar();
        }

        if (!$sql) {
            $sql = new ModuloSql($con);
        }

        parent::__construct($con, $sql);

        $this->tabela = '_modulo';
        $this->chavePrimaria = 'modulo_cod';

        $this->colunasCrud = [
            'grupo_cod',
            'modulo_cod_referente',
            'modulo_nome',
            'modulo_nome_menu',
            'modulo_desc',
            'modulo_visivel_menu',
            'modulo_restrito',
            'modulo_posicao',
            'modulo_class',
            'modulo_namespace',
            'modulo_transforma_letra'
        ];

        $this->colunasGrid = [
            'modulo_nome' => 'Módulo',
            'grupo_nome' => 'Grupo',
            'grupo_aplicacao' => 'Aplicação',
            'modulo_nome_referente' => 'Referência',
            'modulo_nome_menu' => 'Nome no Menu',
            'modulo_visivel_menu' => 'V.Menu',
            'modulo_restrito' => 'ROOT',
            'modulo_posicao' => 'Posição',
            'modulo_class' => 'Ícone',
            'visivel' => 'Visível'
        ];

        $this->colunasGridSemHTML = [
            'modulo_posicao' => true
        ];

        $this->filtroDinamico = [
            'modulo_nome' => 'a',
            'grupo_nome' => 'b',
            'modulo_nome_menu' => 'a',
        ];
    }

    public function grid($grid) {

        $grid->setColunasSemHTML($this->colunasGridSemHTML);

        $grid->naoOrdenePor(['modulo_class']);
        $grid->converterResultado($this, 'mostraIcone', 'modulo_class', ['modulo_class']);
        $grid->converterResultado($this, 'htmlMudaPosicao', 'modulo_posicao', ['modulo_cod', 'modulo_posicao']);
        $grid->converterResultado($this, 'visibilidade', 'visivel', ['modulo_cod']);
        $grid->setAlinhamento(['modulo_class' => 'Centro', 'modulo_posicao' => 'Centro', 'modulo_visivel_menu' => 'Centro']);
        $grid->substituaPor('modulo_nome_referente', ['<i>sem referencia</i>']);
        $grid->substituaPor('modulo_visivel_menu', ['S' => 'Sim', 'N' => 'Não']);
        $grid->substituaPor('modulo_restrito', ['S' => 'Sim', 'N' => 'Não']);
    }

    public function cadastrar($objForm) {

        $this->con->startTransaction();

        $moduloCod = parent::cadastrar($objForm);

        $this->validaDuplicacao($objForm);

        $this->visivel($objForm, $moduloCod);

        $this->con->stopTransaction();

        return $moduloCod;
    }

    public function alterar($objForm) {
        $this->con->startTransaction();

        parent::alterar($objForm);

        $this->validaDuplicacao($objForm);

        $this->visivel($objForm, $objForm->get('cod'));

        $this->con->stopTransaction();
    }

    public function remover($cod) {
        $permissao = new PermissaoClass($_SESSION['organograma_cod'], $this->con);

        if ($this->con->existe('_modulo', 'modulo_cod_referente', $cod)) {
            throw new ValidationException('Não é possível remover este módulo pois ele possui um ou mais módulos dependentes!');
        }

        $this->crudUtil->startTransaction();

        $permissao->removerPorModuloCod($cod);

        $this->crudUtil->delete(null, '_acao_modulo', [$this->chavePrimaria => $cod]);

        $this->crudUtil->delete(null, '_log', [$this->chavePrimaria => $cod]);

        $this->crudUtil->delete(null, '_upload', [$this->chavePrimaria => $cod]);

        $this->crudUtil->delete(null, '_modulo_label', [$this->chavePrimaria => $cod]);

        $this->crudUtil->delete(null, '_modulo_visivel', [$this->chavePrimaria => $cod]);

        $this->crudUtil->delete(null, '_usuario_paginacao', [$this->chavePrimaria => $cod]);

        $this->crudUtil->delete(null, '_usuario_grid', [$this->chavePrimaria => $cod]);

        $removidos = $this->crudUtil->delete(null, $this->tabela, [$this->chavePrimaria => $cod]);

        $this->crudUtil->stopTransaction();

        return $removidos;
    }

    public function setValoresFormManu($cod, $formIntancia) {
        $objForm = parent::setValoresFormManu($cod, $formIntancia);

        $objForm->set('organogramas[]', $this->arrayVisivel($cod));

        return $objForm;
    }

    public function mudaPosicao($moduloCod, $maisMenos) {
        $qb = $this->con->qb();

        $qb->select('modulo_posicao')
                ->from('_modulo', '')
                ->where($qb->expr()->eq('modulo_cod', ':modulo_cod'))
                ->setParameter('modulo_cod', $moduloCod, \PDO::PARAM_INT);

        $posicaoAtual = $this->con->execRLinha($qb);

        if ($maisMenos === '+') {
            $novaPosicao = $posicaoAtual + 1;

            if ($novaPosicao > 99) {
                throw new ValidationException('O valor máximo já foi alcançado!');
            }
        } else {
            $novaPosicao = $posicaoAtual - 1;

            if ($novaPosicao < 1) {
                throw new ValidationException('O valor mínimo já foi alcançado!');
            }
        }

        $update = array('modulo_posicao' => array('Inteiro' => $novaPosicao));

        $this->crudUtil->update(null, $this->tabela, ['modulo_posicao'], $update, [$this->chavePrimaria => $moduloCod]);

        return $novaPosicao;
    }

    public function htmlMudaPosicao($moduloCod, $posicao) {
        return '<div style="min-width: 100px;"><button type="button" class="btn btn-xs" onclick="mudaPosicao(' . $moduloCod . ', \'-\')"><i class="fa fa-minus-square-o"></i></button>
            <button type="button" class="btn btn-xs"><span id="modulo_posicao' . $moduloCod . '"> ' . $posicao . ' </span></button>
            <button type="button" class="btn btn-xs" onclick="mudaPosicao(' . $moduloCod . ', \'+\')"><i class="fa fa-plus-square-o"></i></button></div>';
    }

    public function mostraIcone($moduloClass) {
        return '<i class="' . $moduloClass . '"></i>';
    }

    public function getDadosGrupo($modulo) {
        return $this->con->execLinhaArray($this->sql->getDadosGrupoSql($modulo));
    }

    public function getDadosModulo($modulo) {
        return $this->con->execLinhaArray($this->sql->getDadosModuloSql($modulo));
    }

    public function caminhoModulo($moduloCod) {
        $arrayModulo = array();

        $caminho = '';

        do {
            $dadosModulo = $this->con->execLinha($this->sql->getDadosSql($moduloCod));

            $moduloReferente = $dadosModulo['modulo_cod_referente'];

            $moduloCod = $moduloReferente;

            $arrayModulo[] = $dadosModulo['modulo_nome_menu'];
        } while (!empty($moduloReferente));

        $arrayRevertido = array_reverse($arrayModulo);

        foreach ($arrayRevertido as $nomeMenu) {
            $caminho .= ' -> ' . $nomeMenu;
        }

        return $caminho;
    }

    public function modulosDoGrupo($grupoCod) {
        return $this->con->paraArray($this->sql->modulosDoGrupoSql($grupoCod));
    }

    public function visibilidade($moduloCod) {
        $qb = $this->con->qb();

        $qb->select('organograma_nome')
                ->from('_modulo_visivel', 'mv')
                ->innerJoin('mv', 'organograma', 'o', 'mv.organograma_cod = o.organograma_cod')
                ->where($qb->expr()->eq('mv.modulo_cod', ':modulo_cod'))
                ->setParameter('modulo_cod', $moduloCod);

        $organogramas = $this->con->paraArray($qb, 'organograma_nome');

        if ($organogramas) {
            return implode(', ', $organogramas);
        }

        return 'TODOS';
    }

    private function arrayVisivel($moduloCod) {

        $qb = $this->con->qb();

        $qb->select('organograma_cod')
                ->from('_modulo_visivel')
                ->where($qb->expr()->eq('modulo_cod', ':modulo_cod'))
                ->setParameter('modulo_cod', $moduloCod, \PDO::PARAM_INT);

        return $this->con->paraArray($qb, 'organograma_cod');
    }

    private function visivel($objForm, $moduloCod) {

        $objForm->set('modulo_cod', $moduloCod);

        $this->crudUtil->delete(null, '_modulo_visivel', ['modulo_cod' => $moduloCod]);

        $organogramas = $objForm->retornaValor('organogramas[]');

        if (is_array($organogramas)) {
            foreach ($organogramas as $cod) {
                $objForm->set('organograma_cod', $cod);
                $this->crudUtil->insert(null, '_modulo_visivel', ['modulo_cod', 'organograma_cod'], $objForm, ['masterDetail']);
            }
        }
    }

    protected function validaDuplicacao($objForm) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_modulo', '')
                ->where($qb->expr()->eq('modulo_nome', ':modulo_nome'))
                ->setParameter('modulo_nome', $objForm->get('modulo_nome'), \PDO::PARAM_STR);

        $totalComMesmoNome = $this->con->execNLinhas($qb);

        if ($totalComMesmoNome > 1) {
            throw new ValidationException('Já existe um módulo com este mesmo nome!');
        }
    }

}
