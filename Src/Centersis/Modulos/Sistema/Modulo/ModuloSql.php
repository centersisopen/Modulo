<?php

namespace Centersis\Modulos\Sistema\Modulo;

use Centersis\Zion2\Banco\Conexao;
use Centersis\Ext\Core\Basico\BasicoSql;

class ModuloSql extends BasicoSql {

     public function __construct($con = null) {

        if (!$con) {
            $con = Conexao::conectar();
        }

        parent::__construct($con);
    }

    public function filtrarSql($objForm, $filtroDinamico = []) {
        
        $qb = $this->con->qb();

        $qb->select('a.modulo_cod', 'b.grupo_nome', 'c.modulo_nome as modulo_nome_referente',
                        'a.modulo_nome', 'a.modulo_nome_menu', 'a.modulo_desc',
                        'a.modulo_visivel_menu', 'a.modulo_posicao', 
                        'a.modulo_class','b.grupo_aplicacao','a.modulo_restrito')
                ->from('_modulo', 'a')
                ->innerJoin('a', '_grupo', 'b', 'a.grupo_cod = b.grupo_cod')
                ->leftJoin('a', '_modulo', 'c', 'a.modulo_cod_referente = c.modulo_cod');

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $filtroDinamico, $qb, 'a', null);

        return $qb;
    }

    public function getDadosSql($cod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_modulo', '')
                ->where($qb->expr()->eq('modulo_cod', ':modulo_cod'))
                ->setParameter('modulo_cod', $cod, \PDO::PARAM_INT);

        return $qb;
    }

    public function modulosDoGrupoSql($grupoCod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_modulo', '')
                ->where($qb->expr()->eq('grupo_cod', ':grupo_cod'))
                ->setParameter('grupo_cod', $grupoCod, \PDO::PARAM_INT);

        if ($_SESSION['organograma_cod'] <> '1') {
            $qb->andWhere($qb->expr()->eq('modulo_restrito', ':modulo_restrito'))
                    ->setParameter('modulo_restrito', 'N', \PDO::PARAM_STR);
        }

        $qb->orderBy('modulo_posicao', 'ASC');

        return $qb;
    }

    public function getDadosGrupoSql($modulo) {
        $qb = $this->con->qb();

        $qb->select('b.*')
                ->from('_modulo', 'a')
                ->innerJoin('a', '_grupo', 'b', 'a.grupo_cod = b.grupo_cod')
                ->where('a.modulo_nome = :modulo_nome')
                ->setParameter('modulo_nome', $modulo, \PDO::PARAM_STR);

        return $qb;
    }

    public function getDadosModuloSql($modulo) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_modulo', 'a')
                ->where('a.modulo_nome = :modulo_nome')
                ->setParameter('modulo_nome', $modulo, \PDO::PARAM_STR);

        return $qb;
    }
}
