<?php

namespace Centersis\Modulos\Sistema\Modulo;

use Centersis\Zion2\Acesso\Acesso;
use Centersis\Sistema\Modulo\ModuloClass;
use Centersis\Sistema\Modulo\ModuloForm;
use Centersis\Zion2\Exception\ValidationException;
use Centersis\Ext\Core\BasicoController;

class ModuloController extends BasicoController {

    public function __construct() {
        parent::__construct();

        $this->class = new ModuloClass();
        $this->form = new ModuloForm($this->organograma);
    }

    protected function iniciar($nomeView = null) {
        return parent::iniciar($nomeView);
    }
    
    protected function cadastrar() {
        if ($_SESSION['usuario_cod'] <> '1') {
            throw new ValidationException('Acesso negado');
        }

        return parent::cadastrar();
    }

    protected function alterar() {

        if ($_SESSION['usuario_cod'] <> '1') {
            throw new ValidationException('Acesso negado');
        }

        return parent::alterar();
    }

    protected function remover() {

        if ($_SESSION['usuario_cod'] <> '1') {
            throw new ValidationException('Acesso negado');
        }

        return parent::remover();
    }

    protected function mudaPosicao() {
        new Acesso('alterar');

        $posicao = $this->class->mudaPosicao(filter_input(INPUT_GET, 'modulo_cod'), filter_input(INPUT_GET, 'maisMenos'));

        return parent::jsonSucesso($posicao);
    }

    public function getDadosGrupo($modulo) {
        return $this->class->getDadosGrupo($modulo);
    }

    public function getDadosModulo($modulo) {
        return $this->class->getDadosModulo($modulo);
    }
}
