<?php

namespace Centersis\Modulos\Sistema\Modulo;

use Centersis\Zion2\Pixel\Form\FormFiltro;
use Centersis\Ext\Form\Form;
use Centersis\Sistema\Permissao\PermissaoClass;
use Centersis\Zion2\Banco\Conexao;
use Centersis\Sistema\Grupo\GrupoClass;
use Centersis\Ext\Core\Basico\BasicoForm;
use Centersis\Servicos\Organograma\OrganogramaForm;

class ModuloForm extends BasicoForm {

    private $namespace;
    private $class;

    public function __construct($organogramaCod, $con = null) {

        if (!$con) {
            $con = Conexao::conectar();
        }

        parent::__construct($organogramaCod, $con);
    }

    public function setNamespace($namespace) {
        $this->namespace = $namespace;
    }

    public function setClass($class) {
        $this->class = $class;
    }

    public function getFormFiltro() {
        $form = new FormFiltro();

        $form->config('sisFormFiltro');

        $campos[] = $form->texto('modulo_nome', 'Modulo', 'a');

        $campos[] = $form->escolha('grupo_cod', 'Grupo', 'b')
                ->setTabela('_grupo')
                ->setCampoCod('grupo_cod')
                ->setCampoDesc('CONCAT(grupo_nome," - ",grupo_aplicacao)');

        $campos[] = $form->escolha('modulo_visivel_menu', 'Visivel no menu?', 'a')
                ->setArray(['S' => 'Sim', 'N' => 'Não']);

        $campos[] = $form->escolha('modulo_restrito', 'Restrito ao ROOT', 'a')
                ->setArray(['S' => 'Sim', 'N' => 'Não']);

        $campos[] = $form->numero('modulo_posicao', 'Posição', 'a');

        return $form->processarForm($campos);
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $formCod = null) {

        $form = new Form();

        $form->setAcao($acao);

        $form->config('formManu' . $formCod, 'POST')
                ->setHeader('Modulos');

        $campos[] = $form->hidden('cod')
                ->setValor($form->retornaValor('cod'));

        $campos[] = $form->layout('param', '<div class="form-group"><div class="col-sm-12"><h4><div class="note note-success">Parâmetros</div></h4></div></div>');

        $campos[] = $form->chosen('grupo_cod', 'Grupo', true)
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setTabela('_grupo')
                ->setCampoCod('grupo_cod')
                ->setCampoDesc('CONCAT(grupo_nome," - ",grupo_aplicacao)')
                ->setValor($form->retornaValor('grupo_cod'));

        $campos[] = $form->chosen('modulo_cod_referente', 'Referência', false)
                ->setArray([]) //fake input
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setDependencia('grupo_cod', 'getFormModulo', $this->class)
                ->setValor($form->retornaValor('modulo_cod_referente'));

        $campos[] = $form->texto('modulo_nome', 'Nome do módulo', true)
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setMaximoCaracteres(70)
                ->setValor($form->retornaValor('modulo_nome'));

        $campos[] = $form->texto('modulo_nome_menu', 'Nome em menu', true)
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setMaximoCaracteres(50)
                ->setValor($form->retornaValor('modulo_nome_menu'));

        $campos[] = $form->texto('modulo_desc', 'Descrição do módulo', true)
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setValor($form->retornaValor('modulo_desc'));

        $campos[] = $form->texto('modulo_namespace', 'Namespace', false)
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setValor($form->retornaValor('modulo_namespace'));

        $campos[] = $form->escolha('modulo_transforma_letra', 'Permitir Transformar Letra', false)
                ->setArray(['S' => 'Sim', 'N' => 'Não'])
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setValor($form->retornaValor('modulo_transforma_letra'));

        $campos[] = (new OrganogramaForm())->getFormOrganogramasPrincipais($form, $this->organogramaCod)
                ->setNome('organogramas[]')
                ->setIdentifica('Visivel apenas em')
                ->setMultiplo(true)
                ->setInicio('todos os organogramas')
                ->setOffsetColuna(2)
                ->setIgnoreCod([1])
                ->setObrigatorio(false)
                ->setValor((array) $form->retornaValor('organogramas[]'));

        $campos[] = $form->layout('personal', '<div class="form-group"><div class="col-sm-12"><h4><div class="note note-success">Personalizações de Parêmetros</div></h4></div></div>');

        /* MASTER AÇÕES */
        $objPaiP1 = new Form();
        $objPaiP1->setAcao($acao);

        $confCamposP1 = [
            'organograma_cod' => (new OrganogramaForm())->getFormOrganogramasPrincipais($objPaiP1, $this->organogramaCod)
                    ->setObrigatorio(false)
                    ->setOffsetColuna(4)
                    ->setIgnoreCod([1])
                    ->setEmColunaDeTamanho(4),
            'modulo_label_menu' => $objPaiP1->texto('modulo_label_menu', 'Menu', true)
                    ->setOffsetColuna(4)
                    ->setEmColunaDeTamanho(4)
                    ->setMaximoCaracteres(255),
            'modulo_label_descricao' => $objPaiP1->texto('modulo_label_descricao', 'Descrição', true)
                    ->setOffsetColuna(4)
                    ->setEmColunaDeTamanho(4)
                    ->setMaximoCaracteres(255),
        ];

        $campos[] = $form->masterDetail('p1', 'Personalização de Parâmetros')
                ->setTabela('_modulo_label')
                ->setCodigo('modulo_label_cod')
                ->setCampoReferencia('modulo_cod')
                ->setCodigoReferencia($formCod)
                ->setObjetoPai($objPaiP1)
                ->setAddMax(0)
                ->setTotalItensInicio(0)
                ->setPermitirIgnorar(false)
                ->setNaoRepetir(['organograma_cod'])
                ->setCampos($confCamposP1);

        $campos[] = $form->layout('conf', '<div class="form-group"><div class="col-sm-12"><h4><div class="note note-success has-feedback">Configurações</div></h4></div></div>');

        $campos[] = $form->numero('modulo_posicao', 'Posição', true)
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setValorMaximo(99)
                ->setValorMinimo(1)
                ->setValor($form->retornaValor('modulo_posicao'));

        $campos[] = $form->texto('modulo_class', 'Ícone')
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setMaximoCaracteres(60)
                ->setToolTipMsg('Deve conter o nome da classe do repositório do Bootstrap ou Font Awesome')
                ->setIconFA('fa-font')
                ->setPlaceholder('menu-icon ')
                ->setValor($form->retornaValor('modulo_class'));

        $campos[] = $form->escolha('modulo_visivel_menu', 'Visivel em menu?', true)
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setExpandido(true)
                ->setOrdena(false)
                ->setValorPadrao('S')
                ->setArray(['S' => 'Sim', 'N' => 'Não'])
                ->setValor($form->retornaValor('modulo_visivel_menu'));

        $campos[] = $form->escolha('modulo_restrito', 'Restrito ao ROOT', true)
                ->setOffsetColuna(4)
                ->setEmColunaDeTamanho(6)
                ->setExpandido(true)
                ->setOrdena(false)
                ->setValorPadrao('N')
                ->setArray(['S' => 'Sim', 'N' => 'Não'])
                ->setValor($form->retornaValor('modulo_restrito'));

        /* MASTER AÇÕES */
        $objPai = new Form();
        $objPai->setAcao($acao);

        $inicio = [
            0 => [
                'acao_modulo_permissao' => 'Atualizar',
                'acao_modulo_id_permissao' => 'filtrar',
                'acao_modulo_icon' => 'fa fa-repeat',
                'acao_modulo_funcao_js' => 'sisFiltrarPadrao()',
                'acao_modulo_posicao' => '1',
                'acao_modulo_apresentacao' => 'E'
            ],
            1 => [
                'acao_modulo_permissao' => 'Visualizar',
                'acao_modulo_id_permissao' => 'visualizar',
                'acao_modulo_icon' => 'fa fa-search',
                'acao_modulo_funcao_js' => 'sisVisualizarPadrao()',
                'acao_modulo_posicao' => '2',
                'acao_modulo_apresentacao' => 'E'
            ],
            2 => [
                'acao_modulo_permissao' => 'Cadastrar',
                'acao_modulo_id_permissao' => 'cadastrar',
                'acao_modulo_icon' => 'fa fa-plus',
                'acao_modulo_funcao_js' => 'sisCadastrarLayoutPadrao()',
                'acao_modulo_posicao' => '3',
                'acao_modulo_apresentacao' => 'E'
            ],
            3 => [
                'acao_modulo_permissao' => 'Alterar',
                'acao_modulo_id_permissao' => 'alterar',
                'acao_modulo_icon' => 'fa fa-pencil',
                'acao_modulo_funcao_js' => 'sisAlterarLayoutPadrao()',
                'acao_modulo_posicao' => '4',
                'acao_modulo_apresentacao' => 'E'
            ],
            4 => [
                'acao_modulo_permissao' => 'Remover',
                'acao_modulo_id_permissao' => 'remover',
                'acao_modulo_icon' => 'fa fa-trash-o',
                'acao_modulo_funcao_js' => 'sisRemoverPadrao()',
                'acao_modulo_posicao' => '5',
                'acao_modulo_apresentacao' => 'E'
            ],
            5 => [
                'acao_modulo_permissao' => 'Imprimir',
                'acao_modulo_id_permissao' => 'imprimir',
                'acao_modulo_icon' => 'fa fa-print',
                'acao_modulo_funcao_js' => 'sisImprimir()',
                'acao_modulo_posicao' => '2',
                'acao_modulo_apresentacao' => 'R'
            ],
            6 => [
                'acao_modulo_permissao' => 'Salvar em Excel',
                'acao_modulo_id_permissao' => 'salvarCSV',
                'acao_modulo_icon' => 'fa fa-file-excel-o',
                'acao_modulo_funcao_js' => 'sisSalvarCSV()',
                'acao_modulo_posicao' => '1',
                'acao_modulo_apresentacao' => 'R'
            ]
        ];

        $confCampos = [
            'acao_modulo_permissao' => $objPai->texto('acao_modulo_permissao', 'Permissão', true)
                    ->setOffsetColuna(4)
                    ->setEmColunaDeTamanho(6),
            'acao_modulo_id_permissao' => $objPai->texto('acao_modulo_id_permissao', 'Id', true)
                    ->setOffsetColuna(4)
                    ->setEmColunaDeTamanho(6),
            'acao_modulo_icon' => $objPai->texto('acao_modulo_icon', 'Ícone', false)
                    ->setPlaceholder('Ex: fa fa-search')
                    ->setOffsetColuna(4)
                    ->setEmColunaDeTamanho(6),
            'acao_modulo_funcao_js' => $objPai->texto('acao_modulo_funcao_js', 'JS', false)
                    ->setOffsetColuna(4)
                    ->setEmColunaDeTamanho(6),
            'acao_modulo_posicao' => $objPai->numero('acao_modulo_posicao', 'Posição', true)
                    ->setOffsetColuna(4)
                    ->setEmColunaDeTamanho(6),
            'acao_modulo_apresentacao' => $objPai->escolha('acao_modulo_apresentacao', 'Modo', true)
                    ->setOffsetColuna(4)
                    ->setEmColunaDeTamanho(6)
                    ->setArray(['E' => 'Expandido', 'R' => 'Recolhido', 'I' => 'Acao invisível']),
            'acao_modulo_organogramas[]' => (new OrganogramaForm())->getFormOrganogramasPrincipais($form, $this->organogramaCod)
                    ->setNome('acao_modulo_organogramas[]')
                    ->setIdentifica('Visivel apenas em')
                    ->setMultiplo(true)
                    ->setObrigatorio(false)
                    ->setIgnoreCod([1])
                    ->setInicio('todos os organogramas')
                    ->setOffsetColuna(2)
        ];

        $campos[] = $form->masterDetail('acoes', 'Ações do módulo')
                ->setTabela('_acao_modulo')
                ->setCodigo('acao_modulo_cod')
                ->setCampoReferencia('modulo_cod')
                ->setCodigoReferencia($formCod)
                ->setObjetoPai($objPai)
                ->setObjetoRemover(new PermissaoClass($_SESSION['organograma_cod'], Conexao::conectar()), 'removerPorAcaoModuloCod')
                ->setTotalItensInicio(7)
                ->setAddMax(0)
                ->setValorItensDeInicio($inicio)
                ->setCampos($confCampos);

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

    public function getFormModulo($referenciaCod) {
        $form = new Form();

        $qb = $this->con->qb();

        $qb->select('modulo_cod', 'modulo_nome')
                ->from('_modulo', '')
                ->where($qb->expr()->eq('grupo_cod', ':grupo_cod'))
                ->setParameter('grupo_cod', $referenciaCod, \PDO::PARAM_INT);

        $campos[] = $form->escolha('modulo_cod_referente', 'Módulo de Referência')
                ->setCampoCod('modulo_cod')
                ->setCampoDesc('modulo_nome')
                ->setSqlCompleto($qb);

        return $form->processarForm($campos);
    }

    public function getFormListaModulo(Form $form) {

        $grupoClass = new GrupoClass();
        $moduloClass = new ModuloClass();
        $selecionar = [];
        $naoSelecionar = [];

        $rsGrupos = $grupoClass->grupos('Sistema');

        while ($dadosGrupo = $rsGrupos->fetch()) {

            $grupoCod = $dadosGrupo['grupo_cod'];

            $rsModulo = $moduloClass->modulosDoGrupo($grupoCod);

            $naoSelecionar[] = 'g' . $grupoCod;
            $selecionar['g' . $grupoCod] = $dadosGrupo['grupo_nome'];

            while ($dadosModulo = $rsModulo->fetch()) {

                $espaco = '&nbsp;&nbsp;&nbsp;&nbsp;';

                if ($dadosModulo['modulo_cod_referente']) {
                    $naoSelecionar[] = $dadosModulo['modulo_cod_referente'];
                }

                $selecionar[$dadosModulo['modulo_cod']] = $espaco . $dadosGrupo['grupo_nome'] . $moduloClass->caminhoModulo($dadosModulo['modulo_cod']);
            }
        }

        return $form->escolha('modulo_cod', 'Módulo', false)
                        ->setArray($selecionar)
                        ->setOrdena(false)
                        ->setNaoSelecionaveis($naoSelecionar)
                        ->setValor($form->retornaValor('modulo_cod'));
    }
}
