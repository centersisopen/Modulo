<?php

namespace Centersis\Modulos\Sistema\TrocarSenha\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Ext\Core\Padrao\BaseForm;

class TrocarSenhaForm extends BaseForm {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu() {

        $form = new Form();
        
        $form->setAcao('alterar');
        
        $form->config('formManu', 'POST')
                ->setAcaoSubmit('alterarSenha("formManu")')
                ->setHeader('Alterar Senha');

        $fields = [];

        $fields['usuario_senha'] = $form->senha('usuario_senha', 'Senha', true)
                ->setMaximoCaracteres(30)
                ->setMinimoCaracteres(8)
                ->setToolTipMsg('A senha se for informada, deve conter pelo menos 8 caracteres, sendo pelo menos 1 maiúsculo, 1 minúsculo e 1 número!')
                ->setValor($form->retornaValor('usuario_senha'));

        $fields['usuario_repita_senha'] = $form->senha('usuario_repita_senha', 'Repita a senha', true)
                ->setMaximoCaracteres(30)
                ->setMinimoCaracteres(8)
                ->setValor($form->retornaValor('usuario_repita_senha'));

        $fields[] = $form->botaoSalvarPadrao();

        return $form->processarForm($fields);
    }

}
