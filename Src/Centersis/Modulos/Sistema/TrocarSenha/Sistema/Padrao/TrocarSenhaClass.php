<?php

namespace Centersis\Modulos\Sistema\TrocarSenha\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseClass;
use Centersis\Sistema\Usuario\Sistema\UsuarioClassFactory;

class TrocarSenhaClass extends BaseClass {

    public function __construct($organogramaCod, $con, $sql) {

        parent::__construct($organogramaCod, $con, $sql);
    }

    public function alterar($objForm) {

        $usuarioClass = (new UsuarioClassFactory())->instancia($this->organogramaCod);

        $usuarioClass->alterarSenha($objForm, $objForm->get('usuario_cod'));
    }

}
