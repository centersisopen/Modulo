<?php

namespace Centersis\Modulos\Sistema\Usuario\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Zion2\Pixel\Form\FormFiltro;
use Centersis\Ext\Core\Padrao\BaseForm;

class UsuarioForm extends BaseForm {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function getFormFiltro() {
        $form = new FormFiltro();

        $form->config('sisFormFiltro');

        $campos[] = $form->suggest('usuario_login', 'Login', 'a')
                ->setTabela('_usuario')
                ->setCampoBusca('usuario_login')
                ->setCampoDesc('usuario_login');

        $campos[] = $form->chosen('perfil_cod', 'Perfil', 'a')
                ->setTabela('_perfil')
                ->setCampoCod('perfil_cod')
                ->setCampoDesc('perfil_nome')
                ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                ->setInstrucoes(['perfil_aplicacao', \PDO::PARAM_STR, 'Igual', 'Sistema']);

        return $form->processarForm($campos);
    }

    /**
     *
     * @return Form
     */
    public function getFormManu($acao, $formCod = null, $pessoaCod = null) {

        $form = new Form();

        $form->setAcao($acao);

        $form->config('formManu' . $formCod, 'POST')
                ->setHeader('Usuarios');

        $campos[] = $form->hidden('cod')
                ->setValor($form->retornaValor('cod'));

        $campos['perfil_cod'] = $form->chosen('perfil_cod', 'Perfil', true)
                ->setTabela('_perfil')
                ->setCampoCod('perfil_cod')
                ->setCampoDesc('perfil_nome')
                ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                ->setInstrucoes(['perfil_aplicacao', \PDO::PARAM_STR, 'Igual', 'Sistema'])
                ->setValor($form->retornaValor('perfil_cod'));

        $campos[] = $form->texto('pessoa_nome', 'Nome', true)
                ->setMaximoCaracteres(150)
                ->setValor($form->retornaValor('pessoa_nome'));

        $campos[] = $form->texto('usuario_login', 'Login', true)
                ->setMaximoCaracteres(200)
                ->setValor($form->retornaValor('usuario_login'));

        $campos[] = $form->layout(
                'senha', '<div class="form-group"><div class="col-sm-12"><h4><div class="note note-success">A senha se for informada, deve conter pelo menos 8 caracteres, sendo pelo menos 1 maiúsculo, 1 minúsculo e 1 número!</div></h4></div></div>'
        );

        $campos[] = $form->senha('usuario_senha', 'Senha', false)
                ->setMaximoCaracteres(30)
                ->setMinimoCaracteres(8)
                ->setToolTipMsg('A senha se for informada, deve conter pelo menos 8 caracteres, sendo pelo menos 1 maiúsculo, 1 minúsculo e 1 número!')
                ->setValor($form->retornaValor('usuario_senha'));

        $campos[] = $form->senha('usuario_repita_senha', 'Repita a senha', false)
                ->setMaximoCaracteres(30)
                ->setMinimoCaracteres(8)
                ->setValor($form->retornaValor('usuario_repita_senha'));

        $campos[] = $form->layout(
                'outras', '<div class="form-group"><div class="col-sm-12"><h4><div class="note note-warning">Outras Informações</div></h4></div></div>'
        );

        $campos[] = $form->escolha('usuario_super', 'Super Gestor', true)
                ->setArray(['S' => 'Sim', 'N' => 'Não'])
                ->setValor($form->retornaValor('usuario_super'));

        $campos[] = $form->escolha('usuario_status', 'Situação', true)
                ->setArray(['A' => 'Ativo', 'I' => 'Inativo'])
                ->setValor($form->retornaValor('usuario_status'));

        $campos[] = $form->upload('foto', 'Foto do Usuário', 'Imagem')
                ->setMaximoArquivos(1)
                ->setCodigoReferencia($pessoaCod)
                ->setDimensoes([
                    'altura160' => ['altura' => 160]
                ])
                ->setOrganogramaCod($this->organogramaCod)
                ->setModulo('Pessoa')
                ->setValor('foto');

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

}
