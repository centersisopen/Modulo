<?php

namespace Centersis\Modulos\Sistema\Usuario\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class UsuarioSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function filtrarSql($objForm, $filtroDinamico = []) {
        $qb = $this->con->qb();

        $qb->select(['a.usuario_cod, b.perfil_nome, a.usuario_login, '
            . 'a.numero_acessos, a.usuario_data_cadastro, '
                    . 'a.usuario_ultimo_acesso, c.pessoa_nome, a.usuario_super,'
                    . 'a.usuario_status'])
                ->from('_usuario', 'a')
                ->innerJoin('a', '_perfil', 'b', 'a.perfil_cod = b.perfil_cod')
                ->innerJoin('a', 'pessoa', 'c', 'a.usuario_cod = c.usuario_cod')
                ->leftJoin('c', 'membro', 'd', 'c.pessoa_cod = d.pessoa_cod')
                ->where($qb->expr()->isNull('d.membro_cod'));

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $filtroDinamico, $qb, 'a', $this->organogramaCod);

        return $qb;
    }

    public function getDadosSql($cod) {

        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_usuario', 'u')
                ->innerJoin('u', 'pessoa', 'p', 'u.usuario_cod = p.usuario_cod')
                ->where($qb->expr()->eq('u.usuario_cod', ':usuario_cod'))
                ->setParameter('usuario_cod', $cod, \PDO::PARAM_INT);

        $this->doOrganograma($qb, 'u');

        return $qb;
    }

    public function getDadosSemOrganogramaSql($cod) {

        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_usuario', 'u')
                ->innerJoin('u', 'pessoa', 'p', 'u.usuario_cod = p.usuario_cod')
                ->where($qb->expr()->eq('u.usuario_cod', ':usuario_cod'))
                ->setParameter('usuario_cod', $cod, \PDO::PARAM_INT);

        return $qb;
    }
}
