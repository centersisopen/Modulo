<?php

namespace Centersis\Modulos\Sistema\Usuario\Sistema\Padrao;

use Centersis\Cadastro\Pessoa\Sistema\PessoaClassFactory;
use Centersis\Zion2\Exception\ValidationException;
use Centersis\Accounts\Login\LoginClass;
use Centersis\Ext\Core\Padrao\BaseClass;

class UsuarioClass extends BaseClass {

    public function __construct($organogramaCod, $con, $sql) {

        parent::__construct($organogramaCod, $con, $sql);

        $this->tabela = '_usuario';
        $this->chavePrimaria = 'usuario_cod';

        $this->colunasCrud = [
            'perfil_cod',
            'usuario_login',
            'usuario_status',
            'usuario_super'
        ];

        $this->colunasGrid = [
            'perfil_nome' => 'Perfil',
            'pessoa_nome' => 'Nome',
            'usuario_login' => 'Login',
            'numero_acessos' => 'Acessos',
            'usuario_data_cadastro' => 'Data de Cadastro',
            'usuario_ultimo_acesso' => 'Último Acesso',
            'usuario_super' => 'Super Gestor',
            'usuario_status' => 'Situação'
        ];

        $this->filtroDinamico = [
            'perfil_nome' => 'b',
            'usuario_login' => 'a',
            'pessoa_nome' => 'c'
        ];
    }

    public function grid($grid) {
        $grid->substituaPor('numero_acessos', ['<i>nunca acessou</i>']);
        $grid->substituaPor('usuario_ultimo_acesso', ['<i>nunca acessou</i>']);
        $grid->substituaPor('usuario_super', ['S' => 'Sim', 'N' => 'Não']);
        $grid->substituaPor('usuario_status', ['A' => 'Ativo', 'I' => 'Inativo']);
        $grid->setFormatarComo('usuario_data_cadastro', 'DataHora');
        $grid->setFormatarComo('usuario_ultimo_acesso', 'DataHora');
    }

    public function cadastrarUsuario($objForm) {
        return $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm, ['upload']);
    }

    public function alterarUsuario($objForm) {

        return $this->crudUtil->update($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm, [$this->chavePrimaria => $objForm->get('cod')], [], ['upload']);
    }

    public function cadastrar($objForm) {
        $pessoaClass = (new PessoaClassFactory())->instancia($this->organogramaCod);
        
        $this->loginDuplicado($objForm);

        $this->crudUtil->startTransaction();

        $usuarioCod = $this->cadastrarUsuario($objForm);
        
        $this->alterarSenha($objForm, $usuarioCod);

        $objForm->set('usuario_cod', $usuarioCod);

        $pessoaCod = $pessoaClass->cadastrar($objForm);

        $this->crudUtil->tiposEspeciaisApp($objForm, $objForm, $pessoaCod, []);

        $this->crudUtil->stopTransaction();

        return $usuarioCod;
    }

    public function alterar($objForm) {
        $pessoaClass = (new PessoaClassFactory())->instancia($this->organogramaCod);

        $this->loginDuplicado($objForm);

        $this->crudUtil->startTransaction();

        $usuarioCod = $objForm->get('cod');

        $this->alterarSenha($objForm, $usuarioCod);

        $linhasAfetadas = $this->crudUtil->update($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm, [$this->chavePrimaria => $usuarioCod], [], ['upload']);

        $pessoaCod = $pessoaClass->getDadosPorUsuarioCod($usuarioCod, 'pessoa_cod');

        $objForm->set('cod', $pessoaCod);

        $pessoaClass->alterar($objForm);

        $this->crudUtil->tiposEspeciaisApp($objForm, $objForm, $pessoaCod, []);

        $this->crudUtil->stopTransaction();

        return $linhasAfetadas;
    }

    public function remover($cod) {

        $pessoaClass = (new PessoaClassFactory())->instancia($this->organogramaCod);
        $pessoaCod = $pessoaClass->getDadosPorUsuarioCod($cod, 'pessoa_cod');
                
        $this->con->startTransaction();

        $pessoaClass->remover($pessoaCod);

        parent::remover($cod);

        $this->con->stopTransaction();
    }

    public function getDadosSemOrganograma($cod, $posicao = '') {
        if ($posicao) {
            return $this->con->execRLinha($this->sql->getDadosSemOrganogramaSql($cod), $posicao);
        } else {
            return $this->con->execLinha($this->sql->getDadosSemOrganogramaSql($cod));
        }
    }

    public function setValoresFormManu($cod, $formIntancia) {

        $pessoaClass = (new PessoaClassFactory())->instancia($this->organogramaCod);

        $dadosPessoa = $pessoaClass->getDadosPorUsuarioCod($cod);

        $pessoaCod = $dadosPessoa['pessoa_cod'];

        $objForm = $formIntancia->getFormManu('alterar', $cod, $pessoaCod);

        $parametrosSql = $this->getDados($cod);

        if ($pessoaCod) {
            $parametrosSql['pessoa_nome'] = $dadosPessoa['pessoa_nome'];
        }

        $parametrosSql['usuario_senha'] = '';

        $usuarioCod = $parametrosSql['usuario_cod'];

        $this->crudUtil->setParametrosForm($objForm, $parametrosSql, $usuarioCod);

        return $objForm;
    }

    protected function validaSenha($objForm) {
        $senha1 = $objForm->get('usuario_senha');
        $senha2 = $objForm->get('usuario_repita_senha');

        if ($senha1 OR $senha2) {

            $this->validaForcaSenha($senha1);

            if ($senha1 !== $senha2) {
                throw new ValidationException("Campo Senha e Repita Senha, se preenchidos devem ser iguais!");
            }

            $senhaCriptografada = (new LoginClass())->getSenhaHash2a($senha1);

            $objForm->set('usuario_senha', $senhaCriptografada);
        }
    }

    public function loginDuplicado($objForm) {

        $login = $objForm->get('usuario_login');
        $usuarioCod = $objForm->get('cod');
        $acao = $objForm->getAcao();

        $qb = $this->con->qb();
        $qb->select("usuario_cod")
                ->from('_usuario', '')
                ->where($qb->expr()->eq('usuario_login', ':usuario_login'))
                ->andWhere($qb->expr()->eq('organograma_cod', ':organograma_cod'))
                ->setParameter('usuario_login', $login, \PDO::PARAM_STR)
                ->setParameter('organograma_cod', $this->organogramaCod, \PDO::PARAM_STR);

        if ($usuarioCod and $acao === 'alterar') {
            $qb->andWhere($qb->expr()->neq('usuario_cod', ':usuario_cod'))
                    ->setParameter('usuario_cod', $usuarioCod, \PDO::PARAM_INT);
        }

        if ($this->con->execNLinhas($qb) > 0) {
            throw new ValidationException("Este login já existe no sistema e não pode ser usado!");
        }
    }

    public function alterarSenha($objForm, $usuarioCod) {

        if (!$objForm->get('usuario_senha')) {
            return;
        }

        $this->validaSenha($objForm);

        $this->crudUtil->update($this->organogramaCod, $this->tabela, ['usuario_senha'], $objForm, [$this->chavePrimaria => $usuarioCod], [], ['upload']);
    }

    protected function validaForcaSenha($senha) {

        if (strlen($senha) < 8) {
            throw new ValidationException("A senha teve ter no mínimo 8 caracteres!");
        }

        $requisitos = [
            'lower' => 0,
            'upper' => 0,
            'digit' => 0,
        ];

        $letras = str_split($senha);

        foreach ($letras as $letra) {

            if (ctype_lower($letra)) {
                $requisitos['lower']++;
            }

            if (ctype_upper($letra)) {
                $requisitos['upper']++;
            }

            if (ctype_digit($letra)) {
                $requisitos['digit']++;
            }
        }

        if ($requisitos['lower'] < 1) {
            throw new ValidationException("A senha deve ter pelo menos um caractere minúsculo!");
        }

        if ($requisitos['upper'] < 1) {
            throw new ValidationException("A senha deve ter pelo menos um caractere maiúsculo!");
        }

        if ($requisitos['digit'] < 1) {
            throw new ValidationException("A senha deve ter pelo menos um caractere numérico!");
        }
    }
}
