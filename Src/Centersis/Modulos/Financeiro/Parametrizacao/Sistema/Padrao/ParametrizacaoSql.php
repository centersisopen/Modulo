<?php

namespace Centersis\Modulos\Financeiro\Parametrizacao\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class ParametrizacaoSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function filtrarSql($objForm, $colunas) {

        $qb = $this->con->qb();

        $qb->select('a.fnc_config_cod',
                        'c.fnc_conta_contabil_nome as receber',
                        'd.fnc_conta_contabil_nome as pagar',
                        'e.fnc_convenio_identifica')
                ->from('fnc_config', 'a')
                ->leftJoin('a', 'fnc_conta_contabil', 'c', 'a.fnc_conta_contabil_receber = c.fnc_conta_contabil_cod')
                ->leftJoin('a', 'fnc_conta_contabil', 'd', 'a.fnc_conta_contabil_pagar = d.fnc_conta_contabil_cod')
                ->leftJoin('a', 'fnc_convenio', 'e', 'a.fnc_config_cod = e.fnc_config_cod');

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $colunas, $qb, 'a', $this->organogramaCod);

        return $qb;
    }

    public function getDadosSql($fncConfigCod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_config', '')
                ->where($qb->expr()->eq('fnc_config_cod', ':fnc_config_cod'))
                ->setParameter('fnc_config_cod', $fncConfigCod, \PDO::PARAM_INT);

        $this->doOrganograma($qb);

        return $qb;
    }

    public function getParametrosSql($fncConvenioCod) {

        $qb = $this->con->qb();

        $qb->select('*, a.organograma_cod')
                ->from('fnc_config', 'a')
                ->innerJoin('a', 'empresa', 'c', 'a.empresa_cod = c.empresa_cod')
                ->leftJoin('a', 'fnc_convenio', 'b', 'a.fnc_config_cod = b.fnc_config_cod');

        if ($fncConvenioCod) {
            $qb->where($qb->expr()->eq('b.fnc_convenio_cod', ':fnc_convenio_cod'))
                    ->setParameter('fnc_convenio_cod', $fncConvenioCod, \PDO::PARAM_INT);
        }

        $this->doOrganograma($qb, 'a');

        return $qb;
    }
    
    public function getConveniosSql()
    {
        $qb = $this->con->qb();

        $qb->select('*')
            ->from('fnc_config', 'a')
            ->innerJoin('a', 'fnc_convenio', 'b', 'a.fnc_config_cod = b.fnc_config_cod');

        $this->doOrganograma($qb, 'a');

        return $qb;
    }

}
