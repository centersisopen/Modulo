<?php

namespace Centersis\Modulos\Financeiro\Parametrizacao\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Financeiro\PlanoContas\Sistema\PlanoContasFormFactory;
use Centersis\Financeiro\FormaPagamento\Sistema\FormaPagamentoFormFactory;
use Centersis\Ext\Core\Padrao\BaseForm;

class ParametrizacaoForm extends BaseForm {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $formCod = null) {

        $form = new Form();
        $formPlano = (new PlanoContasFormFactory())->instancia($this->organogramaCod);
        $forma = (new FormaPagamentoFormFactory())->instancia($this->organogramaCod);

        $form->setAcao($acao);

        $form->config('formManu' . $formCod, 'POST')
                ->setHeader('Formas de Pagamento e Recebimento');

        $campos[] = $form->hidden('cod')
                ->setValor($form->retornaValor('cod'));

        $campos[] = $formPlano->getFormContaContabil($form)
                ->setNome('fnc_conta_contabil_receber')
                ->setIdentifica('Conta para receber')
                ->setObrigatorio(true)
                ->setValor($form->retornaValor('fnc_conta_contabil_receber'));

        $campos[] = $formPlano->getFormContaContabil($form)
                ->setNome('fnc_conta_contabil_pagar')
                ->setIdentifica('Conta para pagar')
                ->setObrigatorio(true)
                ->setValor($form->retornaValor('fnc_conta_contabil_pagar'));

        /* MASTER AÇÕES */
        $objPai = new Form();
        $objPai->setAcao($acao);

        $confCampos = [
            'fnc_convenio_identifica' => $objPai->texto('fnc_convenio_identifica', 'Indentifica', false)
                    ->setMaximoCaracteres(100)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_forma_cod' => $forma->getFormaPagamento($objPai, 'R')
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6)
                    ->setIdentifica('F.Recebimento')
                    ->setObrigatorio(true),
            'fnc_convenio_banco' => $objPai->escolha('fnc_convenio_banco', 'Banco', false)
                    ->setArray([
                        '001' => 'Banco do Brasil',
                        '033' => 'Santander',
                        '041' => 'Banrisul',
                        '085' => 'Cecred',
                        '104' => 'Caixa',
                        '136' => 'Unicred',
                        '237' => 'Bradesco',
                        '341' => 'Itaú',
                        '422' => 'Safra',
                        '748' => 'Sicredi',
                        '756' => 'Sicoob',
                        'PBF' => 'Pix - BancoFácil',
                        'ASAAS' => 'ASAAS',
                    ])
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_numero' => $objPai->numero('fnc_convenio_numero', 'Número/Convênio', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_carteira' => $objPai->numero('fnc_convenio_carteira', 'Carteira', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_inicio_nosso_numero' => $objPai->numero('fnc_convenio_inicio_nosso_numero', 'Início NN', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_agencia' => $objPai->texto('fnc_convenio_agencia', 'Agência', false)
                    ->setMaximoCaracteres(4)
                    ->setMinimoCaracteres(4)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_agencia_dv' => $objPai->texto('fnc_convenio_agencia_dv', 'Agência DV', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6)
                    ->setMaximoCaracteres(1)
                    ->setMinimoCaracteres(1),
            'fnc_convenio_conta' => $objPai->texto('fnc_convenio_conta', 'Conta', false)
                    ->setMaximoCaracteres(10)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_conta_dv' => $objPai->texto('fnc_convenio_conta_dv', 'Conta DV', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6)
                    ->setMaximoCaracteres(2)
                    ->setMinimoCaracteres(1),
            'fnc_convenio_byte_idt' => $objPai->numero('fnc_convenio_byte_idt', 'Sicredi Byte Idt', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6)
                    ->setMaximoCaracteres(1)
                    ->setMinimoCaracteres(1),
            'fnc_convenio_posto' => $objPai->numero('fnc_convenio_posto', 'Sicredi Posto', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6)
                    ->setMaximoCaracteres(2)
                    ->setMinimoCaracteres(1),
            'fnc_convenio_multa' => $objPai->float('fnc_convenio_numero', 'Multa', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_multa_juros' => $objPai->float('fnc_convenio_multa_juros', 'Juros', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_dispensa_valor' => $objPai->escolha('fnc_convenio_dispensa_valor', 'Dispensa Valor?', false)
                    ->setArray(['S' => 'Sim', 'N' => 'Não'])
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_dispensa_vencimento' => $objPai->escolha('fnc_convenio_dispensa_vencimento', 'Dispensa Vencimento?', false)
                    ->setArray(['S' => 'Sim', 'N' => 'Não'])
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_permite_remessa' => $objPai->escolha('fnc_convenio_permite_remessa', 'Permite Remessa?', false)
                    ->setArray(['0' => 'Não', '1' => 'Sim'])
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_pix_chave' => $objPai->texto('fnc_convenio_pix_chave', 'Chave Pix', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_pix_conta' => $objPai->texto('fnc_convenio_pix_conta', 'Conta Pix', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_cod_transmissao' => $objPai->texto('fnc_convenio_cod_transmissao', 'Código de Transmissão', false)
                    ->setOffsetColuna(5)
                    ->setEmColunaDeTamanho(6),
            'fnc_convenio_html' => $objPai->editor('fnc_convenio_html', 'Html do Topo', false)
                    ->setFerramentas('Basica')
                    ->setOffsetColuna(2),
            'logo_boleto' => $objPai->upload('logo_boleto', 'Logo do topo', 'Imagem')
                    ->setCodigoReferencia($formCod)
                    ->setDimensoes([
                        'altura78' => ['altura' => 78]
                    ])->setMaximoArquivos(1)
                    ->setOrganogramaCod($this->organogramaCod)
                    ->setValor('logo_boleto')
                    ->setOffsetColuna(2)
        ];

        $campos[] = $form->masterDetail('convenio', 'Informações do convênio')
                ->setTabela('fnc_convenio')
                ->setCodigo('fnc_convenio_cod')
                ->setCampoReferencia('fnc_config_cod')
                ->setCrudExtra('organograma_cod', ['organograma_cod', $this->organogramaCod, 'Inteiro'])
                ->setCodigoReferencia($formCod)
                ->setObjetoPai($objPai)
                ->setNaoRepetir(['fnc_convenio_id'])
                ->setOrganogramaCod($this->organogramaCod)
                ->setCampos($confCampos);

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }
}
