<?php

namespace Centersis\Modulos\Financeiro\Parametrizacao\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseClass;
use Centersis\Servicos\Asaas\AsaasClassFactory;

class ParametrizacaoClass extends BaseClass {

    public function __construct($organogramaCod, $con, $sql) {

        parent::__construct($organogramaCod, $con, $sql);

        $this->tabela = 'fnc_config';
        $this->chavePrimaria = 'fnc_config_cod';

        $this->colunasCrud = [
            'fnc_conta_contabil_receber',
            'fnc_conta_contabil_pagar'
        ];

        $this->colunasGrid = [
            'fnc_convenio_identifica' => 'Convenio',
            'receber' => 'Conta de Receita',
            'pagar' => 'Conta de Despesa'
        ];

        $this->filtroDinamico = [];
    }

    public function grid($grid) {

        $grid->substituaPor('fnc_convenio_identifica', ['<em>não informado</em>']);
    }

    public function cadastrar($objForm) {

        if ($this->con->execNLinhas($this->sql->getDadosSql()) > 0) {
            throw new ValidationException('Não é permitido cadastrar mais de um registro de configuração!');
        }

        $this->con->startTransaction();

        $fncConfigCod = parent::cadastrar($objForm);

        $convenios = $this->getConvenios();

        $asaasClass = (new AsaasClassFactory())->instancia($this->organogramaCod);

        foreach ($convenios as $dadosConvenio) {

            if ($dadosConvenio['fnc_convenio_banco'] == 'ASAAS' and $dadosConvenio['fnc_convenio_pix_chave']) {
                $asaasClass->ativarWebhookCobranca($dadosConvenio);
            }
        }

        $this->con->stopTransaction();

        return $fncConfigCod;
    }
    
    public function alterar($objForm)
    {
        $this->con->startTransaction();

        parent::alterar($objForm);

        $convenios = $this->getConvenios();

        $asaasClass = (new AsaasClassFactory())->instancia($this->organogramaCod);

        foreach ($convenios as $dadosConvenio) {

            if ($dadosConvenio['fnc_convenio_banco'] == 'ASAAS' and $dadosConvenio['fnc_convenio_pix_chave']) {
                $asaasClass->ativarWebhookCobranca($dadosConvenio);
            }
        }

        $this->con->stopTransaction();
    }

    public function remover($cod) {
        throw new ValidationException('Não é permitido remover o registro configuração!');
    }

    public function getDados($fncConvenioCod, $posicao = '') {
        if ($posicao) {
            return $this->con->execRLinha($this->sql->getDadosSql($fncConvenioCod), $posicao);
        } else {
            return $this->con->execLinha($this->sql->getDadosSql($fncConvenioCod));
        }
    }

    public function getParametros($fncConvenioCod, $posicao = '') {
        if ($posicao) {
            return $this->con->execRLinha($this->sql->getParametrosSql($fncConvenioCod), $posicao);
        } else {
            return $this->con->execLinha($this->sql->getParametrosSql($fncConvenioCod));
        }
    }
    
    public function getConvenios()
    {
        return $this->con->paraArray($this->sql->getConveniosSql());
    }

}
