<?php

namespace Centersis\Modulos\Financeiro\Referencia\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class ReferenciaSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function filtrarSql($objForm, $colunas)
    {
        $qb = $this->con->qb();

        $qb->select('a.fnc_referencia_cod, a.fnc_referencia_nome')
            ->from('fnc_referencia', 'a');

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $colunas, $qb, 'a', $this->organogramaCod);

        return $qb;
    }

    public function getDadosSql($cod)
    {
        $qb = $this->con->qb();

        $qb->select('*')
            ->from('fnc_referencia', 'a')
            ->where($qb->expr()->eq('a.fnc_referencia_cod', ':fnc_referencia_cod'))
            ->setParameter('fnc_referencia_cod', $cod);

        $this->doOrganograma($qb,'a');

        return $qb;
    }

    public function getReferenciasSql()
    {
        $qb = $this->con->qb();

        $qb->select('*')
            ->from('fnc_referencia', '');

        $this->doOrganograma($qb);

        return $qb;
    }

}
