<?php

namespace Centersis\Modulos\Financeiro\Referencia\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Ext\Core\Padrao\BaseForm;

class ReferenciaForm extends BaseForm {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $formCod = null) {
        $form = new Form();

        $form->setAcao($acao);

        $form->config('formManu' . $formCod, 'POST')
                ->setHeader('Referências');

        $campos[] = $form->hidden('cod')
                ->setValor($form->retornaValor('cod'));

        $campos[] = $form->texto('fnc_referencia_nome', 'Nome', true)
                ->setValor($form->retornaValor('fnc_referencia_nome'));

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

}
