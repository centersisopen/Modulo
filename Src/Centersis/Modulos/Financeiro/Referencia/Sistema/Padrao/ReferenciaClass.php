<?php

namespace Centersis\Modulos\Financeiro\Referencia\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseClass;

class ReferenciaClass extends BaseClass {

    public function __construct($organogramaCod, $con, $sql) {

        parent::__construct($organogramaCod, $con, $sql);

        $this->tabela = 'fnc_referencia';
        $this->chavePrimaria = 'fnc_referencia_cod';

        $this->colunasCrud = [
            'fnc_referencia_nome',
        ];

        $this->colunasGrid = [
            'fnc_referencia_nome' => 'Referência'
        ];

        $this->filtroDinamico = [
            'fnc_referencia_nome' => 'a',
        ];
    }

    public function getReferencias() {
        return $this->con->paraArray($this->sql->getReferenciasSql());
    }

}
