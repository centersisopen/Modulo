<?php

namespace Centersis\Modulos\Financeiro\FormaPagamento\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Financeiro\PlanoContas\Sistema\PlanoContasFormFactory;
use Centersis\Zion2\Exception\ValidationException;

use Centersis\Ext\Core\Padrao\BaseForm;

class FormaPagamentoForm extends BaseForm
{

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $formCod = null) {
        $form = new Form();
        $formPlano = (new PlanoContasFormFactory())->instancia($this->organogramaCod);

        $form->setAcao($acao);

        $form->config('formManu' . $formCod, 'POST')
                ->setHeader('Formas de Pagamento e Recebimento');

        $campos[] = $form->hidden('cod')
                ->setValor($form->retornaValor('cod'));

        $campos[] = $form->texto('fnc_forma_nome', 'Nome', true)
                ->setMaximoCaracteres(50)
                ->setValor($form->retornaValor('fnc_forma_nome'));

        $campos[] = $formPlano->getFormContaContabilPorGrupo($form, '1')
                ->setIdentifica('Conta Contábil');

        $campos[] = $form->escolha('fnc_forma_tipo', 'Permite', true)
                ->setArray([
                    'P' => 'Para Pagamentos',
                    'R' => 'Para Recebimentos'])
                ->setInLine(false)
                ->setExpandido(true)
                ->setOrdena(false)
                ->setValor($form->retornaValor('fnc_forma_tipo'));

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormaPagamento($form, $operacao) {
        if (!in_array($operacao, ['AP', 'AR', 'RE', 'PA', 'P', 'R'])) {
            throw new ValidationException('Form Forma de Pagamento: Operação informada inválida!');
        }

        $novaOperacao = \str_replace(['A', 'E'], '', $operacao);

        return $form->escolha('fnc_forma_cod', 'Forma de Pagamento', true)
                        ->setTabela('fnc_forma')
                        ->setCampoCod('fnc_forma_cod')
                        ->setCampoDesc('fnc_forma_nome')
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                        ->setInstrucoes(['fnc_forma_tipo', \PDO::PARAM_STR, 'Igual', $novaOperacao])
                        ->setValor($form->retornaValor('fnc_forma_cod'));
    }

}
