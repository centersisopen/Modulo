<?php

namespace Centersis\Modulos\Financeiro\FormaPagamento\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class FormaPagamentoSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function filtrarSql($objForm, $colunas) {
        $qb = $this->con->qb();

        $qb->select('a.fnc_forma_cod, b.fnc_conta_contabil_nome, a.fnc_forma_nome, a.fnc_forma_tipo')
                ->from('fnc_forma', 'a')
                ->innerJoin('a', 'fnc_conta_contabil', 'b', 'a.fnc_conta_contabil_cod = b.fnc_conta_contabil_cod');

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $colunas, $qb, 'a', $this->organogramaCod);

        return $qb;
    }

    public function getDadosSql($cod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_forma', 'a')
                ->where($qb->expr()->eq('a.fnc_forma_cod', ':fnc_forma_cod'))
                ->setParameter('fnc_forma_cod', $cod, \PDO::PARAM_INT);

        $this->doOrganograma($qb, 'a');

        return $qb;
    }

    public function getFormasPagamentoSql() {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_forma', 'a');

        $this->doOrganograma($qb, 'a');

        return $qb;
    }

}
