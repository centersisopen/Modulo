<?php

namespace Centersis\Modulos\Financeiro\FormaPagamento\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseClass;

class FormaPagamentoClass extends BaseClass {

    public function __construct($organogramaCod, $con, $sql) {

        parent::__construct($organogramaCod, $con, $sql);

        $this->tabela = 'fnc_forma';
        $this->chavePrimaria = 'fnc_forma_cod';

        $this->colunasCrud = [
            'fnc_conta_contabil_cod',
            'fnc_forma_nome',
            'fnc_forma_tipo'
        ];

        $this->colunasGrid = [
            'fnc_conta_contabil_nome' => 'Conta Contábil',
            'fnc_forma_nome' => 'Nome',
            'fnc_forma_tipo' => 'Para'
        ];

        $this->filtroDinamico = [
            'fnc_conta_contabil_nome' => 'b',
            'fnc_forma_nome' => 'a'
        ];
    }

    public function grid($grid) {

        $grid->substituaPor('fnc_forma_tipo', ['P' => 'Para Pagamentos', 'R' => 'Para Recebimentos']);
    }

    public function getFormasPagamento() {
        return $this->con->paraArray($this->sql->getFormasPagamentoSql());
    }

}
