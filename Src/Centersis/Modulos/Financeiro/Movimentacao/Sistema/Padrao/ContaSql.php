<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

class ContaSql {

    /**
     * @var \Zion2\Banco\Conexao
     */
    protected $con;
    protected $organogramaCod;

    public function __construct($organogramaCod, $con) {
        $this->con = $con;
        $this->organogramaCod = $organogramaCod;
    }

    public function contasDoLancamentoSql($fnc_lancamento_cod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_conta', '')
                ->where($qb->expr()->eq('fnc_lancamento_cod', ':fnc_lancamento_cod'))
                ->setParameter('fnc_lancamento_cod', $fnc_lancamento_cod)
                ->andWhere($qb->expr()->eq('fnc_conta_removida', ':fnc_conta_removida'))
                ->setParameter('fnc_conta_removida', 'N', \PDO::PARAM_STR);

        $qb->andWhere($qb->expr()->eq('organograma_cod', ':organograma_cod'))
                ->setParameter('organograma_cod', $this->organogramaCod, \PDO::PARAM_INT);

        return $qb;
    }

    public function dadosParcelaSql($fnc_parcela_cod) {
        
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_parcela', '')
                ->where($qb->expr()->eq('fnc_parcela_cod', ':fnc_parcela_cod'))
                ->setParameter('fnc_parcela_cod', $fnc_parcela_cod);

        $qb->andWhere($qb->expr()->eq('organograma_cod', ':organograma_cod'))
                ->setParameter('organograma_cod', $this->organogramaCod, \PDO::PARAM_INT);

        return $qb;
    }

    public function contasDaParcelaSql($fnc_parcela_cod) {        

        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_conta', '')
                ->where($qb->expr()->eq('fnc_parcela_cod', ':fnc_parcela_cod'))
                ->setParameter('fnc_parcela_cod', $fnc_parcela_cod)
                ->andWhere($qb->expr()->eq('fnc_conta_removida', ':fnc_conta_removida'))
                ->setParameter('fnc_conta_removida', 'N', \PDO::PARAM_STR);

        $qb->andWhere($qb->expr()->eq('organograma_cod', ':organograma_cod'))
                ->setParameter('organograma_cod', $this->organogramaCod, \PDO::PARAM_INT);

        return $qb;
    }

    public function contasNegativasSql() {
       
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_conta_contabil', '')
                ->where($qb->expr()->lt('fnc_conta_contabil_saldo', ':zero'))
                ->setParameter('zero', 0, \PDO::PARAM_INT);

        $qb->andWhere($qb->expr()->eq('organograma_cod', ':organograma_cod'))
                ->setParameter('organograma_cod', $this->organogramaCod, \PDO::PARAM_INT);

        return $qb;
    }

}
