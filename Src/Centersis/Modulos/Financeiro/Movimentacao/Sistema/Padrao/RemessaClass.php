<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Crud\CrudUtil;
use Centersis\Ext\Arquivo\ArquivoUpload;
use Centersis\Cadastro\Cliente\Sistema\ClienteClassFactory;
use Centersis\Financeiro\Parametrizacao\Sistema\ParametrizacaoClassFactory;
use Centersis\Modulos\Financeiro\Remessa\GerarRemessa;
use Centersis\Zion2\Exception\ErrorException;
use Centersis\Zion2\Exception\ValidationException;

class RemessaClass {

    protected $crudUtil;
    protected $organogramaCod;

    /**
     * @var RemessaSql 
     */
    protected $remessaSql;

    /**
     * @var \Zion2\Banco\Conexao 
     */
    protected $con;

    public function __construct($organogramaCod, $con, $sql) {

        $this->crudUtil = new CrudUtil($con);

        $this->remessaSql = $sql;
        $this->organogramaCod = $organogramaCod;

        $this->con = $con;
    }

    public function processarRemessa($fncConvenioCod) {

        $arquivoUploadClass = new ArquivoUpload($this->organogramaCod, $this->con);
        $parametrizacaoClass = (new ParametrizacaoClassFactory())->instancia($this->organogramaCod);

        $dadosConvenio = $parametrizacaoClass->getParametros($fncConvenioCod);

        $parcelas = $this->parcelasNaoProcessadas($fncConvenioCod);

        if (!$parcelas) {
            throw new ValidationException('Nenhum arquivo disponível para gerar remessa para este convênio!');
        }

        $this->con->startTransaction();

        $ultimo = $this->ultimaRemessaDoConvenio($fncConvenioCod);

        $remessaNumero = $ultimo + 1;

        $remessaCod = $this->remessaCod($fncConvenioCod, $remessaNumero);

        $pagadores = $this->pagadores($parcelas);

        $nomeOriginal = $this->gerarNomeOriginal($dadosConvenio, $remessaNumero);        
        $nomeFisico = $this->gerarNomeFisico($nomeOriginal, $remessaNumero);

        $arquivoUploadClass->criaDiretorioStorage(date('Y'), date('m'), date('d'), null);

        $caminhoBase = SIS_DIR_BASE . 'Storage/' . $this->organogramaCod . '/' . date('Y') . '/' . date('m') . '/' . date('d');

        (new GerarRemessa())->gerar($remessaNumero, $parcelas, $pagadores, $dadosConvenio, $caminhoBase, $nomeFisico, $this);

        $this->atualizaParcela($parcelas);

        $moduloCod = $arquivoUploadClass->getModuloCod('Movimentacao');

        $this->registraUpload($remessaCod, $nomeOriginal, $nomeFisico, $moduloCod);

        $this->con->stopTransaction();
    }

    protected function gerarNomeFisico($nomeOriginal, $remessaNumero) {
        $hashA = crypt($nomeOriginal, mt_rand()) . crypt($remessaNumero, mt_rand()) . $remessaNumero;
        return str_replace(['/', '\\', '.'], "9", $hashA) . '.rem';
    }

    protected function gerarNomeOriginal($dadosConvenio, $remessaNumero) {

        if ($dadosConvenio['fnc_banco_id'] == 'Sicredi') {

            $qbContaDia = $this->con->qb();
            $contaDia = (int) $this->con->execNLinhas($qbContaDia->select('*')
                                    ->from('fnc_remessa')
                                    ->where($qbContaDia->expr()->eq('organograma_cod', $this->organogramaCod))
                                    ->andWhere('fnc_remessa_data BETWEEN :inicio AND :fim')
                                    ->andWhere($qbContaDia->expr()->eq('fnc_convenio_cod', $dadosConvenio['fnc_convenio_cod']))
                                    ->setParameter('inicio', date('Y-m-d') . ' 00:00:00', \PDO::PARAM_STR)
                                    ->setParameter('fim', date('Y-m-d') . ' 23:59:59', \PDO::PARAM_STR)
            );

            $mesCod = [
                '01' => '1',
                '02' => '2',
                '03' => '3',
                '04' => '4',
                '05' => '5',
                '06' => '6',
                '07' => '7',
                '08' => '8',
                '09' => '9',
                '10' => 'O',
                '11' => 'N',
                '12' => 'D'];

            if ($contaDia > 1) {
                $fimNome = str_pad($contaDia, 3, "0", STR_PAD_LEFT);
            } else {
                $fimNome = '001';
            }

            $nomeOriginal = str_pad($dadosConvenio['fnc_convenio_numero'], 5, "0", STR_PAD_LEFT) . $mesCod[date('m')] . date('d') . '.' . $fimNome;
        } else {

            $nomeOriginal = $remessaNumero . '-' . date('YmdHis') . '.rem';
        }

        return $nomeOriginal;
    }

    protected function pagadores($parcelas) {
        $clienteClass = (new ClienteClassFactory())->instancia($this->organogramaCod);

        $pagadores = [];

        foreach ($parcelas as $parcela) {
            if ($parcela['cliente_cod']) {

                $dadosCliente = $this->getDadosCliente($clienteClass, $parcela['cliente_cod']);

                $this->validaDadosPagador($dadosCliente);

                $pagadores[$parcela['fnc_parcela_cod']] = $dadosCliente;
            } else {
                throw new ErrorException('Destino não encontrado, existe uma insconsistencia e o arquivo de remessa não pode ser gerado, entre em contato com os adminitradores do sistema!');
            }
        }

        return $pagadores;
    }

    protected function validaDadosPagador($dadosPagador) {

        if (empty($dadosPagador)) {
            throw new ValidationException('Os dados do Pagador não estão disponíveis!');
        }

        if (!$dadosPagador['nome']) {
            throw new ValidationException('Nome do Pagador não informado');
        }

        if (!$dadosPagador['documento']) {
            throw new ValidationException('Documento (CNPJ ou CPF) não informado para o Pagador: ' . $dadosPagador['nome']);
        }

        if (!$dadosPagador['endereco']) {
            throw new \Exception('Endereço não informado para o Pagador: ' . $dadosPagador['nome']);
        }
        if (!$dadosPagador['bairro']) {
            throw new ValidationException('Bairro não informado para o Pagador: ' . $dadosPagador['nome']);
        }
        if (!$dadosPagador['prefixo_cep']) {
            throw new ValidationException('Cep - Prefixo não informado para o Pagador: ' . $dadosPagador['nome']);
        }
        if (!$dadosPagador['sufixo_cep']) {
            throw new ValidationException('Cep - Sufixo não informado para o Pagador: ' . $dadosPagador['nome']);
        }
        if (!$dadosPagador['cidade']) {
            throw new ValidationException('Cidade não informado para o Pagador: ' . $dadosPagador['nome']);
        }
        if (!$dadosPagador['estado']) {
            throw new ValidationException('Estado não informado para o Pagador: ' . $dadosPagador['nome']);
        }
    }

    protected function getDadosCliente($cliente, $clienteCod) {
        $dadosPagador = $cliente->getDados($clienteCod);

        if ($dadosPagador['endereco_cep']) {
            $cepLimpo = str_replace(['-', '.'], '', $dadosPagador['endereco_cep']);

            $pefixo_cep = substr($cepLimpo, 0, 5);
            $sufixo_cep = substr($cepLimpo, 5, 3);
        } else {
            $pefixo_cep = '00000';
            $sufixo_cep = '000';
        }

        return [
            'nome' => $dadosPagador['cliente_nome'],
            'tipo' => ($dadosPagador['cliente_tipo'] === 'PF' ? '1' : '2'),
            'documento' => ($dadosPagador['cliente_tipo'] === 'PF' ? $dadosPagador['cliente_cpf'] : $dadosPagador['cliente_cnpj']),
            'endereco' => $dadosPagador['endereco_logradouro'],
            'numero' => $dadosPagador['endereco_numero'],
            'endereco_complemento' => $dadosPagador['endereco_complemento'],
            'cidade' => $dadosPagador['endereco_cidade'],
            'estado' => $dadosPagador['endereco_estado'],
            'prefixo_cep' => $pefixo_cep,
            'sufixo_cep' => $sufixo_cep,            
            'bairro' => $dadosPagador['endereco_bairro']
        ];
    }

    public function registraUpload($remessaCod, $nomeOriginal, $nomeFisico, $moduloCod) {

        $uploadHash = bin2hex(openssl_random_pseudo_bytes(15));

        $campos = [
            'modulo_cod',
            'upload_cod_referencia',
            'upload_nome_campo',
            'upload_nome_original',
            'upload_nome_fisico',
            'upload_data_cadastro',
            'upload_mime',
            'upload_hash'
        ];

        $valores = [
            'modulo_cod' => ['Inteiro' => $moduloCod],
            'upload_cod_referencia' => ['Inteiro' => $remessaCod],
            'upload_nome_campo' => ['Texto' => 'remessa'],
            'upload_nome_original' => ['Texto' => $nomeOriginal],
            'upload_nome_fisico' => ['Texto' => $nomeFisico],
            'upload_data_cadastro' => ['Texto' => date('Y-m-d')],
            'upload_mime' => ['Texto' => 'text/plain'],
            'upload_hash' => ['Texto' => $uploadHash],
        ];

        return $this->crudUtil->insert($this->organogramaCod, '_upload', $campos, $valores);
    }

    protected function parcelasNaoProcessadas($fncConvenioCod) {

        return $this->con->paraArray($this->remessaSql->parcelasNaoProcessadasSql($fncConvenioCod));
    }

    public function arquivosView($maximo) {

        $arquivoUploadClass = new ArquivoUpload($this->organogramaCod, $this->con);

        $moduloCod = $arquivoUploadClass->getModuloCod('Movimentacao');

        return $this->con->paraArray($this->remessaSql->arquivosViewSql($moduloCod, $maximo));
    }

    protected function ultimaRemessaDoConvenio($fncConvenioCod) {

        return (int) $this->con->execRLinha($this->remessaSql->ultimaRemessaDoConvenioSql($fncConvenioCod));
    }

    protected function remessaCod($fncConvenioCod, $remessaNumero) {

        $campos = ['fnc_remessa_numero', 'fnc_convenio_cod', 'fnc_remessa_data'];

        $valores = [
            'fnc_remessa_numero' => ['Inteiro' => $remessaNumero],
            'fnc_convenio_cod' => ['Inteiro' => $fncConvenioCod],
            'fnc_remessa_data' => ['Texto' => date('Y-m-d H:i:s')]
        ];

        return $this->crudUtil->insert($this->organogramaCod, 'fnc_remessa', $campos, $valores, []);
    }

    protected function atualizaParcela($parcelas) {

        foreach ($parcelas as $parcela) {
            $campos = ['fnc_parcela_remessa'];
            $valores = ['fnc_parcela_remessa' => ['Texto' => 'S']];
            $criterio = ['fnc_parcela_cod' => $parcela['fnc_parcela_cod']];

            $this->crudUtil->update($this->organogramaCod, 'fnc_parcela', $campos, $valores, $criterio);
        }
    }

}
