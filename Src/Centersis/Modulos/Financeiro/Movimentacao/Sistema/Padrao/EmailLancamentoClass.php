<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Crud\CrudUtil;
use Centersis\Servicos\Email\EnviarEmail\EnviarEmailClassFactory;
use Centersis\Servicos\RegistraAcao\RegistraAcaoClassFactory;
use Centersis\Cadastro\Cliente\Sistema\ClienteClassFactory;
use Centersis\Cadastro\Empresa\Sistema\EmpresaClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\BoletoClassFactory;
use Centersis\Ext\Twig\Carregador;
use Centersis\Zion2\Exception\ValidationException;

class EmailLancamentoClass {

    protected $organogramaCod;

    /**
     * @var ReferenciaSql 
     */
    protected $emailLancamentoSql;

    /**
     * @var \Zion2\Banco\Conexao 
     */
    protected $con;
    protected $caminhosView = [];

    public function __construct($organogramaCod, $con, $sql) {

        $this->crudUtil = new CrudUtil($con);

        $this->emailLancamentoSql = $sql;
        $this->organogramaCod = $organogramaCod;

        $this->con = $con;
    }

    public function setCaminhosView(array $caminhosView) {
        $this->caminhosView = $caminhosView;
    }

    protected function dadosEnvio($dadosBoleto) {

        if ($dadosBoleto['dadosLancamento']['cliente_cod']) {

            $clienteClass = (new ClienteClassFactory())->instancia($this->organogramaCod);

            $dadosCliente = $clienteClass->getDados($dadosBoleto['dadosLancamento']['cliente_cod']);

            return ['email' => $dadosCliente['cliente_email'], 'nome' => $dadosCliente['cliente_nome']];
        }

        return [];
    }

    public function enviarEmail($lancamentos) {

        $resgistraAcaoClass = (new RegistraAcaoClassFactory())->instancia($this->organogramaCod);
        $empresaClass = (new EmpresaClassFactory())->instancia($this->organogramaCod);
        $boletoClass = (new BoletoClassFactory())->instancia($this->organogramaCod);

        $dadosEmpresa = $empresaClass->getDados(null);

        $enviados = 0;

        foreach ($lancamentos as $lancamentoCod) {

            $dadosBoleto = $boletoClass->dadosParaMontarBoleto($lancamentoCod);

            $dadosEnvio = $this->dadosEnvio($dadosBoleto);

            if (!$dadosEnvio) {
                continue;
            }

            try {

                $mensagem = $this->montaMensagem($dadosBoleto, $dadosEmpresa);
                $assunto = $this->assuntoLancamento($dadosBoleto, $dadosEmpresa);

                $enviarEmail = (new EnviarEmailClassFactory())->instancia($this->organogramaCod);
                $enviarEmail->enviar('email_lancamento', $assunto, $mensagem, [$dadosEnvio['email'] => $dadosEnvio['nome']]);
                $enviados++;
            } catch (\Exception $e) {
                $resgistraAcaoClass->registrar('email_lancamento', false, $e->getMessage() . $e->getTraceAsString());
            }
        }

        if (!$enviados) {
            throw new ValidationException('Nenhum e-mail enviado!');
        }

        return [
            'selecionados' => count($lancamentos),
            'enviados' => $enviados
        ];
    }

    protected function assuntoLancamento($dadosBoleto, $dadosEmpresa) {
        return $dadosEmpresa['empresa_sigla'] . ' - ' . $dadosBoleto['dadosLancamento']['fnc_lancamento_nome'];
    }

    protected function urlLogo() {
        return (new EmpresaClassFactory())->instancia($this->organogramaCod)->getUrlLogo();
    }

    protected function montaMensagem($dadosBoleto, $dadosEmpresa) {

        $carregador = new Carregador(null, $this->caminhosView);

        if (is_null($dadosBoleto['dadosLancamento']['fnc_parcela_desconto'])) {
            $dadosBoleto['fnc_parcela_desconto'] = 0;
        }

        if (is_null($dadosBoleto['dadosLancamento']['fnc_parcela_juros'])) {
            $dadosBoleto['fnc_parcela_juros'] = 0;
        }

        $dados = [
            'lancamento' => $dadosBoleto['dadosLancamento'],
            'empresa' => $dadosEmpresa,
            'boleto' => $dadosBoleto['dadosBoleto'],
            'urlLogo' => $this->urlLogo()
        ];

        return $carregador->render($this->viewName($dadosBoleto), $dados);
    }

    protected function viewName($dadosBoleto) {
        return 'boleto_email.html.twig';
    }

}
