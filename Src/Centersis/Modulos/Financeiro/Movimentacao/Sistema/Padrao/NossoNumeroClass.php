<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Crud\CrudUtil;
use Centersis\Ext\Form\Form;
use Centersis\Zion2\Exception\ValidationException;
use Centersis\Financeiro\Movimentacao\Sistema\BoletoClassFactory;
use Centersis\Financeiro\Parametrizacao\Sistema\ParametrizacaoClassFactory;

class NossoNumeroClass {

    protected $chavePrimaria;
    protected $crudUtil;
    protected $tabela;
    protected $colunasCrud;
    protected $organogramaCod;

    /**
     * @var ReferenciaSql 
     */
    protected $nossoNumeroSql;

    /**
     * @var \Zion2\Banco\Conexao 
     */
    protected $con;

    public function __construct($organogramaCod, $con, $sql) {

        $this->crudUtil = new CrudUtil($con);

        $this->tabela = 'fnc_nosso_numero';
        $this->chavePrimaria = 'fnc_nosso_numero_cod';

        $this->colunasCrud = [
            'fnc_convenio_cod',
            'fnc_nosso_numero_puro',
            'fnc_nosso_numero_com_dv',
        ];

        $this->nossoNumeroSql = $sql;
        $this->organogramaCod = $organogramaCod;

        $this->con = $con;
    }

    public function cadastrar($fncConvenioCod) {
        $form = new Form();
        $parametrizacaoClass = (new ParametrizacaoClassFactory())->instancia($this->organogramaCod);

        $dadosConvenio = $parametrizacaoClass->getParametros($fncConvenioCod);

        if (empty($dadosConvenio)) {
            throw new ValidationException('NossoNúmero: Nenhum registro de convenio foi encontrado com este Id!');
        }

        $fncNossoNumeroPuro = $this->proximoNumeroPuro($fncConvenioCod, $dadosConvenio);
        $fncNossoNumeroComDV = $this->nossoNumeroComDV($fncNossoNumeroPuro, $dadosConvenio);

        $form->set('fnc_convenio_cod', $dadosConvenio['fnc_convenio_cod'], 'Inteiro');
        $form->set('fnc_nosso_numero_puro', $fncNossoNumeroPuro, 'Inteiro');
        $form->set('fnc_nosso_numero_com_dv', $fncNossoNumeroComDV);

        $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $form);

        return $fncNossoNumeroComDV;
    }

    protected function proximoNumeroPuro($fncConvenioCod, $dadosConvenio) {
        $fncConvenioInicioNossoNumero = $dadosConvenio['fnc_convenio_inicio_nosso_numero'];

        $ultimo = $this->getUltimoNumero($fncConvenioCod);

        if (empty($ultimo)) {

            if (empty($fncConvenioInicioNossoNumero)) {
                throw new ValidationException('Início do nosso número não foi configurado!');
            }

            return $fncConvenioInicioNossoNumero;
        }

        return ($ultimo + 1);
    }

    protected function nossoNumeroComDV($fncNossoNumeroPuro, $dadosConvenio) {
        $boletoClass = (new BoletoClassFactory())->instancia($this->organogramaCod);

        if (method_exists($boletoClass, 'nossoNumero')) {
            return $boletoClass->nossoNumero($fncNossoNumeroPuro, $dadosConvenio);
        }

        return $fncNossoNumeroPuro;
    }

    protected function getUltimoNumero($fncConvenioCod) {
        return (int) $this->con->execRLinha($this->nossoNumeroSql->getUltimoNumeroSql($fncConvenioCod));
    }

}
