<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Zion2\Tratamento\Tratamento;
use Centersis\Modulos\Financeiro\Retorno\LerRetorno;
use Centersis\Financeiro\Movimentacao\Sistema\ParcelaClassFactory;
use Centersis\Financeiro\Parametrizacao\Sistema\ParametrizacaoClassFactory;
use Centersis\Zion2\Exception\ValidationException;
use Centersis\Ext\Crud\CrudUtil;
use Centersis\Ext\Form\Form;
use Centersis\Ext\Arquivo\ArquivoUpload;
use Centersis\Zion2\Pixel\Form\FormUpload;

class RetornoClass {

    protected $crudUtil;
    protected $organogramaCod;

    /**
     * @var \Zion2\Banco\Conexao 
     */
    protected $con;

    public function __construct($organogramaCod, $con) {

        $this->crudUtil = new CrudUtil();
        $this->organogramaCod = $organogramaCod;
        $this->con = $con;
    }

    public function lerArquivo($idArquivo, $fncConvenioCod) {

        $dadosleitura = $this->lerRetorno($idArquivo, $fncConvenioCod);

        return [
            'parecelas' => $dadosleitura['parcelas'],
            'uploadCod' => $dadosleitura['uploadCod'],
            'convenio' => $fncConvenioCod
        ];
    }

    public function processar() {
        $form = new Form();
        $trata = Tratamento::instancia();
        $lerRetorno = new LerRetorno();
        $parcelaClass = (new ParcelaClassFactory())->instancia($this->organogramaCod);
        $parametrizacaoClass = (new ParametrizacaoClassFactory())->instancia($this->organogramaCod);

        $colunasDetalhe = [
            'organograma_cod',
            'fnc_retorno_cod',
            'fnc_retorno_detalhe_nn',
            'fnc_retorno_detalhe_valor_pago',
            'fnc_retorno_detalhe_pagamento',
            'fnc_retorno_detalhe_situacao',
            'fnc_retorno_detalhe_processado',
            'fnc_retorno_detalhe_compensa'
        ];

        $this->con->startTransaction();

        $uploadCod = $form->retornaValor('uploadCod');
        $fncConvenioCod = $form->retornaValor('fnc_convenio_cod');
        $arrayNossoNumero = $form->retornaValor('nosso_numero[]');

        if (!is_array($arrayNossoNumero)) {
            $arrayNossoNumero = [];
        }

        $qb = $this->con->qb();

        $dadosUpload = $this->con->execLinha($qb->select('upload_nome_fisico, upload_data_cadastro, upload_cod_referencia')
                        ->from('_upload', '')
                        ->where($qb->expr()->eq('upload_cod', ':upload_cod'))
                        ->andWhere($qb->expr()->eq('organograma_cod', ':organograma_cod'))
                        ->setParameter('upload_cod', $uploadCod)
                        ->setParameter('organograma_cod', $this->organogramaCod));

        if (empty($dadosUpload)) {
            throw new ValidationException('Arquivo de retorno não encontrado!');
        }

        $caminhoArquivo = SIS_DIR_BASE . 'Storage/' . $this->organogramaCod . '/' . str_replace('-', '/', $dadosUpload['upload_data_cadastro']) . '/' . $dadosUpload['upload_nome_fisico'];
        $dadosArquivo = $lerRetorno->lerArquivo($caminhoArquivo);
        $fncRetornoCod = $dadosUpload['upload_cod_referencia'];

        $form->set('organogramaCod', $this->organogramaCod, 'Inteiro');
        $form->set('fnc_retorno_cod', $fncRetornoCod, 'Inteiro');

        $processados = 0;
        foreach ($dadosArquivo as $dados) {

            $chave = array_search($dados['ret_nosso_numero'], $arrayNossoNumero);

            if ($dados['ret_data_compensa'] == '0000-00-00') {
                $dados['ret_data_compensa'] = $dados['ret_data_pagamento'];
            }

            $form->set('fnc_retorno_detalhe_nn', $dados['ret_nosso_numero'], 'Texto');
            $form->set('fnc_retorno_detalhe_valor_pago', $dados['ret_valor_pago'], 'Float');
            $form->set('fnc_retorno_detalhe_pagamento', $dados['ret_data_pagamento'], 'Texto');
            $form->set('fnc_retorno_detalhe_compensa', $dados['ret_data_compensa'], 'Texto');

            if (!$chave) { //Se não foi marcado o checkbox da parcela
                $dadosParcela = $parcelaClass->getDadosPorNossoNumero($dados['ret_nosso_numero'], $fncConvenioCod);

                if ($dadosParcela) {
                    $form->set('fnc_retorno_detalhe_situacao', 'NP', 'Texto');
                    $form->set('fnc_retorno_detalhe_processado', 'N', 'Texto');

                    $this->crudUtil->insert($this->organogramaCod, 'fnc_retorno_detalhe', $colunasDetalhe, $form);
                } else {
                    $dadosParcelaE = $parcelaClass->getDadosPorNossoNumeroComExcluidos($dados['ret_nosso_numero'], $fncConvenioCod);

                    if (!empty($dadosParcelaE)) {
                        $form->set('fnc_retorno_detalhe_situacao', 'EX', 'Texto');
                        $form->set('fnc_retorno_detalhe_processado', 'S', 'Texto');

                        $this->crudUtil->insert($this->organogramaCod, 'fnc_retorno_detalhe', $colunasDetalhe, $form);
                    } else {
                        $form->set('fnc_retorno_detalhe_situacao', 'NE', 'Texto');
                        $form->set('fnc_retorno_detalhe_processado', 'S', 'Texto');

                        $this->crudUtil->insert($this->organogramaCod, 'fnc_retorno_detalhe', $colunasDetalhe, $form);
                    }
                }

                continue;
            }

            $dadosParcela = $parcelaClass->getDadosPorNossoNumero($dados['ret_nosso_numero'], $fncConvenioCod);

            if (!empty($dadosParcela)) {

                $v1 = $trata->numero()->floatBanco($dadosParcela['fnc_parcela_valor']);
                $v2 = $trata->numero()->floatBanco($dados['ret_valor_pago']);

                $fncParcelaCod = $dadosParcela['fnc_parcela_cod'];
                $situacaoParcela = $dadosParcela['fnc_parcela_pago'];

                if ($situacaoParcela === 'S') {

                    $form->set('fnc_retorno_detalhe_situacao', 'JQ', 'Texto');
                    $form->set('fnc_retorno_detalhe_processado', 'S', 'Texto');

                    $this->crudUtil->insert($this->organogramaCod, 'fnc_retorno_detalhe', $colunasDetalhe, $form);

                    continue;
                }

                if ($v2 > $v1) {

                    $form->set('fnc_retorno_detalhe_situacao', 'VMA', 'Texto');
                    $form->set('fnc_retorno_detalhe_processado', 'S', 'Texto');

                    $this->crudUtil->insert($this->organogramaCod, 'fnc_retorno_detalhe', $colunasDetalhe, $form);
                } else if ($v2 < $v1) {
                    $form->set('fnc_retorno_detalhe_situacao', 'VME', 'Texto');
                    $form->set('fnc_retorno_detalhe_processado', 'S', 'Texto');

                    $this->crudUtil->insert($this->organogramaCod, 'fnc_retorno_detalhe', $colunasDetalhe, $form);
                } else {
                    $form->set('fnc_retorno_detalhe_situacao', 'VI', 'Texto');
                    $form->set('fnc_retorno_detalhe_processado', 'S', 'Texto');

                    $this->crudUtil->insert($this->organogramaCod, 'fnc_retorno_detalhe', $colunasDetalhe, $form);
                }

                $fncFormaCod = $parametrizacaoClass->getParametros($fncConvenioCod, 'fnc_forma_cod');

                $form->set('fnc_parcela_pagamento', $dados['ret_data_pagamento'], 'Texto');
                $form->set('fnc_parcela_compensa', $dados['ret_data_compensa'], 'Texto');
                $form->set('fnc_parcela_valor_pago', $dados['ret_valor_pago'], 'Float');
                $form->set('fnc_forma_cod', $fncFormaCod);
                $form->set('cod', $fncParcelaCod);

                $parcelaClass->baixar($form);
                $parcelaClass->informaBaixaManual($fncParcelaCod, 'N');
            } else {


                $dadosParcela = $parcelaClass->getDadosPorNossoNumeroComExcluidos($dados['ret_nosso_numero'], $fncConvenioCod);

                if (!empty($dadosParcela)) {
                    $form->set('fnc_retorno_detalhe_situacao', 'EX', 'Texto');
                    $form->set('fnc_retorno_detalhe_processado', 'S', 'Texto');

                    $this->crudUtil->insert($this->organogramaCod, 'fnc_retorno_detalhe', $colunasDetalhe, $form);
                } else {
                    $form->set('fnc_retorno_detalhe_situacao', 'NE', 'Texto');
                    $form->set('fnc_retorno_detalhe_processado', 'S', 'Texto');

                    $this->crudUtil->insert($this->organogramaCod, 'fnc_retorno_detalhe', $colunasDetalhe, $form);
                }
            }

            $processados++;
        }

        $this->con->stopTransaction();

        return $processados;
    }

    protected function lerRetorno($idArquivo, $fncConvenioCod) {

        $arquivoUploadClass = new ArquivoUpload($this->organogramaCod, $this->con);
        $trata = Tratamento::instancia();
        $lerRetorno = new LerRetorno();
        $parcelaClass = (new ParcelaClassFactory())->instancia($this->organogramaCod);        
        $objForm = new Form();

        $objForm->set('fnc_convenio_cod', $fncConvenioCod, 'Inteiro');

        $fncRetornoCod = $this->crudUtil->insert($this->organogramaCod, 'fnc_retorno', ['fnc_convenio_cod'], $objForm);

        $formUpload = new FormUpload('upload', $idArquivo, 'UploadRetorno', 'ARQUIVO');
        $formUpload->setCodigoReferencia($fncRetornoCod)
                ->setValor('arquivo_retorno');

        $uploadCod = $arquivoUploadClass->sisUpload($formUpload);

        $caminhoArquivo = $arquivoUploadClass->getCaminhoUpload();

        $parcelas = [];
        $dadosArquivo = $lerRetorno->lerArquivo($caminhoArquivo);

        foreach ($dadosArquivo as $dados) {

            $dadosParcela = $parcelaClass->getDadosPorNossoNumero($dados['ret_nosso_numero'], $fncConvenioCod);

            if (!empty($dadosParcela)) {

                $v1 = $trata->numero()->floatBanco($dadosParcela['fnc_parcela_valor']);
                $v2 = $trata->numero()->floatBanco($dados['ret_valor_pago']);

                $dadosParcela['valor_menor'] = ($v2 < $v1 ? 'S' : 'N');

                $dadosParcela['fnc_parcela_valor'] = $trata->numero()->floatCliente($dadosParcela['fnc_parcela_valor']);
                $dadosParcela['ret_valor_pago'] = $trata->numero()->floatCliente($dados['ret_valor_pago']);
                $dadosParcela['ret_data_pagamento'] = $dados['ret_data_pagamento'];
                $dadosParcela['ret_nosso_numero'] = $dados['ret_nosso_numero'];

                $parcelas[] = $dadosParcela;
            }
        }


        return [
            'parcelas' => $parcelas,
            'convenio' => $fncConvenioCod,
            'uploadCod' => $uploadCod
        ];
    }

}
