<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Zion2\Validacao\Valida;
use Centersis\Financeiro\FormaPagamento\Sistema\FormaPagamentoFormFactory;
use Centersis\Financeiro\Movimentacao\Sistema\ParcelaClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\MovimentacaoClassFactory;
use Centersis\Financeiro\Parametrizacao\Sistema\ParametrizacaoClassFactory;
use Centersis\Ext\Core\Padrao\BaseForm;

class ParcelaForm extends BaseForm {

    private $namespace;

    public function __construct($organogramaCod, $con) {

        parent::__construct($organogramaCod, $con);
    }

    public function setNamespace($namespace) {
        $this->namespace = $namespace;
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormParcela($form, $operacao, $valoresInicio = [], $parametrosView = []) {
        $formaForma = (new FormaPagamentoFormFactory())->instancia($this->organogramaCod);

        $objPai = new Form();

        $disabledGeral = (bool) in_array($operacao, ['AP', 'AR']);
        $disabledJurosDesconto = (bool) in_array($operacao, ['PA', 'RE']);

        $padrao = $disabledGeral ? 'N' : 'S';

        $disabledQuitar = $disabledGeral ? false : true;

        $confCampos = [
            'fnc_parcela_nome' => $objPai->texto('fnc_parcela_nome', 'Parcela')
                    ->setEmColunaDeTamanho(4),
            'fnc_parcela_vencimento' => $objPai->data('fnc_parcela_vencimento', 'Data de Vencimento')
                    ->setEmColunaDeTamanho(4),
            'fnc_parcela_pagamento' => $objPai->data('fnc_parcela_pagamento', 'Data de Pagamento')
                    ->setDisabled($disabledGeral)
                    ->setEmColunaDeTamanho(4),
            'fnc_parcela_valor' => $objPai->float('fnc_parcela_valor', 'Valor da Parcela')
                    ->setEmColunaDeTamanho(4),
            'fnc_parcela_valor_pago' => $objPai->float('fnc_parcela_valor_pago', 'Valor Pago')
                    ->setDisabled($disabledGeral)
                    ->setEmColunaDeTamanho(4),
            'fnc_forma_cod' => $formaForma->getFormaPagamento($objPai, $operacao)
                    ->setDisabled($disabledGeral)
                    ->setEmColunaDeTamanho(2),
            'quitar' => $objPai->escolha('quitar', 'Quitar')
                    ->setDisabled($disabledQuitar)
                    ->setValorPadrao($padrao)
                    ->setInicio(false)
                    ->setComplemento('onchange="quitar(this)"')
                    ->setArray(['S' => 'Sim', 'N' => 'Não'])
                    ->setEmColunaDeTamanho(2)];

        if ($operacao == 'AR') {

            $confCampos['desconto'] = $objPai->escolha('desconto', 'Desconto')
                    ->setDisabled($disabledJurosDesconto)
                    ->setValorPadrao($padrao)
                    ->setInicio(false)
                    ->setComplemento('onchange="fnc_desconto(this)"')
                    ->setArray(['S' => 'Sim', 'N' => 'Não'])
                    ->setEmColunaDeTamanho(2);

            $confCampos['fnc_parcela_desconto_forma'] = $objPai->escolha('fnc_parcela_desconto_forma', 'Desconto Forma')
                    ->setDisabled($disabledJurosDesconto)
                    ->setValorPadrao($padrao)
                    ->setInicio(false)
                    ->setArray(['valor' => 'R$ Valor', 'porcentagem' => '% Porcentagem'])
                    ->setEmColunaDeTamanho(2);

            $confCampos['fnc_parcela_desconto'] = $objPai->float('fnc_parcela_desconto', 'Valor')
                    ->setEmColunaDeTamanho(4);

            $confCampos['fnc_parcela_desconto_data'] = $objPai->data('fnc_parcela_desconto_data', 'Desconto se pago até')
                    ->setDisabled($disabledJurosDesconto)
                    ->setEmColunaDeTamanho(4);

            $confCampos['juros'] = $objPai->escolha('juros', 'Juros')
                    ->setDisabled($disabledJurosDesconto)
                    ->setValorPadrao($padrao)
                    ->setInicio(false)
                    ->setComplemento('onchange="fnc_juros(this)"')
                    ->setArray(['S' => 'Sim', 'N' => 'Não'])
                    ->setEmColunaDeTamanho(2);

            $confCampos['fnc_parcela_juros_forma'] = $objPai->escolha('fnc_parcela_juros_forma', 'Juros Forma')
                    ->setDisabled($disabledJurosDesconto)
                    ->setValorPadrao($padrao)
                    ->setInicio(false)
                    ->setArray(['valor' => 'R$ Valor', 'porcentagem' => '% Porcentagem'])
                    ->setEmColunaDeTamanho(2);

            $confCampos['fnc_parcela_juros'] = $objPai->float('fnc_parcela_juros', 'Valor')
                    ->setEmColunaDeTamanho(4);

            $confCampos['fnc_parcela_juros_data'] = $objPai->data('fnc_parcela_juros_data', 'Juros se pago após')
                    ->setDisabled($disabledJurosDesconto)
                    ->setEmColunaDeTamanho(4);
        }

        $campos[] = $form->masterDetail('parcela', 'Parcelas')
                ->setTabela('fnc_parcela')
                ->setCodigo('fnc_parcela_cod')
                ->setCampoReferencia('fnc_lancamento_cod')
                ->setObjetoPai($objPai)
                ->setAddMin(1)
                ->setAddMax(0)
                ->setView('parcela.html.twig', $this->namespace, $parametrosView)
                ->setGravar(false)
                ->setValorItensDeInicio($valoresInicio)
                ->setTotalItensInicio(count($valoresInicio))
                ->setCampos($confCampos);

        return $form->processarForm($campos);
    }

    public function getFormBaixar($fncParcelaCod, $operacao) {

        $form = new Form();
        $parcelaClass = (new ParcelaClassFactory())->instancia($this->organogramaCod);
        $formaPagamentoForm = (new FormaPagamentoFormFactory())->instancia($this->organogramaCod);
        $valida = Valida::instancia();

        $form->setAcao('alterar');

        $form->config('formBaixar' . $fncParcelaCod, 'POST')
                ->setAcaoSubmit('baixarParcela("' . 'formBaixar' . $fncParcelaCod . '")')
                ->setHeader('Baixar Parcelas');

        $campos[] = $form->hidden('cod')
                ->setValor($fncParcelaCod);

        $dadosParcela = $parcelaClass->getDados($fncParcelaCod);

        $campos[] = $form->layout('parcela', '<div class="stat-panel">
            <div class="stat-cell bg-info col-sm-4 valign-middle">
                <i class="fa fa-info-circle"></i><br>' . $parcelaClass->getNomeParcela($fncParcelaCod) . '
            </div>
            <div class="stat-cell bg-info darker col-sm-4 text-center valign-middle">
                <i class="fa fa-calendar"></i> Vencimento em ' . $valida->data()->converteData($dadosParcela['fnc_parcela_vencimento']) . '
            </div>            
            <div class="stat-cell bg-info col-sm-4 text-center valign-middle">
                <i class="fa fa-money"></i> Valor da Parcela ' . $valida->numero()->moedaCliente($dadosParcela['fnc_parcela_valor']) . '
            </div>
        </div>');

        $campos[] = $formaPagamentoForm->getFormaPagamento($form, $operacao)
                ->setInicio('Selecione a forma de quitação')
                ->setEmColunaDeTamanho(4)
                ->setOffsetColuna(3)
                ->setObrigatorio(true)
                ->setIdentifica('Forma');

        $campos[] = $form->data('fnc_parcela_pagamento', 'Pagamento', true)
                ->setPlaceHolder('Informe a data de pagamento')
                ->setEmColunaDeTamanho(4)
                ->setOffsetColuna(3)
                ->setValor($form->retornaValor('fnc_parcela_pagamento'));

        $campos[] = $form->float('fnc_parcela_valor_pago', 'Pago', true)
                ->setEmColunaDeTamanho(4)
                ->setOffsetColuna(3)
                ->setValorPadrao($valida->numero()->floatCliente($dadosParcela['fnc_parcela_valor']))
                ->setValor($form->retornaValor('fnc_parcela_valor_pago'));

        $campos[] = $form->botaoSalvarPadrao()->setValor('Baixar');

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

    public function getFormAtualizar($fncParcelaCod) {
        $form = new Form();
        $movimentacaoClass = (new MovimentacaoClassFactory())->instancia($this->organogramaCod);
        $parcelaClass = (new ParcelaClassFactory())->instancia($this->organogramaCod);
        $parametrizacaoClass = (new ParametrizacaoClassFactory())->instancia($this->organogramaCod);
        $valida = Valida::instancia();

        $form->setAcao('alterar');

        $form->config('formAtualizar' . $fncParcelaCod, 'POST')
                ->setAcaoSubmit('atualizarParcela("' . 'formAtualizar' . $fncParcelaCod . '")')
                ->setHeader('Atualizar Parcelas');

        $campos[] = $form->hidden('cod')
                ->setValor($fncParcelaCod);

        $dadosParcela = $parcelaClass->getDados($fncParcelaCod);

        $fncConvenioCod = $movimentacaoClass->getDadosPorParcela($fncParcelaCod, 'fnc_convenio_cod');

        $parametrizacao = $parametrizacaoClass->getParametros($fncConvenioCod);
        $juros = (float) $parametrizacao['fnc_convenio_multa_juros'];
        $descontos = (float) $parametrizacao['fnc_convenio_multa'];

        $campos[] = $form->layout('abreParcela', '<div class="stat-panel">
            <div class="stat-cell bg-warning col-sm-3 valign-middle">
                <i class="fa fa-info-circle"></i><br>' . $parcelaClass->getNomeParcela($fncParcelaCod) . '
            </div>
            <div class="stat-cell bg-warning darker col-sm-3 text-bg valign-middle">
                <i class="fa fa-calendar"></i> Vencimento atual ' . $valida->data()->converteData($dadosParcela['fnc_parcela_vencimento']) .
                '<br><br><i class="fa fa-calendar"></i> Competência atual ' . $valida->data()->converteData($dadosParcela['fnc_parcela_vencimento']) .
                '<br><br><i class="fa fa-money"></i> Valor Atual ' . $valida->numero()->floatCliente($dadosParcela['fnc_parcela_valor']) . '
            </div>            
            <div class="stat-cell bg-warning col-sm-6 text-center valign-middle">');

        $disabled = $dadosParcela['fnc_parcela_pago'] == 'S' ? true : false;

        $campos[] = $form->hidden('fnc_parcela_vencimento_atual')
                ->setValor($dadosParcela['fnc_parcela_vencimento']);

        $campos[] = $form->hidden('fnc_parcela_valor_atual')
                ->setValor($dadosParcela['fnc_parcela_valor']);

        $campos[] = $form->data('fnc_parcela_vencimento', 'Novo vencimento:', true)
                ->setPlaceHolder('Informe a nova data de vencimento')
                ->setOffsetColuna(5)
                ->setDisabled($disabled)
                ->setComplemento('onchange="calculaJuros(\'' . $fncParcelaCod . '\',' . $juros . ',' . $descontos . ')"')
                ->setValorPadrao($valida->data()->converteData($dadosParcela['fnc_parcela_vencimento']))
                ->setValor($form->retornaValor('fnc_parcela_vencimento'));

        $campos[] = $form->data('fnc_parcela_competencia', 'Nova competência:', true)
                ->setPlaceHolder('Informe a nova data de competência')
                ->setOffsetColuna(5)
                ->setValorPadrao($valida->data()->converteData($dadosParcela['fnc_parcela_competencia']))
                ->setValor($form->retornaValor('fnc_parcela_competencia'));

        $campos[] = $form->float('fnc_parcela_juros', 'Acréscimos:', false)
                ->setOffsetColuna(5)
                ->setDisabled($disabled)
                ->setValorPadrao($valida->numero()->floatCliente($dadosParcela['fnc_parcela_juros']))
                ->setValorPadrao($dadosParcela['fnc_parcela_juros'])
                ->setValor($form->retornaValor('fnc_parcela_juros'));

        $campos[] = $form->float('fnc_parcela_desconto', 'Descontos:', false)
                ->setOffsetColuna(5)
                ->setDisabled($disabled)
                ->setValorPadrao($valida->numero()->floatCliente($dadosParcela['fnc_parcela_desconto']))
                ->setValorPadrao($dadosParcela['fnc_parcela_desconto'])
                ->setValor($form->retornaValor('fnc_parcela_desconto'));

        $campos[] = $form->layout('fechaParcela', '</div></div>');

        $campos[] = $form->botaoSalvarPadrao()->setValor('Atualizar');

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

}
