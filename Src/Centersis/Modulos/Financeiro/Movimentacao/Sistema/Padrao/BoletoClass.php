<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Modulos\Financeiro\Boleto\Boleto;
use Centersis\Zion2\Tratamento\Tratamento;
use Centersis\Ext\Twig\Carregador;
use Centersis\Ext\Arquivo\ArquivoUpload;
use Centersis\Financeiro\Parametrizacao\Sistema\ParametrizacaoClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\ParcelaClassFactory;
use Centersis\Cadastro\Empresa\Sistema\EmpresaClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\ContaClassFactory;
use Centersis\Servicos\RegistroPix\RegistroPixClassFactory;
use Centersis\Servicos\Asaas\AsaasClassFactory;

class BoletoClass extends Boleto {

    protected $organogramaCod;

    /**
     * @var \Zion2\Banco\Conexao 
     */
    protected $con;
    protected $caminhosView = [];

    public function __construct($organogramaCod, $con) {
        $this->organogramaCod = $organogramaCod;

        $this->con = $con;

        parent::__construct();
    }

    public function setCaminhosView(array $caminhosView) {
        $this->caminhosView = $caminhosView;
    }

    public function nossoNumero($numero, $dadosConvenio) {
        return parent::nossoNumero($numero, $dadosConvenio);
    }

    public function dadosParaMontarBoleto($fncLancamentoCod) {
        $arquivoUp = new ArquivoUpload($this->organogramaCod, $this->con);
        $parcelaClass = (new ParcelaClassFactory())->instancia($this->organogramaCod, $this->con);
        $parametrizacaoClass = (new ParametrizacaoClassFactory())->instancia($this->organogramaCod, $this->con);

        $dadosLancamento = $parcelaClass->getDadosPorLancamentoCod($fncLancamentoCod);

        if (!$dadosLancamento) {
            throw new \Exception('Boleto não encontrado!');
        }

        $fncConvenioCod = $dadosLancamento['fnc_convenio_cod'];
        $fncParcelaCod = $dadosLancamento['fnc_parcela_cod'];

        $dadosConvenio = [];
        $dadosConvenio['organograma_cod'] = $this->organogramaCod;

        if ($fncConvenioCod) {
            $dadosConvenio = $parametrizacaoClass->getParametros($fncConvenioCod);

            if (!$dadosConvenio) {
                throw new \Exception('Convênio não encontrado!');
            }
        }

        $logoBoleto = $arquivoUp->getUrlVerImagem('logo_boleto' . $fncConvenioCod, $fncConvenioCod, 'altura78', 'Parametrizacao');

        if (!$logoBoleto) {
            $logoBoleto = logo($this->organogramaCod);
        }

        $parcelasAbertas = $this->parcelasAbertas($dadosLancamento, $parcelaClass);

        if (isset($dadosConvenio['fnc_banco_id']) and $dadosConvenio['fnc_banco_id'] == 'PBF') {
            $registroPixClass = (new RegistroPixClassFactory)->instancia($this->organogramaCod);
            $dadosLancamento['pix'] = $registroPixClass->geraPixCobranca($fncParcelaCod);
        }

        if (isset($dadosConvenio['fnc_banco_id']) and $dadosConvenio['fnc_banco_id'] == 'ASAAS') {
            $asaasClass = (new AsaasClassFactory)->instancia($this->organogramaCod);
            $dadosLancamento['asaas'] = $asaasClass->registrarCobranca($fncParcelaCod);
        }

        return $this->dadosView($dadosLancamento, $dadosConvenio, $parcelasAbertas, $logoBoleto);
    }

    protected function parcelasAbertas($dadosLancamento) {
        return [];
    }

    public function gerar($fncLancamentoCod) {

        $carregador = new Carregador(null, $this->caminhosView);

        $dadosView = $this->dadosParaMontarBoleto($fncLancamentoCod);

        if (isset($dadosView['dadosConvenio']['fnc_convenio_cod'])) {
            $view = 'boleto.html.twig';

            if ($dadosView['dadosConvenio']['fnc_banco_id'] == 'PBF') {
                $view = 'boleto_pix.html.twig';

                $registroPixClass = (new RegistroPixClassFactory)->instancia($this->organogramaCod);
                $dadosView['pix'] = $registroPixClass->geraPixCobranca($dadosView['dadosLancamento']['fnc_parcela_cod']);
            }

            if ($dadosView['dadosConvenio']['fnc_banco_id'] == 'ASAAS') {
                $view = 'boleto_asaas.html.twig';

                $asaasClass = (new AsaasClassFactory)->instancia($this->organogramaCod);
                $dadosView['asaas'] = $asaasClass->registrarCobranca($dadosView['dadosLancamento']['fnc_parcela_cod']);
            }
        } else {
            $view = 'sem_convenio.html.twig';
        }

        return $carregador->render($view, $dadosView);
    }

    protected function dadosPagador($dadosLancamento) {
        throw new \Exception('Implemente o método dadosPagador!');
    }

    protected function dadosBeneficiario($dadosConvenio) {

        $empresaClass = (new EmpresaClassFactory())->instancia($dadosConvenio['organograma_cod']);

        $dadosEmpresa = $empresaClass->getDados(null, null);

        return [
            'nome' => $dadosEmpresa['empresa_nome'],
            'documento' => $dadosEmpresa['empresa_cnpj'],
            'endereco' => $dadosEmpresa['endereco_logradouro'],
            'endereco_numero' => $dadosEmpresa['endereco_numero'],
            'endereco_complemento' => $dadosEmpresa['endereco_complemento'],
            'cidade' => $dadosEmpresa['endereco_cidade'],
            'estado' => $dadosEmpresa['endereco_estado'],
            'cep' => $dadosEmpresa['endereco_cep'],
            'bairro' => $dadosEmpresa['endereco_bairro']
        ];
    }

    protected function dadosView($dadosLancamento, $dadosConvenio, $parcelasAbertas, $logoBoleto) {

        $destinoContabilHtml = $this->montaDestinoContabilHtml($dadosLancamento);

        return [
            'dadosConvenio' => $dadosConvenio,
            'dadosLancamento' => $dadosLancamento,
            'dadosBoleto' => $this->getDadosBoleto($dadosLancamento, $dadosConvenio),
            'beneficiario' => $this->dadosBeneficiario($dadosConvenio),
            'pagador' => $this->dadosPagador($dadosLancamento),
            'logoBoleto' => $logoBoleto,
            'destinoContabil' => $destinoContabilHtml,
            'lancamentoAbertos' => $parcelasAbertas
        ];
    }

    protected function montaDestinoContabilHtml($dadosLancamento) {

        $tratar = Tratamento::instancia();

        $fncParcelaCod = $dadosLancamento['fnc_parcela_cod'];

        $contaClass = (new ContaClassFactory())->instancia($this->organogramaCod);

        $destinos = $contaClass->destinoContabil($fncParcelaCod, 'Manual');

        $html = [];

        $html[] = '<div style="font-size:14px; padding-top:2px; padding-bottom:2px;"><strong>' . $dadosLancamento['fnc_lancamento_nome'] . '</strong></div>';

        $totalDespesas = 0;

        if ($dadosLancamento['fnc_parcela_valor'] > '0.01') {
            foreach ($destinos as $dados) {

                $totalDespesas += $dados['fnc_conta_valor'];

                $html[] = '<div style="font-size:12px; padding-top:2px; padding-bottom:2px;">' .
                        $tratar->numero()->floatCliente($dados['fnc_conta_valor']) . ' <i class="fa fa-long-arrow-right"></i> ' .
                        $dados['fnc_conta_contabil_nome'] . '</div>';
            }
        }

        if ($dadosLancamento['fnc_parcela_pago'] === 'S') {

            if ($dadosLancamento['fnc_parcela_valor'] > $dadosLancamento['fnc_parcela_valor_pago']) {

                $valorDesconto = $dadosLancamento['fnc_parcela_valor'] - $dadosLancamento['fnc_parcela_valor_pago'];

                $html[] = '<div style="font-size:12px; padding-top:2px; padding-bottom:2px;">' .
                        $tratar->numero()->floatCliente($valorDesconto) . ' <i class="fa fa-long-arrow-right"></i> Desconto</div>';
            } else if ($dadosLancamento['fnc_parcela_valor'] < $dadosLancamento['fnc_parcela_valor_pago']) {
                
            }
        }

        if ($dadosLancamento['fnc_parcela_juros'] > 0) {

            $totalDespesas += $dadosLancamento['fnc_parcela_juros'];

            $html[] = '<div style="font-size:12px; padding-top:2px; padding-bottom:2px;">' .
                    $tratar->numero()->floatCliente($dadosLancamento['fnc_parcela_juros']) . ' <i class="fa fa-long-arrow-right"></i> Acréscimos </div>';
        }

        if ($dadosLancamento['fnc_parcela_desconto'] > 0) {

            $totalDespesas -= $dadosLancamento['fnc_parcela_desconto'];

            $html[] = '<div style="font-size:12px; padding-top:2px; padding-bottom:2px;">' .
                    $tratar->numero()->floatCliente($dadosLancamento['fnc_parcela_desconto']) . ' <i class="fa fa-long-arrow-right"></i> Descontos </div>';
        }

        if ($dadosLancamento['fnc_parcela_valor'] > '0.01') {
            $html[] = '<div style="font-size:12px; padding-top:2px; padding-bottom:2px;"><strong>
                Total das despesas: ' . $tratar->numero()->moedaCliente($totalDespesas) . '</strong></div>';
        }

        if ($dadosLancamento['fnc_lancamento_observacao']) {
            $html[] = '<div style="font-size:12px; padding-top:2px; padding-bottom:2px;">' . $dadosLancamento['fnc_lancamento_observacao'] . '</div>';
        }

        return implode('', $html);
    }

}
