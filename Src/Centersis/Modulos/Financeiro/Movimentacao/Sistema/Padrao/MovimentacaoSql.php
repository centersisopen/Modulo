<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class MovimentacaoSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function filtrarSql($objForm, $colunas, $colunasDinamicas) {

        $vencimento = $objForm->get('vencimento');
        $objForm->unsetObjeto('vencimento');
        
        $qb = $this->con->qb();

        //Colunas que não podem faltar no select, usada para geração de resultados extras
        $colunasSelect = $this->crudUtil->colunasNecessarias($colunasDinamicas, [
            'a.fnc_lancamento_cod',
            'b.fnc_parcela_cod',
            'b.fnc_parcela_pago',
            'b.fnc_parcela_tipo_lancamento',
            'b.fnc_parcela_vencimento',
            'a.fnc_lancamento_cod',
            'b.fnc_parcela_valor_pago',
            'b.fnc_parcela_valor'
        ]);

        $qb->select($colunasSelect)
                ->from('fnc_lancamento', 'a')
                ->innerJoin('a', 'fnc_parcela', 'b', 'a.fnc_lancamento_cod = b.fnc_lancamento_cod')
                ->leftJoin('a', 'empresa', 'c', 'a.empresa_cod = c.empresa_cod')
                ->leftJoin('a', 'cliente', 'd', 'a.cliente_cod = d.cliente_cod')
                ->leftJoin('a', 'funcionario', 'e', 'a.funcionario_cod = e.funcionario_cod')
                ->leftJoin('a', 'fornecedor', 'f', 'a.fornecedor_cod = f.fornecedor_cod')
                ->leftJoin('a', 'fnc_convenio', 'k', 'a.fnc_convenio_cod = k.fnc_convenio_cod')
                ->leftJoin('a', 'fnc_referencia', 'l', 'a.fnc_referencia_cod = l.fnc_referencia_cod')
                ->andWhere($qb->expr()->eq('a.fnc_lancamento_removido', ':fnc_lancamento_removido'))
                ->setParameter('fnc_lancamento_removido', 'N', \PDO::PARAM_STR)
                ->andWhere($qb->expr()->eq('b.fnc_parcela_removida', ':fnc_parcela_removida'))
                ->setParameter('fnc_parcela_removida', 'N', \PDO::PARAM_STR);

        $this->filtrarClass->interpretarComo('a.fnc_lancamento_data', 'DATE(a.fnc_lancamento_data)');
        
        if ($vencimento == "N") {
            
            $qb->andWhere($qb->expr()->gt('b.fnc_parcela_vencimento', ':fnc_parcela_vencimento'))
                    ->setParameter('fnc_parcela_vencimento', date('Y-m-d'),\PDO::PARAM_STR);            
        }
        
        if ($vencimento == "H") {
            
            $qb->andWhere($qb->expr()->eq('b.fnc_parcela_vencimento', ':fnc_parcela_vencimento'))
                    ->setParameter('fnc_parcela_vencimento', date('Y-m-d'),\PDO::PARAM_STR);            
        }
        
        if ($vencimento == "S") {
            
            $qb->andWhere($qb->expr()->lt('b.fnc_parcela_vencimento', ':fnc_parcela_vencimento'))
                    ->setParameter('fnc_parcela_vencimento', date('Y-m-d'),\PDO::PARAM_STR);            
        }

        return $qb;
    }

    public function getDadosSql($fncLancamentoCod, $removidos) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_lancamento', 'a')
                ->innerJoin('a', 'fnc_parcela', 'b', 'a.fnc_lancamento_cod = b.fnc_lancamento_cod')
                ->where($qb->expr()->eq('a.fnc_lancamento_cod', ':fnc_lancamento_cod'))
                ->setParameter('fnc_lancamento_cod', $fncLancamentoCod);

        if (!$removidos) {
            $qb->andWhere($qb->expr()->eq('a.fnc_lancamento_removido', ':fnc_lancamento_removido'))
                    ->andWhere($qb->expr()->eq('b.fnc_parcela_removida', ':fnc_parcela_removida'))
                    ->setParameter('fnc_lancamento_removido', 'N', \PDO::PARAM_STR)
                    ->setParameter('fnc_parcela_removida', 'N', \PDO::PARAM_STR);
        }

        $this->doOrganograma($qb, 'a');

        return $qb;
    }

    public function getDadosPorParcelaSql($fncParcelaCod, $removidos) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_lancamento', 'a')
                ->innerJoin('a', 'fnc_parcela', 'b', 'a.fnc_lancamento_cod = b.fnc_lancamento_cod')
                ->where($qb->expr()->eq('b.fnc_parcela_cod', ':fnc_parcela_cod'))
                ->setParameter('fnc_parcela_cod', $fncParcelaCod);

        if (!$removidos) {
            $qb->andWhere($qb->expr()->eq('a.fnc_lancamento_removido', ':fnc_lancamento_removido'))
                    ->andWhere($qb->expr()->eq('b.fnc_parcela_removida', ':fnc_parcela_removida'))
                    ->setParameter('fnc_lancamento_removido', 'N', \PDO::PARAM_STR)
                    ->setParameter('fnc_parcela_removida', 'N', \PDO::PARAM_STR);
        }

        $this->doOrganograma($qb, 'a');

        return $qb;
    }

    public function consultaParcelasQuitadasSql($fncLancamentoCod) {
        $qb = $this->con->qb();
        $qb->select('fnc_parcela_pago')
                ->from('fnc_parcela', '')
                ->where($qb->expr()->eq('fnc_parcela_removida', ':fnc_parcela_removida'))
                ->andWhere($qb->expr()->eq('fnc_parcela_pago', ':fnc_parcela_pago'))
                ->andWhere($qb->expr()->eq('fnc_lancamento_cod', ':fnc_lancamento_cod'))
                ->setParameter('fnc_parcela_removida', 'N', \PDO::PARAM_STR)
                ->setParameter('fnc_parcela_pago', 'S', \PDO::PARAM_STR)
                ->setParameter('fnc_lancamento_cod', $fncLancamentoCod, \PDO::PARAM_INT);

        $this->doOrganograma($qb, 'a');

        return $qb;
    }

}
