<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class ParcelaSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function getDadosSql($fncParcelaCod, $removidos = false) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_parcela', '')
                ->where($qb->expr()->eq('fnc_parcela_cod', ':fnc_parcela_cod'))
                ->setParameter('fnc_parcela_cod', $fncParcelaCod);

        if (!$removidos) {
            $qb->andWhere($qb->expr()->eq('fnc_parcela_removida', ':fnc_parcela_removida'))
                    ->setParameter('fnc_parcela_removida', 'N', \PDO::PARAM_STR);
        }

        $this->doOrganograma($qb);

        return $qb;
    }

    public function getParcelaSql($fncParcelaCod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_parcela', '')
                ->where($qb->expr()->eq('fnc_parcela_cod', ':fnc_parcela_cod'))
                ->andWhere($qb->expr()->eq('fnc_parcela_removida', $qb->expr()->literal('N')))
                ->setParameter('fnc_parcela_cod', $fncParcelaCod, \PDO::PARAM_INT);

        $this->doOrganograma($qb);

        return $qb;
    }

    public function getDadosPorLancamentoCodSql($fncLancamentoCod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_lancamento', 'a')
                ->innerJoin('a', 'fnc_parcela', 'b', 'a.fnc_lancamento_cod = b.fnc_lancamento_cod')
                ->where($qb->expr()->eq('b.fnc_lancamento_cod', ':fnc_lancamento_cod'))
                ->setParameter('fnc_lancamento_cod', $fncLancamentoCod)
                ->andWhere($qb->expr()->eq('a.fnc_lancamento_removido', ':nao'))
                ->andWhere($qb->expr()->eq('b.fnc_parcela_removida', ':nao'))
                ->setParameter('nao', 'N', \PDO::PARAM_STR);

        $this->doOrganograma($qb, 'a');

        return $qb;
    }

    public function getDadosPorNossoNumeroSql($fncParcelaNossoNumero, $fncConvenioCod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_lancamento', 'a')
                ->innerJoin('a', 'fnc_parcela', 'b', 'a.fnc_lancamento_cod = b.fnc_lancamento_cod')
                ->where($qb->expr()->eq('b.fnc_parcela_nosso_numero', ':fnc_parcela_nosso_numero'))
                ->setParameter('fnc_parcela_nosso_numero', $fncParcelaNossoNumero)
                ->andWhere($qb->expr()->eq('a.fnc_convenio_cod', ':fnc_convenio_cod'))
                ->setParameter('fnc_convenio_cod', $fncConvenioCod)
                ->andWhere($qb->expr()->eq('a.fnc_lancamento_removido', ':fnc_lancamento_removido'))
                ->setParameter('fnc_lancamento_removido', 'N', \PDO::PARAM_STR)
                ->andWhere($qb->expr()->eq('b.fnc_parcela_removida', ':fnc_parcela_removida'))
                ->setParameter('fnc_parcela_removida', 'N', \PDO::PARAM_STR);

        $this->doOrganograma($qb, 'a');

        return $qb;
    }

    public function getDadosPorNossoNumeroComExcluidosSql($fncParcelaNossoNumero, $fncConvenioCod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_lancamento', 'a')
                ->innerJoin('a', 'fnc_parcela', 'b', 'a.fnc_lancamento_cod = b.fnc_lancamento_cod')
                ->where($qb->expr()->eq('b.fnc_parcela_nosso_numero', ':fnc_parcela_nosso_numero'))
                ->setParameter('fnc_parcela_nosso_numero', $fncParcelaNossoNumero)
                ->andWhere($qb->expr()->eq('a.fnc_convenio_cod', ':fnc_convenio_cod'))
                ->setParameter('fnc_convenio_cod', $fncConvenioCod)
                ->andWhere($qb->expr()->eq('a.fnc_lancamento_removido', ':fnc_lancamento_removido'))
                ->setParameter('fnc_lancamento_removido', 'S', \PDO::PARAM_STR)
                ->andWhere($qb->expr()->eq('b.fnc_parcela_removida', ':fnc_parcela_removida'))
                ->setParameter('fnc_parcela_removida', 'S', \PDO::PARAM_STR);

        $this->doOrganograma($qb, 'a');

        $qb->setMaxResults(1);

        return $qb;
    }

    public function parcelasDoLancamentoSql($fncLancamentoCod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_parcela', '')
                ->where($qb->expr()->eq('fnc_lancamento_cod', ':fnc_lancamento_cod'))
                ->setParameter('fnc_lancamento_cod', $fncLancamentoCod)
                ->andWhere($qb->expr()->eq('fnc_parcela_removida', ':fnc_parcela_removida'))
                ->setParameter('fnc_parcela_removida', 'N', \PDO::PARAM_STR);

        $this->doOrganograma($qb);

        return $qb;
    }

    public function parcelasAbertasSql($chavePesquisa, $codigo) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_lancamento', 'a')
                ->innerJoin('a', 'fnc_parcela', 'b', 'a.fnc_lancamento_cod = b.fnc_lancamento_cod')
                ->where($qb->expr()->eq('a.' . $chavePesquisa, ':' . $chavePesquisa))
                ->setParameter($chavePesquisa, $codigo)
                ->andWhere($qb->expr()->eq('b.fnc_parcela_pago', ':fnc_parcela_pago'))
                ->setParameter('fnc_parcela_pago', 'N', \PDO::PARAM_STR)
                ->andWhere($qb->expr()->eq('b.fnc_parcela_tipo_lancamento', ':fnc_parcela_tipo_lancamento'))
                ->setParameter('fnc_parcela_tipo_lancamento', 'R', \PDO::PARAM_STR)
                ->andWhere($qb->expr()->eq('b.fnc_parcela_removida', ':fnc_parcela_removida'))
                ->setParameter('fnc_parcela_removida', 'N', \PDO::PARAM_STR)
                ->andWhere($qb->expr()->eq('a.fnc_lancamento_removido', ':fnc_lancamento_removido'))
                ->setParameter('fnc_lancamento_removido', 'N', \PDO::PARAM_STR)
                ->orderBy('b.fnc_parcela_vencimento');

        $this->doOrganograma($qb, 'a');

        return $qb;
    }

}
