<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class NossoNumeroSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function getUltimoNumeroSql($convenioCod) {
        $qb = $this->con->qb();

        $qb->select('fnc_nosso_numero_puro')
                ->from('fnc_nosso_numero', '')
                ->where($qb->expr()->eq('fnc_convenio_cod', ':fnc_convenio_cod'))
                ->setParameter('fnc_convenio_cod', $convenioCod)
                ->orderBy('fnc_nosso_numero_puro', 'DESC')
                ->setMaxResults(1);

        $this->doOrganograma($qb);

        return $qb;
    }

}
