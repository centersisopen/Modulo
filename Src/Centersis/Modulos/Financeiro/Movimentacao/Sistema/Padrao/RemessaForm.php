<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Ext\Core\Padrao\BaseForm;

class RemessaForm extends BaseForm {

    public function __construct($organogramaCod, $con) {

        parent::__construct($organogramaCod, $con);
    }

    public function getFormRemessa() {
        $form = new Form();

        $form->setAcao('cadastrar');

        $form->config('formRemessa', 'POST')
                ->setAcaoSubmit('gerarRemessa(\'formRemessa\')')
                ->setHeader('Remessa');

        $campos[] = $form->escolha('fnc_convenio_cod', 'Convênio Bancário', true)
                ->setTabela('fnc_convenio')
                ->setCampoCod('fnc_convenio_cod')
                ->setCampoDesc('fnc_convenio_identifica')
                ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                ->setValor($form->retornaValor('fnc_convenio_cod'));

        $campos[] = $form->botaoSalvarPadrao()
                ->setValor('Gerar Remessa');

        $campos[] = $form->botaoSimples('Descartar', 'Descartar')
                ->setClassCss('btn btn-default')
                ->setComplemento('onclick="descartarRemessa()"');

        return $form->processarForm($campos);
    }

}
