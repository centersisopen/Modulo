<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class RemessaSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function ultimaRemessaDoConvenioSql($fncConvenioCod) {

        $qb = $this->con->qb();

        $qb->select('MAX(fnc_remessa_numero) as ultimo')
                ->from('fnc_remessa')
                ->where($qb->expr()->eq('organograma_cod', $this->organogramaCod))
                ->andWhere($qb->expr()->eq('fnc_convenio_cod', $fncConvenioCod));

        return $qb;
    }

    public function arquivosViewSql($moduloCod, $maximo) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('_upload')
                ->where($qb->expr()->eq('upload_nome_campo', $qb->expr()->literal('remessa')))
                ->andWhere($qb->expr()->eq('modulo_cod', ':modulo_cod'))
                ->setParameter('modulo_cod', $moduloCod)
                ->andWhere($qb->expr()->eq('organograma_cod', $this->organogramaCod))
                ->setMaxResults($maximo)
                ->orderBy('upload_cod', 'DESC');

        return $qb;
    }

    public function parcelasNaoProcessadasSql($fncConvenioCod) {

        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_lancamento', 'a')
                ->innerJoin('a', 'fnc_parcela', 'b', 'a.fnc_lancamento_cod = b.fnc_lancamento_cod')
                ->where($qb->expr()->eq('a.fnc_lancamento_removido', $qb->expr()->literal('N')))
                ->andWhere($qb->expr()->eq('b.fnc_parcela_removida', $qb->expr()->literal('N')))
                ->andWhere($qb->expr()->eq('b.fnc_parcela_pago', $qb->expr()->literal('N')))
                ->andWhere($qb->expr()->eq('b.fnc_parcela_tipo_lancamento', $qb->expr()->literal('R')))
                ->andWhere($qb->expr()->eq('b.fnc_parcela_remessa', $qb->expr()->literal('N')))
                ->andWhere($qb->expr()->eq('a.fnc_convenio_cod', $fncConvenioCod))
                ->andWhere($qb->expr()->gte('b.fnc_parcela_vencimento', $qb->expr()->literal(date('Y-m-d'))))
                ->andWhere($qb->expr()->eq('a.organograma_cod', $this->organogramaCod));

        return $qb;
    }

}
