<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Crud\CrudUtil;
use Centersis\Zion2\Pixel\Grid\GridPadrao;
use Centersis\Zion2\Paginacao\Parametros;
use Centersis\Financeiro\Movimentacao\Sistema\ContaClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\ParcelaClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\ParcelaFormFactory;
use Centersis\Ext\Form\Form;
use Centersis\Zion2\Tratamento\Tratamento;
use Centersis\Zion2\Exception\ValidationException;

class MovimentacaoClass {

    protected $chavePrimaria;
    protected $crudUtil;
    protected $tabela;
    protected $colunasCrud;
    protected $colunasGrid;
    protected $filtroDinamico;
    protected $organogramaCod;

    /**
     * @var MovimentacaoSql 
     */
    protected $movimentacaoSql;

    /**
     * @var \Zion2\Banco\Conexao 
     */
    protected $con;

    protected function origens() {
        return 'COALESCE(
                c.empresa_nome,
                d.cliente_nome,
                e.funcionario_nome,
                f.fornecedor_nome) as origem';
    }

    public function __construct($organogramaCod, $con, $sql) {
        $this->crudUtil = new CrudUtil();

        $this->tabela = 'fnc_lancamento';
        $this->chavePrimaria = 'fnc_lancamento_cod';

        $this->colunasCrud = [
            'fnc_convenio_cod' => 'fnc_convenio_cod',
            'fnc_referencia_cod' => 'fnc_referencia_cod',
            'empresa_cod' => 'empresa_cod',
            'cliente_cod' => 'cliente_cod',
            'funcionario_cod' => 'funcionario_cod',
            'fornecedor_cod' => 'fornecedor_cod',
            'fnc_lancamento_nome' => 'fnc_lancamento_nome',
            'fnc_lancamento_data_base' => 'fnc_lancamento_data_base',
            'fnc_lancamento_observacao' => 'fnc_lancamento_observacao',
            'fnc_lancamento_tipo_lancamento' => 'fnc_lancamento_tipo_lancamento',
            'fnc_lancamento_tipo' => 'fnc_lancamento_tipo',
            'fnc_lancamento_faturado' => 'fnc_lancamento_faturado',
            'fnc_lancamento_origem' => 'fnc_lancamento_origem'
        ];

        $this->colunasGrid = [
            'documento' => ['Documento', 'CONCAT(a.fnc_lancamento_nome,"<br>",b.fnc_parcela_nome) as documento', true],
            'origem' => ['Origem', $this->origens(), true],
            'configuracao' => ['Configurações Contábeis', null, true],
            'fnc_convenio_identifica' => ['Convênio', 'k.fnc_convenio_identifica', false],
            'fnc_parcela_valor' => ['Valor Doc.', 'b.fnc_parcela_valor', true],
            'fnc_parcela_valor_pago' => ['Pago', 'b.fnc_parcela_valor_pago', true],
            'fnc_lancamento_data' => ['Lançamento', 'a.fnc_lancamento_data', true],
            'fnc_parcela_vencimento' => ['Vencimento', 'b.fnc_parcela_vencimento', true],
            'fnc_parcela_pagamento' => ['Pagamento', 'b.fnc_parcela_pagamento', true],
            'situacao' => ['Situação', null, true],
            'fnc_lancamento_observacao' => ['Observações', 'a.fnc_lancamento_observacao', false],
            'fnc_lancamento_parcelado' => ['Parcelado', 'a.fnc_lancamento_parcelado', false],
            'fnc_parcela_nosso_numero' => ['Nosso Nº', 'b.fnc_parcela_nosso_numero', false],
            'fnc_parcela_desconto' => ['Desconto', 'b.fnc_parcela_desconto', false],
            'fnc_parcela_juros' => ['Acrécimos', 'if(fnc_parcela_pago = "S",(fnc_parcela_valor_pago - fnc_parcela_valor),0) as fnc_parcela_juros', false],
            'fnc_referencia_nome' => ['Referência', 'l.fnc_referencia_nome', false],
            'fnc_parcela_baixa_manual' => ['Forma Baixa', 'b.fnc_parcela_baixa_manual', false],
            'fnc_parcela_remessa' => ['Enviado Remessa', 'b.fnc_parcela_remessa', false],
            'anexo' => ['Anexos', false, false],
        ];

        $this->filtroDinamico = [
            'fnc_lancamento_nome' => 'a',
            'fnc_parcela_nome' => 'b',
            'fnc_parcela_nosso_numero' => 'b',
            'cliente_nome' => 'd',
            'funcionario_nome' => 'e',
            'fornecedor_nome' => 'f',
            'fnc_parcela_valor' => 'b',
            'fnc_parcela_valor_pago' => 'b',
        ];

        $this->movimentacaoSql = $sql;
        $this->organogramaCod = $organogramaCod;

        $this->con = $con;
    }

    protected function grid() {
        return new GridPadrao();
    }

    public function filtrar($objForm) {
        $grid = $this->grid();
        $conta = (new ContaClassFactory())->instancia($this->organogramaCod);

        Parametros::setParametros("GET", $this->crudUtil->getParametrosGrid($objForm));

        $grid->setColunas($this->colunasGrid);

        $grid->setColunas($this->crudUtil->getColunasDinamicas($this->colunasGrid));
        $grid->setSql($this->movimentacaoSql->filtrarSql($objForm, $this->filtroDinamico, $this->crudUtil->getColunasDinamicasSql($grid->getColunas())));
        $grid->substituaPor('fnc_parcela_valor_pago', ['0,00' => '<em>não pago</em>']);
        $grid->substituaPor('fnc_parcela_pagamento', ['<em>não pago</em>']);
        $grid->substituaPor('fnc_convenio_identifica', ['<em>não informado</em>']);
        $grid->substituaPor('fnc_lancamento_parcelado', ['S' => 'Sim', 'N' => 'Não']);
        $grid->substituaPor('fnc_parcela_remessa', ['S' => 'Sim', 'N' => 'Não']);
        $grid->substituaPor('fnc_parcela_baixa_manual', ['<em>não informado</em>', 'S' => 'Manual', 'N' => 'Automática']);
        $grid->setFormatarComo('fnc_parcela_valor', 'Moeda');
        $grid->setFormatarComo('fnc_parcela_desconto', 'Moeda');
        $grid->setFormatarComo('fnc_parcela_juros', 'Moeda');
        $grid->setFormatarComo('fnc_parcela_vencimento', 'Data');
        $grid->setFormatarComo('fnc_parcela_pagamento', 'Data');
        $grid->setFormatarComo('fnc_lancamento_data', 'Data');
        $grid->converterResultado($this, 'converteValorPago', 'fnc_parcela_valor_pago', ['fnc_parcela_valor', 'fnc_parcela_valor_pago', 'fnc_parcela_pago']);
        $grid->converterResultado($conta, 'destinoContabilHtml', 'configuracao', ['fnc_parcela_cod']);
        $grid->converterResultado($this, 'montaSituacao', 'situacao', ['fnc_parcela_tipo_lancamento', 'fnc_parcela_pago']);
        $grid->cssTD($this, 'linhaSituacao', ['fnc_parcela_pago', 'fnc_parcela_vencimento']);
        $grid->setTotalizador('fnc_parcela_valor', ['tipo' => 'soma', 'mascara' => 'moeda']);
        $grid->setTotalizador('fnc_parcela_valor_pago', ['tipo' => 'soma', 'mascara' => 'moeda']);
        $grid->setTotalizador('fnc_parcela_juros', ['tipo' => 'soma', 'mascara' => 'moeda']);
        $grid->naoOrdenePor(['anexo', 'configuracao', 'situacao']);
        $grid->setAlinhamento(['anexo' => 'centro']);
        $grid->setLegenda($this->getLegenda());
        $grid->setChave($this->chavePrimaria);
        $grid->setQLinhas(50);
        $grid->processarUpload([
            'anexo' => [
                'referenciaCod' => 'fnc_lancamento_cod',
                'nome' => 'anexo',
                'urlBaseStorage' => SIS_URL_BASE_STORAGE . $this->organogramaCod . '/',
                'modulo' => 'Movimentacao',
                'organogramaCod' => $this->organogramaCod]
        ]);

        return $grid->montaGridPadrao();
    }

    protected function getLegenda() {
        return '<div class="table-footer">
        <div class="row">
            <div class="col-sm-1">
                <strong>Legenda:</strong>
            </div>
            <div class="col-sm-2">
                <span class="label label-default label-tag btn-block">Parcelas ainda não vencidas</span>
            </div>
            <div class="col-sm-2">
                <span class="label label-danger label-tag btn-block">Parcelas vencidas e não quitadas</span>
            </div>
            <div class="col-sm-2">
                <span class="label label-success label-tag btn-block">Parcelas já quitadas</span>
            </div>
        </div>
        </div>';
    }

    public function converteValorPago($fncParcelaValor, $fncParcelaValorPago, $fncParcelaPago) {
        $trata = Tratamento::instancia();

        if ($fncParcelaPago == 'S') {

            if ($fncParcelaValor > $fncParcelaValorPago) {
                return '<div class="bg-danger text-bg padding-sm">' . $trata->numero()->floatCliente($fncParcelaValorPago) . '</div>';
            } else if ($fncParcelaValor < $fncParcelaValorPago) {
                return '<div class="bg-info text-bg padding-sm">' . $trata->numero()->floatCliente($fncParcelaValorPago) . '</div>';
            }
        }

        return $trata->numero()->floatCliente($fncParcelaValorPago);
    }

    public function persisteLancamento($objForm) {
        return $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm);
    }

    /**
     * @param \Pixel\Form\Form $objForm
     * @return int
     */
    public function cadastrar($objForm, $configuracaoContabil = null, $parcelas = null) {

        $this->validaAnexo($objForm);

        $parcelaClass = (new ParcelaClassFactory())->instancia($this->organogramaCod);

        $objForm->set('valorTotal', $objForm->get('valorContabilHidden'), 'Float');

        $this->crudUtil->startTransaction();

        $this->interpretaOrigem($objForm);

        if (!$objForm->get('fnc_lancamento_tipo')) {
            $objForm->set('fnc_lancamento_tipo', 'Padrao');
        }

        if (!$objForm->get('fnc_lancamento_faturado')) {
            $objForm->set('fnc_lancamento_faturado', 'S');
        }

        $fncLancamentoCod = $this->persisteLancamento($objForm);

        $objForm->set('fnc_lancamento_cod', $fncLancamentoCod);

        if (!$parcelas) {
            $parcelas = $this->getParcelas($objForm, $fncLancamentoCod);
        }

        $tipoLancamento = $objForm->get('fnc_lancamento_tipo_lancamento');

        $idValorContabil = 'R';
        if ($tipoLancamento == 'AP' or $tipoLancamento == 'PA') {
            $idValorContabil = 'P';
        }

        if (!$configuracaoContabil) {
            $objContabil = $objForm->getObjetos('valorContabil' . $idValorContabil);
            $configuracaoContabil = $this->atualizaConfiguracaoContabil($objForm, $objContabil->getDados());
        }

        $parcelaClass->cadastrar($objForm, $configuracaoContabil, $parcelas);

        $this->crudUtil->stopTransaction();

        return $fncLancamentoCod;
    }

    /**
     * @param \Pixel\Form\Form $objForm
     * @return int
     */
    public function alterar($objForm, $configuracaoContabil = null, $parcelas = null) {

        $this->validaAnexo($objForm);

        $parcelaClass = (new ParcelaClassFactory())->instancia($this->organogramaCod);
        $contaClass = (new ContaClassFactory())->instancia($this->organogramaCod);

        $this->crudUtil->startTransaction();

        $this->interpretaOrigem($objForm);

        $fncLancamentoCod = $objForm->get('cod');

        $afetados = $this->crudUtil->update($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm, [$this->chavePrimaria => $fncLancamentoCod]);

        $objForm->set('fnc_lancamento_cod', $fncLancamentoCod);
        $objForm->set('valorTotal', $objForm->retornaValor('valorContabilHidden'), 'Float');

        $contaClass->removerPorLancamento($this, $fncLancamentoCod);
        $parcelaClass->removerPorLancamento($fncLancamentoCod, 'Editado');

        if (!$parcelas) {
            $parcelas = $this->getParcelas($objForm, $fncLancamentoCod);
        }

        if (!$configuracaoContabil) {

            $tipoLancamento = $objForm->get('fnc_lancamento_tipo_lancamento');

            $idValorContabil = 'R';
            if ($tipoLancamento == 'AP' or $tipoLancamento == 'PA') {
                $idValorContabil = 'P';
            }

            $objContabil = $objForm->getObjetos('valorContabil' . $idValorContabil);
            $configuracaoContabil = $objContabil->getDados();
        }

        $parcelaClass->cadastrar($objForm, $configuracaoContabil, $parcelas);

        $this->crudUtil->stopTransaction();

        return $afetados;
    }

    public function atualizaConfiguracaoContabil($objFormLancamento, $configuracaoContabil) {

        foreach (array_keys($configuracaoContabil) as $chave) {

            $configuracaoContabil[$chave]['multiplicar'] = 1;
        }

        return $configuracaoContabil;
    }

    protected function getParcelas($objForm, $fncLancamentoCod) {
        $parcelaForm = (new ParcelaFormFactory())->instancia($this->organogramaCod);

        $operacao = $objForm->get('fnc_lancamento_tipo_lancamento');

        $objFormParcelas = $parcelaForm->getFormParcela(new Form(), $operacao);

        $this->crudUtil->masterDetail($objFormParcelas, $fncLancamentoCod);

        $objParcela = $objFormParcelas->getObjetos('parcela');

        return $objParcela->getDados();
    }

    public function interpretaOrigem($objForm) {
        $origem = $objForm->get('origem');
        $origemCod = $objForm->get('origemCod');

        if (!$origemCod) {
            throw new ValidationException('Origem não informada!');
        }

        $objForm->set('empresa_cod', null, 'Inteiro');
        $objForm->set('cliente_cod', null, 'Inteiro');
        $objForm->set('funcionario_cod', null, 'Inteiro');
        $objForm->set('fornecedor_cod', null, 'Inteiro');

        switch ($origem) {
            case 'empresa_cod' : $objForm->set('empresa_cod', $origemCod, 'Inteiro');
                break;
            case 'cliente_cod': $objForm->set('cliente_cod', $origemCod, 'Inteiro');
                break;
            case 'funcionario_cod' : $objForm->set('funcionario_cod', $origemCod, 'Inteiro');
                break;
            case 'fornecedor_cod': $objForm->set('fornecedor_cod', $origemCod, 'Inteiro');
                break;

            default : throw new \Exception('Origem Inválida!');
        }
    }

    public function remover($fncLancamentoCod, $mensagem) {
        $form = new Form();
        $parcelaClass = (new ParcelaClassFactory())->instancia($this->organogramaCod);
        $contaClass = (new ContaClassFactory())->instancia($this->organogramaCod);

        $crud = [
            'fnc_lancamento_removido',
            'fnc_lancamento_removido_data',
            'fnc_lancamento_removido_motivo',
            'fnc_lancamento_removido_quem'
        ];

        $usuarioCod = $_SESSION['usuario_cod'] ?? session()->get('usuario_cod');

        $form->set('fnc_lancamento_removido', 'S', 'Texto');
        $form->set('fnc_lancamento_removido_data', \date('d/m/Y H:i:s'), 'DataHora');
        $form->set('fnc_lancamento_removido_motivo', $mensagem, 'Texto');
        $form->set('fnc_lancamento_removido_quem', $usuarioCod, 'Inteiro');
        $form->set('fnc_lancamento_cod', $fncLancamentoCod, 'Inteiro');

        $this->crudUtil->startTransaction();

        $contaClass->removerPorLancamento($this, $fncLancamentoCod);
        $parcelaClass->removerPorLancamento($fncLancamentoCod, $mensagem);
        $this->crudUtil->update($this->organogramaCod, $this->tabela, $crud, $form, ['fnc_lancamento_cod' => $fncLancamentoCod]);
        $contaClass->confereValoresContabeis();

        $this->crudUtil->stopTransaction();
    }

    public function getDados($fncParcelaCod, $posicao = '', $removidos = false) {
        if ($posicao) {
            return $this->con->execRLinha($this->movimentacaoSql->getDadosSql($fncParcelaCod, $removidos), $posicao);
        } else {
            return $this->con->execLinha($this->movimentacaoSql->getDadosSql($fncParcelaCod, $removidos));
        }
    }

    public function getDadosPorParcela($fncParcelaCod, $posicao = '', $removidos = false) {
        if ($posicao) {
            return $this->con->execRLinha($this->movimentacaoSql->getDadosPorParcelaSql($fncParcelaCod, $removidos), $posicao);
        } else {
            return $this->con->execLinha($this->movimentacaoSql->getDadosPorParcelaSql($fncParcelaCod, $removidos));
        }
    }

    public function setValoresFormManu($fncLancamentoCod, $formIntancia) {

        $parcelaClass = (new ParcelaClassFactory())->instancia($this->organogramaCod);

        $parametrosSql = $this->con->execLinhaArray($this->movimentacaoSql->getDadosSql($fncLancamentoCod, false));

        $parametrosSql['fnc_lancamento_data_base'] = $parametrosSql['fnc_parcela_vencimento'];

        $objForm = $formIntancia->getFormManu('alterar', $fncLancamentoCod, $parametrosSql['fnc_parcela_tipo_lancamento']);

        if (empty($parametrosSql)) {
            throw new \Exception('Lançamento não encontrado!');
        }

        $parametrosComOrigem = $this->setOrigem($parametrosSql);

        $parametrosComOrigem['numero_parcelas'] = $parcelaClass->getNumeroParcelas($fncLancamentoCod);
        $objForm->set('valorContabilHidden', $parcelaClass->getValorDocumento($fncLancamentoCod));
        $this->crudUtil->setParametrosForm($objForm, $parametrosComOrigem, $fncLancamentoCod);

        return $objForm;
    }

    protected function setOrigem($parametrosSql) {
        /**
         * Verifica qual chave esta preechida e então seta a origem
         */
        if ($parametrosSql['empresa_cod']) {
            $parametrosSql['origem'] = 'empresa_cod';
        }

        if ($parametrosSql['cliente_cod']) {
            $parametrosSql['origem'] = 'cliente_cod';
        }

        if ($parametrosSql['funcionario_cod']) {
            $parametrosSql['origem'] = 'funcionario_cod';
        }

        if ($parametrosSql['fornecedor_cod']) {
            $parametrosSql['origem'] = 'fornecedor_cod';
        }

        return $parametrosSql;
    }

    public function linhaSituacao($fncParcelaPago, $fncParcelaVencimento) {
        if ($fncParcelaPago === 'S') {
            return ' bg-success ';
        } else {
            $dataAtual = date('Y-m-d');
            if ($fncParcelaVencimento < $dataAtual) {
                return ' bg-danger ';
            }
        }

        return ' bg-default ';
    }

    public function montaSituacao($fncParcelaTipoLancamento, $fncParcelaPago) {
        $texto = '';
        $class = '';
        if ($fncParcelaTipoLancamento === 'P') {

            if ($fncParcelaPago === 'S') {
                $texto = 'pago';
                $class = 'success';
            } else {
                $texto = 'a pagar';
                $class = 'info';
            }
        } else {
            if ($fncParcelaPago === 'S') {
                $texto = 'recebido';
                $class = 'success';
            } else {
                $texto = 'a receber';
                $class = 'info';
            }
        }
        return $texto;
    }

    public function informaParcelamento($fncLancamentoCod, $sn) {

        $campos = ['fnc_lancamento_parcelado'];
        $valores = ['fnc_lancamento_parcelado' => ['Texto' => $sn]];
        $criterio = ['fnc_lancamento_cod' => $fncLancamentoCod];

        $this->crudUtil->update($this->organogramaCod, $this->tabela, $campos, $valores, $criterio);
    }

    public function calculaValorTotal($configuracaoContabil) {
        $trata = Tratamento::instancia();

        $total = 0;
        foreach ($configuracaoContabil as $dadosContabeis) {

            $multiplicar = array_key_exists('multiplicar', $dadosContabeis) ? $dadosContabeis['multiplicar'] : 1;

            $total += $trata->numero()->floatBanco($dadosContabeis['fnc_conta_valor']) * $multiplicar;
        }

        return $total;
    }

    private function validaAnexo($objForm) {
        if (!isset($_FILES['anexo'])) {
            return;
        }

        $parcelado = $objForm->get('parcelado');

        $existeAnexo = empty($_FILES['anexo']['name'][0]) ? false : true;

        if ($existeAnexo and $parcelado == 'S') {

            throw new ValidationException('Lançamentos parcelados não devem conter anexos!');
        }
    }
}
