<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Cadastro\Empresa\Sistema\EmpresaClassFactory;
use Centersis\Cadastro\Cliente\Sistema\ClienteClassFactory;
use Centersis\Cadastro\Funcionario\Sistema\FuncionarioClassFactory;
use Centersis\Cadastro\Fornecedor\Sistema\FornecedorClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\ContaClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\NossoNumeroClassFactory;
use Centersis\Financeiro\PlanoContas\Sistema\PlanoContasClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\MovimentacaoClassFactory;
use Centersis\Financeiro\FecharMes\Sistema\FecharMesClassFactory;
use Centersis\Ext\Crud\CrudUtil;
use Centersis\Zion2\Tratamento\Tratamento;
use Centersis\Zion2\Validacao\Valida;
use Centersis\Ext\Form\Form;
use Centersis\Zion2\Exception\ValidationException;

class ParcelaClass {

    protected $crudUtil;
    protected $chavePrimaria;
    protected $tabela;
    protected $colunasCrud;
    protected $tratar;
    protected $organogramaCod;

    /**
     * @var ParcelaSql 
     */
    protected $parcelaSql;

    /**
     * @var \Zion2\Banco\Conexao 
     */
    protected $con;

    public function __construct($organogramaCod, $con, $sql) {
        $this->crudUtil = new CrudUtil();

        $this->parcelaSql = $sql;
        $this->organogramaCod = $organogramaCod;

        $this->tabela = 'fnc_parcela';
        $this->chavePrimaria = 'fnc_parcela_cod';

        $this->colunasCrud = [
            'fnc_lancamento_cod',
            'fnc_forma_cod',
            'fnc_parcela_nosso_numero',
            'fnc_parcela_nome',
            'fnc_parcela_valor',
            'fnc_parcela_valor_pago',
            'fnc_parcela_vencimento',
            'fnc_parcela_competencia',
            'fnc_parcela_pagamento',
            'fnc_parcela_compensa',
            'fnc_parcela_tipo_lancamento',
            'fnc_parcela_pago',
            'fnc_parcela_obs',
            'fnc_parcela_desconto_forma',
            'fnc_parcela_desconto',
            'fnc_parcela_desconto_data',
            'fnc_parcela_juros_forma',
            'fnc_parcela_juros',
            'fnc_parcela_juros_data',
            'fnc_parcela_hash'
        ];

        $this->tratar = Tratamento::instancia();

        $this->con = $con;
    }

    protected function validaNivelContabil($configuracaoContabil, $tipoLancamento) {

        $planoContasClass = (new PlanoContasClassFactory())->instancia($this->organogramaCod);
        $contasContabeis = $planoContasClass->getContasContabeis();

        foreach ($configuracaoContabil as $dadosContabeis) {

            $fncContaContabilCod = $dadosContabeis['fnc_conta_contabil_cod'];

            if (!key_exists($fncContaContabilCod, $contasContabeis)) {
                throw new ValidationException('Conta contabil não informada para o valor!');
            }

            $nivel = substr($contasContabeis[$fncContaContabilCod]['fnc_conta_contabil_nivel'], 0, 1);

            if ($nivel == '3' and $tipoLancamento == 'P') {
                throw new ValidationException('Em lançamentos pago ou a pagar, use apenas contas contábeis do nível 4.x');
            }

            if ($nivel == '4' and $tipoLancamento == 'R') {
                throw new ValidationException('Em lançamentos recebido ou a receber, use apenas contas contábeis do nível 3.x');
            }
        }
    }

    protected function gravarParcela($objFormParcela, $objFormLancamento) {
        return $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $objFormParcela);
    }

    public function cadastrar($objFormLancamento, $configuracaoContabil, $parcelas) {

        $form = new Form();
        $contaClass = (new ContaClassFactory())->instancia($this->organogramaCod);
        $nossoNumeroClass = (new NossoNumeroClassFactory)->instancia($this->organogramaCod);
        $movimentacaoClass = (new MovimentacaoClassFactory())->instancia($this->organogramaCod);

        $parcelaCod = [];

        $fncLancamentoCod = $objFormLancamento->get('fnc_lancamento_cod');
        $operacao = $objFormLancamento->get('fnc_lancamento_tipo_lancamento');
        $tipoLancamento = str_replace(['A', 'E'], '', $operacao);

        $form->set('fnc_lancamento_cod', $fncLancamentoCod, 'Inteiro');
        $form->set('fnc_parcela_tipo_lancamento', $tipoLancamento, 'Texto');

        $vTotalTemp = $objFormLancamento->getSql('valorTotal');

        $this->validaNivelContabil($configuracaoContabil, $tipoLancamento);

        if ($vTotalTemp <= 0) {
            throw new ValidationException('Valor total deve ser maior que zero!');
        }

        $valorTotal = $this->calculaValorTotal($configuracaoContabil);

        $dadosParcela = $this->validaParcelas($parcelas, $operacao, $objFormLancamento->get('multiplicar'), $valorTotal);

        $objFormLancamento->set('valorTotal', $valorTotal);

        $acumulado = 0;
        $acomuladoPago = 0;
        $acumuladoContabil = 0;
        $hashParcelamento = bin2hex(openssl_random_pseudo_bytes(5));

        $cont = 0;
        foreach ($dadosParcela as $fncParcelaCod => $dados) {

            if ($cont === 0) {
                $form->set('fnc_lancamento_cod', $fncLancamentoCod, 'Inteiro');

                if (count($dadosParcela) > 1) {
                    $movimentacaoClass->informaParcelamento($fncLancamentoCod, $hashParcelamento);
                }
            } else {
                $fncLancamentoCod = $movimentacaoClass->persisteLancamento($objFormLancamento);
                $objFormLancamento->set('fnc_lancamento_cod', $fncLancamentoCod);
                $form->set('fnc_lancamento_cod', $fncLancamentoCod, 'Inteiro');
                $movimentacaoClass->informaParcelamento($fncLancamentoCod, $hashParcelamento);
            }

            $cont++;

            $dadosParcela = $this->getDados($fncParcelaCod, null, true);

            $vencimentoOriginal = array_key_exists('fnc_parcela_vencimento_original', $dadosParcela) ? $this->tratar->data()->converteData($dadosParcela['fnc_parcela_vencimento_original']) : $dados['fnc_parcela_vencimento'];

            if (!$vencimentoOriginal or $vencimentoOriginal === '0000-00-00') {
                $vencimentoOriginal = date('d/m/Y');
            }

            $form->set('fnc_parcela_nome', $dados['fnc_parcela_nome'], 'Texto');
            $form->set('fnc_parcela_vencimento', $dados['fnc_parcela_vencimento'], 'Data');
            $form->set('fnc_parcela_competencia', $objFormLancamento->get('fnc_parcela_competencia'), 'Data');
            $form->set('fnc_parcela_vencimento_original', $vencimentoOriginal, 'Data');
            $form->set('fnc_parcela_pagamento', $dados['fnc_parcela_pagamento'], 'Data');
            $form->set('fnc_parcela_compensa', $dados['fnc_parcela_pagamento'], 'Data');
            $form->set('fnc_parcela_valor', $dados['fnc_parcela_valor'], 'Float');
            $form->set('fnc_parcela_valor_pago', $dados['fnc_parcela_valor_pago'], 'Float');
            $form->set('fnc_forma_cod', $dados['fnc_forma_cod'], 'Inteiro');
            $form->set('fnc_parcela_pago', $dados['fnc_parcela_pago'], 'Texto');

            $form->set('fnc_parcela_desconto_forma', $dados['fnc_parcela_desconto_forma'], 'Texto');
            $form->set('fnc_parcela_desconto', $dados['fnc_parcela_desconto'], 'Float');
            $form->set('fnc_parcela_desconto_data', $dados['fnc_parcela_desconto_data'], 'Data');

            $form->set('fnc_parcela_juros_forma', $dados['fnc_parcela_juros_forma'], 'Texto');
            $form->set('fnc_parcela_juros', $dados['fnc_parcela_juros'], 'Float');
            $form->set('fnc_parcela_juros_data', $dados['fnc_parcela_juros_data'], 'Data');

            if ($tipoLancamento === 'R') {

                if ($objFormLancamento->get('fnc_convenio_cod')) {

                    $dadosIguais = false;

                    if (isset($dadosParcela['fnc_parcela_vencimento'])) {
                        $vencimentoConvertido = $this->tratar->data()->converteData($dados['fnc_parcela_vencimento']);

                        $dadosIguais = (($vencimentoConvertido == $dadosParcela['fnc_parcela_vencimento']) and (round($dados['fnc_parcela_valor'], 2) == round($dadosParcela['fnc_parcela_valor'], 2))) ? true : false;
                    }

                    if (isset($dadosParcela['fnc_parcela_nosso_numero']) and $dadosParcela['fnc_parcela_nosso_numero'] and $dadosIguais) {
                        $form->set('fnc_parcela_nosso_numero', $dadosParcela['fnc_parcela_nosso_numero'], 'Texto');
                    } else {
                        $nossoNumero = $nossoNumeroClass->cadastrar($objFormLancamento->get('fnc_convenio_cod'));
                        $form->set('fnc_parcela_nosso_numero', $nossoNumero, 'Texto');
                    }
                }

                if (array_key_exists('fnc_parcela_hash', $dadosParcela)) {
                    $form->set('fnc_parcela_hash', $dadosParcela['fnc_parcela_hash'], 'Texto');
                } else {
                    $form->set('fnc_parcela_hash', bin2hex(openssl_random_pseudo_bytes(32)), 'Texto');
                }
            }

            $this->colunasCrud[] = 'fnc_parcela_vencimento_original';

            $fncParcelaCod = $this->gravarParcela($form, $objFormLancamento);

            $parcelaCod[] = $fncParcelaCod;

            if ($dados['fnc_parcela_pago'] === 'S') {
                $this->informaBaixaManual($fncParcelaCod, 'S');
            } else {
                $this->informaBaixaManual($fncParcelaCod, null);
            }

            $form->set('fnc_parcela_cod', $fncParcelaCod, 'Inteiro');

            $acumulado += $dados['fnc_parcela_valor'];
            $acomuladoPago += $dados['fnc_parcela_valor_pago'] ? $dados['fnc_parcela_valor_pago'] : $dados['fnc_parcela_valor'];

            $acumuladoContabil += $contaClass->cadastrar($form, $configuracaoContabil, $objFormLancamento);
        }

        $contaClass->confereValoresContabeis();

        if (abs(($acumuladoContabil - $acomuladoPago) / $acomuladoPago) > 0.00001) {
            throw new ErrorException("($acumuladoContabil - $acomuladoPago) - A divisão do valor contábil não esta correta, verifique os valores informados, se o erro persistir contate o administrador do sistema!");
        }

        if (round($acumulado, 2) <> round($valorTotal, 2)) {
            throw new ValidationException("O Valor das parcelas devem bater com o valor total do documento!");
        }

        return $parcelaCod;
    }

    public function calculaValorTotal($configuracaoContabil) {
        $total = 0;
        foreach ($configuracaoContabil as $dadosContabeis) {

            $multiplicar = array_key_exists('multiplicar', $dadosContabeis) ? $dadosContabeis['multiplicar'] : 1;

            $total += $this->tratar->numero()->floatBanco($dadosContabeis['fnc_conta_valor']) * $multiplicar;
        }

        return $total;
    }

    public function removerPorLancamento($fncLancamentoCod, $mensagem) {
        $form = new Form();
        $fecharMesClass = (new FecharMesClassFactory())->instancia($this->organogramaCod);

        $crud = [
            'fnc_parcela_removida',
            'fnc_parcela_removido_motivo'
        ];

        $form->set('fnc_parcela_removida', 'S', 'Texto');
        $form->set('fnc_parcela_removido_motivo', $mensagem, 'Texto');

        $dadosParcela = $this->getDadosPorLancamentoCod($fncLancamentoCod);

        $fncParcelaPagamento = $dadosParcela['fnc_parcela_pagamento'];
        $fncParcelaCod = $dadosParcela['fnc_parcela_cod'];

        $fecharMesClass->permiteLancamento($fncParcelaPagamento);

        $this->crudUtil->update($this->organogramaCod, $this->tabela, $crud, $form, ['fnc_parcela_cod' => $fncParcelaCod]);
    }

    public function reverterPorParcela($fncParcelaCod) {
        $form = new Form();

        $crud = [
            'fnc_parcela_removida',
            'fnc_parcela_removido_motivo'
        ];

        $form->set('fnc_parcela_removida', 'N', 'Texto');
        $form->set('fnc_parcela_removido_motivo', null, 'Texto');

        $this->crudUtil->update($this->organogramaCod, $this->tabela, $crud, $form, ['fnc_parcela_cod' => $fncParcelaCod], [], []);
    }

    public function baixar($objForm) {

        $movimentacaoClass = (new MovimentacaoClassFactory())->instancia($this->organogramaCod);
        $contaClass = (new ContaClassFactory())->instancia($this->organogramaCod);
        $fecharMesClass = (new FecharMesClassFactory())->instancia($this->organogramaCod);

        $crud = [
            'fnc_parcela_valor_pago',
            'fnc_parcela_pagamento',
            'fnc_parcela_compensa',
            'fnc_parcela_pago',
            'fnc_forma_cod'
        ];

        $objForm->set('fnc_parcela_pago', 'S', 'Texto');
        $fncParcelaCod = $objForm->get('cod');
        $fncLancamentoCod = $movimentacaoClass->getDadosPorParcela($fncParcelaCod, 'fnc_lancamento_cod');
        $objForm->set('fnc_lancamento_cod', $fncLancamentoCod);

        $this->crudUtil->startTransaction();

        $fecharMesClass->permiteLancamento($objForm->get('fnc_parcela_pagamento'));

        if (!$objForm->get('fnc_parcela_compensa')) {
            $objForm->set('fnc_parcela_compensa', $objForm->get('fnc_parcela_pagamento'), 'Texto');
        }

        $contaClass->baixar($this, $objForm);

        $this->crudUtil->update($this->organogramaCod, $this->tabela, $crud, $objForm, ['fnc_parcela_cod' => $objForm->get('cod')]);

        $obs = $objForm->get('fnc_lancamento_observacao');

        if ($obs !== null) {
            $this->crudUtil->update($this->organogramaCod, 'fnc_lancamento', ['fnc_lancamento_observacao'], $objForm, ['fnc_lancamento_cod' => $fncLancamentoCod]);
        }

        $contaClass->confereValoresContabeis();

        $this->crudUtil->stopTransaction();
    }

    public function informaBaixaManual($fncParcelaCod, $baixaManual) {
        $form = new Form();

        $baixaUpper = strtoupper($baixaManual);

        if ($baixaUpper <> 'S' and $baixaUpper <> 'N') {
            $baixaUpper = null;
        }

        $crud = [
            'fnc_parcela_baixa_manual'
        ];

        $form->set('fnc_parcela_baixa_manual', $baixaUpper, 'Texto');

        $this->crudUtil->update($this->organogramaCod, $this->tabela, $crud, $form, ['fnc_parcela_cod' => $fncParcelaCod]);
    }

    public function estornar($fncParcelaCod) {

        $movimentacaoClass = (new MovimentacaoClassFactory())->instancia($this->organogramaCod);
        $contaClass = (new ContaClassFactory())->instancia($this->organogramaCod);
        $fecharMesClass = (new FecharMesClassFactory())->instancia($this->organogramaCod);

        $formParcela = new Form();
        $formConta = new Form();

        $crud = [
            'fnc_parcela_valor_pago',
            'fnc_parcela_pagamento',
            'fnc_parcela_compensa',
            'fnc_parcela_pago',
            'fnc_forma_cod'
        ];

        $formParcela->set('fnc_parcela_valor_pago', null);
        $formParcela->set('fnc_parcela_pagamento', null);
        $formParcela->set('fnc_parcela_compensa', null);
        $formParcela->set('fnc_forma_cod', null);
        $formParcela->set('fnc_parcela_pago', 'N', 'Texto');

        $dadosParcela = $movimentacaoClass->getDadosPorParcela($fncParcelaCod);
        $formConta->set('fnc_parcela_cod', $fncParcelaCod);
        $formConta->set('fnc_lancamento_cod', $dadosParcela['fnc_lancamento_cod']);
        $formConta->set('fnc_parcela_valor_pago', $this->getDados($fncParcelaCod, 'fnc_parcela_valor_pago'), 'Float');

        $fecharMesClass->permiteLancamento($dadosParcela['fnc_parcela_pagamento']);

        $this->crudUtil->startTransaction();

        $contaClass->estornar($this, $formConta);

        $this->crudUtil->update($this->organogramaCod, $this->tabela, $crud, $formParcela, ['fnc_parcela_cod' => $fncParcelaCod]);

        $contaClass->confereValoresContabeis();

        $this->crudUtil->stopTransaction();
    }

    public function atualizar($objForm, $pago) {
        $crud = [
            'fnc_parcela_vencimento',
            'fnc_parcela_juros',
            'fnc_parcela_desconto'
        ];

        $this->crudUtil->update($this->organogramaCod, $this->tabela, $crud, $objForm, ['fnc_parcela_cod' => $objForm->get('cod')]);
    }

    public function setValoresFormBaixar($fncParcelaCod, $formIntancia) {

        $movimentacaoClass = (new MovimentacaoClassFactory())->instancia($this->organogramaCod);

        $fncLancamentoCod = $this->getDados($fncParcelaCod, 'fnc_lancamento_cod');

        $operacao = $movimentacaoClass->getDados($fncLancamentoCod, 'fnc_lancamento_tipo_lancamento');

        $objForm = $formIntancia->getFormBaixar($fncParcelaCod, $operacao);

        return $objForm;
    }

    public function setValoresFormAtualizar($fncParcelaCod, $formIntancia) {
        $objForm = $formIntancia->getFormAtualizar($fncParcelaCod);

        return $objForm;
    }

    public function getDados($fncParcelaCod, $posicao = '', $removidos = false) {
        if ($posicao) {
            return $this->con->execRLinha($this->parcelaSql->getDadosSql($fncParcelaCod, $removidos), $posicao);
        } else {
            return $this->con->execLinha($this->parcelaSql->getDadosSql($fncParcelaCod, $removidos));
        }
    }

    public function getDadosPorLancamentoCod($fncLancamentoCod, $posicao = '') {
        if ($posicao) {
            return $this->con->execRLinha($this->parcelaSql->getDadosPorLancamentoCodSql($fncLancamentoCod), $posicao);
        } else {
            return $this->con->execLinha($this->parcelaSql->getDadosPorLancamentoCodSql($fncLancamentoCod));
        }
    }

    public function getDadosPorNossoNumero($fncParcelaNossoNumero, $fncConvenioCod, $posicao = '') {
        if ($posicao) {
            return $this->con->execRLinha($this->parcelaSql->getDadosPorNossoNumeroSql($fncParcelaNossoNumero, $fncConvenioCod), $posicao);
        } else {
            return $this->con->execLinha($this->parcelaSql->getDadosPorNossoNumeroSql($fncParcelaNossoNumero, $fncConvenioCod));
        }
    }

    public function getDadosPorNossoNumeroComExcluidos($fncParcelaNossoNumero, $fncConvenioCod, $posicao = '') {
        if ($posicao) {
            return $this->con->execRLinha($this->parcelaSql->getDadosPorNossoNumeroComExcluidosSql($fncParcelaNossoNumero, $fncConvenioCod), $posicao);
        } else {
            return $this->con->execLinha($this->parcelaSql->getDadosPorNossoNumeroComExcluidosSql($fncParcelaNossoNumero, $fncConvenioCod));
        }
    }

    public function montaParcela($parcelaForm, $formCod) {
        $tratar = Tratamento::instancia();
        $form = new Form();

        $form->config('formManu' . $formCod, 'POST');

        $operacao = $form->retornaValor('operacao');
        $dataBase = $form->retornaValor('dataBase');
        $valorContabil = $tratar->numero()->floatBanco($form->retornaValor('valorContabil'));
        $numeroParcelas = $form->retornaValor('numeroParcelas');

        $this->validaDados($operacao, $dataBase, $valorContabil, $numeroParcelas);

        $valoresInicio = $this->novasParcelas($valorContabil, $dataBase, $numeroParcelas);

        return $form->montaForm($parcelaForm->getFormParcela($form, $operacao, $valoresInicio));
    }

    public function getNumeroParcelas($fncLancamentoCod) {
        return $this->con->execNLinhas($this->parcelaSql->parcelasDoLancamentoSql($fncLancamentoCod));
    }

    public function getValorDocumento($fncLancamentoCod) {
        $valorDocumento = 0;
        $parcelas = $this->con->executar($this->parcelaSql->parcelasDoLancamentoSql($fncLancamentoCod));

        while ($dados = $parcelas->fetch()) {
            $valorDocumento += $dados['fnc_parcela_valor'];
        }

        return $this->tratar->numero()->floatBanco($valorDocumento);
    }

    public function montaParcelaAlt($fncLancamentoCod, $parcelaForm) {
        $form = new Form();
        $movimentacaoClass = (new MovimentacaoClassFactory())->instancia($this->organogramaCod);

        $form->config('formManu' . $fncLancamentoCod);

        $operacao = $movimentacaoClass->getDados($fncLancamentoCod, 'fnc_lancamento_tipo_lancamento');
        $parcelas = $this->con->executar($this->parcelaSql->parcelasDoLancamentoSql($fncLancamentoCod));

        $valoresInicio = [];
        $j = 0;

        while ($dados = $parcelas->fetch()) {

            $valoresInicio[$j]['sisCoringa'] = $dados['fnc_parcela_cod'];
            $valoresInicio[$j]['fnc_parcela_vencimento'] = $dados['fnc_parcela_vencimento'];
            $valoresInicio[$j]['fnc_parcela_valor'] = $dados['fnc_parcela_valor'];
            $valoresInicio[$j]['fnc_parcela_vencimento'] = $dados['fnc_parcela_vencimento'];
            $valoresInicio[$j]['fnc_parcela_nome'] = $dados['fnc_parcela_nome'];
            $valoresInicio[$j]['fnc_parcela_pagamento'] = $dados['fnc_parcela_pagamento'] ? $dados['fnc_parcela_pagamento'] : '';
            $valoresInicio[$j]['fnc_parcela_valor_pago'] = $dados['fnc_parcela_valor_pago'] > '0.00' ? $dados['fnc_parcela_valor_pago'] : '';
            $valoresInicio[$j]['fnc_forma_cod'] = $dados['fnc_forma_cod'];

            $valoresInicio[$j]['fnc_parcela_desconto_forma'] = $dados['fnc_parcela_desconto_forma'];
            $valoresInicio[$j]['fnc_parcela_desconto'] = $dados['fnc_parcela_desconto'];
            $valoresInicio[$j]['fnc_parcela_desconto_data'] = $dados['fnc_parcela_desconto_data'];

            $valoresInicio[$j]['fnc_parcela_juros_forma'] = $dados['fnc_parcela_juros_forma'];
            $valoresInicio[$j]['fnc_parcela_juros'] = $dados['fnc_parcela_juros'];
            $valoresInicio[$j]['fnc_parcela_juros_data'] = $dados['fnc_parcela_juros_data'];

            $j++;
        }

        return $form->montaForm($parcelaForm->getFormParcela($form, $operacao, $valoresInicio, ['acao' => 'alterar']));
    }

    public function novasParcelas($valor, $dataBase, $numeroParcelas) {

        $tratar = Tratamento::instancia();

        $retorno = [];
        $valorParcela = bcdiv($valor, $numeroParcelas, 2);

        $j = 0;

        $retorno[$j]['fnc_parcela_vencimento'] = $tratar->data()->converteData($dataBase);

        for ($i = 1; $i <= $numeroParcelas; $i++) {

            if ($i == $numeroParcelas) {

                $multParcela = bcmul($numeroParcelas, $valorParcela, 2);

                if ($multParcela > $valor) {

                    $retorno[$j]['fnc_parcela_valor'] = $tratar->numero()->floatCliente($valorParcela - ($multParcela - $valor));
                } elseif ($multParcela < $valor) {

                    $retorno[$j]['fnc_parcela_valor'] = $tratar->numero()->floatCliente($valorParcela + ($valor - $multParcela));
                } else {

                    $retorno[$j]['fnc_parcela_valor'] = $tratar->numero()->floatCliente($valorParcela);
                }
            } else {
                $retorno[$j]['fnc_parcela_valor'] = $tratar->numero()->floatCliente($valorParcela);
            }

            if ($i > 1) {

                $dataBase = $tratar->data()->atribuiData($dataBase, 0, 1, 0, "/", "+");

                $retorno[$j]['fnc_parcela_vencimento'] = $tratar->data()->converteData($dataBase);
            }

            $zero = ($i < 10) ? "0" : "";

            $retorno[$j]['fnc_parcela_nome'] = ($numeroParcelas == 1) ? "Parcela Única" : "Parcela " . $zero . $i;
            $retorno[$j]['fnc_parcela_pagamento'] = $retorno[$j]['fnc_parcela_vencimento'];
            $retorno[$j]['fnc_parcela_valor_pago'] = $retorno[$j]['fnc_parcela_valor'];
            $retorno[$j]['fnc_forma_cod'] = '';

            $j++;
        }

        return $retorno;
    }

    protected function validaParcelas($dadosParcela, $operacao, $multiplicar, $valorTotal) {

        $fecharMesClass = (new FecharMesClassFactory())->instancia($this->organogramaCod);
        $valida = Valida::instancia();

        $retorno = [];

        foreach ($dadosParcela as $chave => $dados) {

            $quitar = $dados['quitar'];

            if (!$dados['fnc_parcela_nome']) {
                throw new ValidationException('Informe corretamente o nome de todas as parcelas!');
            }

            $retorno[$chave]['quitar'] = $dados['quitar'];
            $retorno[$chave]['fnc_parcela_nome'] = $dados['fnc_parcela_nome'];

            //Converte valores
            $valorParcela = $valida->numero()->floatBanco($dados['fnc_parcela_valor']);

            if ($multiplicar === 'S') {

                if ($quitar === 'S') {
                    throw new ValidationException($dados['fnc_parcela_nome'] . ' - ATENÇÃO ao usar o recurso de multiplicar, você não poderá quitar as parcelas no ato do lançamento!');
                }

                $valorParcela = $valorTotal / count($dadosParcela);
            }

            if (!$valorParcela) {
                throw new ValidationException($dados['fnc_parcela_nome'] . ' - informe o valor da parcela corretamente!');
            }

            if (!$valida->data()->validaData($dados['fnc_parcela_vencimento'])) {
                throw new ValidationException($dados['fnc_parcela_nome'] . ' - informe a data de vencimento corretamente!');
            }

            $retorno[$chave]['fnc_parcela_vencimento'] = $dados['fnc_parcela_vencimento'];
            $retorno[$chave]['fnc_parcela_valor'] = $valorParcela;

            if ($quitar === 'S' or in_array($operacao, ['PA', 'RE'])) {

                //Converte valores
                $valorPago = $valida->numero()->floatBanco($dados['fnc_parcela_valor_pago']);

                if (!$valida->data()->validaData($dados['fnc_parcela_pagamento'])) {
                    throw new ValidationException($dados['fnc_parcela_nome'] . ' - informe a data do pagamento corretamente!');
                }

                if (!$valorPago) {
                    throw new ValidationException($dados['fnc_parcela_nome'] . ' - informe o valor pago corretamente!');
                }

                if (!is_numeric($dados['fnc_forma_cod'])) {
                    throw new ValidationException($dados['fnc_parcela_nome'] . ' - informe a forma de pagamento corretamente!');
                }

                $fecharMesClass->permiteLancamento($valida->data()->converteData($dados['fnc_parcela_pagamento']));

                $retorno[$chave]['fnc_parcela_pagamento'] = $dados['fnc_parcela_pagamento'];
                $retorno[$chave]['fnc_parcela_valor_pago'] = $valida->numero()->floatBanco($dados['fnc_parcela_valor_pago']);
                $retorno[$chave]['fnc_forma_cod'] = $dados['fnc_forma_cod'];
                $retorno[$chave]['fnc_parcela_pago'] = 'S';
            } else {

                $retorno[$chave]['fnc_parcela_pagamento'] = null;
                $retorno[$chave]['fnc_parcela_valor_pago'] = null;
                $retorno[$chave]['fnc_forma_cod'] = null;
                $retorno[$chave]['fnc_parcela_pago'] = 'N';
            }

            if (isset($dados['desconto']) and $dados['desconto'] == 'S') {
                $retorno[$chave]['fnc_parcela_desconto_forma'] = $dados['fnc_parcela_desconto_forma'];
                $retorno[$chave]['fnc_parcela_desconto'] = $dados['fnc_parcela_desconto'];
                $retorno[$chave]['fnc_parcela_desconto_data'] = $dados['fnc_parcela_desconto_data'];
            } else {
                $retorno[$chave]['fnc_parcela_desconto_forma'] = null;
                $retorno[$chave]['fnc_parcela_desconto'] = null;
                $retorno[$chave]['fnc_parcela_desconto_data'] = null;
            }

            if (isset($dados['juros']) and $dados['juros'] == 'S') {
                $retorno[$chave]['fnc_parcela_juros_forma'] = $dados['fnc_parcela_juros_forma'];
                $retorno[$chave]['fnc_parcela_juros'] = $dados['fnc_parcela_juros'];
                $retorno[$chave]['fnc_parcela_juros_data'] = $dados['fnc_parcela_juros_data'];
            } else {
                $retorno[$chave]['fnc_parcela_juros_forma'] = null;
                $retorno[$chave]['fnc_parcela_juros'] = null;
                $retorno[$chave]['fnc_parcela_juros_data'] = null;
            }
        }

        return $retorno;
    }

    protected function validaDados($operacao, $dataBase, $valorContabil, $numeroParcelas) {
        $valida = Valida::instancia();

        if (!$valida->data()->validaData($dataBase)) {
            throw new ValidationException('A data base informada é inválida!');
        }

        if (!in_array($operacao, ['AP', 'AR', 'PA', 'RE'])) {
            throw new ValidationException('Operação de pagamento é inválida!');
        }

        if (!is_numeric($valorContabil) or $valorContabil <= 0) {
            throw new ValidationException('Informe o valor corretamente, valor deve ser númerico maior que zero!');
        }

        if (!is_numeric($numeroParcelas) or $numeroParcelas <= 0 or $numeroParcelas > 120) {
            throw new ValidationException('Informe o número de parcelas corretamente, valor deve ser um inteiro maior que zero e menor que 120!');
        }
    }

    public function parcelasAbertas($fncParcelaCod, $chavePesquisa, $codigo, $limite = 0) {
        $tratar = Tratamento::instancia();

        $rsAberto = $this->con->executar($this->parcelaSql->parcelasAbertasSql($chavePesquisa, $codigo));

        $retorno = [];
        $conta = 0;
        while ($dados = $rsAberto->fetch()) {

            if ($dados['fnc_parcela_cod'] === $fncParcelaCod) {
                continue;
            }

            if ($conta >= $limite) {
                break;
            }

            $hoje = date('Y-m-d');
            if ($dados['fnc_parcela_vencimento'] <= $hoje) {
                continue;
            }

            $conta++;

            $dados['fnc_parcela_valor'] = $tratar->numero()->floatCliente($dados['fnc_parcela_valor']);

            $retorno[] = $dados;
        }

        return $retorno;
    }

    public function getNomeParcela($fncParcelaCod) {

        $movimentacaoClass = (new MovimentacaoClassFactory())->instancia($this->organogramaCod);

        $fncLancamentoCod = $this->getDados($fncParcelaCod, 'fnc_lancamento_cod');
        $parcelaNome = $this->getDados($fncParcelaCod, 'fnc_parcela_nome');

        $dadosMov = $movimentacaoClass->getDados($fncLancamentoCod);

        if (!$dadosMov) {
            throw new \Exception('Parcela não encontrada coma referencia:' . $fncParcelaCod);
        }

        $origem = '';

        if ($dadosMov['empresa_cod']) {

            $empresaClass = (new EmpresaClassFactory())->instancia($this->organogramaCod);

            $origem = '<br>' . $empresaClass->getDados('empresa_nome');
        }

        if ($dadosMov['cliente_cod']) {

            $cliente = (new ClienteClassFactory())->instancia($this->organogramaCod);

            $origem = '<br>Cliente: ' . $cliente->getDados($dadosMov['cliente_cod'], 'cliente_nome');
        }

        if ($dadosMov['funcionario_cod']) {

            $funcionario = (new FuncionarioClassFactory())->instancia($this->organogramaCod);

            $origem = '<br>Funcionário: ' . $funcionario->getDados($dadosMov['funcionario_cod'], 'funcionario_nome');
        }

        if ($dadosMov['fornecedor_cod']) {

            $fornecedor = (new FornecedorClassFactory())->instancia($this->organogramaCod);

            $origem = '<br>Fornecedor: ' . $fornecedor->getDados($dadosMov['fornecedor_cod'], 'fornecedor_nome');
        }

        return $dadosMov['fnc_lancamento_nome'] . ' - ' . $parcelaNome . $origem;
    }

}
