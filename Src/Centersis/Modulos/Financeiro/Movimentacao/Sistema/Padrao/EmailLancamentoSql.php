<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

class EmailLancamentoSql {

    /**
     * @var \Zion2\Banco\Conexao
     */
    protected $con;
    protected $organogramaCod;

    public function __construct($organogramaCod, $con) {
        $this->con = $con;
        $this->organogramaCod = $organogramaCod;
    }   
}
