<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Ext\Core\Padrao\BaseForm;

class RetornoForm extends BaseForm {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu() {
        $form = new Form();

        $form->setAcao('cadastrar');

        $form->config('formRetorno', 'POST')
                ->setAcaoSubmit('submeterArquivoRetorno()')
                ->setHeader('Retorno');

        $campos[] = $form->escolha('fnc_convenio_cod', 'Convênio Bancário', true)
                ->setTabela('fnc_convenio')
                ->setCampoCod('fnc_convenio_cod')
                ->setCampoDesc('fnc_convenio_identifica')
                ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                ->setValor($form->retornaValor('fnc_convenio_cod'));

        $campos[] = $form->upload('arquivo_retorno', 'Arquivo de Retorno', 'ARQUIVO')
                ->setMaximoArquivos(1)
                ->setOrganogramaCod($this->organogramaCod)
                ->setValor('arquivo_retorno');

        $campos[] = $form->botaoSalvarPadrao()
                ->setValor('Ler arquivo');

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

}
