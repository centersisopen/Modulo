<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Zion2\Validacao\Valida;
use Centersis\Ext\Form\Form;
use Centersis\Ext\Crud\CrudUtil;
use Centersis\Financeiro\Parametrizacao\Sistema\ParametrizacaoClassFactory;
use Centersis\Financeiro\FormaPagamento\Sistema\FormaPagamentoClassFactory;
use Centersis\Financeiro\PlanoContas\Sistema\PlanoContasClassFactory;
use Centersis\Zion2\Exception\ValidationException;
use Centersis\Sistema\Configuracoes\Sistema\ConfiguracoesClassFactory;

class ContaClass {

    protected $chavePrimaria;
    protected $crudUtil;
    protected $tabela;
    protected $colunasCrud;
    protected $organogramaCod;

    /**
     * @var ReferenciaSql 
     */
    protected $contaSql;

    /**
     * @var \Zion2\Banco\Conexao 
     */
    protected $con;

    public function __construct($organogramaCod, $con, $sql) {

        $this->crudUtil = new CrudUtil($con);

        $this->tabela = 'fnc_conta';
        $this->chavePrimaria = 'fnc_conta_cod';

        $this->colunasCrud = [
            'fnc_lancamento_cod',
            'fnc_parcela_cod',
            'fnc_conta_contabil_cod',
            'fnc_conta_valor',
            'fnc_conta_valor_real',
            'fnc_conta_tipo',
            'fnc_conta_selecao',
            'fnc_conta_multiplicador'
        ];

        $this->contaSql = $sql;
        $this->organogramaCod = $organogramaCod;

        $this->con = $con;
    }

    public function cadastrar($objFormParcela, $configuracaoContabil, $objFormLancamento) {

        $forma = (new FormaPagamentoClassFactory())->instancia($this->organogramaCod);
        $parametrizacao = (new ParametrizacaoClassFactory())->instancia($this->organogramaCod);
        $form = new Form();

        $pago = $objFormParcela->get('fnc_parcela_pago');
        $operacao = $objFormLancamento->get('fnc_lancamento_tipo_lancamento');
        $tipoLancamento = in_array($operacao, ['AP', 'PA']) ? 'D' : 'C';

        $valorTotal = $objFormLancamento->getSql('valorTotal');
        $valorParcela = $pago === 'S' ? $objFormParcela->getSql('fnc_parcela_valor_pago') : $objFormParcela->getSql('fnc_parcela_valor');
        $valorParcelaReal = $objFormParcela->getSql('fnc_parcela_valor');

        $form->set('fnc_lancamento_cod', $objFormLancamento->get('fnc_lancamento_cod'));
        $form->set('fnc_parcela_cod', $objFormParcela->get('fnc_parcela_cod'));

        $valorAcumulado = 0;
        $valorNaoQuitado = 0;

        foreach ($configuracaoContabil as $dados) {

            $multiplicar = array_key_exists('multiplicar', $dados) ? $dados['multiplicar'] : 1;

            $valorContabil = (Valida::instancia()->numero()->floatBanco($dados['fnc_conta_valor']) * $multiplicar);
            $porcentagemDoTotal = (($valorContabil / $valorTotal) * 100);
            $valorFinal = (($valorParcela / 100) * $porcentagemDoTotal);
            $valorFinalReal = (($valorParcelaReal / 100) * $porcentagemDoTotal);
            $valorAcumulado += $valorFinal;

            $form->set('fnc_conta_valor', $valorFinal, 'Float');
            $form->set('fnc_conta_valor_real', $valorFinalReal, 'Float');
            $form->set('fnc_conta_contabil_cod', $dados['fnc_conta_contabil_cod'], 'Inteiro');
            $form->set('fnc_conta_tipo', $tipoLancamento, 'Texto');
            $form->set('fnc_conta_selecao', 'Automatica', 'Texto');
            $form->set('fnc_conta_multiplicador', $multiplicar, 'Inteiro');

            if ($pago !== 'S') {
                $valorNaoQuitado += $valorFinal;
            }

            $this->persisteLancamento($form->get('fnc_conta_contabil_cod'), $form->getSql('fnc_conta_valor'), 'C');

            $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $form);
        }

        $form->set('valor_nao_quitado', $valorNaoQuitado, "Float");
        $form->set('fnc_conta_valor', $valorParcela, "Float");
        $form->set('fnc_conta_valor_real', $valorParcelaReal, "Float");
        $form->set('fnc_conta_selecao', 'Manual');

        if ($pago === 'S') {

            $formCod = $objFormParcela->get('fnc_forma_cod');
            $contaContabilForma = $forma->getDados($formCod, 'fnc_conta_contabil_cod');
            $form->set('fnc_conta_contabil_cod', $contaContabilForma, 'Inteiro');

            $this->persisteLancamento($form->get('fnc_conta_contabil_cod'), $valorParcela, $tipoLancamento);
        } else {
            $enderecoConta = $tipoLancamento === 'C' ? 'fnc_conta_contabil_receber' : 'fnc_conta_contabil_pagar';
            $form->set('fnc_conta_contabil_cod', $parametrizacao->getParametros($objFormLancamento->get('fnc_convenio_cod'), $enderecoConta), 'Inteiro');

            $this->persisteLancamento($form->get('fnc_conta_contabil_cod'), $valorParcela, 'C'); //$tipoLancamento
        }

        $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $form);

        return $valorAcumulado;
    }

    public function removerPorLancamento($movimentacaoClass, $fncLancamentoCod) {

        $form = new Form();

        $rsContasDoLancamento = $this->con->executar($this->contaSql->contasDoLancamentoSql($fncLancamentoCod));
        $form->set('fnc_conta_removida', 'S', 'Texto');
        $fncLancamentoTipoLancamento = $movimentacaoClass->getDados($fncLancamentoCod, 'fnc_lancamento_tipo_lancamento');

        while ($dados = $rsContasDoLancamento->fetch()) {

            if (in_array($fncLancamentoTipoLancamento, ['AP', 'PA'])) {

                $parcelaPaga = $this->con->execRLinha($this->contaSql->dadosParcelaSql($dados['fnc_parcela_cod']), 'fnc_parcela_pago');

                if ($parcelaPaga == 'N') {
                    $tipoInvertido = 'D';
                } else {
                    $tipoInvertido = ($dados['fnc_conta_selecao'] === 'Manual') ? 'C' : 'D';
                }

                $this->persisteLancamento($dados['fnc_conta_contabil_cod'], $dados['fnc_conta_valor'], $tipoInvertido, false);
                $this->crudUtil->update($this->organogramaCod, $this->tabela, ['fnc_conta_removida'], $form, [$this->chavePrimaria => $dados['fnc_conta_cod']]);
            } else {

                $this->persisteLancamento($dados['fnc_conta_contabil_cod'], $dados['fnc_conta_valor'], 'D', false);
                $this->crudUtil->update($this->organogramaCod, $this->tabela, ['fnc_conta_removida'], $form, [$this->chavePrimaria => $dados['fnc_conta_cod']]);
            }
        }
    }

    public function reverterPorLancamento($fncLancamentoCod, $fncParcelaCod) {
        $form = new Form();

        $rsContasDoLancamento = $this->con->executar($this->contaSql->contasDoLancamento2Sql($fncLancamentoCod, $fncParcelaCod));
        $form->set('fnc_conta_removida', 'N', 'Texto');

        while ($dados = $rsContasDoLancamento->fetch()) {

            $this->persisteLancamento($dados['fnc_conta_contabil_cod'], $dados['fnc_conta_valor'], 'C', false);
            $this->crudUtil->update($this->organogramaCod, $this->tabela, ['fnc_conta_removida'], $form, [$this->chavePrimaria => $dados['fnc_conta_cod']]);
        }
    }

    public function persisteLancamento($fncContaContabilCod, $fncContaValor, $fncContaTipo, $verificarPermissao = true) {

        if (empty($fncContaValor)) {
            return;
        }

        $planoContas = (new PlanoContasClassFactory())->instancia($this->organogramaCod);
        $form = new Form();

        $permissoesConta = $planoContas->getDados($fncContaContabilCod, 'fnc_conta_contabil_tipo');
        $contaContabilNome = $planoContas->getDados($fncContaContabilCod, 'fnc_conta_contabil_nome');
        $operacao = $fncContaTipo === 'C' ? '+' : '-';

        if ($verificarPermissao) {
            if ($operacao === '+') { //Verificação de permissão de lançamento
                if (!in_array($permissoesConta, ['C', 'CD'])) {
                    throw new ValidationException('A conta ' . $contaContabilNome . ' - ' . $fncContaContabilCod . ' esta configurada para não receber lançamento de crédito!');
                }
            } else {
                if (!in_array($permissoesConta, ['D', 'CD'])) {
                    throw new ValidationException('A conta ' . $contaContabilNome . ' esta configurada para não receber lançamento de débito!');
                }
            }
        }

        $form->set('fnc_conta_contabil_saldo', $fncContaValor, "Float");
        $valor = $form->getSql('fnc_conta_contabil_saldo');

        $qb = $this->con->qb();

        $qb->update('fnc_conta_contabil', 'f')
                ->set('fnc_conta_contabil_saldo', '(fnc_conta_contabil_saldo ' . $operacao . ' ' . $valor . ')')
                ->where('fnc_conta_contabil_cod = ' . $fncContaContabilCod)
                ->andWhere('organograma_cod = ' . $this->organogramaCod);

        $qb->execute();
    }

    public function baixar($parcelaClass, $objForm) {

        $forma = (new FormaPagamentoClassFactory())->instancia($this->organogramaCod);

        $fncParcelaCod = $objForm->get('cod');
        $objForm->set('fnc_parcela_cod', $fncParcelaCod);

        $fncFormaCod = $objForm->get('fnc_forma_cod');
        $fncParcelaValorPago = $objForm->get('fnc_parcela_valor_pago');

        $valorTotal = $objForm->getSql('fnc_parcela_valor_pago');
        $valorParcela = $parcelaClass->getDados($fncParcelaCod, 'fnc_parcela_valor');

        $fncContaContabilCod = $forma->getDados($fncFormaCod, 'fnc_conta_contabil_cod');

        $rsContasDaParcela = $this->con->executar($this->contaSql->contasDaParcelaSql($fncParcelaCod));

        while ($dados = $rsContasDaParcela->fetch()) {

            $objForm->set('fnc_conta_valor', $fncParcelaValorPago, 'Float');
            $objForm->set('fnc_conta_valor_real', $valorParcela, 'Float');
            $objForm->set('fnc_conta_contabil_cod', $fncContaContabilCod);

            if ($dados['fnc_conta_selecao'] === 'Manual') {

                $objForm->set('fnc_conta_selecao', 'Manual', 'String');

                $this->persisteLancamento($dados['fnc_conta_contabil_cod'], $dados['fnc_conta_valor'], 'D', false);
                $this->crudUtil->update($this->organogramaCod, $this->tabela, ['fnc_conta_removida'], ['fnc_conta_removida' => ['Texto' => 'S']], [$this->chavePrimaria => $dados['fnc_conta_cod']]);

                //Refaz o lançamento
                $objForm->set('fnc_conta_tipo', $dados['fnc_conta_tipo']);
                $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm);
                $this->persisteLancamento($fncContaContabilCod, $fncParcelaValorPago, $dados['fnc_conta_tipo'], false);
            } else {

                $objForm->set('fnc_conta_selecao', 'Automatica', 'String');
                $valorContabil = $dados['fnc_conta_valor'];
                $valorContabilReal = $dados['fnc_conta_valor_real'];
                $tipoInvertido = $dados['fnc_conta_tipo'] === 'C' ? 'D' : 'C';
                $this->persisteLancamento($dados['fnc_conta_contabil_cod'], $dados['fnc_conta_valor'], $tipoInvertido, false);
                $this->crudUtil->update($this->organogramaCod, $this->tabela, ['fnc_conta_removida'], ['fnc_conta_removida' => ['Texto' => 'S']], [$this->chavePrimaria => $dados['fnc_conta_cod']]);

                $porcentagemDoTotal = (($valorContabil / $valorParcela) * 100);
                $porcentagemDoTotalReal = (($valorContabilReal / $valorParcela) * 100);

                $valorFinal = (($valorTotal / 100) * $porcentagemDoTotal);
                $valorFinalReal = (($valorParcela / 100) * $porcentagemDoTotalReal);

                $objForm->set('fnc_lancamento_cod', $dados['fnc_lancamento_cod'], 'Inteiro');
                $objForm->set('fnc_conta_contabil_cod', $dados['fnc_conta_contabil_cod'], 'Inteiro');
                $objForm->set('fnc_conta_tipo', $dados['fnc_conta_tipo']);
                $objForm->set('fnc_conta_valor', $valorFinal, 'Float');
                $objForm->set('fnc_conta_valor_real', $valorFinalReal, 'Float');
                $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm);
                $this->persisteLancamento($dados['fnc_conta_contabil_cod'], $valorFinal, $dados['fnc_conta_tipo'], false);
            }
        }
    }

    public function estornar($parcelaClass, $formConta) {

        $parametrizacaoClass = (new ParametrizacaoClassFactory())->instancia($this->organogramaCod);
        
        $fncParcelaCod = $formConta->get('fnc_parcela_cod');

        $fncParcelaValorPago = $parcelaClass->getDados($fncParcelaCod, 'fnc_parcela_valor_pago');
        $valorParcela = $parcelaClass->getDados($fncParcelaCod, 'fnc_parcela_valor');
        

        $rsContasDaParcela = $this->con->executar($this->contaSql->contasDaParcelaSql($fncParcelaCod));

        while ($dados = $rsContasDaParcela->fetch()) {

            $formConta->set('fnc_conta_valor', $valorParcela, 'Float');
            $formConta->set('fnc_conta_valor_real', $valorParcela, 'Float');

            if ($dados['fnc_conta_selecao'] === 'Manual') {

                $formConta->set('fnc_conta_selecao', 'Manual', 'String');
                $tipoInvertido = $dados['fnc_conta_tipo'] === 'C' ? 'D' : 'C';
                $this->persisteLancamento($dados['fnc_conta_contabil_cod'], $dados['fnc_conta_valor'], $tipoInvertido, false);
                $this->crudUtil->update($this->organogramaCod, $this->tabela, ['fnc_conta_removida'], ['fnc_conta_removida' => ['Texto' => 'S']], [$this->chavePrimaria => $dados['fnc_conta_cod']]);

                $enderecoConta = $dados['fnc_conta_tipo'] === 'C' ? 'fnc_conta_contabil_receber' : 'fnc_conta_contabil_pagar';
                $formConta->set('fnc_conta_contabil_cod', $parametrizacaoClass->getParametros($formConta->get('fnc_convenio_cod'), $enderecoConta), 'Inteiro');

                $formConta->set('fnc_conta_tipo', $dados['fnc_conta_tipo']);
                $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $formConta);
                $this->persisteLancamento($formConta->get('fnc_conta_contabil_cod'), $valorParcela, 'C'); //$tipoLancamento
            } else {

                $formConta->set('fnc_conta_selecao', 'Automatica', 'String');
                $valorContabil = $dados['fnc_conta_valor'];
                $valorContabilReal = $dados['fnc_conta_valor_real'];
                $tipoInvertido = $dados['fnc_conta_tipo'] === 'C' ? 'D' : 'C';
                $this->persisteLancamento($dados['fnc_conta_contabil_cod'], $dados['fnc_conta_valor'], $tipoInvertido, false);
                $this->crudUtil->update($this->organogramaCod, $this->tabela, ['fnc_conta_removida'], ['fnc_conta_removida' => ['Texto' => 'S']], [$this->chavePrimaria => $dados['fnc_conta_cod']]);

                $valorFinal = 0;
                $valorFinalReal = 0;
                if ($fncParcelaValorPago > 0) {
                    $porcentagemDoTotal = (($valorContabil / $fncParcelaValorPago) * 100);
                    $porcentagemDoTotalReal = (($valorContabilReal / $valorParcela) * 100);

                    $valorFinal = (($valorParcela / 100) * $porcentagemDoTotal);
                    $valorFinalReal = (($valorParcela / 100) * $porcentagemDoTotalReal);
                }

                $formConta->set('fnc_lancamento_cod', $dados['fnc_lancamento_cod'], 'Inteiro');
                $formConta->set('fnc_conta_contabil_cod', $dados['fnc_conta_contabil_cod'], 'Inteiro');
                $formConta->set('fnc_conta_tipo', $dados['fnc_conta_tipo']);
                $formConta->set('fnc_conta_valor', $valorFinal, 'Float');
                $formConta->set('fnc_conta_valor_real', $valorFinalReal, 'Float');
                $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $formConta);
                $this->persisteLancamento($dados['fnc_conta_contabil_cod'], $valorFinal, $dados['fnc_conta_tipo'], false);
            }
        }
    }

    public function confereValoresContabeis() {
        $config = (new ConfiguracoesClassFactory())->instancia($this->organogramaCod);
        $saldoNegativo = $config->getConfiguracao('saldo_negativo');

        
        $permiteSaldoNegativo = 'N';

        if (isset($saldoNegativo['configuracao_valor']) and $saldoNegativo['configuracao_valor']) {
            $permiteSaldoNegativo = $saldoNegativo['configuracao_valor'];
        }
        
        if ($permiteSaldoNegativo <> 'S') {
            $contas = $this->con->paraArray($this->contaSql->contasNegativasSql());

            foreach ($contas as $dadosConta) {

                throw new ValidationException('Operação inválida! Conta contábil ' . $dadosConta['fnc_conta_contabil_nome'] . ' -
              não tem saldo suficiente para realizar esta operação(' . $dadosConta['fnc_conta_contabil_saldo'] . ').');
            }
        }
    }

    public function destinoContabil($fncParcelaCod, $ignorarSelecao = '') {
        $planoContas = (new PlanoContasClassFactory())->instancia($this->organogramaCod);

        $retorno = [];

        $qb = $this->con->qb();

        $qb->select('a.fnc_conta_valor, a.fnc_conta_contabil_cod, '
                        . 'b.fnc_conta_contabil_nome, a.fnc_conta_selecao, '
                        . 'a.fnc_conta_multiplicador, b.fnc_conta_contabil_codigo')
                ->from('fnc_conta', 'a')
                ->innerJoin('a', 'fnc_conta_contabil', 'b', 'a.fnc_conta_contabil_cod = b.fnc_conta_contabil_cod')
                ->innerJoin('a', 'fnc_parcela', 'c', 'a.fnc_parcela_cod = c.fnc_parcela_cod')
                ->innerJoin('c', 'fnc_lancamento', 'd', 'a.fnc_lancamento_cod = d.fnc_lancamento_cod')
                ->where($qb->expr()->eq('a.fnc_parcela_cod', ':fnc_parcela_cod'))
                ->andWhere($qb->expr()->eq('d.fnc_lancamento_removido', ':fnc_lancamento_removido'))
                ->setParameter('fnc_parcela_cod', $fncParcelaCod, \PDO::PARAM_INT)
                ->setParameter('fnc_lancamento_removido', 'N', \PDO::PARAM_STR)
                ->andWhere($qb->expr()->eq('c.fnc_parcela_removida', ':fnc_parcela_removida'))
                ->setParameter('fnc_parcela_removida', 'N', \PDO::PARAM_STR)
                ->andWhere($qb->expr()->eq('a.fnc_conta_removida', ':fnc_conta_removida'))
                ->setParameter('fnc_conta_removida', 'N', \PDO::PARAM_STR);

        $rs = $this->con->executar($qb);
        $nr = $this->con->nLinhas($rs);

        if ($nr < 1) {
            return $retorno;
        }

        while ($dados = $rs->fetch()) {

            if ($dados['fnc_conta_selecao'] === $ignorarSelecao) {
                continue;
            }

            $contas = $planoContas->arvoreDeReferencia($dados['fnc_conta_contabil_cod'], false);

            $contaPai = end($contas);

            $retorno[] = [
                'conta_pai' => $contaPai,
                'fnc_conta_contabil_cod' => $dados['fnc_conta_contabil_cod'],
                'fnc_conta_contabil_nome' => $dados['fnc_conta_contabil_nome'],
                'fnc_conta_valor' => $dados['fnc_conta_valor'],
                'fnc_conta_multiplicador' => $dados['fnc_conta_multiplicador'],
                'fnc_conta_contabil_codigo' => $dados['fnc_conta_contabil_codigo']
            ];
        }

        return $retorno;
    }

    public function destinoContabilHtml($fncParcelaCod, $ignorarSelecao = '') {
        $valida = Valida::instancia();

        $arrayDestino = $this->destinoContabil($fncParcelaCod, $ignorarSelecao);

        if (empty($arrayDestino)) {
            return ' - ';
        }

        $html = [];

        foreach ($arrayDestino as $dados) {
            $parteHtml = '<div style="font-size:10px; padding-top:2px; padding-bottom:2px;">' .
                    $valida->numero()->floatCliente($dados['fnc_conta_valor']) . ' - ' . $dados['conta_pai'] .
                    ' <i class="fa fa-long-arrow-right"></i> ' .
                    $dados['fnc_conta_contabil_nome'];
            if ($dados['fnc_conta_contabil_codigo']) {
                $parteHtml .= ' - ' . $dados['fnc_conta_contabil_codigo'] . ' - ';
            }

            $parteHtml .= '</div>';

            $html[] = $parteHtml;
        }

        return implode('', $html);
    }
    
    public function destinoContabilTexto($fncParcelaCod, $ignorarSelecao = '') {
        $valida = Valida::instancia();

        $arrayDestino = $this->destinoContabil($fncParcelaCod, $ignorarSelecao);

        if (empty($arrayDestino)) {
            return ' - ';
        }

        $html = [];

        foreach ($arrayDestino as $dados) {
            $parteHtml = ' ' .
                    $valida->numero()->floatCliente($dados['fnc_conta_valor']) . ' - ' . $dados['conta_pai'] .
                    ' ' .
                    $dados['fnc_conta_contabil_nome'];
            if ($dados['fnc_conta_contabil_codigo']) {
                $parteHtml .= ' - ' . $dados['fnc_conta_contabil_codigo'] . ' - ';
            }

            $parteHtml .= ' ';

            $html[] = $parteHtml;
        }

        return implode('', $html);
    }

}
