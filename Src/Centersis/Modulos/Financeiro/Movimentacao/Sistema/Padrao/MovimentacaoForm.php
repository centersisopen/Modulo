<?php

namespace Centersis\Modulos\Financeiro\Movimentacao\Sistema\Padrao;

use Centersis\Servicos\Organograma\OrganogramaSql;
use Centersis\Ext\Form\Form;
use Centersis\Zion2\Pixel\Form\FormFiltro;
use Centersis\Zion2\Validacao\Valida;
use Centersis\Financeiro\PlanoContas\Sistema\PlanoContasFormFactory;
use Centersis\Financeiro\Movimentacao\Sistema\MovimentacaoClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\ParcelaClassFactory;
use Centersis\Ext\Core\Padrao\BaseForm;

class MovimentacaoForm extends BaseForm {

    private $namespace;
    private $class;

    public function __construct($organogramaCod, $con) {

        parent::__construct($organogramaCod, $con);
    }

    public function setNamespace($namespace) {
        $this->namespace = $namespace;
    }

    public function setClass($class) {
        $this->class = $class;
    }

    protected function selecao($acao) {
        return [
            'empresa_cod' => 'Empresa',
            'cliente_cod' => 'Clientes',
            'funcionario_cod' => 'Funcionários',
            'fornecedor_cod' => 'Fornecedores',
        ];
    }

    protected function getOrigemValor($parametrosSql) {
        if ($parametrosSql['empresa_cod']) {
            return[
                'origem' => 'empresa_cod',
                'valor' => $parametrosSql['empresa_cod']
            ];
        }

        if ($parametrosSql['cliente_cod']) {
            return[
                'origem' => 'cliente_cod',
                'valor' => $parametrosSql['cliente_cod']
            ];
        }

        if ($parametrosSql['funcionario_cod']) {

            return[
                'origem' => 'funcionario_cod',
                'valor' => $parametrosSql['funcionario_cod']
            ];
        }

        if ($parametrosSql['fornecedor_cod']) {
            return[
                'origem' => 'fornecedor_cod',
                'valor' => $parametrosSql['fornecedor_cod']
            ];
        }
    }

    ###FILTROS

    protected function filtroLancamentoNome($form) {
        return $form->texto('fnc_lancamento_nome', 'Documento', 'a');
    }

    protected function filtroNossoNumero($form) {
        return $form->texto('fnc_parcela_nosso_numero', 'Nosso Número', 'b');
    }

    protected function filtroTipoLancamento($form) {
        return $form->escolha('fnc_parcela_tipo_lancamento', 'Receita/Despesa', 'b')
                        ->setArray([
                            'P' => 'Despesas',
                            'R' => 'Receitas']);
    }

    protected function filtroParcelaPago($form) {
        return $form->escolha('fnc_parcela_pago', 'Quitado', 'b')
                        ->setArray([
                            'S' => 'Quitados',
                            'N' => 'Não Quitados']);
    }

    protected function filtroSituacaoVencimento($form) {
        return $form->escolha('vencimento', 'Situação Vencimento', '')
                        ->setArray([
                            'S' => 'Vencidos',
                            'H' => 'Vencimento Hoje',
                            'N' => 'Não Vencidos'])
                        ->setValor($form->retornaValor('vencimento'));
    }

    protected function filtroLancamentoData($form) {
        return $form->data('fnc_lancamento_data', 'Data Lançamento', 'a');
    }

    protected function filtroParcelaVencimento($form) {
        return $form->data('fnc_parcela_vencimento', 'Data Vencimento', 'b');
    }

    protected function filtroParcelaPagamento($form) {
        return $form->data('fnc_parcela_pagamento', 'Data Pagamento', 'b');
    }

    protected function filtroForma($form) {
        return $form->escolha('fnc_forma_cod', 'Forma Pagar/Receber', 'b')
                        ->setSqlCompleto("SELECT fnc_forma_cod, 
                    CONCAT(fnc_forma_nome,' (', IF(fnc_forma_tipo = 'P','Pagar','Receber'),')') AS fnc_forma_nome 
                    FROM fnc_forma
                    WHERE organograma_cod = " . $this->organogramaCod)
                        ->setCampoCod('fnc_forma_cod')
                        ->setCampoDesc("fnc_forma_nome");
    }

    protected function filtroParcelaValor($form) {
        return $form->float('fnc_parcela_valor', 'Valor', 'b');
    }

    protected function filtroParcelaValorPago($form) {
        return $form->float('fnc_parcela_valor_pago', 'Valor Pago', 'b');
    }

    protected function filtroConvenio($form, $alias = 'a') {
        return $form->escolha('fnc_convenio_cod', 'Convênio', $alias)
                        ->setTabela('fnc_convenio')
                        ->setCampoCod('fnc_convenio_cod')
                        ->setCampoDesc("fnc_convenio_identifica")
                        ->setArray(['SisNULL' => '[Sem Convênio]'])
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod]);
    }

    protected function filtroReferencia($form, $alias = 'a') {
        return $form->escolha('fnc_referencia_cod', 'Referência', $alias)
                        ->setTabela('fnc_referencia')
                        ->setCampoCod('fnc_referencia_cod')
                        ->setCampoDesc("fnc_referencia_nome")
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod]);
    }

    protected function filtroEmpresa($form) {
        return $form->chosen('empresa_cod', 'Empresa', 'c')
                        ->setTabela('empresa')
                        ->setCampoCod('empresa_cod')
                        ->setCampoDesc("empresa_nome")
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod]);
    }

    protected function filtroCliente($form) {
        return $form->chosen('cliente_cod', 'Cliente', 'd')
                        ->setTabela('cliente')
                        ->setCampoCod('cliente_cod')
                        ->setCampoDesc("cliente_nome")
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod]);
    }

    protected function filtroFornecedor($form) {
        return $form->chosen('fornecedor_cod', 'Fornecedor', 'f')
                        ->setTabela('fornecedor')
                        ->setCampoCod('fornecedor_cod')
                        ->setCampoDesc("fornecedor_nome")
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod]);
    }

    ###FILTROS

    public function getFormFiltro() {
        $form = new FormFiltro();

        $form->config('sisFormFiltro');

        $campos[] = $this->filtroLancamentoNome($form);
        $campos[] = $this->filtroNossoNumero($form);
        $campos[] = $this->filtroTipoLancamento($form);
        $campos[] = $this->filtroParcelaPagamento($form);
        $campos[] = $this->filtroLancamentoData($form);
        $campos[] = $this->filtroParcelaVencimento($form);
        $campos[] = $this->filtroParcelaPago($form);
        $campos[] = $this->filtroForma($form);
        $campos[] = $this->filtroParcelaValor($form);
        $campos[] = $this->filtroParcelaValorPago($form);
        $campos[] = $this->filtroConvenio($form);
        $campos[] = $this->filtroReferencia($form);
        $campos[] = $this->filtroEmpresa($form);
        $campos[] = $this->filtroCliente($form);
        $campos[] = $this->filtroForma($form);

        return $form->processarForm($campos);
    }

    ##CAMPOS DE MANUTENÇÂO

    protected function manuValorContabilHidden($form) {
        return $form->hidden('valorContabilHidden')
                        ->setObrigatorio(true)
                        ->setValor($form->retornaValor('valorContabilHidden'));
    }

    protected function manuTipoLancamento($form, $fncLancamentoCod) {

        $acao = $form->getAcao();

        return $form->escolha('fnc_lancamento_tipo_lancamento', 'Operação', true)
                        ->setArray([
                            'AP' => 'A Pagar',
                            'AR' => 'A Receber',
                            'PA' => 'Pago',
                            'RE' => 'Recebido'])
                        ->setComplemento('onchange="atualizaOperacao(\'' . $fncLancamentoCod . '\')"')
                        ->setOffsetColuna(4)
                        ->setEmColunaDeTamanho(6)
                        ->setClassCss($acao == 'alterar' ? 'readOnly' : 'no')
                        ->setValor($form->retornaValor('fnc_lancamento_tipo_lancamento'));
    }

    protected function manuParcelaCompetencia($form) {
        return $form->data('fnc_parcela_competencia', 'Competência', true)
                        ->setEmColunaDeTamanho(6)
                        ->setOffsetColuna(2)
                        ->setValorPadrao(date('d/m/Y'))
                        ->setValor($form->retornaValor('fnc_parcela_competencia'));
    }

    protected function manuConvenioCod($form, $obrigatorio = true) {

        $acao = $form->getAcao();

        return $form->escolha('fnc_convenio_cod', 'Convênio Bancário', $obrigatorio)
                        ->setOffsetColuna(2)
                        ->setInicio('Não especificar')
                        ->setTabela('fnc_convenio')
                        ->setCampoCod('fnc_convenio_cod')
                        ->setCampoDesc('fnc_convenio_identifica')
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                        ->setComplementoExterno('style="display:none"')
                        ->setClassCss($acao == 'alterar' ? 'readOnly' : 'no')
                        ->setValor($form->retornaValor('fnc_convenio_cod'));
    }

    protected function manuLancamentoNome($form) {
        return $form->texto('fnc_lancamento_nome', 'Documento', true)
                        ->setMaximoCaracteres(250)
                        ->setOffsetColuna(2)
                        ->setValor($form->retornaValor('fnc_lancamento_nome'));
    }

    protected function manuReferenciaCod($form, $obrigatorio = true) {
        return $form->escolha('fnc_referencia_cod', 'Referencia', $obrigatorio)
                        ->setOffsetColuna(2)
                        ->setTabela('fnc_referencia')
                        ->setCampoCod('fnc_referencia_cod')
                        ->setCampoDesc('fnc_referencia_nome')
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                        ->setValor($form->retornaValor('fnc_referencia_cod'));
    }

    protected function manuAnexo($form, $fncLancamentoCod) {
        return $form->upload('anexo[]', 'Anexos', 'ARQUIVO')
                        ->setOffsetColuna(2)
                        ->setCodigoReferencia($fncLancamentoCod)
                        ->setMultiple(true)
                        ->setMaximoArquivos(10)
                        ->setOrganogramaCod($this->organogramaCod)
                        ->setValor('anexo[]');
    }

    protected function manuLancamentoObservacao($form) {
        return $form->textArea('fnc_lancamento_observacao', 'Observações', false)
                        ->setLinhas(2)
                        ->setOffsetColuna(2)
                        ->setValor($form->retornaValor('fnc_lancamento_observacao'));
    }

    /**
     * 
     * @param Form $form
     * @return type
     */
    protected function manuOrigem($form) {

        $acao = $form->getAcao();

        $selecao = $this->selecao($acao);

        $naoSelecionaveis = [];
        foreach (array_keys($selecao) as $chave) {
            if (substr($chave, 0, 10) == '_separador') {
                $naoSelecionaveis[] = $chave;
            }
        }

        return $form->escolha('origem', 'Origem', true)
                        ->setArray($selecao)
                        ->setOrdena(false)
                        ->setEmColunaDeTamanho(4)
                        ->setOffsetColuna(6)
                        ->setNaoSelecionaveis($naoSelecionaveis)
                        ->setClassCss($acao == 'alterar' ? 'readOnly' : 'no')
                        ->setValor($form->retornaValor('origem'));
    }

    protected function manuOrigemCod($form, $acao, $fncLancamentoCod) {

        $origem = null;
        $valorOrigem = null;

        if ($acao === 'alterar') {

            $movimentacaoClass = (new MovimentacaoClassFactory())->instancia($this->organogramaCod);

            $parametrosSql = $movimentacaoClass->getDados($fncLancamentoCod);

            $origemValor = $this->getOrigemValor($parametrosSql);
            $origem = $origemValor['origem'];
            $valorOrigem = $origemValor['valor'];
        }

        return $form->escolha('origemCod', 'Seleção', false)
                        ->setArray([]) //fake input
                        ->setEmColunaDeTamanho(8)
                        ->setDependencia('origem', 'getFormOrigem', $this->class, ['formCod' => $fncLancamentoCod, 'origem' => $origem, 'valor_origem' => $valorOrigem])
                        ->setValor($form->retornaValor('origemCod'));
    }

    protected function manuVolorContabil($form, $acao, $fncLancamentoCod, $tipoLancamento) {

        $formPlano = (new PlanoContasFormFactory())->instancia($this->organogramaCod);
        $objPaiP = new Form();
        $objPaiP->setAcao($acao);

        $confCamposP = [
            'fnc_conta_valor' => $objPaiP->float('fnc_conta_valor', 'Valor', true)
                    ->setOffsetColuna(4)
                    ->setValorMinimo('0,01')
                    ->setComplemento('onchange="somaValorContabil(\'' . $fncLancamentoCod . '\')"')
                    ->setEmColunaDeTamanho(6),
            'fnc_conta_contabil_cod' => $formPlano->getFormContaContabilPorGrupo($objPaiP, '4')
                    ->setEmColunaDeTamanho(6)
                    ->setNome('fnc_conta_contabil_cod_destino')
                    ->setIdentifica('Conta de Destino')];

        $objPaiR = new Form();
        $objPaiR->setAcao($acao);

        $confCamposR = [
            'fnc_conta_valor' => $objPaiR->float('fnc_conta_valor', 'Valor', true)
                    ->setOffsetColuna(4)
                    ->setValorMinimo('0,01')
                    ->setComplemento('onchange="somaValorContabil(\'' . $fncLancamentoCod . '\')"')
                    ->setEmColunaDeTamanho(6),
            'fnc_conta_contabil_cod' => $formPlano->getFormContaContabilPorGrupo($objPaiR, '3')
                    ->setEmColunaDeTamanho(6)
                    ->setNome('fnc_conta_contabil_cod_destino')
                    ->setIdentifica('Conta de Destino')];

        if ($acao === 'alterar') {

            $parcelaClass = (new ParcelaClassFactory())->instancia($this->organogramaCod);
            $valida = Valida::instancia();

            $sqlBusca = $this->con->qb();
            $sqlBusca->select('fnc_conta_cod, SUM(fnc_conta_valor) as fnc_conta_valor, fnc_conta_contabil_cod')
                    ->from('fnc_conta')
                    ->where($sqlBusca->expr()->eq('fnc_lancamento_cod', ':fnc_lancamento_cod'))
                    ->andWhere($sqlBusca->expr()->eq('fnc_conta_selecao', ':fnc_conta_selecao'))
                    ->andWhere($sqlBusca->expr()->eq('fnc_conta_removida', ':fnc_conta_removida'))
                    ->setParameter('fnc_lancamento_cod', $fncLancamentoCod)
                    ->setParameter('fnc_conta_selecao', 'Automatica', \PDO::PARAM_STR)
                    ->setParameter('fnc_conta_removida', 'N', \PDO::PARAM_STR)
                    ->groupBy('fnc_conta_contabil_cod');

            (new OrganogramaSql())->doOrganograma($this->organogramaCod, $sqlBusca);

            $valorDocumento = $valida->numero()->moedaCliente($parcelaClass->getValorDocumento($fncLancamentoCod));
        } else {
            $sqlBuscaP = null;
            $sqlBuscaR = null;
            $valorDocumento = null;
        }

        if ($tipoLancamento) {
            if ($tipoLancamento == 'P') {
                $sqlBuscaP = $sqlBusca;
                $sqlBuscaR = null;
                $objPaiP->setAcao('alterar');
                $objPaiR->setAcao('cadastrar');
            } else {
                $sqlBuscaP = null;
                $sqlBuscaR = $sqlBusca;
                $objPaiP->setAcao('cadastrar');
                $objPaiR->setAcao('alterar');
            }
        } else {
            $sqlBuscaP = null;
            $sqlBuscaR = null;
        }

        if (!$tipoLancamento) {
            switch ($form->retornaValor('fnc_lancamento_tipo_lancamento')) {
                case 'AP': case 'PA': $tipoLancamento = 'P';
                    break;
                case 'AR': case 'RE': $tipoLancamento = 'R';
                    break;
            }
        }

        $campoP = null;
        if (!$tipoLancamento or $tipoLancamento == 'P') {
            $campoP = $form->masterDetail('valorContabilP', 'Valor Contábil')
                    ->setTabela('fnc_conta')
                    ->setCodigo('fnc_conta_cod')
                    ->setCampoReferencia('fnc_lancamento_cod')
                    ->setCodigoReferencia($fncLancamentoCod)
                    ->setObjetoPai($objPaiP)
                    ->setView('conf_contabil.html.twig', $this->namespace, ['valorContabil' => $valorDocumento, 'formCod' => $fncLancamentoCod])
                    ->setCrudExtra('organograma_cod', ['organograma_cod', $this->organogramaCod, 'Inteiro'])
                    ->setCrudExtra('fnc_conta_tipo', ['fnc_conta_tipo', 'C', 'Texto'])
                    ->setSqlBusca($sqlBuscaP)
                    ->setGravar(false)
                    ->setNaoRepetir(['fnc_conta_contabil_cod'])
                    ->setAddMin(1)
                    ->setTotalItensInicio(1)
                    ->setConexao($this->con)
                    ->setOrganogramaCod($this->organogramaCod)
                    ->setComplementoExterno('style="display:none"')
                    ->setCampos($confCamposP);
        }

        $campoR = null;
        if (!$tipoLancamento or $tipoLancamento == 'R') {
            $campoR = $form->masterDetail('valorContabilR', 'Valor Contábil')
                    ->setTabela('fnc_conta')
                    ->setCodigo('fnc_conta_cod')
                    ->setCampoReferencia('fnc_lancamento_cod')
                    ->setCodigoReferencia($fncLancamentoCod)
                    ->setObjetoPai($objPaiR)
                    ->setView('conf_contabil.html.twig', $this->namespace, ['valorContabil' => $valorDocumento, 'formCod' => $fncLancamentoCod])
                    ->setCrudExtra('organograma_cod', ['organograma_cod', $this->organogramaCod, 'Inteiro'])
                    ->setCrudExtra('fnc_conta_tipo', ['fnc_conta_tipo', 'C', 'Texto'])
                    ->setSqlBusca($sqlBuscaR)
                    ->setGravar(false)
                    ->setNaoRepetir(['fnc_conta_contabil_cod'])
                    ->setAddMin(1)
                    ->setTotalItensInicio(1)
                    ->setConexao($this->con)
                    ->setOrganogramaCod($this->organogramaCod)
                    ->setComplementoExterno('style="display:none"')
                    ->setCampos($confCamposR);
        }

        return ['campoP' => $campoP, 'campoR' => $campoR];
    }

    protected function manuParcelado($form, $fncLancamentoCod) {

        return $form->escolha('parcelado', 'Dividir Lançamento', true)
                        ->setArray(['S' => 'Sim', 'N' => 'Não'])
                        ->setOrdena(false)
                        ->setInicio(false)
                        ->setValorPadrao('N')
                        ->setComplemento('onchange="verificaParcelamento(\'' . $fncLancamentoCod . '\')"')
                        ->setOffsetColuna(2)
                        ->setValor($form->retornaValor('parcelado'));
    }

    protected function manuLancamentoDataBase($form, $fncLancamentoCod) {

        return $form->data('fnc_lancamento_data_base', 'Vencimento Base', true)
                        ->setValorPadrao(date('d/m/Y'))
                        ->setComplemento('onchange="atualizaParcela(\'' . $fncLancamentoCod . '\')"')
                        ->setEmColunaDeTamanho(6)
                        ->setOffsetColuna(4)
                        ->setValorPadrao(date('d/m/Y'))
                        ->setComplementoExterno('style="display:none"')
                        ->setValor($form->retornaValor('fnc_lancamento_data_base'));
    }

    protected function manuNumeroParcelas($form, $fncLancamentoCod) {

        return $form->numero('numero_parcelas', 'Parcelas', true)
                        ->setValorPadrao(1)
                        ->setValorMaximo(99)
                        ->setComplemento('onchange="atualizaParcela(\'' . $fncLancamentoCod . '\')"')
                        ->setEmColunaDeTamanho(6)
                        ->setOffsetColuna(4)
                        ->setComplementoExterno('style="display:none"')
                        ->setValor($form->retornaValor('numero_parcelas'));
    }

    protected function manuLayoutCarregando($form, $acao) {

        $carregando = $acao === 'alterar' ? '<i class="fa fa-refresh fa-spin"></i> carregando...' : '';
        return $form->layout('parcelas', '<div id="parcelas">' . $carregando . '</div>');
    }

    ##CAMPOS DE MANUTENÇÂO

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $fncLancamentoCod = null, $tipoLancamento = null) {

        $form = new Form();

        $form->setAcao($acao);

        $form->config('formManu' . $fncLancamentoCod, 'POST')
                ->setHeader('Movimentação Financeira');

        $campos[] = $form->hidden('cod')
                ->setValor($form->retornaValor('cod'));

        $campos[] = $this->manuValorContabilHidden($form);
        $campos[] = $this->manuTipoLancamento($form, $fncLancamentoCod);
        $campos[] = $this->manuParcelaCompetencia($form);
        $campos[] = $this->manuConvenioCod($form, false);
        $campos[] = $this->manuLancamentoNome($form);
        $campos[] = $this->manuReferenciaCod($form);
        $campos[] = $this->manuAnexo($form, $fncLancamentoCod);
        $campos[] = $this->manuLancamentoObservacao($form);
        $campos[] = $this->manuOrigem($form);
        $campos[] = $this->manuOrigemCod($form, $acao, $fncLancamentoCod);
        $camposPR = $this->manuVolorContabil($form, $acao, $fncLancamentoCod, $tipoLancamento);

        if ($camposPR['campoP']) {
            $campos[] = $camposPR['campoP'];
        }

        if ($camposPR['campoR']) {
            $campos[] = $camposPR['campoR'];
        }

        $campos[] = $this->manuParcelado($form, $fncLancamentoCod);
        $campos[] = $this->manuLancamentoDataBase($form, $fncLancamentoCod);
        $campos[] = $this->manuNumeroParcelas($form, $fncLancamentoCod);
        $campos[] = $this->manuLayoutCarregando($form, $acao);
        $campos[] = $form->botaoSalvarPadrao();
        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

    public function getFormOrigem($referencia, $parametros = []) {

        $form = new Form();

        $form->config('formManu' . $form->retornaValor('formCod', 'GET'));

        if (is_array($parametros) and array_key_exists('origem', $parametros) and $parametros['origem']) {
            $referencia = $parametros['origem'];
        }

        switch ($referencia) {

            case 'empresa_cod':

                $campos[0] = $form->escolha('origemCod', 'Empresa')
                        ->setInicio(false)
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                        ->setCampoCod('empresa_cod')
                        ->setCampoDesc('empresa_nome')
                        ->setTabela('empresa');
                break;

            case'cliente_cod':
                $campos[0] = $form->chosen('origemCod', 'Cliente')
                        ->setInicio('Selecione o Cliente')
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                        ->setCampoCod('cliente_cod')
                        ->setCampoDesc('cliente_nome')
                        ->setTabela('cliente');
                break;

            case'funcionario_cod':
                $campos[0] = $form->chosen('origemCod', 'Funcionário')
                        ->setInicio('Selecione o Funcionário')
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                        ->setCampoCod('funcionario_cod')
                        ->setCampoDesc('funcionario_nome')
                        ->setTabela('funcionario');
                break;

            case'fornecedor_cod':
                $campos[0] = $form->chosen('origemCod', 'Fornecedor')
                        ->setInicio('Selecione o Fornecedor')
                        ->setInstrucoes(['organograma_cod', \PDO::PARAM_INT, 'Igual', $this->organogramaCod])
                        ->setCampoCod('fornecedor_cod')
                        ->setCampoDesc('fornecedor_nome')
                        ->setTabela('fornecedor');
                break;

            default : throw new \Exception('Origem Inválida!');
        }

        if (is_array($parametros) and array_key_exists('origem', $parametros) and $parametros['valor_origem']) {
            $campos[0]->setValorPadrao($parametros['valor_origem']);
        }

        return $form->processarForm($campos);
    }
}
