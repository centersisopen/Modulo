<?php

namespace Centersis\Modulos\Financeiro\PlanoContas\Sistema\Padrao;

use Centersis\Zion2\Exception\ValidationException;

use Centersis\Ext\Core\Padrao\BaseClass;

class PlanoContasClass extends BaseClass {

    public function __construct($organogramaCod, $con, $sql) {

        parent::__construct($organogramaCod, $con, $sql);

        $this->tabela = 'fnc_conta_contabil';
        $this->chavePrimaria = 'fnc_conta_contabil_cod';

        $this->colunasCrud = [
            0 => 'fnc_conta_contabil_ref_cod',
            1 => 'fnc_conta_contabil_nome',
            2 => 'fnc_conta_contabil_nivel',
            3 => 'fnc_conta_contabil_tipo',
            5 => 'fnc_conta_contabil_ordem',
            8 => 'fnc_conta_contabil_codigo',
        ];

        $this->colunasGrid = [
            'fnc_conta_contabil_nivel' => 'Nível',
            'fnc_conta_contabil_codigo' => 'Código',
            'fnc_conta_contabil_nome' => 'Nome',
            'fnc_conta_contabil_tipo' => 'Lançamentos',
        ];

        $this->filtroDinamico = [
            'fnc_conta_contabil_nome' => '',
            'fnc_conta_contabil_nivel' => '',
            'fnc_conta_contabil_tipo' => ''
        ];
    }

    public function grid($grid)
    {
        $grid->substituaPor('fnc_conta_contabil_tipo', [
            'C' => '(C) - Aceita Créditos',
            'D' => '(D) - Aceita Débitos',
            'CD' => '(CD) - Aceita Créditos e Débitos',
            'NP' => '(NA) - Não Aceita Lançamentos'
        ]);
        $grid->setQuemOrdena(['fnc_conta_contabil_ordem' => 'ASC']);
        $grid->converterResultado($this, 'interpretaNivel', 'fnc_conta_contabil_nivel', ['fnc_conta_contabil_nivel']);
        $grid->substituaPor('fnc_conta_contabil_codigo', ['' => '<em>não informado</em>']);
        $grid->setQLinhas(5000);
    }

    public function cadastrar($objForm) {
        $nivel = $this->defineNivel($objForm->get('fnc_conta_contabil_ref_cod'));

        $permiteLancamento = $objForm->get('permite');

        $objForm->set('fnc_conta_contabil_tipo', $this->defineTipoPeloNivel($nivel, $permiteLancamento), 'Texto');

        $objForm->set('fnc_conta_contabil_nivel', $nivel);
        $objForm->set('fnc_conta_contabil_ordem', $this->converteNivel($nivel));

        return $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm, []);
    }

    public function alterar($objForm) {
        unset($this->colunasCrud[0], $this->colunasCrud[2], $this->colunasCrud[5]);

        $permiteLancamento = $objForm->get('permite');

        $nivel = $this->getDados($objForm->get('cod'), 'fnc_conta_contabil_nivel');

        $objForm->set('fnc_conta_contabil_tipo', $this->defineTipoPeloNivel($nivel, $permiteLancamento), 'Texto');

        return $this->crudUtil->update($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm, [$this->chavePrimaria => $objForm->get('cod')], [], []);
    }

    protected function defineTipoPeloNivel($nivel, $permiteLancamento) {
        if ($permiteLancamento == 'N') {
            return 'NP';
        }

        switch (substr($nivel, 0, 1)) {
            case '1':
                return 'CD';
            case '2':
                return 'CD';
            case '3':
                return 'C';
            case '4':
                return 'C';
        }
    }

    public function remover($cod) {
        $nivel = $this->getDados($cod, 'fnc_conta_contabil_nivel');

        if (\in_array($nivel, ['1', '2', '3', '4'])) {
            throw new ValidationException('Não é possível remover os níveis básicos!');
        }

        return $this->crudUtil->delete($this->organogramaCod, $this->tabela, [$this->chavePrimaria => $cod], []);
    }

    public function setValoresFormManu($cod, $formIntancia) {
        $objForm = $formIntancia->getFormManu('alterar', $cod);

        $parametrosSql = $this->con->execLinhaArray($this->sql->getDadosSql($cod));

        $this->crudUtil->setParametrosForm($objForm, $parametrosSql, $cod);

        if ($parametrosSql['fnc_conta_contabil_tipo'] == 'NP') {
            $objForm->set('permite', 'N', "Texto");
        } else {
            $objForm->set('permite', 'S', "Texto");
        }

        return $objForm;
    }

    /**
     * Retorna todas as contas contabeis de um plano de contas tendo como indice
     * fnc_conta_contabil_cod
     * @return array
     */
    public function getContasContabeis() {
        return $this->con->paraArray($this->sql->getDadosSql(), null, 'fnc_conta_contabil_cod');
    }

    protected function defineNivel($fncContaContabilCod = 0) {
        $nivel = 0;

        if (!$fncContaContabilCod) {
            $nivel = $this->somaNivel($this->ultimoNivelSemReferencia());

            if ($nivel > 4) {
                throw new ValidationException('O sistema esta programado para suportar apenas 4 níveis básicos');
            }

            return $nivel;
        }

        $nivelUltimoFilho = $this->ultimoFilho($fncContaContabilCod);

        if ($nivelUltimoFilho) {
            $nivel = $this->somaNivel($nivelUltimoFilho);
        } else {
            $nivel = $this->addNivel($this->nivelAtual($fncContaContabilCod));
        }

        return $nivel;
    }

    protected function somaNivel($ultimoNivel) {
        if (substr_count($ultimoNivel, '.')) {
            $partes = explode('.', $ultimoNivel);

            $totalPartes = count($partes);
            $cont = 0;
            $novoNivel = '';

            foreach ($partes as $parte) {
                $cont++;
                if ($cont === $totalPartes) {
                    $novoNivel .= ($parte + 1);
                } else {
                    $novoNivel .= $parte . '.';
                }
            }
        } else {
            $novoNivel = $ultimoNivel + 1;
        }

        return $novoNivel;
    }

    protected function addNivel($ultimoNivel) {
        return $ultimoNivel . '.1';
    }

    protected function ultimoFilho($fncContaContabilCod) {
        $qb = $this->con->qb();

        $qb->select('fnc_conta_contabil_nivel')
                ->from('fnc_conta_contabil', '')
                ->where($qb->expr()->eq('fnc_conta_contabil_ref_cod', '?'))
                ->setParameter(0, $fncContaContabilCod, \PDO::PARAM_INT)
                ->orderBy('fnc_conta_contabil_cod', 'DESC')
                ->setMaxResults(1);

        $qb->andWhere($qb->expr()->eq('organograma_cod', $this->organogramaCod));

        return $this->con->execRLinha($qb);
    }

    protected function nivelAtual($fncContaContabilCod) {
        $qb = $this->con->qb();

        $qb->select('fnc_conta_contabil_nivel')
                ->from('fnc_conta_contabil', '')
                ->where($qb->expr()->eq('fnc_conta_contabil_cod', '?'))
                ->setParameter(0, $fncContaContabilCod, \PDO::PARAM_INT);

        $qb->andWhere($qb->expr()->eq('organograma_cod', $this->organogramaCod));

        return $this->con->execRLinha($qb);
    }

    protected function ultimoNivelSemReferencia() {
        $qb = $this->con->qb();

        $qb->select('fnc_conta_contabil_nivel')
                ->from('fnc_conta_contabil', '')
                ->where($qb->expr()->isNull('fnc_conta_contabil_ref_cod'))
                ->orderBy('fnc_conta_contabil_cod', 'DESC')
                ->setMaxResults(1);

        $qb->andWhere($qb->expr()->eq('organograma_cod', $this->organogramaCod));

        $nivel = $this->con->execRLinha($qb);

        return (int) $nivel;
    }

    public function montaArrayPlanoDeContas($grupo = '') {
        $retorno = [];

        $rsPlano = $this->con->executar($this->sql->getPlanoDeContas($grupo));

        while ($dados = $rsPlano->fetch()) {
            $retorno[$dados['fnc_conta_contabil_cod']] = $this->interpretaNivel($dados['fnc_conta_contabil_nivel'], $dados['fnc_conta_contabil_nome']);
        }

        return $retorno;
    }

    public function getContasNaoSelecionaveis() {
        $retorno = [];

        $rsPlano = $this->con->executar($this->sql->getPlanoDeContas());

        while ($dados = $rsPlano->fetch()) {

            if ($dados['fnc_conta_contabil_tipo'] === 'NP') {
                $retorno[] = $dados['fnc_conta_contabil_cod'];
            }
        }

        return $retorno;
    }

    public function interpretaNivel($nivel, $nome = '') {
        $total = substr_count($nivel, '.');

        return str_repeat('&nbsp;', $total) . ' ' . $nivel . ' ' . $nome;
    }

    /**
     * Maetodo Recursivo
     * Monta um array contendo a arvore de referecnia de uma conta contabil
     * Parte da conta contabil especificada é vai até o seu último pai
     * Retorna um array com com as contas contábeis ordenadas inversamente
     * [0] => 4.1.1 - Energia Elétrica
     * [1] => 4.1 - Despesas Variáveis
     * [2] => 4 - DESPESAS
     * @param int $fncContaContabilCod - Conta contabil de referencia
     * @param bool $niveis - Booleando que indica se deve ou não ser concatenado
     * no nome da referência o seu nível hierárquico
     * @return array
     */
    public function arvoreDeReferencia($fncContaContabilCod, $niveis) {
        $arvore = [];

        $qb = $this->con->qb();

        $qb->select('fnc_conta_contabil_ref_cod, fnc_conta_contabil_nome, fnc_conta_contabil_nivel')
                ->from('fnc_conta_contabil', '')
                ->where($qb->expr()->eq('fnc_conta_contabil_cod', '?'))
                ->setParameter(0, $fncContaContabilCod, \PDO::PARAM_INT);

        $qb->andWhere($qb->expr()->eq('organograma_cod', $this->organogramaCod));

        $dados = $this->con->execLinha($qb);

        $arvore[] = ($niveis ? $dados['fnc_conta_contabil_nivel'] . ' - ' : '') . $dados['fnc_conta_contabil_nome'];

        if ($dados['fnc_conta_contabil_ref_cod']) {

            $ganho = $this->arvoreDeReferencia($dados['fnc_conta_contabil_ref_cod'], $niveis);

            $arvore = array_merge($arvore, $ganho);
        }

        return $arvore;
    }

    protected function converteNivel($nivel) {
        $retorno = '';

        $partes = explode('.', $nivel);

        foreach ($partes as $valor) {

            $zeros = 4 - strlen($valor);

            if ($zeros < 0) {
                $zeros = 0;
            }

            $retorno .= str_repeat('0', $zeros) . $valor;
        }

        return $retorno;
    }

}
