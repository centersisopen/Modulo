<?php

namespace Centersis\Modulos\Financeiro\PlanoContas\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Financeiro\PlanoContas\Sistema\PlanoContasClassFactory;
use Centersis\Ext\Core\Padrao\BaseForm;

class PlanoContasForm extends BaseForm {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $formCod = null) {
        $form = new Form();

        $form->setAcao($acao);

        $form->config('formManu' . $formCod, 'POST')
                ->setHeader('Plano de Contas');

        $campos[] = $form->hidden('cod')
                ->setValor($form->retornaValor('cod'));

        $campos[] = $this->getFormContaContabil($form)
                ->setNome('fnc_conta_contabil_ref_cod')
                ->setObrigatorio(false)
                ->setNaoSelecionaveis(null)
                ->setDisabled($acao === 'alterar')
                ->setValor($form->retornaValor('fnc_conta_contabil_ref_cod'));

        $campos[] = $form->texto('fnc_conta_contabil_nome', 'Nome', true)
                ->setMaximoCaracteres(50)
                ->setValor($form->retornaValor('fnc_conta_contabil_nome'));

        $campos[] = $form->numero('fnc_conta_contabil_codigo', 'Código', false)
                ->setValor($form->retornaValor('fnc_conta_contabil_codigo'));

        $campos[] = $form->escolha('permite', 'Permite Lançamentos', true)
                ->setArray([
                    'S' => 'Sim',
                    'N' => 'Não'])
                ->setExpandido(false)
                ->setOrdena(false)
                ->setToolTipMsg('Ao selecionar (Não), esta conta não poderá ser selecionada nos lançamentos!')
                ->setValor($form->retornaValor('permite'));

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

    public function getFormContaContabil(Form $form) {
        $planoContas = (new PlanoContasClassFactory())->instancia($this->organogramaCod);

        $arrayPlano = $planoContas->montaArrayPlanoDeContas();
        $naoSelecionaveis = $planoContas->getContasNaoSelecionaveis();

        return $form->escolha('fnc_conta_contabil_cod', 'Conta Pai', true)
                        ->setArray($arrayPlano)
                        ->setOrdena(false)
                        ->setNaoSelecionaveis($naoSelecionaveis)
                        ->setComplemento('style="font-family: \'Courier New\'"')
                        ->setValor($form->retornaValor('fnc_conta_contabil_cod'));
    }

    public function getFormContaContabilPorGrupo($form, $grupo) {
        $planoContas = (new PlanoContasClassFactory())->instancia($this->organogramaCod);

        $arrayPlano = $planoContas->montaArrayPlanoDeContas($grupo);
        $naoSelecionaveis = $planoContas->getContasNaoSelecionaveis();

        return $form->escolha('fnc_conta_contabil_cod', 'Conta Pai', true)
                        ->setArray($arrayPlano)
                        ->setOrdena(false)
                        ->setNaoSelecionaveis($naoSelecionaveis)
                        ->setComplemento('style="font-family: \'Courier New\'"')
                        ->setValor($form->retornaValor('fnc_conta_contabil_cod'));
    }

}
