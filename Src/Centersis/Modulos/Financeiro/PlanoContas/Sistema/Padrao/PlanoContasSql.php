<?php

namespace Centersis\Modulos\Financeiro\PlanoContas\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class PlanoContasSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function filtrarSql($objForm, $colunas)
    {
   

        $qb = $this->con->qb();

        $qb->select('fnc_conta_contabil_cod, fnc_conta_contabil_nome, '
                . 'fnc_conta_contabil_nivel, fnc_conta_contabil_tipo, '
                . 'fnc_conta_contabil_saldo, '
                . 'fnc_conta_contabil_codigo')
            ->from('fnc_conta_contabil', '');

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $colunas, $qb, '', $this->organogramaCod);

        return $qb;
    }

    public function getDadosSql($cod = 0)
    {
        $qb = $this->con->qb();

        $qb->select('*')
            ->from('fnc_conta_contabil', '');

        if ($cod) {
            $qb->where($qb->expr()->eq('fnc_conta_contabil_cod', ':fnc_conta_contabil_cod'))
                ->setParameter('fnc_conta_contabil_cod', $cod);
        }

        $this->doOrganograma($qb);

        return $qb;
    }

    public function getPlanoDeContas($grupo = '')
    {
        $qb = $this->con->qb();

        $qb->select('fnc_conta_contabil_cod, fnc_conta_contabil_nome, fnc_conta_contabil_nivel, fnc_conta_contabil_tipo')
            ->from('fnc_conta_contabil', '')
            ->orderBy('fnc_conta_contabil_ordem');        

        if ($grupo) {
            $qb->andWhere($qb->expr()->in('SUBSTR(fnc_conta_contabil_nivel,1,1)', $grupo));
        }
        
        $this->doOrganograma($qb);

        return $qb;
    }

}
