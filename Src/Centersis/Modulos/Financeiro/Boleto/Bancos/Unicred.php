<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class Unicred extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {
        $dadosboleto = [];
        $codigobanco = $dadosConvenio['fnc_convenio_banco'];
        $codigobancoDV = '8';
        $codigoBancoComDv = $codigobanco . '-' . $codigobancoDV;
        $nummoeda = '9';

        $fatorVencimento = $this->fatorVencimento($dadosLancamento["fnc_parcela_vencimento"]);

        $valorParcela = str_replace('.', ',', $dadosLancamento["fnc_parcela_valor"]);
        $valor = $this->formataNumero($valorParcela, 10, 0, "valor");

        $agencia = $this->formataNumero($dadosConvenio["fnc_convenio_agencia"], 4, 0);
        $conta = $this->formataNumero($dadosConvenio["fnc_convenio_conta"] . $dadosConvenio["fnc_convenio_conta_dv"], 10, 0);
        $nossonumero = $dadosLancamento["fnc_parcela_nosso_numero"];

        $agenciaCodigo = $agencia . " / " . $conta;

        $campoLivre = $agencia . $conta . $nossonumero;

        $aVerificarDv = $codigobanco . $nummoeda . $fatorVencimento . $valor . $campoLivre;
        $dv = $this->digitoVerificadorBarra($aVerificarDv);
        $linha = $codigobanco . $nummoeda . $dv . $fatorVencimento . $valor . $campoLivre;

        $dadosboleto["codigo_barras"] = $this->htmCodigoBarras($linha);
        $dadosboleto["linha_digitavel"] = $this->montaLinhaDigitavel($linha);
        $dadosboleto["agencia_codigo"] = substr($agenciaCodigo, 0, -1) . '-' . substr($agenciaCodigo, -1);
        $dadosboleto["nosso_numero"] = substr($nossonumero, 0, -1) . '-' . substr($nossonumero, -1);
        $dadosboleto["codigo_banco_com_dv"] = $codigoBancoComDv;
        $dadosboleto["campo_livre"] = $campoLivre;
        $dadosboleto["logoBanco"] = $this->logo();

        return $dadosboleto;
    }

    protected function digitoVerificadorBarra($num) {
        $modulo = $this->modulo11($num);
        if ($modulo['resto'] == 0 || $modulo['resto'] == 1 || $modulo['resto'] == 10) {
            $dv = 1;
        } else {
            $dv = 11 - $modulo['resto'];
        }

        return $dv;
    }

    protected function modulo10($num) {
        $numtotal10 = 0;
        $fator = 2;

        // Separacao dos numeros
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo (falor 10)
            // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Itaú
            $temp = $numeros[$i] * $fator;
            $temp0 = 0;
            foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                $temp0 += $v;
            }
            $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            // monta sequencia para soma dos digitos no (modulo 10)
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }

        // várias linhas removidas, vide função original
        // Calculo do modulo 10
        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
    }

    public function modulo11($num, $base = 9) {
        $fator = 2;

        $soma = 0;
        // Separacao dos numeros.
        for ($i = strlen($num); $i > 0; $i--) {
            //  Pega cada numero isoladamente.
            $numeros[$i] = substr($num, $i - 1, 1);
            //  Efetua multiplicacao do numero pelo falor.
            $parcial[$i] = $numeros[$i] * $fator;
            //  Soma dos digitos.
            $soma += $parcial[$i];
            if ($fator == $base) {
                //  Restaura fator de multiplicacao para 2.
                $fator = 1;
            }
            $fator++;
        }
        $result = array(
            'digito' => ($soma * 10) % 11,
            // Remainder.
            'resto' => $soma % 11,
        );
        if ($result['digito'] == 10) {
            $result['digito'] = 0;
        }
        return $result;
    }

    public function nossoNumero($numero, $dadosConvenio) {
        $numeroCompleto = $this->formataNumero($numero, 10, 0);

        $modulo11 = $this->modulo11($numeroCompleto);

        return $numeroCompleto . $modulo11['digito'];
    }

    public function logo() {
        return 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4QDERXhpZgAASUkqAAgAAAACADEBAgAHAAAAJgAAAGmHBAABAAAALgAAAAAAAABQaWNhc2EAAAYAAJAHAAQAAAAwMjIwAaADAAEAAAABAAAAAqAEAAEAAAChAAAAA6AEAAEAAAAbAAAABaAEAAEAAACeAAAAIKQCACEAAAB8AAAAAAAAADc4ZjFmMThjMWEyMGNkNTQwMDAwMDAwMDAwMDAwMDAwAAACAAEAAgAEAAAAUjk4AAIABwAEAAAAMDEwMAAAAAD/4QEiaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjUuMCI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiLz4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/9sAhAADAgIDAgIDAwMDBAMDBAUQBQUQBBAQEA4PEREPEw0OEBAQEA0QDw8ODg8TDxAQDxENEBAPEBAOEBAQEA4ODQ0QAQMEBAYFBgoGBgoNDQoNDQ0NDQ0QDQ8NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0IDQ0NDQ0NDQ0IDQ0NDQ0IDQ3/wAARCAAbAKEDAREAAhEBAxEB/8QAHAAAAQUBAQEAAAAAAAAAAAAACAQFBgcJAwIA/8QAPhAAAQMDAwMCBQEEBQ0AAAAAAQIDBAUGEQAHIQgSMUFRExQiMmFxCUKBsiNzkbPSFzY3ODlydHWEkqG04f/EABsBAAEFAQEAAAAAAAAAAAAAAAIAAwQFBgEH/8QAJREAAgICAgICAwADAAAAAAAAAQIAAxESBCEFMRMyIkFRFCMz/9oADAMBAAIRAxEAPwA4d1OrS1tpN3La27qtOq0ms1/4YjKiBJbHxHPhp7iVgjCvP0njSjJtAOJclVqSKRSpk51KlNRWVPr7POEjJx+dKO5wMylNkur61N+LRu+4qHTaxDh2w18aWJwSFEdil/SErUDwk+SOcaUZS0MMxy6dOqqy+pmnVF+2FTIc2nqHzDdSADgB+1YAUoFB5GQeCMHHGVElof1JZvZu7RtiNsa5fVwIkOUekJSt8RsFR7lpbGMkDPcoeulHDkSAXf1hWlZuwls7tS6dWJFu3AtCIqY6Ulwd4UpPcO/t8JOcKPppQDaAMmWntzfkHcuxKFddOZkR4FYiJmMCTgKAUMgKAJGf0J0JjitkZkL3B6m7G28qDkCXUXKjUWjhxNNHcUn2UchIP47s/jVZd5Cus6mPrUzSJ0frUtCuVeDTotIrapMx9MdvhHlRCR+/+edMp5JWbAEM0kRyvXq6tSxrqqdCm0+qvyYDvwXTHCe3OM8ZWDx+mk/lEViuO51aCRmLdvOquzNxK+xRo3z1NnyT2x/nwAFH2BSogH2Bxn050VXkq7W0HuA1JHccN1+oq39oK1DpdViVCVJlR/mU/JhJAGe3nKhzkHRcjnpxzqYkrL+oy7f9WNs7jXdT7dplMq7cyaT2F8I7RgFRJwsnGB7a5T5BbGxiJqys43h1fWlZlz1SiSoFWkP094sOlgJ7cjzjKwePHjQWeTSt9Z0VEjMc9veqix9wqsxTGH5VLnSD2sCogAKPsFAkZPoCQT6aKjyVdra+pw1Edy486uB3GfUjW4N/0jbe15dbrEgsxmRhIT9yj6JSPVR/8eTxk6ici8VL3CUFpVG20Xcbc+BWLnrNemWxDqTRFEaihI7OPpWvKST6YB+7kkAdo1WUtbaCxOI8cDqUBA6iNybAvxEa5qzJnN06X8KoNvBOCBwrBCAfH1JP6emqdefalurHoST8QZciHdQ6vEr1IiVKC8mRCltB9kp9QRka2Vb7qCJXkYOIv0WDOTODrX/18Nnv+l/9o6OV1v8A0E0Bvn/Mu4P+XufyHSk1vrM7P2cf+gffv/gR/cO6Ug0fUyodmbAvHa/Zu2Oofb552RMo05yJXWz4U0CBkgeWyD2uDyjhwfacKNopUbCFJ1qbzUDfj9mpfF2W+8THkiMh9Ln3NrExnuQr8pP9owocEaUsUbYSrt5/9ljsn/WRv7t3SkTkfWEFS7+l2B0J7fv051TFRqFIagMlHlPcklSh+QkHB9CRqn8neaqupY8NcgZnbo62hpNVo0u8axDaqMlUkx4Yk/UE4+5WDwVKJ4J8Acck6q/G8YOu7dyXc5BwIUBotLilLogRG+z6shI4xz7a0ZqRRnEi5Jmflg3fQTv0u6LokBulGc7MX3JKsk93YMAEnyPT01ia7VHILWepYMp0wI9UChnefqIVU7QpbkChIqSZiykYCEo7SVH0ClkZCfOVewOnak+a/ev6wSdVwZw6qK6zWd+Kih3udi09LcRXb7ABSwPzkkab59n+8D+Q61/DMJrZneq2tzJtRbpNuP0cUqMHnFSEoHHjAKec8HWh4t6uvS4wJDZSDBx6dow3B6jF1SS2JDQceqS/iDI5yE/zDVHxUFvKJPqSnOqYnrrEtqnWnufFk0dluE7NhCY6I/GFBRAVgeCcA/qM675FBVcNBO0nKnMMJu/YVt7YU+57ilCGwKciRIK/cpBwB6qJ4A8k60wt0pDmQdctgQFN1t0a5vvejJajvmP8T4NKaZ5Iz4OB5Wr1Pp4HAOshyOQ/KfC+pPRAgzCYonT7cEbaSoxJtzVV69JbXx2lJdX2tqHKW0/VjtP2qPrnjgDWhTiOtJye5FLjaBTWXqi/VZJq7kh2pIX8OR84SVZHBBJycjGNY2xSGOZYqRrCp6L92vjMv2JUH8rZBkUzu9R5Wj+H3D8d3trVeK5JI+MyDcn7ELHuPtrS5EiYmdf7SW3azZe8G227EeC5JotO7GJBR4Stt74qQr2+IkkJJwCoEZ5Gjldep2BEvPdrrj2mZ2RrtYo1502qVObTFNQWWVf0vetJSkKR5T2k/UVYAAPPjSjzWrrKe6CNuKraHSfulcFUjOw2rhhuLhB8EFSG2FJC8H0Uoqx7gZ12NUghSTLH/ZkMsz+k2Ow823IYeqbyFhzBBBwCCPBB8Ea5HqPr3BR61Nga101QrvpdvKdVs/uCENPo5IZcQ4l9Cc/uqSpH9Go57kEtnPbyow+1RyJNt+Q1F/ZgbOtpUEtokx0pz/uO40oNuSgl6Q7Cn3/0K7copLJlz6dS2ZzaUclQCSFAe5wcgeuMap/J0myrqWfEbUCNfTl1KULa21JNt3K1LZ+DJU8wWE5PPlKhkEEHP/zGqbg80Uro49SZZXsciXJM6krXvbby95dAdllyk0xTjhkp7RlQKUYOfJOrhuatlZKxkV4IzBz6bdq4e5FIv0y4zctyNTPgQyrntcVkgj2I7R/AnWf41ItVmYSZa4BAEk3RLfQpV4VW15LgSipM/MthXotHCh/FP8unvGP8TMhg8gAgESuaFfdFR1BvXbXnFmlJqzkxXwh3E4KuwY9vGoS2j/ILuOo5jKaiFHUeoG07u2wvydbQfQqm04h0uo7BlYKEc55OfTWjbk1vUdJB0IYZg39Ne61tbQ1au1WufMPPvQxGiCKMk85VkkgDOE8nWf4N60sWIkqxCwxH6i2jc3VXuou4Z8F2nW13gOKXntCE+G0H95SuckcAkn2GpC12cu3cjqCSEXAhe7gMWpBtF1+640FdBgJDpE0ApGBhOAf3vRIHPtrT2hEr/P8AUiDOeoIFl0qqb8btqm2VCRY1Ap57FLpgCVIQfcj7nXB4HhI/A5zFaNyLNkGBJROo7he3hZsyr7eyqHSatOptRTH7YjiVHvCk8pKlZye48K9wTrTWVk14B7kQHuZtVqRUo1xy11dSnK0zJPzXzvJKwee7Pnnz7jXn7q6v+Q/ctVxiHzsfCsO9bXo93UW26TAqKR2u/LoSFNuAYWM+R+PdJHvrccJK2QMB3KywkHEtzB1aaiMxHUoEWqwnIk2MzMivfQ4mQAUkexBBBGjnCJCYvTxtdT5yJsbbu2GJSD3JKGG8g/8AZrsb0H8k2dgx5Md2K6w25Gcb+EtKx9JB4II8YI4x4xrkdI/GJbdtej2jAbp1DpcOjwAorCYCQhOT5OEgDJ9TjSggDE9XFQKZc1Ocp1Yp0Wq090j4iJqQpJxyMpUCDg8jjShEAxBO28taq0CNQZtuUqXRIqsx2320ltOMgYQR2jGT4HqdKAwEdqZSoVEp7ECnxGYMGMjsZTHACUj0AA4AHsBrmM+4fqN0+yLdrUhUifQabNkKPJfQkn+0jUN6UP6EMMZ9Hsa3IUWREj0GmsRZQxIS2hICscjuGMHB8Z11KkAOBEWMU0i3KTbqHUUqmRKah0gr+USE5/XAGdOV1qB0IDE5iOPY1uU2R87EoNNjTEkqCmUJCsng8gZ5yc/roErUH1DLGcf8mNnnk2tRs/1SP8OmfhQk5AhbGKWbGtyLCfhM0GmtQ5JBeShCQlWPGRjBx6Z0QpQDGIJY5nFrba0mHAtu2KOhY8draP8ADptaK8/UQixj80y2w32NoS2hIwkI8DU9EVfQjWTE1WotPr8dMSpwo9Qi57+2UAoZHg4IIyNcsUN0YWZ5o9Cp1vsKjUyBGp0cq7yIqQkZPk4AAz+dBWiqOhOk5jhgFOnMQZH51gWxVJjsqZb1LlSXT3OKebSSf1JGTqHZUhPYjgYxfSLfplusFilU6LTmFr71CKkJBPjOABz451KqUKuBBJjpp6DP/9k="';
    }

}
