<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class Ailos extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {
        $dadosboleto = [];
        $codigobanco = $dadosConvenio['fnc_convenio_banco'];
        $codigobancoDV = '0';
        $codigoBancoComDv = $codigobanco . '-' . $codigobancoDV;
        $nummoeda = '9';

        $fatorVencimento = $this->fatorVencimento($dadosLancamento["fnc_parcela_vencimento"]);

        $valorParcela = str_replace('.', ',', $dadosLancamento["fnc_parcela_valor"]);
        $valor = $this->formataNumero($valorParcela, 10, 0, "valor");

        $convenio = $this->formataNumero($dadosConvenio["fnc_convenio_numero"], 6, 0);
        $conta = $this->formataNumero($dadosConvenio["fnc_convenio_conta"] . $dadosConvenio["fnc_convenio_conta_dv"], 8, 0);
        $nossonumero = $dadosLancamento["fnc_parcela_nosso_numero"];
        $sequencial = substr($dadosLancamento["fnc_parcela_nosso_numero"], -9);
        $carteira = $this->formataNumero($dadosConvenio["fnc_convenio_carteira"], 2, 0);

        $agencia = $dadosConvenio["fnc_convenio_agencia"];
        $agenciaDv = $dadosConvenio["fnc_convenio_agencia_dv"];

        $agenciaCodigo = $agencia . '-' . $agenciaDv . " / " . $dadosConvenio["fnc_convenio_conta"] . '-' . $dadosConvenio["fnc_convenio_conta_dv"];

        $campoLivre = $convenio . $conta . $sequencial . $carteira;

        $aVerificarDv = $codigobanco . $nummoeda . $fatorVencimento . $valor . $campoLivre;
        $dv = $this->digitoVerificadorBarra($aVerificarDv);

        $linha = $codigobanco . $nummoeda . $dv . $fatorVencimento . $valor . $campoLivre;

        $dadosboleto["codigo_barras"] = $this->htmCodigoBarras($linha);
        $dadosboleto["linha_digitavel"] = $this->montaLinhaDigitavel($linha);
        $dadosboleto["agencia_codigo"] = $agenciaCodigo;
        $dadosboleto["nosso_numero"] = $nossonumero;
        $dadosboleto["codigo_banco_com_dv"] = $codigoBancoComDv;
        $dadosboleto["campo_livre"] = $campoLivre;
        $dadosboleto["logoBanco"] = $this->logo();

        return $dadosboleto;
    }

    protected function digitoVerificadorBarra($num) {
        $modulo = $this->modulo11($num);
        if ($modulo['resto'] == 0 || $modulo['resto'] == 1 || $modulo['resto'] == 10) {
            $dv = 1;
        } else {
            $dv = 11 - $modulo['resto'];
        }

        return $dv;
    }

    protected function modulo10($num) {
        $numtotal10 = 0;
        $fator = 2;

        //  Separacao dos numeros.
        for ($i = strlen($num); $i > 0; $i--) {
            //  Pega cada numero isoladamente.
            $numeros[$i] = substr($num, $i - 1, 1);
            //  Efetua multiplicacao do numero pelo (falor 10).
            $temp = $numeros[$i] * $fator;
            $temp0 = 0;
            foreach (preg_split('// ', $temp, -1, PREG_SPLIT_NO_EMPTY) as $v) {
                $temp0 += $v;
            }
            $parcial10[$i] = $temp0; // $numeros[$i] * $fator;
            //  Monta sequencia para soma dos digitos no (modulo 10).
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                // Intercala fator de multiplicacao (modulo 10).
                $fator = 2;
            }
        }

        $remainder = $numtotal10 % 10;
        $digito = 10 - $remainder;

        // Make it zero if check digit is 10.
        $digito = ($digito == 10) ? 0 : $digito;

        return $digito;
    }

    public function modulo11($num, $base = 9) {
        $fator = 2;

        $soma = 0;
        // Separacao dos numeros.
        for ($i = strlen($num); $i > 0; $i--) {
            //  Pega cada numero isoladamente.
            $numeros[$i] = substr($num, $i - 1, 1);
            //  Efetua multiplicacao do numero pelo falor.
            $parcial[$i] = $numeros[$i] * $fator;
            //  Soma dos digitos.
            $soma += $parcial[$i];
            if ($fator == $base) {
                //  Restaura fator de multiplicacao para 2.
                $fator = 1;
            }
            $fator++;
        }
        $result = array(
            'digito' => ($soma * 10) % 11,
            // Remainder.
            'resto' => $soma % 11,
        );
        if ($result['digito'] == 10) {
            $result['digito'] = 0;
        }
        return $result;
    }
    
    public function nossoNumero($numero, $dadosConvenio) {
        $contaComDv = $dadosConvenio['fnc_convenio_conta'] . $dadosConvenio['fnc_convenio_conta_dv'];
        return $this->formataNumero($contaComDv, 8, 0) . $this->formataNumero($numero, 9, 0);
    }

    public function logo() {
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAoCAYAAAAcwQPnAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9bpUUqDlYQ6RCkOlkQFXHUKhShQqgVWnUwufQLmjQkKS6OgmvBwY/FqoOLs64OroIg+AHi5uak6CIl/i8ptIjx4Lgf7+497t4B/kaFqWbXOKBqlpFOJoRsblUIviKAEAYQxbDETH1OFFPwHF/38PH1Ls6zvM/9OXqVvMkAn0A8y3TDIt4gnt60dM77xBFWkhTic+Ixgy5I/Mh12eU3zkWH/TwzYmTS88QRYqHYwXIHs5KhEk8RxxRVo3x/1mWF8xZntVJjrXvyF4bz2soy12lGkcQiliBCgIwayqjAQpxWjRQTadpPePiHHL9ILplcZTByLKAKFZLjB/+D392ahckJNymcALpfbPtjBAjuAs26bX8f23bzBAg8A1da219tADOfpNfbWuwI6NsGLq7bmrwHXO4Ag0+6ZEiOFKDpLxSA9zP6phzQfwv0rLm9tfZx+gBkqKvUDXBwCIwWKXvd492hzt7+PdPq7wdXYHKc2I3cqAAAAAlwSFlzAAALEwAACxMBAJqcGAAAEwNJREFUeNrtXHlYVGXb/80GMwMz7MMq+yKgqAiIgqIYuaaZuX2YubQv9lpm+ZZmXn5aWunba11ZaWmWZq9LipmiZqiIC5ssIruAoCDbwOxn+f5AwcOZmTOgXeH7cV8XF/As9znnuX/PvT33OTyapmn0gqqbbiKl4AQ+LNkLkiBBkcBcSRTOFWbjqq6Jc/4Sv1hM9xAigbcF/LttNA1QAGiq6zdNARQNUBRAUh2/CRqwCf8BithkgMdDP/U94vUUWEpNG/ZkHcCq0p/Ao2joKBoiksZUq8HIKixkgeoF33hMHJoIXzdvuDm7QiKWQGwtBkWR0Gg10LfXw0ldDbIhB0Ttp6DoeouARVKAXeQBuETN6Jfiow6s/JvXsOzcR8jX3QZJ053AiqBdIK2mcLylDAAQLJJh7eOvIiEyHm7OrhbfDK1TQX8zC7qCrSBU+ziBRZKA64RM2PtH9kvyUQXW74WnsChrEwgaENJgAGuxIQabr50AAHw7/nXMGj8DchtZr2+KpgjoKi5AnfkWKP1ls8AieZEISE6FtcyxX5p9iPiWDNqfcwSLsj4x7iuJR2HztRNIsvND0bIfsWTaggcCFQDw+EKIA0bDbupxiNzXmB1LGbJw89x3/ZJ81DTWmZLzmHdx3T1dwtBYFEkj8fYAOInt8b+L34Oj3cPXGjRFoO3KbqivLTKqsQwEQJCA/5xSyN0D+iX6KGissoYKzLq8zmT/Ykkc7K1l2PjCh38JqO5pL3nMQkhCd5kdV3f5lwe+lk6vR2tbOyiKfmQFqtV1PENrWzvaVRr0MuhnEEGSUKk1nXy1Oj0nX6HJGzTosCp9s9nJIo0AHz+3BjIb2798weyi54NQ34G67E2j/a3FK6FtexFimUOPeWs0Wuz4zwm8lnIJAJDkIsMHz05CXHSExTxWbPoOjW1qRtv2ta8y/q+91YBVX+5jtMUPDsCiWRMfYDMYkJlXhPNZRTiSWYKzzcx7UAj4SI7wwZjIEMRHDYKzoz23laBpFJdXISOnCGm5pfil5Bbaum02hYCPxAGOiAz0RJC3GyJCA+Dv7cENrONFp5GqKjF58RA4IXns03CQ23PeZLtKA5mt1Owu4/N4sLISmVFdPDiMegm6+ksglHuNDmksy4Ln0PE9EgxF0Vi/bR/WXSjubEttaEPqJ/tw4R0BYiPDLeKTc6MeqQ1tTGB1G2MgSey4XsdoC/Zy6TWoSiuqsWLrPhysaTY5pp6ksDm7ApuzKyD77jh2LZqAaY/Fgc83nv9TqTX4dMdBfHC20Oy160kKeyvvYG/lHQC5OPz8ZAawjJrCNm07VhZ8bZbxS6FPIcx/IOfDHzmZDvmS9SgqvWHS/Cx4/wu8vO5rGAjCvN22ksBxzIcm+1vKM3osnLyiUgao7qfVO1KgNxj6pMkru3ETce99YxZULLlSNGZs/x0/Hko1sckorN/2CyeojJGzg5zbx0orS0cDpTXJJEBkjxlDJnNerF2lxtu7UzFzgCO+PXDK6JgLmQVoaNci52YTMq8WcfIUuwVD4rfJuDks39WRvu8BHf0z02RfakMbCq5X9DlQGQwEVm7dh3qSMjlGxjd9IrHg5zSjGz2vqAzrM4pNzouVS5DkIkO0TMzqc7CTmfexaJrG9yWHzD7Y8rAFkFpLORcgI7sQQfY22LwsGd5v/huv19yCj5cbwwxtP3wWy54aA7VWhx9/S0ds5CBOvsKBM6CpzoCVZj/z3olikIQBApGVRQJStqvwUVqB2TFpVwowbFBwnwLWlatF+KWqkdX+w9wEJMUPh5OjHQR8PtpVGhSWVODdrw/jTJOKMXbvb2exZqkPo+1i7nUWz7m+zli5cCqCfL0gkXQBiqQo6HR6tLS24U5zK7zcFeY1VlVTDc6oze/SUX4xFvkuO4+m48Un4jDA0xWr4wbixDmmdigqrcSvlQ0YMyICY0cMwda8KtyoucWtdj0DUC2IBMFjO9ckobfcDF4rYzilk1zl2DJpOGPM56dyodXp+xSwTl3MY7XtfWY85s9IgquLI4QCAXg8HmS2UowYFo5dqxazxn94vggtSqZPmFNSwxr36bJkRIQGMkAFAAI+H1KJGB5uLogIDYStjcQ8sIobysw+1BLFaCjkzpwPX1JRhd3l9YiP7tBAM5NisTblEpTtXTvn0KmL2DA5GvZyGdwUTlg1KgQn07MtWtwBIbGoEE4ylviyWEAnM5gCmpMwBHGRoYy2cj2BwuK+Yw4JgsS2dLbLMDUx1vRaebjik8eHstorqpmBRHmDkjXGVeH0cPJYRY2lZieM9oy2iPGxtExsShoKB7sOpy482B/RrnKkX8kHANxuaMJ7Z/IxKSGqc86TiTHYePQi1BotJ38v/zCUXv0M7bJXWKbcEmpRtmHNuWuMtqjBQQgN9GH5J2czC/sMsFqUbagxkIy2t4b7w0YqMTsvZnAQO/dXzywYkFoJ2JF2U8vDAVZ+S4nZCX6OPhY8fDtWn8jGlLFdoBEI+Hhp+hh8dfgcCJLEqfRsLB3iwwhRI0ID4COT4FLONc5r2DspIBAFoKxRDYqRUBVY9OC5hUzN7G8lRKDvANhIJXhzZEifNYdNLWytEuLNfdCvcGKnhWq7ASvEg62d/r07Bco21YMDK1dVaXaCm52Ck2l6Zj4mDXBCiL83o31kZDgy65XIyCrAx4fOI3lyHNMpFwrxyhOj8MOxdE7Nw+Px4R44DQ03foTGfmnXA1kIrOPpuYz/X04YBOu7ebSx0eF91hwquyVhAUAqtuaOpo2MaWhm+ljxQ0NYY9ZdKEb0a5uw7acUZOVdR1u7unfAKiOUZidIRGKz/SRF4duU81gyNQ58PpO9zFaKNdNiMe9f++EstcKwcHa0NTo6Ajuu16Hsxk3Om7e5C/J6XVdiVSDiXuSm5lZsuMjUzKOGdS3qoBA/1py+Yg71BDuvZiMVc84TCdgbrlWlYa79iAijqYRiLYGXfs3A8HU/QL5kPVZt2YULmfnQ6Q2WA4sT+RzAKiyuwPm6VsQOCzPa/1jcMNQYSCydHg+RiJ34d3K0w4bEwTh+Not7scQ2HZHstR9BCqMhdX8FPD73I+VcY/uR4UFdYHJ2tMfzAz36pDnU69lJZEtqaIVCNrD03Xw1ua0Ndr37DMIkIrO81l0oxqiNezFj+WZkFxRbBixfgYzDBPE4Eo5XsHpSFOQyG6P9urvCaVebdtCnJERh7bErjAjSHNFUIwz2Y2EfMNaypOjZHMb/y4b5wU7OPO+cGj+kT5pDgiQfGi+Vlr1RBgb64PSm17BuLPdR1rHbSkSu3YXT5zO5gTVKbv6YRq0zbWMbm1qx8nQeJsQPMx3ip+cgTCLCv1IyoNMb1wBhQX6Ic7dDRpZ586NRdiUJNZDAKWA452I0NDbjsywmQJJGsBdxSFiA0WTp300SC/wpS8lBZjySdHVxwnsvz0PV5texJzkRk1zlZvk8ufUQ6m7fMQ+sgfb+Zpk0q1tN9p29fBULg9wQ4OtlfIeoNdjweya2L30aAJCdbzwCFQj4WDJlFLannAdFmc5LNd/uUsNaige5mx/nYmYXsM2gUqVBemYe46emtp6VdtiQmmNRKuSvJL4Ri6E3EJzzNEbMuFgkNJ8r9HDF3GmJSNmyHKUbX8ZPyeMQacsGdhtF41S3/COLc6hzEGBG41c118DTwd1o3/fHL2Hp02NNmsv86+XwsbFGVEQIXpscg1MXr5qsHhg1fBCmbjuKj2tvw9eLfT29ToOmm3u6QOvgbdEbO7/+yfbd5v5wyiKh1pMUCoorED0k9G8DVvcMNwCLfD9jG9ReJrUMzHw+Anw8EeDjiSnjYvHB1j3YksPMHpzJKcH8GUlmNJYi0OxFrjeYRl11qxoUSZtMFej0BihsraHR6kDTNFrbTe9+8q4vodUaX7SGuqquxVZMxzkldxrkVn0jvsyvfiDB/nn57zWHDnK2D1x3hzuJ2aJsZ7V5KnpenCmX2eCN+VNY7X+U15vXWG52rphpNwT7W3ONMi6vbwRFUaxUAgB8tHAikrYeArZ2HWJ/ND4C77wwG0tWf9FZi3RwyYbO/k1z3kflZ6+huaUNw9buZPBbER2AQBNmtazgUufflR7joCaEFpjBkgcW7KbTV/FK8lRIJeKHAhStrmdlOQ72bGCdvFqB5RRtssYKAMpu1LFzks4OvbpnZ0c7Ni66RatGpTEv+Ansv2wcWCq9HkVVNxHmO4DVlzQ6GrcGBnQebh4+fQnUfdprT3IihoUHMHyDiA+6XoSIlUvw/bvzO1IJIhF8PN0gELABbDDoUZjeMc8u4l0sa/LEKwruxOj+M0wzGCwWYu/b/2M20j146iLWni96KOZQaGQz3mpq6xEPG6kEs32cse9Gl7Oc2tCG4vIqDAw0fipCEAT2/cF2Abw8eldkaCwTn+Qi4wbWCN8oDM1VIEdfz478yEb8ciEbHxgBVkdE4QhXlw4V65FXgpr7jg28PV0QEuDDcOYZCVRrIaPfFJXmX4K27RzsfJ7FuvaOCFQhNR8t1d5qwPaiWkbbiwmDOUti9AYDA1j3zGFvgGVtzS7n+aqwBu/V1bPKTszRU2OGYF83v/DDbw7i8xXPwsXJgQWqnQdSWWU2ic628PZwY7ofFAUBRx7QYCCw5+ifrPYofzduYIlF1nh/yAt42siLFJd1pdhZ6oJ51bUIHuBhfiHvKzU29oICeV+hGg3LDo8Neh3Sj3wKO8+5+E48AeV6KwAE/JzM598yjUSgI4eEcF4vNND3oZlDezsZwiQiFGqY5i9hxZd4Pj4UXgpHk9ozISYCXh6Kzr/RDVh7K+/g3BtbsPyxofBxd4a1lQj1Ta04cqEA+6vZnzxYOj2eZQ1+O30B638+gxg/Bfw9nCG3kcBGYg2J2AoGgkSzUoWD5/Nx9BY7M5AwPJQbWAAQHxCLheWj8H1jOjMyQhNmOcnwccoZfPX8bIiEpn0bD4UjNuxPg9T6EPaX3cY/uxXy29pIMUEhx1d7f0dLuxaxQR6cwrmSdgQCG2fstJmIszobAB1Ovp+LnekEKk1j36kr7HxZsC/n9WS2Urw/MphRvtxbcygUCPCPiVF44eAFln+y8nSeef8wyKcTWG4KJ+yeNxbz95xhjKkxkPjHsUzO+5g5wBGPj45iBwENzchQapCRewPIvWHxcyW5yBAfPdh8Hqszl8QX4O2RLyFAyE6ODXQVY8etRvz850WzFxwRGY5Pn5sKRztbnHxnHoL8BnQLY3nY8f5iDA3xQVJMGJYtfNIsv5uVxSiqzMMG6UycIZhACnAzXTdUU1eP3d2ilreG+8NOZtnbReOiwx9adDhrcgKme/bOab6f5kwdi01JQ3s8b5KrHJ+/vYBVuAcAlXWNPeYXLRNj24pnWGU7Zg2qQu6Cb+PXsNpbBR0RxjNpWTifZ7pOXcDnY+zIYUh+8jHEDDV+dujh6ow5U8fhqUljWHXTTCe3Ed9kXMJicjyKwVyUmXYSuDmZzg5fyWOfZz0WE27x4g0K8TdqDnuTLLWX22Lnmhfx3ax4o8lGi7WfUIg3F8/EueWzMduHu/DS30qInXPGYO/6V+Hhanz8nVbLy2PCJCJ8M2Mkft/0Ovy82ZbGom83XK7MxrRz/8S9N6GbKAGSGmbgeFuH831x7kTEhAb9ZbmbBqUWz/9WgF/btABF3v2QA9HxQ5E4HBeIJ2JM+0vF5VVQa3TMhfb2MHmeaYyuXitl+YkBPh6Q2XbwyCsqY/iMADA03PyaGAgCDY0taG5VQq3Rdb6lJLG2Bo/Hg0gogFAoAJ/Hh4ebs8liPpqmUXf7Dqrr6nGnWQmVWgsDQcJWKoa93AbuCif4eLnB2or7XQBluwoNjS2409SCFqUKWp0eaq0OAj4f1lYiODnI4ersAG9PN4YP3StgAcDVmgIsOrsaVYQKQhp4TjoP6/K6SmyOTRmNCdFDOA+pe0pl9Uq8erIUxzX6u6BiAssLFPKfS4SdjRj91HfI4rKZCK9wpEz+EnMcOg56D6uPI/E+YU46ehYb/3MMLe2qh3JjBEkhJa8Wo45dx3Gt6bOwTVG+/aDqg9TjD68RJIHT19OwOvsrJFglYmM+M3SOtBZhbeIIjBsaZlFlY3eiaBpXa1rwZXYtvmnWdn0gi6ZYGmuCNR8HFoyG1FrUL8lHHVidyU2dCmnFF5CW24yPStivbEVai7A0KhwjQwPg7+5qtNCsy0cAbik1yKlVYtf1Ruxt09/9nB9tFlj50yIQ7q3ol+J/E7DukZ4gsfFUAQhVE5qVzbh2pwmp3eqy4yTWmB0eDIWLOyQye4iFfGgJGu16AhVKHf5o1OC0jrz7EVK6C1BmgHUgygszogP7JfjfCiwAaFTpseSPG/hVc7e6kSIRDApCHo1CWgDw7nfl6Pt+3QMR3aW6LADW1wNd8NyYgQ89UOinPgYsAGhS6fFGWjV2txvYdo7Z8EDA2hmuQHJsIAT8flD9vwAWALTrCHxxpQ7v1qoeOrC8eDR2j/RGwkD3fqk9AvR/R6FdBIXlSYwAAAAASUVORK5CYII=';
    }

}
