<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class Santander extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {
        $dadosboleto = [];
        $codigobanco = '033';
        $codigo_banco_com_dv = '033-7';
        $nummoeda = '9';
        $fixo = "9";   // Numero fixo para a posição 05-05
        $ios = "0";   // IOS - somente para Seguradoras (Se 7% informar 7, limitado 9%)

        $fatorVencimento = $this->fatorVencimento($dadosLancamento["fnc_parcela_vencimento"]);

        //valor tem 10 digitos, sem virgula
        $valorParcela = str_replace('.', ',', $dadosLancamento["fnc_parcela_valor"]);
        $valor = $this->formataNumero($valorParcela, 10, 0, "valor");

        //carteira
        $carteira = $dadosConvenio['fnc_convenio_carteira'];

        $codigoCliente = $this->formataNumero($dadosConvenio["fnc_convenio_numero"], 7, 0);

        //nosso número (sem dv) é 11 digitos
        $nnum = $this->formataNumero($dadosLancamento["fnc_parcela_nosso_numero"], 8, 0);

        // nosso número (com dvs) são 13 digitos
        $nossonumero = "00000" . $nnum;

        //agencia é sempre 4 digitos
        $agencia = $this->formataNumero($dadosConvenio["fnc_convenio_agencia"], 4, 0);

        //conta é sempre 8 digitos
        $conta = $this->formataNumero($dadosConvenio["fnc_convenio_conta"], 8, 0);

        //agencia e conta
        $agenciaCodigo = $agencia . "-" . $this->modulo11($agencia) . " / " . $conta . "-" . $this->modulo11($conta);

        $barra = "$codigobanco$nummoeda$fatorVencimento$valor$fixo$codigoCliente$nossonumero$ios$carteira";
        $dv = $this->digitoVerificadorBarra($barra);
        $linha = substr($barra, 0, 4) . $dv . substr($barra, 4);

        $dadosboleto["codigo_barras"] = $this->htmCodigoBarras($linha);
        $dadosboleto["linha_digitavel"] = $this->montaLinhaDigitavel($linha);
        $dadosboleto["agencia_codigo"] = $agenciaCodigo;
        $dadosboleto["nosso_numero"] = $nossonumero;
        $dadosboleto["codigo_banco_com_dv"] = $codigo_banco_com_dv;
        $dadosboleto["logoBanco"] = $this->logo();

        return $dadosboleto;
    }

    protected function digitoVerificadorBarra($barra) {
        $resto2 = $this->modulo11($barra, 9, 1);
        if ($resto2 == 0 || $resto2 == 1 || $resto2 == 10) {
            $dv = 1;
        } else {
            $dv = 11 - $resto2;
        }
        return $dv;
    }

    protected function modulo10($num) {
        $numtotal10 = 0;
        $fator = 2;

        // Separacao dos numeros
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo (falor 10)
            // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Itaú
            $temp = $numeros[$i] * $fator;
            $temp0 = 0;
            foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                $temp0 += $v;
            }
            $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            // monta sequencia para soma dos digitos no (modulo 10)
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }

        // várias linhas removidas, vide função original
        // Calculo do modulo 10
        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
    }

    public function modulo11Invertido($num) {  // Calculo de Modulo 11 "Invertido" (com pesos de 9 a 2  e não de 2 a 9)
        $ftini = 2;
        $fator = $ftfim = 9;
        $soma = 0;

        for ($i = strlen($num); $i > 0; $i--) {
            $soma += substr($num, $i - 1, 1) * $fator;
            if (--$fator < $ftini) {
                $fator = $ftfim;
            }
        }

        $digito = $soma % 11;

        if ($digito > 9) {
            $digito = 0;
        }

        return $digito;
    }

    public function modulo11($num, $base = 9, $r = 0) {
        /**
         *   Autor:
         *           Pablo Costa <pablo@users.sourceforge.net>
         *
         *   Função:
         *    Calculo do Modulo 11 para geracao do digito verificador 
         *    de boletos bancarios conforme documentos obtidos 
         *    da Febraban - www.febraban.org.br 
         *
         *   Entrada:
         *     $num: string numérica para a qual se deseja calcularo digito verificador;
         *     $base: valor maximo de multiplicacao [2-$base]
         *     $r: quando especificado um devolve somente o resto
         *
         *   Saída:
         *     Retorna o Digito verificador.
         *
         *   Observações:
         *     - Script desenvolvido sem nenhum reaproveitamento de código pré existente.
         *     - Assume-se que a verificação do formato das variáveis de entrada é feita antes da execução deste script.
         */
        $soma = 0;
        $fator = 2;

        /* Separacao dos numeros */
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo falor
            $parcial[$i] = $numeros[$i] * $fator;
            // Soma dos digitos
            $soma += $parcial[$i];
            if ($fator == $base) {
                // restaura fator de multiplicacao para 2 
                $fator = 1;
            }
            $fator++;
        }

        /* Calculo do modulo 11 */
        if ($r == 0) {
            $soma *= 10;
            $digito = $soma % 11;
            if ($digito == 10) {
                $digito = 0;
            }
            return $digito;
        } elseif ($r == 1) {
            $resto = $soma % 11;
            return $resto;
        }
    }

    public function nossoNumero($numero, $dadosConvenio) {
        return $numero . $this->modulo11($numero);
    }

    public function logo() {
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJAAAAAkCAYAAABmHbPbAAAABHNCSVQICAgIfAhkiAAAABl0RVh0U29mdHdhcmUAZ25vbWUtc2NyZWVuc2hvdO8Dvz4AAAAmdEVYdENyZWF0aW9uIFRpbWUAc2VnIDAzIGp1biAyMDI0IDA5OjI4OjAwZ7H7bQAAEeVJREFUeJztm3l4ldWdxz/nvNtdcm/2hSQQQNkUAoiiCAYER7EIVGx1RKltp9NaKyp1tJ221LbWzjOty8x0Gbv6dK+1OjrWdlqnPGqVqi1YVlskCoEiSxaSe3OXdzln/nhvEgKBhJIp9mm+z3MJ933P+zvL+z2/8/39zrniT9PO1oxgBCeJiu9+H3v6dOTpbsgI/roxQqARnBJGCDSCU8LwE8j30fk8CDHspkfw1sPwEkhrnLlziV99NTqdGlbTI3hrYvgJNH8B9owZ6Fx2WE2P4K0Jc3itmTiN01FuHmFHhtX0CN6aGB4PpDUoBWgwJNbkyZQ/9FCog/Qwppm0BqXDv8Np968JWkMQoN08srQUWVqKiJy+yTosBBK2jT11KrguQWsrMhrDnnUuMpnse+GnAt+HIAApwTBCYioVXldqOLrw1wNDIuJxzPrRRJcuxT7nHGRpyWlrzrAQSJaWEr/2WnQuh//GG2jfRyaSWBMnIRz71AikFVgWsrwMs74ec8wYjPrRGJUVCMcB428pE6ERjoNZW0ukaT7Jm24m0tSELC07bS0aFg1kTpxIdPmVqA/dhPviizizZ2OddTZF73433Y5F7tln+bO4qjU6nyP29hXErr4Go7QM0BCJoloPkX3qp7ivvIK7bSvCtIajK29taIUsLsY8cwLmpEmIaDT0xvL0pUxOnUC+jzVuHABln/8Ch+/+DO6OPxBbfiXJ1bdgjh2HPXMWXfffC5Z9EoY1RmUlpV+4FxGN0HH7hwnaO0AphONgT59Ocs3t2I3Tabvpg4iSv4wb156HsE4TWYVBsP8A2ZYnyTzyI2LLloEQiNOYczt1/y8lQVsbAJGmBZi1tajWVrJPPYnq6sIYVYvVOB1zwsSTW8p8H2vKWZjjz8DdsAF32zZUqguVyRC0HiL/wgt4O/4Y6qK/FLTCHDPmL1ff8SAlGCZoAFH4nKam/NlPFqIBhCDYuxeVTmPU1uKcPwdhWfjNzQQtu9EqwKiqwplzYSh4h0gi7XnYjdMRloVqa0MYZuFjIGwH7bq4Gzegc7mBs956CNHakfdOVL5wXVgWkUWLTvy+hrPe49jWWhXIw4n5M1TbPfd7o+mh4+QIpDU6n0XnspijRhG//npkZQXepk20Xns1SEnJZ++h8seP4lxwAR0f+wjZn/43Rk01satWYM+ciUwmwM3TNwLHa5lBsP9NtApI3PZhij/9aXAsVHcKVACmSfcPfkDHP38UeeTyFQToTAajphqjdhQimUD7br9B1Pk8ujuNiDgI20Z3dyPjcYxRo5DxWP8kqBAQcXCamqj6+S9Irr41fL7w6RkXfA+dy2DW12HU1CDiMfC8/n3yXHR3GlkURzg2OtONiDiY9fXIRBKd6R5gzFVoO5PGqKnGbBiDWVuLiDqF9snwc2R7gwA8F6OyAnNsAzJRhM5m+pPD99HZDEIKZGUloDDKS7EmTjipJXroGigI0IGPNfksYsuWY8+YGb6k+jo6P7kW1byT7u99l/j1qzCqqyi68SY671qLamtH513M0Q0Ur70Lv2U33saNpB76BkKafWH5URCWRf63vyV++DAyWUz08rch40Vkn/4l7ksvotLh4Asp+2ZZECBiMSIXX0zsHe9ExONoz6PjttXoXD4sI8A680zs82Zjz5pF9/e+gz1lCpHLr0DEY+i2VjJP/5LM448jDAOdz1F8xx0451+ALCsHISi5+56wjRo67/1XVCoVetlZs4iteAfEYqj2djo/8ynU4cP0uAhjVC2RBQuINM2n84H7iV7yd8SWLUcUl6C6usg9s470d76NMM3eZxACWVFB9LLLcObNh8BDBwFCabCP1pQanc8jk8Uk7/wIsiiBsC1kURGZp54i8+gjvQST5eVE5s3DvugiguZmdBAQWXAxwjRJffVBcuvWhe9mEBi3V1d96oQlhEB7HjIWI7r4cuIrrsKZOw/rjDOQJaXIsjLyL72MOvAmKpUmsmABMlmMLCsn+1+PYY4bjz1tGjKZxCgvx6ioxKitQ2czqEOtaK/gHY4mkRDoXA6jvBxr6lRkIomsrMSsq0dWV6NzWdShQ6EeEAIMidnQQGTBxThNTXjbtiMdB2vceAgC1L596FwOmUgSu3Yl0YWLsKdNg8DHGFUTimME9rnnYlRX423cSNDRDn5AdNElGJWVGDU1EAR4mzeHXjAIyP36OexJk4ksWIg9Yybe1i0Y5RVY48YhIlHc9etDoes4xN91A9FLL8WaOg1yWYyxY8Hz0VrjzJiJMaoGb+tW1MFCv7TGmjKF+NuvJHrpZbgvvYS/cyfBwYMIKbBnzMR/7TW87dsI9u8HrTGra3CamrBnzMB9+WWClt3Iigqc82aj2tvxdu7ASBYTvWIJsSuWYU+fGXqhklKMsjJkshi/eWfYxxPoy9iKq0JPO9iJRO26WI2NFN98C05T04Blgn37aL99Dd6mTcRXriSx+lZkcTHtt9yMc955xFfdMOBzqq2Nzs99lvxzz6F6loOj689msCZPoegf30/0ssVh7gdAa7r+/QHSX/8aGCbmmDHErrqK6JKlpL/yZdI//D5G/Wiil1xCcvWtdHz0TtxXNqKyWYRlUfXETzGqquh64D5SX/4SCFDZLGVfuI/4davIPv1L2v7hPcjSMmRxkujixRR/bC06nWbfzOm9es6cMIHkmtsx6+vp/tEP6X7kYUQsRuK97yOx+hYOLFxA0NYOKIzKKip/8hgymaTr/vtIfeVLaBF6ssQtt5G8bQ2ZR39C1333otJpVFcXo15YD7ZD6j8eoPvhHyMsC+266K4u6t7YTW7dr+j+8cO4v/89Rnk5yTvvxJkzl32NZ/dOTHVgP/UH2wFovX4lfksLQgrK/u2LOPPnow4e5OCyJajDh5ElJRijx+C9+uoJCdRzInHQJcyoKKfk45/AmjDx+GWqqym7934OXHYJuReeR1ZWkfjAjSRu/CCytPS4z8nychK33YYoKSHzyCMDlhGxGN5rr9Gx5layixYRv2YlTlMTwrKIX/33GKWldNz1SfA9VEcHwd49Bc/ho7vTqFQK4TgI2wwHxA9Aha5eK4W/axciEgXTRDoRcs//mvh1qzBr68D3QvEcjYFZWC6EQBTFIQj1hE6n0V2HUR1RVHsr+AGqtRXV1Rn2sbKCoLMT8j6qsxMCHwBvxx8RRUWIwlLl79wZLsFFRYhoBLq6iMyfj1EzCm/nDrI//5++LQvLCtvQ47mlRLsuTlMT5rgzELEoJXd9ChGPh2103dB2NEpk8WJS998PEQeVSYd1795N0N6BcByCVJpg69Yh66DBCdTQgFk/OkxaHbeQgVFdTfKOO+j83D3kn1lH4gM3Yp45ATFIptiorMY648xjbwiBzmYRth12xjTJv/Qy3patOOefT/ITazGqqohcehnmN79JcKiVzKOPknn8cYID+4ksXIg9dRr2zHPQaDSiL2KRYe5EAFhWXxSjAWEc8WUg9A97VCpF6sEHQSv8AweILr4cc9w4nIsuKhTQfbYMo/e/wjQJ3U/BkBS9Gi1czlXY9mwW1dZ+fD0iCjbcfBiklJehuzMEe/ZCtG+PLP31r6IBb/v2UDtJiSiIb2HIXikgDOOkUiODEkjG4oVBHiTXYJpEFl5C+uvfQB3uRKVSyERiCC0wEbHYMZe15+LMmoXXvDMUwEKAH87i/G9+Q+rB/6Tk42sRiQRmXR3uls0AGDU1OPPnY9bWgueF2gBRGKywD1of8VKl7B8S90RrR/e39zpH8gfteejuNEbDWOJNCzAqKggO7A/1Wa8dcYSZguA/mhC9EVJfWVlSEpZXA2jEfhBopZCJJMKyIQjIPPVkwfsXCOb5oDUqlyuQVxXGYYC+nkRiclACqbY2dDYL8figqtwcPYbo0qXk1q8fGnkA8nmCN/cdc1mn0yRuupn0N79GfuMr4cVC/SqbJfvYYxStuiGccYJwc3H5cuLXXofqaOPgkrchkgmchYuIvn1F/0HpOTkACCFCx9NbcfgiRT+W9BFOH5UnMWpqSN58C878+fjNzbSuvAbtuohojOgVS09AxKOu99jt9UAC1daKLCrCqKs9ds9P9/wTlhWWgb9nD9bkKRg1NWjXJXhzf/96jqz7iDmEHDzaOh4G9VVe805yzz1H0No6JIOxpctJfmj1kBuQ/vZDZJ54YuCblklk8dsK+ZEjk2/h7JHJJLqrC2/DBpx584hcvDCcfY8/jva80E0HCgRo10Nnc6E3A+jlwZFLVSGHctQt7XvodJijkUVFvfkcnU4Ru2wx1tlno1pbyf3v0+GSW2gjFHJO2e6+aFOrftX14silDkBIsuvWhWF8WTnFN96E6s6EesbNoTPpMP+jNXguwoqQ/dlT+Hv3gmGQvHUN2s2jc1l0IeeDCgqb24U6evoqxFHjMHQMngcKFF3/cg/B7jdI/tOdgxa3Jk/GmjRp0HKqq4uu+z5P7plnUa2tA3g3gRAGsWXL8d94nfS3voUOfISQCMem5DN34+/dS+rLX0RrXRhYF5FIEFuylOzPnsKedS7RxZeHGeQLL8SoqcF7bQf59S+gzYKoNix0JoNIJtG5bK9Q1Y6NymaRQYBqbcP9w6u9LSt6/434zc2Y48ZCNg++h9HQQGThQnLPPoMzezaRhYsAiF9zDf7rr5P/3e8IWnb3Jv2E46C7u8NcVSaDFgKEDD2i70Pg47/6Kvn1z+PMmUvs2pVk1/0Kf+9eZDKJM+cCRCSCNW0asdwKgkMH8Zt3kn/h1+HpiFXvQpSVkX/6F7ibt2BPmkhs1Q0cvvvT6D/tC/tqF4SyaYDrooVE2Cd3emJIeSB8j/yGDQS7d2HW1WFUVZ24/AlEmPZ9gpbdtL5rFfnnn4dcfsClUXse9nnnYk2cRGThIqJLlmA3Tie25AqSaz6MeeYEOj+5lvz6FxCRaJiwsx3saY3hlsqcCyEICFpasKdPR1gmursbnU5R9N73hdskhoE1fjzCsfG2bCYyfwHFH/kosrgEGY1hNjTgv/E6urMTnU7jv95MZO5cnDlzsBsbUa2tZB7+Eea48VhTpmCOHYdz/vmoA/tRqa5QBHthdKgOHiDxoZuxzj4LIQ2syZMRQuJt2UT8uutI3HADsrISmUhiNowlaNmNSnXi/XEHKIU5bjzxq68h/o534sybBwhkUREyFkcHPtmf/wzd3Y23aRP+nhaiiy/HnjqV6JIriF+7EmfuPHJPPkH+pReRJSUUvfs9RK9cEW4NFRWFWlxK/JbdhUTmiTHkPFDfG9XoTJjyt86bTXTBxVjnnIM1/owTR2hahy40nSL7xBPkXn4Jd+sW1MFDIduP/yAIgVlXj9nQEEZ08RiqsxPvtR34mzYRdHb1hbYqQEQcjKpqzImT8LZtI9j3JxBgjh2Pv7cFnckgY/FwyclkwhC9EOW5W7ZgNjQgotFwi0JKZDKJv7MZlUqF/cjlMEbXY5SUoNrbw+XCspDJJGZdHWb9aPKbfo9qPQS2jXXmRLzNm9C+QsQi2DPPQaW6evfVhG3jbt6MPa0RHYS/ZhGmibAdgv37CQ4eDHcAtMaoq8NubESnU/h79uDv2oU9dRqq8zDBm2+GK5BhhMu752E2NGA1jEUkEqiuTtzt21H79yPiRaHnmjgh9HxKhRGpE0Ed7sDfsxuMwU9N9OSBhk6gHiiFdnPgB+EMqKlBVtdgVFVhjh6DUV4Rvn43i7f9VYK2tnAGHj6Mv3dPGCaa1pBYjtahlimcSNSEohfTCKONASIZ7fvgeyE5jbAO7XlhfYXsrnbdY6oSjhPWdZRIFpbV36P6XiikpdHXB6XA89C+HxJa9mkTbKf3aO9AP3cSjtO3p9avXrNP3GqNDnzIu6Ft00SYJtrLI4QBpnmsKPdctOeHmktKsOy+9g40BgKEDN/NUPTQkBOJx0BKRCRWaIfG37MHdu2CIBS2PQJfFBqFkOFAGBIZLzq5uoQIiVDwVIMGl1L2K99r5sjvhW2FAasbSvLMtI5th5TgOP3tCgFOpN/3451dPl57+j1rWoWXe8TlE/1wwbLDSXY8e8et8+T8yakdKBMibGShXz0DO/KTwr8d/C0dKB7B/wNGCDSCU8IIgUZwShgh0AhOCSMEGsEpYYRAIzgl/B/xAorn6Af8gwAAAABJRU5ErkJggg==';
    }

}
