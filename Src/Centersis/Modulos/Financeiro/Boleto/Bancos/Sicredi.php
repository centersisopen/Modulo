<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class Sicredi extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {
        $dadosboleto = [];
        $codigobanco = $dadosConvenio['fnc_convenio_banco'];
        $codigoBancoComDv = '748-X';
        $nummoeda = '9';

        $fatorVencimento = $this->fatorVencimento($dadosLancamento["fnc_parcela_vencimento"]);

        $valorParcela = str_replace('.', ',', $dadosLancamento["fnc_parcela_valor"]);
        $valor = $this->formataNumero($valorParcela, 10, 0, "valor");

        $agencia = $this->formataNumero($dadosConvenio["fnc_convenio_agencia"], 4, 0);

        $posto = $this->formataNumero($dadosConvenio["fnc_convenio_posto"], 2, 0);

        $convenio = $this->formataNumero($dadosConvenio["fnc_convenio_numero"], 5, 0);

        //fillers - zeros Obs: filler1 contera 1 quando houver valor expresso no campo valor
        $filler1 = 1;
        $filler2 = 0;

        $tipoCobranca = 1;

        $tipoCarteira = $dadosConvenio["fnc_convenio_carteira"];

        $nnumComDv = $dadosLancamento["fnc_parcela_nosso_numero"];

        $campoLivre = "$tipoCobranca$tipoCarteira$nnumComDv$agencia$posto$convenio$filler1$filler2";
        $campoLivreDv = $campoLivre . $this->digitoVerificadorCampoLivre($campoLivre);

        $dv = $this->digitoVerificadorBarra("$codigobanco$nummoeda$fatorVencimento$valor$campoLivreDv");

        $linha = "$codigobanco$nummoeda$dv$fatorVencimento$valor$campoLivreDv";

        $nossonumero = substr($nnumComDv, 0, 2) . '/' . substr($nnumComDv, 2, 6) . '-' . substr($nnumComDv, 8, 1);
        $agenciaCodigo = $agencia . "." . $posto . "." . $this->formataNumero($dadosConvenio["fnc_convenio_numero"], 5, 0);

        $dadosboleto["codigo_barras"] = $this->htmCodigoBarras($linha);
        $dadosboleto["linha_digitavel"] = $this->montaLinhaDigitavel($linha);
        $dadosboleto["agencia_codigo"] = $agenciaCodigo;
        $dadosboleto["nosso_numero"] = $nossonumero;
        $dadosboleto["codigo_banco_com_dv"] = $codigoBancoComDv;
        $dadosboleto["campo_livre"] = $campoLivreDv;
        $dadosboleto["logoBanco"] = $this->logo();

        return $dadosboleto;
    }

    public function digitoVerificadorNossoNumero($numero) {
        $resto2 = $this->modulo11($numero, 9, 1);

        $digito = 11 - $resto2;
        if ($digito > 9) {
            return 0;
        } else {
            return $digito;
        }
    }

    protected function digitoVerificadorCampoLivre($numero) {
        $resto2 = $this->modulo11($numero, 9, 1);

        if ($resto2 <= 1) {
            return 0;
        } else {
            return (11 - $resto2);
        }
    }

    protected function digitoVerificadorBarra($numero) {
        $resto2 = $this->modulo11($numero, 9, 1);
        $digito = 11 - $resto2;

        if ($digito <= 1 || $digito >= 10) {
            return 1;
        } else {
            return $digito;
        }
    }

    protected function modulo10($num) {
        $numtotal10 = 0;
        $fator = 2;

        for ($i = strlen($num); $i > 0; $i--) {
            $numeros[$i] = substr($num, $i - 1, 1);
            $temp = $numeros[$i] * $fator;
            $temp0 = 0;
            foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                $temp0 += $v;
            }
            $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }

        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
    }

    protected function modulo11($num, $base = 9, $r = 0) {
        $soma = 0;
        $fator = 2;

        for ($i = strlen($num); $i > 0; $i--) {

            $numeros[$i] = substr($num, $i - 1, 1);
            $parcial[$i] = $numeros[$i] * $fator;
            $soma += $parcial[$i];

            if ($fator == $base) {
                $fator = 1;
            }

            $fator++;
        }

        if ($r == 0) {
            $soma *= 10;
            $digito = $soma % 11;
            return $digito;
        } elseif ($r == 1) {
            $rDiv = (int) ($soma / 11);
            $digito = ($soma - ($rDiv * 11));
            return $digito;
        }
    }

    protected function montaLinhaDigitavel($codigo) {
        $p1 = substr($codigo, 0, 4);
        $p2 = substr($codigo, 19, 5);
        $p3 = $this->modulo10("$p1$p2");
        $p4 = "$p1$p2$p3";
        $p5 = substr($p4, 0, 5);
        $p6 = substr($p4, 5);
        $campo1 = "$p5.$p6";

        $p1 = substr($codigo, 24, 10);
        $p2 = $this->modulo10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo2 = "$p4.$p5";

        $p1 = substr($codigo, 34, 10);
        $p2 = $this->modulo10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo3 = "$p4.$p5";

        $campo4 = substr($codigo, 4, 1);

        $p1 = substr($codigo, 5, 4);
        $p2 = substr($codigo, 9, 10);
        $campo5 = "$p1$p2";

        return "$campo1 $campo2 $campo3 $campo4 $campo5";
    }

    public function nossoNumero($numero, $dadosConvenio) {
        $nn = date('y') . $dadosConvenio['fnc_convenio_byte_idt'] . $this->formataNumero($numero, 5, 0);

        $agrupamento = $dadosConvenio['fnc_convenio_agencia'] . $this->formataNumero($dadosConvenio['fnc_convenio_posto'], 2, 0) . $this->formataNumero($dadosConvenio['fnc_convenio_numero'], 5, 0) . $nn;

        return $nn . $this->digitoVerificadorNossoNumero($agrupamento);
    }

    public function logo() {
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJQAAAApCAYAAADTaAB/AAAABHNCSVQICAgIfAhkiAAAABl0RVh0U29mdHdhcmUAZ25vbWUtc2NyZWVuc2hvdO8Dvz4AAAAmdEVYdENyZWF0aW9uIFRpbWUAcXVpIDA5IGRleiAyMDIxIDIyOjM4OjM2OWetewAAGDFJREFUeJztnHmQHNWd5z+ZWZV130ffh1p3d0stqaUWYwQIMOBlMGMLMNhm8HpiZljPbMTYs7ZjTGyEwbvhcMx4jxjAGGww18B6JmwtCGvMpV2DBULoQrTObkmtPqq7q7uurivryMz9I6tL3a1uScg2ErP6RlR0V1Xmy8z3vu/3fr/v7/dK0HVd5yIwfZogCPN8q6GVdU4N9FEqa+zdu5dgMITH42b9+g2YTOZ5ztcrL/FibucKLhNc1OjNJIOu67NemqbxzDPPcMtnbuLJJ59kfHycYDBMS0sL6XSaeDxWPXfm+XPbv0ieX8ElhuliTpq2KvMNuiiK5HIK77yziy984U6uvfZaQKRQKJBOT80ikaqqaJqGJEmIooAoCrPa1HV9AQt4BZcr/iDry6ZNm/AHfLz11m9nEWSaQADZbJY333ydJ5/8Ke+99x6JRAJNm98qXbFYnxxc9JI386Wq6qwBb25uZnFbGwcOHCCZTM44U6heMpGI879efJGnf/YzstksmUyWROLMsdNL4hV8svA7WahIJMKHH37I3r17icfjVevjdrvZsGEDJ0+e5NChQzPOMJYwTdMYHR3l6NGjrFrVyYYNG7BYLIyPj5NKpc6ySFfI9cnBRRFKEKBcLvEv//JzHnjg7zhwYB8jIyMoilIlwtq16ygUiuzfv3/GeQYpstkMBz84gKIoXHfddXi9Hvx+P06nk2g0Si6X+z082hVcClz0knfw4Ae8/PLL+Lw+rr/+ejweFyPDwxSLRQC6urrw+by8s3MnmqqdiQxFgYlolLffeptgMEj3+vUASJKE1WqlVCqRz+c/skWa68xf8bkuDS6KULlslm0vv0xscpIvfunLtLQsoq6uoSILxNF1ndZFS1i+fDkH9+0lHoshCDogIAg6A6dOcfDAflZ3ddHWtgQQyOfzTE1N4fF48Pl8wNm+2nyYS6SFjrlCsI8HH5lQqqry7rvv8pvf/IZrr72WNWu6kGUZWZZpaW0mEhlGURRsNgtr1qxhMDJKX39f5VICxUKRAwcOkM8rdHd3Y7PZKJfLTE5OUi6XCQQCmEznVzNmkmQheeEKiT5+fGRC5bJZXn/9dVKpFDfeeCOhUAi9MpaBQBCPx8Pp06dQ1RLXXHMNhUKRPXt2GxcTReKxGO+9t4uamhq613WjqirJZJJCoUA4HMZmswFnk2Gm9rUQUaaPmUmuK6T6ePGRCWUymenp6aGjo4O3336b99/fTTadqX7f0tJKsVgilUqwuqsLr9fDnj170HUNSRI53tdH/4kTrFrVSVNTE7lcjng8jtvtri51M3GhEd60fDFT65rbzhX84fGRlXKrzcaWO+5i6bJl/PQnj/PIIw9zyy2f4frrb6Shvh6zyUxr6yKGhwcNp7u7m6NHjzA6GkFR8ry3axeKonD1tdcimSSio+OYzWYCgQCCcLZFmV7OFrI0+XyefD6PqqqUSiXjoUym6rJpNpux2+0XtIxewe8O6cEHH3zwYk6sra2hc9VqplJT7NjxJpFIBLvNTjAYwuFwoKoqsViMYCCAw+Givr6BfD7Pv27fTqlU4r777sNslpmaStPY2IjVakXTNCYnowwPD5HJZFBVDbPZhCTNJoMgCJTLZZLJJNFolMnJSaamplAUhVwuRzqdJpVKkUgkSCQSWK1WbDbbFSv1MeAjE0oQdIxxEfB4PKxbtxaf18fOnTvZvfs9bFYrgWCQUCjE6FiEtrbF3PrHt+Hz+fB6vRw/foxSqcT69euRrVb8gQBul4tEIsHRo0fZseNN3n//ffL5PKDjcrmx2awIApWXQKlUYnx8nLGxMUwmEzU1NYTDYZqamgiHw3i9XpxOJ5IkMTExgcPhwOPxXCHUxwBhofKV+T42BmT688oypBsDPTBwihdefJF9e/dy9aZNbNlyB3klg1rWaG/vBEDTNAYHBzh65ChNzc34A34EBPqO99Hf38eJkycxm010d3ezYsVK8vkctbW1hELhKol1Hfbt20cmk6Gzs7Pqd6VSSeKxGJqmIcsyXq+PTDbLxMQEixYtwuFwoGkqYAQHkiRhpIIMlMvls57ZZDJVl9tyuXzOjjSZTIiiWK24UFV1uier/TW3L00mCVXV5vX5pmE2m2dNBE3TZt2LJEmVZ7k8cBGEWjhyyuVybNu2jV27drFlyxZsNhvLli3D7XbPOk7TNBKJOEePHObAgQMMDw9TU1tLZ0cnq7vWEA6HKRQKnDhxAr/fT01NDaJoxA+xWIzHH3+MG264gfXre5AkiUgkwmuv/ZpTff2UyyWsDhuNjc2Ea2pZunQZdXV1xONxVFVFEHQkScLn8+NyuatLZKFQqA6srmsIglAhppdMJkM6nUGSxMr3MNvYCTgcjiq5Y7EYmUymMtDTdV4zSaEjCDrhcJhUaopCoVj1Hw0Cn2nf4XDicDiw2+2Uy2USiQTpdLo6Fl6vF5/Pd9lY3wU91XM5wvMfo2O327j77ru55pprMJvNqKqK2+0+67hkMsHWrb/kjTfeYOXKlfy7W/+Yrq4uvF4vMzt+bifpus7OnTsZHR2lpaWlOjNHRkY4cuQoJlHEYndgsVgYGYkQDIVxuVwcOtSLrkMg4EdVy2SzWTRNR9dhYmICRVGqFmYmAVKpJAMDp0gkEjQ2NmG329F1HZPJhK4bE0PXNXTd8OlKpRKFgsLp04P4/X7sdvuM9qpPRalUJhodY2hoCBAJBALIsmnefp+cnCSRSFBXV8fw8DBjY2PU1NRgNpvJ5XKoqlrpt8sjkp2XUPMRSdd1isUiqlquvjeOm14GjXNMJplQKIQoitXIam57yWSSN9/cwcDAAH/9V3/Num4j/ZLJZKvHlMtlFEWhWCzOIuOJE30MDw9XIzowqhvuvPMuzCYTssWCw2GQymq10N/fz7vvvMOWO+6gsbGJTCbNyMgwk5OTZDJZ3G434XAYq9VatYIAAgLj42P8+BePEQwGue7azdjsduBMGY7FYqmQSieTydDf38fx48eQZQsbN25ElmXOHmOBdDpNJDLCm2++wc0330JraysWi1ypB6v2OCCQy+UZGRnh5MmTvPXWWzQ2NrJp0yYEQWBsbAxFUeYd2EuFc8bSM2dLuVxm//79HPxgP21tbdTW1VUGYKZfBaJozPRSqURLSwsOh2Nuq9jtDtat66aurh4dgaGhobOqCzRNI5/Pz1LDAZYvX84LL7zA66+/zpe/fC8Wi5Xa2lpcLhe5XM6QEZQ8JpOEomgc2L+PTCZNbW0tkiRRLBaJRCKMjY3T3t5BXV0dJpOJaDTK+PhY1fexyDKJRILdu3fzpS99EYvVgskkkU6nef7550mn06xevZobb7wRs9mMxWIhFotx8OBBrr56EzabDVVV6e/v59ixYxQKBQRBQBRFampqGB4eYmJiglAohNVqRZIkent7GRwcpL29ndbWVgAcDgfBYJAdO3Zw/Phxenp6MJvNKIqCoihVH+tysE4wD6Hm1npP/xVFgcHB0zz33HPceuutfO7zn8fpdDLX2SyXjdqoiYkJZFlmyZIlZ13U7/dz9913k8lkcDgcCzqV02LnTMuxYcMGurq6eOqpp9A0jU99ahPhcAifz4fdbkfTNLK5HMl4nKHTg5w8eYrly5dXZAmdVCrF4cOHcTicNDY2YjabiUaj7NjxBocPHyaZTFIsFGhoaKC+vh5VLbNq1arqPQ4NDXHgwAF27drF4cOH2bx5MyaTiUJBIRIZIZfL0dzcDECxWOTQoUO8+uqr1agVBG677Tampqaor68nEAgiSRKaprJ9+6/Ytm0bV111FQ899BB2ux1BEFDVMidPnkCWZZYuXQpAoVCgUChUl7vLBQtaqLn5MUmS8Ho9FAoFstksfr+fcLiG6WhvepZomkaxWCSTyZBKpapWbmZbsizT3Nx8no0O80EgGAxx//338/TTT7Nt2zZ6ew/h9/uora3lqqv+iNVdXTidTjS1zP/9P33EJmO0f76j6vMMDw9z8uRJrrtuM4FAgHK5zPj4OBaLlVWrVpNMJlEUhXAwRCaboSZcS1NTM6JoRIXFYombbrqJ2tpawuEaymWVbDZBNDrO1NQUbrebhoaGap8tW7YMWZYr1ag6yWSS1tZFHDlyhIaGBqxWCwCpVIpQKEhzcxOvvLKNb3/7W9jtNjRNJx6PMTAwQGvrIrxeL7quV5f8y01fuyD5eHrg/X4/Xq+XaDRKOp2uEOoMisUisViMVCoFUO3YmTtbSqUix4/3cezYMRYvXkwoZOhG9op/slAOb/q7ZDJFXV0dX/3qV+nt7SUWizE2NsajP3qEbCbD6q4uBKBYLHDk6GEQmDGrFQYGThGPx3G5XEiSVF2K1qxZS1NTI4IgkMvl0DSNp558kmXLl1UsMSiKgtNpZ9Omq+np2cDw8AiPPfYYixcvprm5CUmSaG5urkZ70WiUgYEBcrksIOB2u+ns7MRkMjE6GqGnZ0P1uaPRKJs334AsW0ilprDZ7AiCiKoWGRsdJZVKsnr1akRRpFwuk8/nEUWxmvu8XHBOQs0eXAGXy43D4SSZTFaK4Az/qVQqkkgkSaVSlMslHA5nVWDUNG3WkpVOp9m6dSvvvvsu3/zmN3E6nWQyaUKhEC6X+5yaysDAKY4fP044HMLlcrFx40aKxSLZbJbm5ibWrulGADRdI5FIEhkdZXHb4kqkCfl8jlQqRTqdJpPJVLQgE8FgkGQyQX9/X1V7GhgYYN/+fdyxZQuSaTqaHOb5557lb/7m6wQCQSaiMSIjZ4gxNZVm7dq1SJIJVVUZGBjg179+lUQihgBVh9qIMjVqa2sxm80ATE2lgTTlsspXvvLvcTic6DooSp7xsSgW2crSpUurwm6hUMBitSJeRhoUfMRcnsfjJRQOMTI8RC6bQ1XLTExESSaTqKqG2+3B7TZC9X379hKNTrBs2TKCQcPHMfQVEV3XOD04QCw2SU/PRiYnJ4hEIjgcKUKhcNV3mIudO39LLpejvX0lXq8PVTUERJNJYs2atVitNlTV0LgGBgbQdYGV7e2IkiEglkolampqEASB3779Wzas76GhsYFgMIjVaiWfNypFZVnm1VdfIx6Ls3zFSiTJhADEYpP88z//nMbGJv7sz/+SJUuXcPc9d9HY2MT4eBRFKbBkiWENRVFk8eLF3HPPPRSLRQR0HE4nArD9X7dTV2f4T4YYCi0tLRQKBXw+Y/kWRRFN00ilpkilktSEwzQ2NgBGtF0oFKipqTmrjy41PhKh7HY7wUCQY0ePMjQ8RDAUolgsYLfbK2TyIEkSo6MRfvCDH+Dz+XjggQc4deokkYhMPB4nGAyxceNG9uzZy9ZfbmX16i5aWlpJJhMkkymGhoYIhUJ4vd6qtZoOy3t7e+ns7MRWiYpMJgmQq3rQ8PAwiUS8OottNiutLS0UlDzZrFHV0N7eTk9PD9tefplgMMC9995LfUMDPp8Hr9dNsVhkcjLG0NAg4ZoaPB7DbywVS5RKJe644y7+6YUX2Lz5etoWL2blynZOnz5Nb++HyLKJQMBfJabP56W7ex3TlnxqaorR0Qi5XI6VK9uRZZlczqhOdbvdVV9T0zT6+/vJ5XI4HQ4ioxF8fi+SJJLNZEgk4pRKJZwVgl5OOItQ51LDLRYLN9x4I/X19bhdBnlqa+txOh0V0y1QKhZ4/PEfs2fPHn74wx9SW1tLf18fL730ErHYJN/42/9ETbiWO7fcycMP/yPPP/csX//GNwgGg9jtDuLxOOPj4+TzeUKhEBaL4bROTEwwEY2y+7336O/rm5HaMNITTqcTBJGOjnba29s5dKiX3t4PefHFF3jlFReBQJB13evp6Ojg9tv/hKGhIV566X/T399H15ouvB4jWkpnMrS2tBCbnGTw9ACPPPIwoiDQ3tFJz/oN/OX9/4HeQx/y9JM/4T9/90EcDieiKLJ3z276+vooFhT0SjQnYCQgdc0QQzs62gmGQowMDxMZGeHY0SOYTOZZyouOYSFNJombb74FweXg4MEPUFUNRVGQZZn2jg5Wreqq9s3lhHmTwzOjr5lLjyiK1NXV0t7eQX1DA4FAoBL2mzDKewW2b9/G97//fe754hf5i7+4n8nJCYrFAs8++zRTUym++md/TqlcYvnyZSQSCV577VWCwSCdnZ1YrdZq6B+LxapVAtP35A/48fv95HI5CoVCNW/mcrlobW2lo7OT9vZ2/H4/8XiMYrGI1WoHBBoaGtjQ04Pfb7SxbOkSPB4P4+PjnD49wMRElEKhSCAQoKurC5vNhsViQaxYjGXLl7Fm7ToCgSCBgJ/nn3mG2oYGVqxYiSgKJBMJcrksTqezKvieKV828otda9ZQW1ODoigz8ok6VI7RdR1d02hoqGfjxo20t7eDbkR5LpcLALvdQfe69bS1tSHL8h+YHh8d8+by5rNOpVKJ0dFRJieiRuoBkGUzICIIAhaLzIkTJ/iv/+UhfD4///DD/8aSJUvZvv0VNLXEg9/9Lj6Pl5//YitDQ0Nks1mi42M8/I8P4/Z4eOh7D9HV1YWuGwrwkSNHcDqd1TygQSBDoVYUZdZeQNlsxmK1IstWJElE01RyuRyKohh5MwSsNitOp6PyfAA6iqKQSCTIVvJuNrsdh92Ow+lEURQKioJWIYXD4apGZKVSgad++lMio6P89//xP6mtrSUyEmFwaJDGxsaze1kHURIrmpNIIpGkUFBmfT+9dum6jtPpxOfzV4TYArHJCUqlEpquI5stBCo+3+WIeX2oucueIBibCE6cOMH+/XuJxyaxWi18+qabqaurRxAEkskkjz7yMNFolL//+3+grW0xsVgMSTIRCgYrFl1HFEUcDgcTExN0da3hvq/cxxOP/4Snf/Y03/r2twgEjIjL6XTg9/swmSTi8Ti9vYdYsWI54XB4lm81fa+aplW1HjAU5unsv2Et5mb1BRwOV6WSwcjhTR8zbfWmo1PDygjoukFiiyxz082f4fEnfswL//Q83/7W31FXX49ZlimXyzQ0NJxl3Wf2ZV2dbdb7uXrczKJCWbZQV9/ANOMuJ81pPpw39TKN6YeOjETYufNtvF4vN938GerrDa1p6y+3cvDgQb7x9b9l9aouRFFgYmKcRYvaCAR8mCSpKiG43W68Xi/19fXceuttjI1F2br1F2zbtoLPfvazCAK0trZUa5iMEmEXDQ2NBIOhee5z/o0Ks3ONzPgrMEvhF4TKu/PtrNEREA2rJwrc9OlP86tf/YrGxkaaW1opFAqUyyVk2VSJwPQZubnZBDvfporZlarzE/NyxAVHedPRh8vlYt3abiSTuRKVDfPhhwd59tlnufrqq/mTz30Oq82GgMDJkwOEQiHi8UnKxSKiw/ADzGYT2WyGQ4cP4XK56OhYya5ddezevZv6+gY8Hi+lUplYzIjYBgeHyOcVxsbGSaczv8PGg/lrky7s+WcnwxWlQGNTE4va2vjx40/g8/no7FzF7bffjixb0XVhnsTwQm2fvSLMZ7U+CThvtcH0AzkcDtrb243aJEFAB6wWmeGh0zz66CO43S6+9rWvUd/QAILAVDqNKIp4vV4S8UkkUULHGBSzWcZmc1AqlTGZTKxYsYLvfOc7lEplXG4PFotlVg14R0cn5XJ5VsHbxw3DCp7pE7NZZu3adXR3r2doaBhVVfH5fLS1tVX9m5nHX9g1FrZgl6tFmosFk8NzP5MkiZqaGoKhUHV+q+USzz//HCPDw3zve9+jq2stknimRqmlpYXW1lZcLrsRkRgBDbJsIRQKUS6XaGxsAgRaWhexEE+mfaH5iH4pMDN3KUmSUTWh6zMitwurJ/u3iHMW2M2FJElVqV/AqGxsaGzEYrEwODhIsVTALjgol8ukUqlqYlQUxEqbRjvTutHo6CiSZDor3XIhA3EpCLXQDpyFdtTM/C2sT4qF+V2xoLB5vs8AzGaZe+75EuWSyhNPPE6xWORrf/Ufq7tSpksrtGpN0xlty2KxIAhGsZnH4znndS7kXi4FFrJCnzS/5/eJC3bK5zqMeuV/p9PJvX/6p3g8bn70o0ex2R1s2nQNgUCgGnaL85SwmEyGZUomk1VCnW9H8KXG+XycucWACx37bxnndMrP1xGaplVJddcX7qK5uQkEQx5oamo60x6GMClJJkqlEtFolKGhIfL5PIFAoHqtc83oSx0uz50QC1mmy8XPu1S4IKd8LmZ3lLFPz2yWueqPPlV1rOf6FZ2dnZjNZj74YD8ulwuv18fy5curJcJz/ZNzzfhLhYV8of8fibMQFtxGdQVXcDG48qPgV/B7xf8D0V+z5/Si2joAAAAASUVORK5CYII=';
    }

}
