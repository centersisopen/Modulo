<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class ASAAS extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {

        return [
            'dados_lancamento' => $dadosLancamento,
            'dados_convenio' => $dadosConvenio,
        ];
    }

    public function logo() {
        return '';
    }

}
