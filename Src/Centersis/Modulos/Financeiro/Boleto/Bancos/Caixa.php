<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class Caixa extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {
        $dadosboleto = [];
        $codigobanco = $dadosConvenio['fnc_convenio_banco'];
        $codigo_banco_com_dv = '104-0';
        $nummoeda = '9';

        $fator_vencimento = $this->fatorVencimento($dadosLancamento["fnc_parcela_vencimento"]);

        //valor tem 10 digitos, sem virgula
        $valorParcela = str_replace('.', ',', $dadosLancamento["fnc_parcela_valor"]);
        $valor = $this->formataNumero($valorParcela, 10, 0, "valor");

        //agencia é sempre 4 digitos
        $agencia = $this->formataNumero($dadosConvenio["fnc_convenio_agencia"], 4, 0);

        //conta cedente (sem dv) com 6 digitos
        $conta_cedente = $this->formataNumero($dadosConvenio["fnc_convenio_numero"], 6, 0);

        //dv da conta cedente
        $conta_cedente_dv = $this->digitoVerificadorCedente($conta_cedente);

        //campo livre (sem dv) é 24 digitos
        $campo_livre = $conta_cedente;
        $campo_livre .= $conta_cedente_dv;
        $campo_livre .= $this->formataNumero('000', 3, 0);
        $campo_livre .= $this->formataNumero('1', 1, 0);
        $campo_livre .= $this->formataNumero('000', 3, 0);
        $campo_livre .= $this->formataNumero('4', 1, 0);
        $campo_livre .= substr($dadosLancamento["fnc_parcela_nosso_numero"], -9);

        //dv do campo livre
        $dv_campo_livre = $this->digitoVerificadorNossoNumero($campo_livre);
        $campo_livre_com_dv = "$campo_livre$dv_campo_livre";

        //nosso número (sem dv) é 17 digitos
        $nnum = $dadosLancamento["fnc_parcela_nosso_numero"];

        //nosso número completo (com dv) com 18 digitos
        $nossonumero = $nnum . '-' . $this->digitoVerificadorNossoNumero($nnum);

        // 43 numeros para o calculo do digito verificador do codigo de barras
        $dv = $this->digitoVerificadorBarra("$codigobanco$nummoeda$fator_vencimento$valor$campo_livre_com_dv", 9, 0);

        // Numero para o codigo de barras com 44 digitos
        $linha = "$codigobanco$nummoeda$dv$fator_vencimento$valor$campo_livre_com_dv";

        $agencia_codigo = $agencia . " / " . $conta_cedente . "-" . $conta_cedente_dv;

        $dadosboleto["codigo_barras"] = $this->htmCodigoBarras($linha);
        $dadosboleto["linha_digitavel"] = $this->montaLinhaDigitavel($linha);
        $dadosboleto["agencia_codigo"] = $agencia_codigo;
        $dadosboleto["nosso_numero"] = $nossonumero;
        $dadosboleto["codigo_banco_com_dv"] = $codigo_banco_com_dv;
        $dadosboleto["logoBanco"] = $this->logo();

        return $dadosboleto;
    }

    protected function modulo10($num) {
        $numtotal10 = 0;
        $fator = 2;

        // Separacao dos numeros
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo (falor 10)
            $temp = $numeros[$i] * $fator;
            $temp0 = 0;
            foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                $temp0 += $v;
            }
            $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            // monta sequencia para soma dos digitos no (modulo 10)
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }

        // várias linhas removidas, vide função original
        // Calculo do modulo 10
        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
    }

    protected function modulo11($num, $base = 9, $r = 0) {
        $soma = 0;
        $fator = 2;

        /* Separacao dos numeros */
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo falor
            $parcial[$i] = $numeros[$i] * $fator;
            // Soma dos digitos
            $soma += $parcial[$i];
            if ($fator == $base) {
                // restaura fator de multiplicacao para 2 
                $fator = 1;
            }
            $fator++;
        }

        /* Calculo do modulo 11 */
        if ($r == 0) {
            $soma *= 10;
            $digito = $soma % 11;
            if ($digito == 10) {
                $digito = 0;
            }
            return $digito;
        } elseif ($r == 1) {
            $resto = $soma % 11;
            return $resto;
        }
    }

    protected function montaLinhaDigitavel($codigo) {

        // Posição 	Conteúdo
        // 1 a 3    Número do banco
        // 4        Código da Moeda - 9 para Real
        // 5        Digito verificador do Código de Barras
        // 6 a 9   Fator de Vencimento
        // 10 a 19 Valor (8 inteiros e 2 decimais)
        // 20 a 44 Campo Livre definido por cada banco (25 caracteres)
        // 1. Campo - composto pelo código do banco, código da moéda, as cinco primeiras posições
        // do campo livre e DV (modulo10) deste campo
        $p1 = substr($codigo, 0, 4);
        $p2 = substr($codigo, 19, 5);
        $p3 = $this->modulo10("$p1$p2");
        $p4 = "$p1$p2$p3";
        $p5 = substr($p4, 0, 5);
        $p6 = substr($p4, 5);
        $campo1 = "$p5.$p6";

        // 2. Campo - composto pelas posiçoes 6 a 15 do campo livre
        // e livre e DV (modulo10) deste campo
        $p1 = substr($codigo, 24, 10);
        $p2 = $this->modulo10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo2 = "$p4.$p5";

        // 3. Campo composto pelas posicoes 16 a 25 do campo livre
        // e livre e DV (modulo10) deste campo
        $p1 = substr($codigo, 34, 10);
        $p2 = $this->modulo10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo3 = "$p4.$p5";

        // 4. Campo - digito verificador do codigo de barras
        $campo4 = substr($codigo, 4, 1);

        // 5. Campo composto pelo fator vencimento e valor nominal do documento, sem
        // indicacao de zeros a esquerda e sem edicao (sem ponto e virgula). Quando se
        // tratar de valor zerado, a representacao deve ser 000 (tres zeros).
        $p1 = substr($codigo, 5, 4);
        $p2 = substr($codigo, 9, 10);
        $campo5 = "$p1$p2";

        return "$campo1 $campo2 $campo3 $campo4 $campo5";
    }

    protected function digitoVerificadorBarra($numero) {
        $resto2 = $this->modulo11($numero, 9, 1);

        if ($resto2 == 0 || $resto2 == 1 || $resto2 == 10) {
            $dv = 1;
        } else {
            $dv = 11 - $resto2;
        }

        return $dv;
    }

    protected function digitoVerificadorNossoNumero($numero) {
        $resto2 = $this->modulo11($numero, 9, 1);
        $digito = 11 - $resto2;

        if ($digito == 10 || $digito == 11) {
            $dv = 0;
        } else {
            $dv = $digito;
        }
        return $dv;
    }

    protected function digitoVerificadorCedente($numero) {
        $resto2 = $this->modulo11($numero, 9, 1);
        $digito = 11 - $resto2;

        if ($digito == 10 || $digito == 11) {
            $digito = 0;
        }

        $dv = $digito;

        return $dv;
    }

    public function nossoNumero($numero, $dadosConvenio) {

        $nnum = $this->formataNumero('1', 1, 0);
        $nnum .= $this->formataNumero('4', 1, 0);
        $nnum .= $this->formataNumero('000', 3, 0);
        $nnum .= $this->formataNumero('000', 3, 0);
        $nnum .= $this->formataNumero($numero, 9, 0);

        return $nnum;
    }

    public function logo() {
        return 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHCAgICAgICAgICD/2wBDAQcHBw0MDRgQEBgaFREVGiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICD/wAARCAAoAJYDAREAAhEBAxEB/8QAGwAAAwEBAQEBAAAAAAAAAAAABgcIBQQDAQL/xABJEAABAwIDBAYFAxALAAAAAAABAgMEBREABhIHEyExCBQiQVFhIzJxobFCgZMVFhgzNERSVGJygpGissHTQ1Nzg5KzwtHS8PH/xAAbAQADAQEBAQEAAAAAAAAAAAAEBQYDAgcAAf/EAC0RAAEDAwMDAwMEAwAAAAAAAAIAAQMEBRESITEiQWEGFVETMnEWI0KBFDNi/9oADAMBAAIRAxEAPwCqDj5fO+F8Jt34+XyDcwbXtn1BkKjTqshUpPBUdgKfUD4HdhQB9uDIqCU+GQslYAMuKkbdNm9UlNRWampp95QQ0h5l1AKibAa9JTxJ8cdS22WPd1+R1oFsmBgBFoUzRtOyXlioN0+s1ERpTiA6GtDizoJ0gnQFW5H44KhozkbLIeSpEOVlMbddmTzqGm6sStwhKbsPjibWuSiw9bx+Bxo9vkbdctWC6YAwE/KJZDGbdo2UspPx2a5MVGclJUplKWnXbhNgftaVW59+CYKSSXcVhNUjHysyi7adn9bq0elU2et6bKVpZb3D6bm2o8VJA5DGklBKDZfhcBWgT4W1m7O1BypDam1txxmK6vdJdbbW4Ndr2Oi9uWMaemOXZuVtJKwIRV0itmYP3VJV7I7n+wwZ7NM6F9xjRtR8z0ar0JqvQpIVTHUFwPK7NgkkK1X5FJBBwsqG+lnXthHQ/ufZu7rIO1LK4Ngt0jx3ZwmK+Qs/dOhsNQ7dlqULNlLri3Ewd6Q1661JKU/rwXTVozv05wgqmhOD7sLozBXqfQKNLq9RWW4cNBcdUOJ8AALjiSQB54ZRxEZMI90vlkYGy6W32TWQPxeofRN/zMNPY5vCC9yjRlkXaJQ86Q5Mqkh5CYrgadQ+kJVcp1A9lSuGAKqkOF98ImKoE22RWMDIhfk4/HXynLbnthmvz38r5fklmHGJbqctr1nHBwLSFDklJ9a3Eny50tqtrY1kktbWfxFJaDT506QI0GM7KfPJlhCnFG3kkE8hh6ZhG3wlbCT+UTZLy1VWdoeX6fU4T8R1c5hW6kNqaJShes2Cwm4sMDVVQDxE7PnZERRE0jM7YVjVKoRKbTZE+W4G4sVtTrzh7koFycRQDqLHyqMullEmb8yS8yZkn1uTcLmOFTbf4KE2DafOyQBi5pIGiBmUvPK5ksftJVw7LiTz7wf/AHBBiL7LMXcSVz5Tqoq2WaXUwb9bitPE+akAn34gagdMjsqqnLIqcOkvURIz+xFB4QoTST+ctS3PgRimsQYif8pLcjya4ejzT+s7S4jtriFHffPfzRuh73MaXgsQf2uLc2TyqXzlleFmfLU6jSx2ZKPRud7bg4tuDzSrEpSzPEbEnk4ahUT1KnS6bUJNNmI3cqI4ph5vwUgkH38j4YvYTYx1Nw6lpA0kjLZ/m2e3Acy05KtTnXOstME29Lbj83Zvp8cQvrahd4NYZ8qz9HVQtPoPHhMjLOWZtdmhlm6GEfb5BFwkH4nwH/R5nQURzlx0/K9DuNwCnH/r4TupVKhUuE1EiICGmxbzPmT44vYKcYxwK8+mnKQnIkh+kxnYLdiZSiL4ItLqNvHky37yo/NiostJ/N0huE/ZInq7wZ3+7XuCSgO6ezqHMauVwDxGKTO6TY2Tp6LtV3derFLUeEqOl5A/KZVb4OYQX6NtLOm1sPqwqSHLEynayM21RVJyxVqokXVCiPvpHm2gqHwxrAGo2byspnwDqGXXHHXFOuK1OOEqWo8yVcScegAOhsKUJ8uqv2A5cpFPyFDqUZpKp9TCnZcm3bOlZQG78wEhPLl34jbpORSuz8MqGgiZo890wZdMp0t6M9KjoeeiL3sVxaQVNrsRqQeY4HC0ZC7cI1xZ33Sc6SuduqUuNlWI56eoenn6TyjoPYT+mv3DDuzU2X1u3CWXGow2lkptj2VzmDP9NjrRrhxF9dlXHDQzxAP5y9I+fDq4S/Tid+7pbSR6yQ1mWOqPmOqsL9ZmY+hXdxS6oYKpt42dYzfc6qLo9Vj6obN4rBN10512Krv4BW8T+y4BiRuoYmfyn1AWY0gNstQ67tMrrt7ht8MfQoDf+nFJax0wslFW+ZHR90WoBXWa3UCODMdpgHzcXqP+WMLb4fSzeUZah5VFKISkqVwA4knyxMk+PwnQ7pD7SMt7OsxZpfqD709qSUpakqhpY3Ti0XSFHX2rgAJPjbGA+sxpv226mR36Ulm6uMoPkZW2WUZTM1+oVhjQ4ktkdWKr37gBc2HH2YOp/UR3DMTBs+zoeosI0LtIRbsqYy/T6XBpTDVNsYakpcbcHHWFi+u/ffAsdM0XS22FzLUvM+p919zBWoVDosyrzVaY0NpTrnnYeqPMngMEQg8hMLIeWRgHKiGvVmZWqzMq0xV5E11TrhPdq9UexI7IxewQtGDeFLyPqfKYW0DKyaHspygCkdaW6/IljUCQuUhK0g2PchKU4W0kznUE/bhFzRsMbLG2K1Y0zaXR3L2bkuKiud3B5JQP2tONbqGuL8LKiPBsrIHLEYqZZuYqWmrUKo0tZsmdGdjk/wBogp918aQnpJn+HWco5FQ3VKbNpdQkU6a2WpUVZbdQoW7STb5wbcDi9gl+qOVLSBh0R5O2o5xykw5FpEpJiLOsxXkB1AV3lPem/fgae3hM+SW0FUUbJqbGc612v16uZmzPUSuJSYPL1GGgtWtSkoTpTyZ9uE9xpQjFgDl3R9HUETu5cJM5zzLKzPmafW5FwZbt2G1fIZTwbR+ikcfPDujhaMGZLZ5nIs9lzUv644p39L65HLqdJdi71JUgnldFu8Y6kKJ9iX4Am3C45hlmS51zedaJ9Jvb7y549rVxvxvjaImxtws3Z87p69FurgfV6lKPCzUxsf4kOH93E9e4txJNbYfLJKZinGfX6lNUbmTKedufy3Cf44fQYYBbwlcv3O6oXowQNzlOqT1ffUzQD5NIH8VnE1fTxIzeE7tY9K29oGe1LU5SaW7ZsXEqQg8Se9tBHh3nHnF3u38AV9Z7S3+yT+mQHApdRqDi2qfHMh1CFObtPD1R+rwGElDSlUSsLJ/X1o08ep+eyTVaqk6pTVOy7pIJSlj+rseXt8ce6We0xUsTaecLxu6XSSrNyLj4VG9HLPJquXXMuy3LzqP9zX5qiq9Uf3Z4ewjCq802iTU3Dre3T6h0rC6TOdQBEylEc4m0qp28P6Fs/v29mCLJT763WVymz0pG0qjVWryjFpcN2bJ0lZZYSVr0jgTYXxQnMMe5cJQEbk+F3VTJ+b6ZCMyqUmXFiNkAvPtLQkX4C2q3hbGUVVCWwcraSGTvwsymzXINQizWz6SK628gjxbVq/hjeaPIP+FmBYNnV4wZTUuExLaN2n20uIP5KxcfHHnxjh8KrB8tle1scOukDZ92RZVzksSJja4tSSNKZ8ayXCByCwQUrA8xfBtJcZIuOEJPSMaXw6K0PXxzC7o/B6qm9vbvCPdhl74fwg3tbIqhbCaPAynUMuQqpMaTVHG3J0v0ZUpLXJtI0gBF+J7+69sBFciI2J24RX+CzDjKwh0Wssd9Xm35eq1y7/k4J99kdsYWLWsflOCj0mJSqXFpsVNo8NpDLV+elAsL27+HHCiSRzLLo8ImFkvM57A8vZpzFJrcmfKjPSgjetMhvT2EBAPFJPJOGFPczjHSyEloBJ8r0yZsOpGU6k/Pg1SY45IjORVoc3YTZy3HspHIp4Y+qLmUmMs2y+ioGDusIdFzKfyqtUD9D/LxuN8kFuGXBW0XR5l7Z5CoGU/rcps2S3HK1uKlHRvjvDxHBIHuwluEhVT77JlQ4p+2pZ42Q0Uffcm3h2P+OJ/9OxZzl1Qt6ilxjDImy9linUKKWIiSSr13Veur2nDOlowh4SqsrZKh8kgus9H/ACFVqpKqTyZLL0txTrjbLgQ3rVzITpNrnjh/FdZRbHwkp0AEurKuxTKWWK01WKW7NTLZCkgKeuhSVixSsaRqHf7cZ1FfJK2Cwu4qIQ4XjXNg+Sq5V5dWqC5rkyaveOq31he1hp7PAJAsPAY6hukgNpbC5OgEnytbJWyzKuTpUmVR0Pb+UgNuKeXr7KTeyeAtjKorZJeVpFSCC2M0ZWpOZqO5SKs2pcN1SFKCFFCroVqHEezGMMxRvllrJCJNhBP2OezT8Xk/Trwe95m8IP24ExaXTY9MpkWnRdXV4bSGGdZ1K0NpCU3J58BhaR6nz8o8Rw2F/9k="';
    }

}
