<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class Banrisul extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {
        $dadosboleto = [];
        $codigobanco = $dadosConvenio['fnc_convenio_banco'];
        $codigo_banco_com_dv = '041-0';
        $nummoeda = '9';

        $fator_vencimento = $this->fatorVencimento($dadosLancamento["fnc_parcela_vencimento"]);

        //valor tem 10 digitos, sem virgula
        $valorParcela = str_replace('.', ',', $dadosLancamento["fnc_parcela_valor"]);
        $valor = $this->formataNumero($valorParcela, 10, 0, "valor");

        //agencia é sempre 4 digitos
        $agencia = $this->formataNumero($dadosConvenio["fnc_convenio_agencia"], 4, 0);

        //conta cedente (sem dv) com 6 digitos
        $conta_cedente = $this->formataNumero($dadosConvenio["fnc_convenio_numero"], 7, 0);

        //dv da conta cedente
        $conta_cedente_dv = $this->duploDigito($agencia . $conta_cedente);

        //campo livre 25 posições                
        $campo_livre = '2'; //1
        $campo_livre .= '1'; //2
        $campo_livre .= $agencia; //6
        $campo_livre .= $conta_cedente; //13
        $campo_livre .= substr($dadosLancamento["fnc_parcela_nosso_numero"], 0, -2); //21
        $campo_livre .= '40'; //23         
        //dv do campo livre
        $dv_campo_livre = $this->duploDigito($campo_livre); //25

        $campo_livre_com_dv = "$campo_livre$dv_campo_livre";

        //nosso número completo (com dv) com 18 digitos
        $nossonumero = substr($dadosLancamento["fnc_parcela_nosso_numero"], 0, -2) . '-' . substr($dadosLancamento["fnc_parcela_nosso_numero"], -2);

        // 43 numeros para o calculo do digito verificador do codigo de barras
        $dv = $this->digitoVerificadorBarra("$codigobanco$nummoeda$fator_vencimento$valor$campo_livre_com_dv");

        // Numero para o codigo de barras com 44 digitos
        $linha = "$codigobanco$nummoeda$dv$fator_vencimento$valor$campo_livre_com_dv";

        $agencia_codigo = $agencia . " / " . $conta_cedente . "-" . $conta_cedente_dv;

        $dadosboleto["codigo_barras"] = $this->htmCodigoBarras($linha);
        $dadosboleto["linha_digitavel"] = $this->montaLinhaDigitavel($linha);
        $dadosboleto["agencia_codigo"] = $agencia_codigo;
        $dadosboleto["nosso_numero"] = $nossonumero;
        $dadosboleto["codigo_banco_com_dv"] = $codigo_banco_com_dv;
        $dadosboleto["logoBanco"] = $this->logo();

        return $dadosboleto;
    }

    protected function duploDigito($campo) {
        $dv1 = $this->modulo10($campo);
        $dv2 = $this->modulo11($campo . $dv1, 2, 7, 1, 10);

        if ($dv2 == 1) {
            if ($dv1 == 9) {
                $dv1 = 0;
            } else {
                $dv1++;
            }

            $dv2 = $this->modulo11($campo . $dv1, 2, 7, 0, 10);
        } elseif ($dv2 != 0) {
            $dv2 = (11 - $dv2);
        }

        return $dv1 . $dv2;
    }

    protected function modulo10($n) {
        $chars = array_reverse(str_split($n, 1));
        $odd = array_intersect_key($chars, array_fill_keys(range(1, count($chars), 2), null));
        $even = array_intersect_key($chars, array_fill_keys(range(0, count($chars), 2), null));
        $even = array_map(
                function ($n) {
                    return ($n >= 5) ? 2 * $n - 9 : 2 * $n;
                }, $even
        );
        $total = array_sum($odd) + array_sum($even);
        return ((floor($total / 10) + 1) * 10 - $total) % 10;
    }

    protected function modulo11($n, $factor = 2, $base = 9, $x10 = 0, $resto10 = 0) {
        $sum = 0;
        for ($i = mb_strlen($n); $i > 0; $i--) {
            $sum += ((int) mb_substr($n, $i - 1, 1)) * $factor;
            if ($factor == $base) {
                $factor = 1;
            }
            $factor++;
        }

        if ($x10 == 0) {
            $sum *= 10;
            $digito = $sum % 11;
            if ($digito == 10) {
                $digito = $resto10;
            }
            return $digito;
        }
        return $sum % 11;
    }

    protected function montaLinhaDigitavel($codigo) {

        // Posição 	Conteúdo
        // 1 a 3    Número do banco
        // 4        Código da Moeda - 9 para Real
        // 5        Digito verificador do Código de Barras
        // 6 a 9   Fator de Vencimento
        // 10 a 19 Valor (8 inteiros e 2 decimais)
        // 20 a 44 Campo Livre definido por cada banco (25 caracteres)
        // 1. Campo - composto pelo código do banco, código da moéda, as cinco primeiras posições
        // do campo livre e DV (modulo10) deste campo
        $p1 = substr($codigo, 0, 4);
        $p2 = substr($codigo, 19, 5);
        $p3 = $this->modulo10("$p1$p2");
        $p4 = "$p1$p2$p3";
        $p5 = substr($p4, 0, 5);
        $p6 = substr($p4, 5);
        $campo1 = "$p5.$p6";

        // 2. Campo - composto pelas posiçoes 6 a 15 do campo livre
        // e livre e DV (modulo10) deste campo
        $p1 = substr($codigo, 24, 10);
        $p2 = $this->modulo10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo2 = "$p4.$p5";

        // 3. Campo composto pelas posicoes 16 a 25 do campo livre
        // e livre e DV (modulo10) deste campo
        $p1 = substr($codigo, 34, 10);
        $p2 = $this->modulo10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo3 = "$p4.$p5";

        // 4. Campo - digito verificador do codigo de barras
        $campo4 = substr($codigo, 4, 1);

        // 5. Campo composto pelo fator vencimento e valor nominal do documento, sem
        // indicacao de zeros a esquerda e sem edicao (sem ponto e virgula). Quando se
        // tratar de valor zerado, a representacao deve ser 000 (tres zeros).
        $p1 = substr($codigo, 5, 4);
        $p2 = substr($codigo, 9, 10);
        $campo5 = "$p1$p2";

        return "$campo1 $campo2 $campo3 $campo4 $campo5";
    }

    protected function digitoVerificadorBarra($numero) {
        $resto2 = $this->modulo11($numero, null, 9, null, 1);

        if ($resto2 == 0 || $resto2 == 1 || $resto2 == 10) {
            $dv = 1;
        } else {
            $dv = 11 - $resto2;
        }

        return $dv;
    }

    public function nossoNumero($numero, $dadosConvenio) {
        return $numero . $this->duploDigito($numero);
    }

    public function logo() {
        return '';
    }

}
