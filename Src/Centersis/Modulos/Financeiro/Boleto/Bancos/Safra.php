<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class Safra extends Base
{

    public function dadosBoleto($dadosLancamento, $dadosConvenio)
    {
        $dadosboleto = [];
        $codigobanco = $dadosConvenio['fnc_convenio_banco'];
        $dvSafra = '7';
        $codigoBancoComDv = '422-' . $dvSafra;
        $nummoeda = '9';

        $fatorVencimento = $this->fatorVencimento($dadosLancamento["fnc_parcela_vencimento"]);

        $valorParcela = str_replace('.', ',', $dadosLancamento["fnc_parcela_valor"]);
        $valor = $this->formataNumero($valorParcela, 10, 0, "valor");

        $agencia = $this->formataNumero($dadosConvenio["fnc_convenio_agencia"], 4, 0);
        $agenciaCodBar = $this->formataNumero($dadosConvenio["fnc_convenio_agencia"].$dadosConvenio["fnc_convenio_agencia_dv"], 5, 0);

        $conta = $this->formataNumero($dadosConvenio["fnc_convenio_conta"].$dadosConvenio["fnc_convenio_conta_dv"], 9, 0);

        $nnumComDv = $this->formataNumero($dadosLancamento["fnc_parcela_nosso_numero"], 9, 0);

        $tipoCobranca = 2;

        $nossonumero = substr($nnumComDv, 0, 8) . '-' . substr($nnumComDv, 8, 1);
        $agenciaCodigo = $agencia .$dadosConvenio["fnc_convenio_agencia_dv"]. " / " . $this->formataNumero($dadosConvenio["fnc_convenio_conta"].$dadosConvenio["fnc_convenio_conta_dv"],9,0);

        $dadosboleto["codigo_barras"] = $this->htmCodigoBarras($this->geraCodigoDeBarras($codigobanco, $dvSafra, $nummoeda, $fatorVencimento, $nnumComDv, $agenciaCodBar, $conta, $tipoCobranca, $valor));
        $dadosboleto["linha_digitavel"] = $this->geraLinhaDigitavel($codigobanco, $dvSafra, $nummoeda, $fatorVencimento, $nnumComDv, $agenciaCodBar, $conta, $tipoCobranca, $valor);
        $dadosboleto["agencia_codigo"] = $agenciaCodigo;
        $dadosboleto["nosso_numero"] = $nossonumero;
        $dadosboleto["codigo_banco_com_dv"] = $codigoBancoComDv;
        $dadosboleto["logoBanco"] = $this->logo();

        return $dadosboleto;
    }

    function modulo10($cod, $p = 0)
    {
        $mod10 = 0;
        $mod = "";
        $i = 0;
        do {
            if (($i + $p) % 2 == 0) {
                $val = intval($cod[$i]);
                $val = $val * 2;
                $mod = $mod . (string) $val;
            } else {
                $mod = $mod . $cod[$i];
            }
            $i++;
        } while ($i < strlen($cod));
        $i = 0;
        do {
            $mod10 = $mod10 + intval($mod[$i]);
            $i++;
        } while ($i < strlen($mod));
        $mod10 = $mod10 % 10;
        $mod10 = 10 - $mod10;
        if ($mod10 == 10) {
            $mod10 = 0;
        }
        return $mod10;
    }

    /* Este método calcula o modulo 11 de um código ($cod). */

    protected function modulo11($cod)
    {
        $i = 2;
        $c = strlen($cod) - 1;
        $mod11 = 0;
        do {
            if ($i == 10) {
                $i = 2;
            }
            $mod11 += intval($cod[$c]) * $i;
            $i++;
            $c--;
        } while ($c >= 0);
        $mod11 %= 11;
        if (11 - $mod11 == 0 || 11 - $mod11 == 11 | 11 - $mod11 == 10) {
            return 1;
        }
        return 11 - $mod11;
    }

    protected function geraLinhaDigitavel($banco, $dvBanco, $moeda, $vencimento, $nossoNumero, $agencia, $conta, $tipoCobranca, $valor)
    {
        $ldSemDv = $banco . $moeda . $dvBanco . $agencia . $conta . $nossoNumero . $tipoCobranca . $vencimento . $valor;
        $codBarrasSemDac = $banco . $moeda . $vencimento . $valor . $dvBanco . $agencia . $conta . $nossoNumero . $tipoCobranca;

        $g1 = substr($ldSemDv, 0, 9);
        $g2 = substr($ldSemDv, 9, 10);
        $g3 = substr($ldSemDv, 19, 10);
        $g4 = $vencimento . $valor;
        $dvG1 = $this->modulo10($g1, 0);
        $dvG2 = $this->modulo10($g2, 1);
        $dvG3 = $this->modulo10($g3, 1);
        $dac = $this->modulo11($codBarrasSemDac);

        $linhaDigitavel = $g1 . $dvG1 . $g2 . $dvG2 . $g3 . $dvG3 . $dac . $g4;

        return substr($linhaDigitavel, 0, 5)
            . '.' . substr($linhaDigitavel, 5, 5)
            . ' ' . substr($linhaDigitavel, 10, 5)
            . '.' . substr($linhaDigitavel, 15, 6)
            . ' ' . substr($linhaDigitavel, 21, 5)
            . '.' . substr($linhaDigitavel, 26, 6)
            . ' ' . substr($linhaDigitavel, 32, 1)
            . ' ' . substr($linhaDigitavel, 33, 14);
    }

    protected function geraCodigoDeBarras($banco, $dvBanco, $moeda, $vencimento, $nossoNumero, $agencia, $conta, $tipoCobranca, $valor)
    {
        $codBarrasSemDac = $banco . $moeda . $vencimento . $valor . $dvBanco . $agencia . $conta . $nossoNumero . $tipoCobranca;

        $dac = $this->modulo11($codBarrasSemDac);

        return $banco . $moeda . $dac . $vencimento . $valor . $dvBanco . $agencia . $conta . $nossoNumero . $tipoCobranca;
    }
    
    public function nossoNumero($numero, $dadosConvenio) {

        
    }
    
    public function logo()
    {
        return '';
    }

}
