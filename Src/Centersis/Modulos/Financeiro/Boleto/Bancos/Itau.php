<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class Itau extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {
        $dadosboleto = [];
        $codigobanco = $dadosConvenio['fnc_convenio_banco'];
        $codigoBancoComDv = '341-7';
        $nummoeda = '9';

        $fatorVencimento = $this->fatorVencimento($dadosLancamento["fnc_parcela_vencimento"]);

        //valor tem 10 digitos, sem virgula
        $valorParcela = str_replace('.', ',', $dadosLancamento["fnc_parcela_valor"]);
        $valor = $this->formataNumero($valorParcela, 10, 0, "valor");

        //carteira
        $carteira = $dadosConvenio['fnc_convenio_carteira'];

        //nosso número
        $nnum = $this->formataNumero($dadosLancamento["fnc_parcela_nosso_numero"], 8, 0);

        //agencia é sempre 4 digitos
        $agencia = $this->formataNumero($dadosConvenio["fnc_convenio_agencia"], 4, 0);

        //conta
        $conta = $this->formataNumero($dadosConvenio["fnc_convenio_conta"], 6, 0);

        $codigoBarras = $codigobanco . $nummoeda . $fatorVencimento . $valor . $carteira . $nnum . $this->modulo10($agencia . $conta . $carteira . $nnum) . $agencia . $conta . $this->modulo10($agencia . $conta) . '000';

        $dv = $this->digitoVerificadorBarra($codigoBarras);

        $linha = substr($codigoBarras, 0, 4) . $dv . substr($codigoBarras, 4, 43);

        $nossonumero = $carteira . '/' . $nnum . '-' . $this->modulo10($agencia . $conta . $carteira . $nnum);
        $agenciaCodigo = $agencia . " / " . $conta . "-" . $this->modulo10($agencia . $conta);

        $dadosboleto["codigo_barras"] = $this->htmCodigoBarras($linha);
        $dadosboleto["linha_digitavel"] = $this->montaLinhaDigitavel($linha);
        $dadosboleto["agencia_codigo"] = $agenciaCodigo;
        $dadosboleto["nosso_numero"] = $nossonumero;
        $dadosboleto["codigo_banco_com_dv"] = $codigoBancoComDv;
        $dadosboleto["logoBanco"] = $this->logo();

        return $dadosboleto;
    }

    protected function digitoVerificadorBarra($barra) {
        $resto2 = $this->modulo11($barra, 9, 1);
        if ($resto2 == 0 || $resto2 == 1 || $resto2 == 10) {
            $dv = 1;
        } else {
            $dv = 11 - $resto2;
        }
        return $dv;
    }

    protected function modulo10($num) {
        $numtotal10 = 0;
        $fator = 2;

        // Separacao dos numeros
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo (falor 10)
            // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Itaú
            $temp = $numeros[$i] * $fator;
            $temp0 = 0;
            foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                $temp0 += $v;
            }
            $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            // monta sequencia para soma dos digitos no (modulo 10)
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }

        // várias linhas removidas, vide função original
        // Calculo do modulo 10
        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
    }

    protected function modulo11($num, $base = 9, $r = 0) {
        $soma = 0;
        $fator = 2;

        /* Separacao dos numeros */
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo falor
            $parcial[$i] = $numeros[$i] * $fator;
            // Soma dos digitos
            $soma += $parcial[$i];
            if ($fator == $base) {
                // restaura fator de multiplicacao para 2 
                $fator = 1;
            }
            $fator++;
        }

        /* Calculo do modulo 11 */
        if ($r == 0) {
            $soma *= 10;
            $digito = $soma % 11;
            if ($digito == 10) {
                $digito = 0;
            }
            return $digito;
        } elseif ($r == 1) {
            $resto = $soma % 11;
            return $resto;
        }
    }

    public function nossoNumero($numero, $dadosConvenio) {

        return $numero . $this->modulo11($numero);
    }

    public function logo() {
        return '';
    }

}
