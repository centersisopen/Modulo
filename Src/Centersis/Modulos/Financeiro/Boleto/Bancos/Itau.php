<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class Itau extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {
        $dadosboleto = [];
        $codigobanco = $dadosConvenio['fnc_convenio_banco'];
        $codigoBancoComDv = '341-7';
        $nummoeda = '9';

        $fatorVencimento = $this->fatorVencimento($dadosLancamento["fnc_parcela_vencimento"]);

        //valor tem 10 digitos, sem virgula
        $valorParcela = str_replace('.', ',', $dadosLancamento["fnc_parcela_valor"]);
        $valor = $this->formataNumero($valorParcela, 10, 0, "valor");

        //carteira
        $carteira = $dadosConvenio['fnc_convenio_carteira'];

        //nosso número
        $nnum = $this->formataNumero($dadosLancamento["fnc_parcela_nosso_numero"], 8, 0);

        //agencia é sempre 4 digitos
        $agencia = $this->formataNumero($dadosConvenio["fnc_convenio_agencia"], 4, 0);

        //conta
        $conta = $this->formataNumero($dadosConvenio["fnc_convenio_conta"], 5, 0);

        $codigoBarras = $codigobanco . $nummoeda . $fatorVencimento . $valor . $carteira . $nnum . $this->modulo10($agencia . $conta . $carteira . $nnum) . $agencia . $conta . $this->modulo10($agencia . $conta) . '000';

        $dv = $this->digitoVerificadorBarra($codigoBarras);

        $linha = substr($codigoBarras, 0, 4) . $dv . substr($codigoBarras, 4, 43);

        $nossonumero = $carteira . '/' . $nnum . '-' . $this->modulo10($agencia . $conta . $carteira . $nnum);
        $agenciaCodigo = $agencia . " / " . $conta . "-" . $this->modulo10($agencia . $conta);

        $dadosboleto["codigo_barras"] = $this->htmCodigoBarras($linha);
        $dadosboleto["linha_digitavel"] = $this->montaLinhaDigitavel($linha);
        $dadosboleto["agencia_codigo"] = $agenciaCodigo;
        $dadosboleto["nosso_numero"] = $nossonumero;
        $dadosboleto["codigo_banco_com_dv"] = $codigoBancoComDv;
        $dadosboleto["logoBanco"] = $this->logo();

        return $dadosboleto;
    }

    protected function digitoVerificadorBarra($barra) {
        $resto2 = $this->modulo11($barra, 9, 1);
        if ($resto2 == 0 || $resto2 == 1 || $resto2 == 10) {
            $dv = 1;
        } else {
            $dv = 11 - $resto2;
        }
        return $dv;
    }

    protected function modulo10($num) {
        $numtotal10 = 0;
        $fator = 2;

        // Separacao dos numeros
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo (falor 10)
            // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Itaú
            $temp = $numeros[$i] * $fator;
            $temp0 = 0;
            foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                $temp0 += $v;
            }
            $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            // monta sequencia para soma dos digitos no (modulo 10)
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }

        // várias linhas removidas, vide função original
        // Calculo do modulo 10
        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
    }

    protected function modulo11($num, $base = 9, $r = 0) {
        $soma = 0;
        $fator = 2;

        /* Separacao dos numeros */
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo falor
            $parcial[$i] = $numeros[$i] * $fator;
            // Soma dos digitos
            $soma += $parcial[$i];
            if ($fator == $base) {
                // restaura fator de multiplicacao para 2 
                $fator = 1;
            }
            $fator++;
        }

        /* Calculo do modulo 11 */
        if ($r == 0) {
            $soma *= 10;
            $digito = $soma % 11;
            if ($digito == 10) {
                $digito = 0;
            }
            return $digito;
        } elseif ($r == 1) {
            $resto = $soma % 11;
            return $resto;
        }
    }

    public function nossoNumero($numero, $dadosConvenio) {

        return $numero . $this->modulo11($numero);
    }

    public function logo() {
        return 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHCAgICAgICAgICD/2wBDAQcHBw0MDRgQEBgaFREVGiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICD/wAARCAAoAJYDAREAAhEBAxEB/8QAHAABAAICAwEAAAAAAAAAAAAAAAYHAQQDBQgC/8QANxAAAQQBAwMCAgYJBQAAAAAAAQIDBAUGAAcREhMhFTEiQRQjUWFxgQgWGDJCRpGT0hczVaHB/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAIEBQMB/8QAIhEBAAEEAgIDAQEAAAAAAAAAAAECAwQRBRIUMRMhIhZR/9oADAMBAAIRAxEAPwCU7zbiZum9dpcckKr4kThD8loDuurIBPCuOUgc8Dj3OtTExYmFa5WrI5bvD8sjnf3Dq1GJSh3YOW7xD+Y5/wDdOp+Jbed3wcx3hH8xT/zcOniWzuuDYvcfKrSa5QZM4ZTnbLsKcoAOfBxy250jg+PIUfOs7Lx4pdbdS8NZ6wzoGgaBoGgH20DQNA0DQNA0DQeYNxcnYj5lbsL4KkSFjnx8tbeJT9KVyEaOZRx56U/hwNWes7Q0415rH9igf0GvfjNuBzL4xB+EafGbTXZDIGpm4URhIA6mXz4+5B1T5CjUOtp6Z1jLbOgaBoGgaBoGgaBoGgaBoGg8wza6ssc/3GM+M3IMJp96MXBz21gH4h9ntrhyuZVZop1KFq122j+SYfAa23o5sRyF6m8r615DnC5RdJAab5/eUnkcjxxx89UuN525OTPb1Cd3G/DpHNp7YxZK491XTpkRovyYEd4qdQlI5PsEj28a1v6Wia9VfUOUYk9fTRYo5OPRKm8sY0W1j3jbv0CO6t0hCgUo7joCE88FfsCddbvLxcnrTPWEYtTEek62dxuyxvfGLV2DjLslMV9wmNz2wFsk8DkJ/wCtToz6cmzOibPWpJ9yd2sxqM9ymmr8gjVvpEWE9SVLsNMp2wkyG0kxkdJS5ypRHkc8c/Zqp9RDusq13FtMdx2HY3mNWUiQitbsLxVahhbENQR1SEFbz7ZV0EK8J6jx516Ne23rxaDOqYMeFZ20y9rEXFUxXRu8t6OvykBJUlQV0/F5AAA8kaDP+t+GHAq7M0plrh2z4h19chkKnOyytTf0dDQV0lzqQf4+Pv8AbQbNhurW1OON3N1T2lY/ImorYVM+y2qdJkugFtDCGnXEK6+TwSsexB40G9h24dVk82zrExZdXd05bFlUWCEIfbDyeptY7a3UKQsexSs/fxyOQiUjcvIYO4meVrjX06qxuoj2MKC0hKXS4toLXy4enkfj7DQb2K7uWd1hFZkX6o20iTYBZ+iwUMLT0oAPdQ48+0O2rq4TzwokH4fGg5k7tUF9TY36K9Jiv5wJbFJMVHSsxnoqVB0vtqcT5QUEDjqBI+Y9w7rA8lq7aJY10F2ZIXjc52lnSp/SXXpMYJ7jnUknqCuvnnhP4AcaCvL3Pcpe3ctMRl5SjCozTcU42l2Cy+3Y91PLyy+/45Sv4QlJTz7e4PIXNNmR4UR+ZKWGo0ZtTz7qvZKEAqUo/cAOdBUm0O5+W3mVz6fLGkRjZwWb/F20ICFemvuKHbWf4lIBR9/voLj0Hli7vqah3A3HFrKTFM9l1mL1hXxrWkkAdA+8e+qvL4Vd6inrHp5j1a2jKMkwqZt/ikeysmw5TS0vT6pSVF19lbpDiW+Bx+4vn39vHGsyvDvWq5/PtYpr3SlCtwsPg2NoP1oiGplQnWquphxS0ho9I/3VBCj1c8gf+ayJ4u7Vr8frfvaxN2IpRf1jBL/BMXRZ5Aipl0AX9IjKaUtx0KIP1XHzPQPY/jrVuY+RYrnrRvs4UzEwmmB39PkH6RrdhTSEzIXp6h3kc9IKWSCPPz5I1p8TZu0WZ7xpyyJ3UlOdbR59bZdl9lUIon63Kq9ivPqq5Hej9pntl5tDbC0dYPlB6/B4Orn0g1Mg2Lzec3BrnJ8G6rY+PJpkG0dloEWaAQZjTDYWh1RHABWQR+XBDv8ACdsMurMow+5tVwEox7GvQJTUZ15xSnGnOG1o62mwUqbCSeSCFeACByQ6eFsZlkLbXG6lidBTleLXCrqAsl5cF095bgZcPQlwBQWOSEeD4H26CR5hhOfZdTU86Wmsrcpxy4ZtaqO08+/CdQwB9U+4pptaStXV5Sg8Dj7TwG9guEZHEzTI81yVcVu0u0R4rFdAWt1liPFT0jl51DJWtZ8/uAD8/AcA2ztnNws1v3pDCazJqpqtjJSVqfbWhntqU4gpSjj5jhZ/LQQqLs5ud6HiVPYejTazGxJjSKtcuamJKQ6B2JTiUNJKltEq+rPj7COo8B90+zW5VLjmFR4T1Q5cYVY2DsZLrsgRpMWeVK6lkNdbbiS4odIBHHnq58aCe7S4PkeKMZIb+VGmTLu6k2weh9xKOmQlA8oWB0HlB+EFQA/iOg6LdvB9wM6hTcb9NovR33G1V93IefMyKkFJWoMBpQ7nwqHwuAEHz89Bv3WGbj3GP22Ky7CB6FKVAhxJaFPpm+mI4Fj3/hUhT7qE9KADx5PJHjQaVhsk3WZTi2S4W+uPNppXbnN2c2bJQ5WOoLbzTXeMnpUkH4AAE8+SfA0E+xNGYojTBlK4S5RlumAa/udAh+OyHO6Enu+/Vx4+zQVtu/8Ao/IzaxFzVzk11qoJRJS6nraeCPAJI8pVx454Pj7NWrWT1Q6q0P6IOckebyuP5P8A+Ou85sJRDH7H+bf85Xf0f/x155kf490fsfZv87yu4/B//HTzIeaW3s7sXD2/78+RL9Ru5KO0uQlJQhtokKLbYJUT5A+I8fgNV7t/sLW1XesaDOgaBoGgaBoGgaBoGgaBoP/Z';
    }

}
