<?php

namespace Centersis\Modulos\Financeiro\Boleto\Bancos;

class PBF extends Base {

    public function dadosBoleto($dadosLancamento, $dadosConvenio) {

        return [
            'dados_lancamento' => $dadosLancamento,
            'dados_convenio' => $dadosConvenio,
        ];
    }

    public function logo() {
        return '';
    }

}
