<?php

namespace Centersis\Modulos\Financeiro\Twig;

class Carregador {

    private $twig;
    private $loader;
    private $caminhos;
    private $conf;

    public function __construct($paths = []) {
        $this->caminhos = $paths;

        //$this->loader = new \Twig_Loader_Filesystem ($this->caminhos);
        $this->loader = new \Twig\Loader\FilesystemLoader($this->caminhos);

        $this->conf['debug'] = true;

        $this->twig = new \Twig\Environment($this->loader, $this->conf);

        $this->moedaCliente();
    }

    private function moedaCliente() {
        $this->twig()->addFunction(new \Twig\TwigFunction('moedaCliente', function ($valor) {

                            if ($valor === '' || $valor === null) {
                                return $valor;
                            }

                            if (is_numeric($valor)) {
                                $valor = (float) $valor;
                            } else {

                                $valorA = str_replace('.', '', $valor);
                                $valorB = str_replace(',', '.', $valorA);
                                $valor = (float) $valorB;
                            }

                            return number_format($valor, 2, ',', '.');
                        }));
    }

    /**
     * Retorna a instancia do Twig
     *
     * @return \Twig_Environment
     */
    public function twig() {
        return $this->twig;
    }

    /**
     * Retorna o Loader do Twig
     *
     * @return \Twig_Loader_Filesystem
     */
    public function loader() {
        return $this->loader;
    }

    /**
     * Renderiza um template do twig com dados opcionais, caso o template nao
     * exista, o Twig lancara um exception, caso aconteca um erro de compilacao,
     * outra exception sera lancada e por fim, caso haja um erro em tempo de
     * execucao, outra exception sera lancada.
     *
     * @param string $template o nome do template a ser renderizado
     * @param array  $dados    os dados para serem disponibilizados  para o tpl
     * @throws \Twig_Error_Loader  Quando um template nao pode ser encontrado
     * @throws \Twig_Error_Syntax  Quando ha um erro durante a compilacao
     * @throws \Twig_Error_Runtime Quando ha um erro em tempo de execucao
     * @return string O Template renderizado
     */
    public function render($template, $dados = []) {
        return $this->twig->render($template, $dados);
    }

    /**
     * Adiciona um caminho absoluto no comeco do \Twig_Loader
     *
     * @param string $caminho
     * @return self
     */
    public function setCaminhoAbsolutoAntes($caminho) {
        $this->loader->prependPath($caminho);
        return $this;
    }

    /**
     * Adiciona um caminho no inicio dos caminhos ja existentes e carregados no
     * \Twig_Loader
     *
     * @param string $caminho o caminho a ser adicionado
     * @return self
     */
    public function setCaminhoAntes($caminho) {
        $caminhoCompleto = SIS_DIR_BASE . $caminho;

        array_unshift($this->caminhos, $caminhoCompleto);

        $this->loader->prependPath($caminhoCompleto);
        return $this;
    }

    /**
     * Adiciona um caminho no final dos caminhos ja existentes e carregados no
     * \Twig_Loader
     *
     * @param string $caminho o caminho a ser adicionado
     * @return self
     */
    public function setCaminhoDepois($caminho) {
        $caminhoCompleto = SIS_DIR_BASE . $caminho;

        $this->caminhos[] = $caminhoCompleto;

        $this->loader->addPath($caminhoCompleto);
        return $this;
    }

    /**
     * Adiciona configuracao ao Twig, o argumento tem que ser um array onde a
     * chave sera a chave de configuracao e o valor, o valor da configuracao.
     *
     * $carregador = new Carregador();
     * $carregador->setConf(['debug' => true]);
     *
     * @param array $conf
     * @return self
     */
    public function setConf($conf) {
        if (is_array($conf)) {
            $key = key($conf);
            $this->conf[$key] = $conf[$key];
        }
        return $this;
    }

    /**
     * Retorna os caminhos adicionados ao \Twig_Loader
     *
     * @return array lista de caminhos
     */
    public function getCaminhos() {
        return $this->caminhos;
    }

}
