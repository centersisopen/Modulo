<?php

namespace Centersis\Modulos\Financeiro\Testes;

use Centersis\Financeiro\Boleto\Boleto;
use Centersis\Financeiro\Twig\Carregador;

class GerarBoleto extends Boleto {

    public function __construct() {
        parent::__construct();
    }

    public function gerar($lancamentoCod) {

        $carregador = new Carregador(__DIR__ . '/../Recursos/Views');

        $dadosLancamento = [
            'fnc_parcela_nosso_numero' => '10757791000000600',
            'fnc_parcela_cod' => 1,
            'fnc_parcela_vencimento' => '2021-07-30',
            'fnc_parcela_valor' => '300.00',
            'fnc_parcela_pago' => 'N',
            'fnc_lancamento_data_base' => '2021-07-30',
            'fnc_lancamento_data' => '2021-07-30',
        ];

        $dadosConvenio = [
            "fnc_convenio_cod" => 1,
            "organograma_cod" => 4,
            "empresa_cod" => 1,
            "fnc_forma_cod" => 1,
            "fnc_config_cod" => 1,
            "fnc_convenio_identifica" => "Viacred",
            "fnc_convenio_numero" => 101004,
            "fnc_convenio_carteira" => 1,
            "fnc_convenio_banco" => "085",
            "fnc_convenio_banco_dv" => 0,
            "fnc_convenio_agencia" => "0101",
            "fnc_convenio_agencia_dv" => "5",
            "fnc_convenio_conta" => "1075779",
            "fnc_convenio_conta_dv" => "1",
            "fnc_convenio_multa" => 0.00,
            "fnc_convenio_multa_juros" => 0.00,
            "fnc_convenio_dias_reaviso" => NULL,
            "fnc_convenio_inicio_nosso_numero" => 600,
            "fnc_convenio_dispensa_valor" => "N",
            "fnc_convenio_dispensa_vencimento" => "N",
            "fnc_convenio_carteira_variacao" => NULL,
            "fnc_convenio_permite_remessa" => NULL,
            "fnc_convenio_byte_idt" => NULL,
            "fnc_convenio_posto" => NULL,
        ];

        $logoBoleto = $this->logoBoleto();

        $dadosView = $this->dadosView($dadosLancamento, $dadosConvenio, $logoBoleto);       

        return $carregador->render('boleto.html.twig', $dadosView);
    }

    public function dadosPagador() {
        return [
            'nome' => 'Associação Filosófica e Beneficente Arquitetos da Ponte',
            'documento' => '38.011.675/0001-09',
            'endereco' => 'Rua Julia Sartori Tozzo',
            'numero' => '49',
            'endereco_complemento' => 'apto 102',
            'cidade' => 'Cordilheira Alta',
            'estado' => 'SC',
            'cep' => '89.819-000',
            'bairro' => 'Lotm Ludovico J Tozzo'
        ];
    }

    public function dadosBeneficiario() {
        return [
            'nome' => 'Pablo Vanni',
            'documento' => '002.895.351-74',
            'endereco' => 'Rua das Oliveiras',
            'numero' => '33',
            'endereco_complemento' => 'Apt 1501',
            'cidade' => 'Cuiabá',
            'estado' => 'MT',
            'cep' => '78050-267',
            'bairro' => 'Centro'
        ];
    }

    private function dadosView($dadosLancamento, $dadosConvenio, $logoBoleto) {

        return [
            'dadosConvenio' => $dadosConvenio,
            'dadosLancamento' => $dadosLancamento,
            'dadosBoleto' => $this->getDadosBoleto($dadosLancamento, $dadosConvenio),
            'beneficiario' => $this->dadosBeneficiario(),
            'pagador' => $this->dadosPagador(),
            'logoBoleto' => $logoBoleto,
            'destinoContabil' => '',
            'lancamentoAbertos' => []
        ];
    }

    private function logoBoleto() {
        return 'http://www.centersis.com.br/v03/img/logo.png';
    }

}
