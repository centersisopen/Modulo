<?php

namespace Centersis\Modulos\Financeiro\Remessa;

class GerarRemessa {

    public function gerar($remessaNumero, $parcelas, $pagadores, $dadosConvenio, $caminhoBase, $nomeFisico, $objetoRemessa) {

        $iCnab = 'Centersis\\Modulos\\Financeiro\\Remessa\\' . $dadosConvenio['fnc_tipo'].'\\'.$dadosConvenio['fnc_tipo'];
        $cnab = new $iCnab();

        $iBanco = 'Centersis\\Modulos\\Financeiro\\Remessa\\' . $dadosConvenio['fnc_tipo'] . '\\Gerar\\' . $dadosConvenio['fnc_banco_id'];
        $banco = new $iBanco();

        $banco->gerar($cnab, $remessaNumero, $parcelas, $pagadores, $dadosConvenio, $caminhoBase, $nomeFisico, $objetoRemessa);
    }

}
