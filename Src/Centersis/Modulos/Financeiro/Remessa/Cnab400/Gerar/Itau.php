<?php

namespace Centersis\Modulos\Financeiro\Remessa\Cnab400\Gerar;

class Itau
{
    public function gerar($cnab, $remessaNumero, $parcelas, $pagadores, $dadosConvenio, $caminhoBase, $nomeFisico)
    {
        $sequencial = 0;               
        
        $headerArquivo = [
            6 => $dadosConvenio['fnc_convenio_agencia'],
            8 => $dadosConvenio['fnc_convenio_conta'],
            9 => $dadosConvenio['fnc_convenio_conta_dv'],
            11 => $dadosConvenio['empresa_nome'],
            16 => ++$sequencial,
        ];               
        
        $cnab->setHeaderArquivo($headerArquivo);        

        $valorTotal = 0;

        foreach ($parcelas as $parcela) {

            $sequencial++;
            
            $valorTotal += $parcela['fnc_parcela_valor'];

            $dadosPagador = $pagadores[$parcela['fnc_parcela_cod']];
            
            $tipoUm = [
                2 => 2,
                3 => $dadosConvenio['empresa_cnpj'],
                4 => $dadosConvenio['fnc_convenio_agencia'],
                6 => $dadosConvenio['fnc_convenio_conta'],
                7 => $dadosConvenio['fnc_convenio_conta_dv'],
                10 => $parcela['fnc_parcela_cod'],
                11 => $parcela['fnc_parcela_nosso_numero'],
                13 => $dadosConvenio['fnc_convenio_carteira'],
                17 => $parcela['fnc_parcela_cod'],
                18 => $parcela['fnc_parcela_vencimento'],
                19 => $parcela['fnc_parcela_valor'],
                24 => (new \DateTime($parcela['fnc_lancamento_data']))->format('Y-m-d'),
                28 => date('Y-m-d'),
                32 => ((strlen($dadosPagador['documento']) > 14) ? 2 : 1),
                33 => $dadosPagador['documento'],
                34 => $dadosPagador['nome'],
                36 => $dadosPagador['endereco'] . " N. " . $dadosPagador['endereco_numero'],
                37 => $dadosPagador['bairro'],
                38 => $dadosPagador['prefixo_cep'] . $dadosPagador['sufixo_cep'],
                39 => $dadosPagador['cidade'],
                40 => $dadosPagador['estado'],
                41 => $dadosConvenio['empresa_nome'],
                46 => ++$sequencial
            ];                                  

            $cnab->setTipoUm($tipoUm);                                 
        }

        $trailerArquivo = [
            3 => (++$sequencial)
        ];

        $cnab->setTrailerArquivo($trailerArquivo);      

        $cnab->gerar($dadosConvenio['fnc_banco_id'], $caminhoBase, $nomeFisico, [
            'case' => 'upper',
            'obrigatorio' => ['headerArquivo', 'tipoUm', 'trailerArquivo']
        ]);
    }

}