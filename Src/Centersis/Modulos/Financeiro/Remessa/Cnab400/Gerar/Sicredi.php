<?php

namespace Centersis\Modulos\Financeiro\Remessa\Cnab400\Gerar;

class Sicredi
{
    public function gerar($cnab, $remessaNumero, $parcelas, $pagadores, $dadosConvenio, $caminhoBase, $nomeFisico)
    {
        $sequencial = 1;
        
        $headerArquivo = [
            6 => $dadosConvenio['fnc_convenio_numero'],
            7 => $dadosConvenio['empresa_cnpj'],
            13 => $remessaNumero,
            16 => $sequencial,
        ];
        
        $cnab->setHeaderArquivo($headerArquivo);        

        $valorTotal = 0;

        foreach ($parcelas as $parcela) {

            $sequencial++;
            
            $valorTotal += $parcela['fnc_parcela_valor'];

            $dadosPagador = $pagadores[$parcela['fnc_parcela_cod']];
            
            $tipoUm = [
                10 => $parcela['fnc_parcela_nosso_numero'],
                12 => date('Y-m-d'),
                24 => $parcela['fnc_parcela_cod'],
                25 => $parcela['fnc_parcela_vencimento'],
                26 => $parcela['fnc_parcela_valor'],
                30 => (new \DateTime($parcela['fnc_lancamento_data']))->format('Y-m-d'),
                31 => ($parcela['fnc_parcela_protestar'] == 'S') ? '06' : '00',
                32 => ($parcela['fnc_parcela_protestar'] == 'S') ? $parcela['fnc_parcela_protestar_dias'] : '00',
                38 => ((strlen($dadosPagador['documento']) > 14) ? 2 : 1),
                40 => $dadosPagador['documento'],
                41 => $dadosPagador['nome'],
                42 => $dadosPagador['endereco'] . " N. " . $dadosPagador['endereco_numero'],
                46 => $dadosPagador['prefixo_cep'] . $dadosPagador['sufixo_cep'],
                50 => $sequencial,
            ];

            $cnab->setTipoUm($tipoUm);                                 
        }

        $trailerArquivo = [
            4 => $dadosConvenio['fnc_convenio_numero'],
            6 => ($sequencial + 1)
        ];

        $cnab->setTrailerArquivo($trailerArquivo);      

        $cnab->gerar($dadosConvenio['fnc_banco_id'], $caminhoBase, $nomeFisico, [
            'case' => 'upper',
            'obrigatorio' => ['headerArquivo', 'tipoUm', 'trailerArquivo']
        ]);
    }

}
