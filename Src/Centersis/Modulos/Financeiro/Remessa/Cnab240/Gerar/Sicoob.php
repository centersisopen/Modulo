<?php

namespace Centersis\Modulos\Financeiro\Remessa\Cnab240\Gerar;

class Sicoob {

    public function gerar($cnab, $remessaNumero, $parcelas, $pagadores, $dadosConvenio, $caminhoBase, $nomeFisico, $objetoRemessa) {
        $headerArquivo = [
            6 => $dadosConvenio['empresa_cnpj'],
            8 => $dadosConvenio['fnc_convenio_agencia'],
            9 => $dadosConvenio['fnc_convenio_agencia_dv'],
            10 => $dadosConvenio['fnc_convenio_conta'],
            11 => $dadosConvenio['fnc_convenio_conta_dv'],
            13 => $dadosConvenio['empresa_nome'],
            19 => $remessaNumero,
        ];

        if (method_exists($objetoRemessa, 'headerArquivo')) {

            $dadosGeracao = [
                'remessaNumero' => $remessaNumero,
                'dadosConvenio' => $dadosConvenio,
            ];

            $headerArquivo = $objetoRemessa->headerArquivo($headerArquivo, $dadosGeracao);
        }

        $cnab->setHeaderArquivo($headerArquivo);

        $lote = 1;

        $headerLote = [
            10 => $dadosConvenio['empresa_cnpj'],
            12 => $dadosConvenio['fnc_convenio_agencia'],
            13 => $dadosConvenio['fnc_convenio_agencia_dv'],
            14 => $dadosConvenio['fnc_convenio_conta'],
            15 => $dadosConvenio['fnc_convenio_conta_dv'],
            17 => $dadosConvenio['empresa_nome'],
            20 => $remessaNumero,
        ];

        if (method_exists($objetoRemessa, 'headerLote')) {

            $dadosGeracao = [
                'remessaNumero' => $remessaNumero,
                'dadosConvenio' => $dadosConvenio,
            ];

            $headerLote = $objetoRemessa->headerLote($headerLote, $dadosGeracao);
        }

        $cnab->setHeaderLote($headerLote, $lote);

        $valorTotal = 0;
        $sequencial = 0;
        $registros = 0;

        foreach ($parcelas as $parcela) {

            $valorTotal += $parcela['fnc_parcela_valor'];

            $dadosPagador = $pagadores[$parcela['fnc_parcela_cod']];

            $sequencial++;
            $registros++;
            $segmentoP = [
                4 => $sequencial,
                8 => $dadosConvenio['fnc_convenio_agencia'],
                9 => $dadosConvenio['fnc_convenio_agencia_dv'],
                10 => $dadosConvenio['fnc_convenio_conta'],
                11 => $dadosConvenio['fnc_convenio_conta_dv'],
                13 => $this->montaNossoNumeroSicoob($dadosConvenio, $parcela),
                14 => $dadosConvenio['fnc_convenio_carteira'],
                19 => $parcela['fnc_parcela_cod'],
                20 => $parcela['fnc_parcela_vencimento'],
                21 => $parcela['fnc_parcela_valor'],
                26 => (new \DateTime($parcela['fnc_lancamento_data']))->format('Y-m-d'),
                35 => $parcela['fnc_lancamento_nome'],
            ];

            if ($parcela['fnc_parcela_juros_forma']) {
                $segmentoP[27] = (($parcela['fnc_parcela_juros_forma'] == 'valor') ? '1' : '2');
                $segmentoP[28] = (($parcela['fnc_parcela_juros_data']) ? $parcela['fnc_parcela_juros_data'] : $parcela['fnc_parcela_vencimento']);
                $segmentoP[29] = $parcela['fnc_parcela_juros'];
            }

            if ($parcela['fnc_parcela_desconto_forma']) {
                $segmentoP[30] = (($parcela['fnc_parcela_desconto_forma'] == 'valor') ? '1' : '2');
                $segmentoP[31] = (($parcela['fnc_parcela_desconto_data']) ? $parcela['fnc_parcela_desconto_data'] : $parcela['fnc_parcela_vencimento']);
                $segmentoP[32] = $parcela['fnc_parcela_desconto'];
            }

            if (method_exists($objetoRemessa, 'segmentoP')) {

                $dadosGeracao = [
                    'sequencial' => $sequencial,
                    'parcela' => $parcela,
                    'dadosConvenio' => $dadosConvenio,
                ];

                $segmentoP = $objetoRemessa->segmentoP($segmentoP, $dadosGeracao);
            }

            $cnab->setSegmentoP($segmentoP, $lote);

            $sequencial++;
            $registros++;
            $segmentoQ = [
                4 => $sequencial,
                8 => ((strlen($dadosPagador['documento']) > 14) ? 2 : 1),
                9 => $dadosPagador['documento'],
                10 => $dadosPagador['nome'],
                11 => $dadosPagador['endereco'] . " N. " . $dadosPagador['endereco_numero'],
                12 => $dadosPagador['bairro'],
                13 => $dadosPagador['prefixo_cep'],
                14 => $dadosPagador['sufixo_cep'],
                15 => $dadosPagador['cidade'],
                16 => $dadosPagador['estado'],
                18 => $dadosConvenio['empresa_cnpj'],
                19 => $dadosConvenio['empresa_nome'],
            ];

            if (method_exists($objetoRemessa, 'segmentoQ')) {

                $dadosGeracao = [
                    'sequencial' => $sequencial,
                    'dadosPagador' => $dadosPagador,
                ];

                $segmentoQ = $objetoRemessa->segmentoQ($segmentoQ, $dadosGeracao);
            }

            $cnab->setSegmentoQ($segmentoQ, $lote);
        }

        $trailerLote = [
            5 => $registros,
            6 => 0,
            7 => 0,
        ];

        if (method_exists($objetoRemessa, 'trailerLote')) {

            $dadosGeracao = [
                'registros' => $registros,
            ];

            $trailerLote = $objetoRemessa->trailerLote($trailerLote, $dadosGeracao);
        }

        $cnab->setTrailerLote($trailerLote, $lote);

        $trailerArquivo = [
            6 => ($registros + 4)
        ];

        if (method_exists($objetoRemessa, 'trailerArquivo')) {

            $dadosGeracao = [
                'registros' => $registros,
            ];

            $trailerArquivo = $objetoRemessa->trailerArquivo($trailerArquivo, $dadosGeracao);
        }

        $cnab->setTrailerArquivo($trailerArquivo);

        $cnab->gerar($dadosConvenio['fnc_banco_id'], $caminhoBase, $nomeFisico, [
            'case' => 'upper',
            'obrigatorio' => ['headerArquivo', 'headerLote', 'segmentoP', 'segmentoQ', 'trailerLote', 'trailerArquivo']
        ]);
    }

    private function montaNossoNumeroSicoob($dadosConvenio, $parcela) {
        $p1 = $this->completa($parcela['fnc_parcela_nosso_numero'], 10, '0');
        $p2 = '01';
        $p3 = $this->completa($dadosConvenio['fnc_convenio_carteira'], '2', '0');
        $p4 = '4';
        $p5 = $this->completa('', 5, ' ');

        return $p1 . $p2 . $p3 . $p4 . $p5;
    }

    private function completa($valor, $casas, $completar) {
        $tamanho = strlen($valor);

        if ($tamanho > $casas) {
            return substr($valor, 0, $casas);
        } else {

            if ($completar === '0') {
                return str_repeat($completar, $casas - $tamanho) . $valor;
            } else {
                return $valor . str_repeat($completar, $casas - $tamanho);
            }
        }
    }

}
