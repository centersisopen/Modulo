<?php

namespace Centersis\Modulos\Financeiro\Remessa\Cnab240\Gerar;

class Ailos
{
    public function gerar($cnab, $remessaNumero, $parcelas, $pagadores, $dadosConvenio, $caminhoBase, $nomeFisico, $posicoesFixas)
    {
        $headerArquivo = [
            6 => $dadosConvenio['empresa_cnpj'],
            7 => $dadosConvenio['fnc_convenio_numero'],
            8 => $dadosConvenio['fnc_convenio_agencia'],
            9 => $dadosConvenio['fnc_convenio_agencia_dv'],
            10 => $dadosConvenio['fnc_convenio_conta'],
            11 => $dadosConvenio['fnc_convenio_conta_dv'],
            13 => $dadosConvenio['empresa_nome'],
            19 => $remessaNumero,
        ];

        $cnab->setHeaderArquivo($headerArquivo);

        $lote = 1;

        $headerLote = [
            10 => $dadosConvenio['empresa_cnpj'],
            11 => $dadosConvenio['fnc_convenio_numero'],
            12 => $dadosConvenio['fnc_convenio_agencia'],
            13 => $dadosConvenio['fnc_convenio_agencia_dv'],
            14 => $dadosConvenio['fnc_convenio_conta'],
            15 => $dadosConvenio['fnc_convenio_conta_dv'],
            17 => $dadosConvenio['empresa_nome'],
            20 => $remessaNumero,
        ];

        $cnab->setHeaderLote($headerLote, $lote);

        $valorTotal = 0;
        $sequencial = 0;
        $registros = 0;

        foreach ($parcelas as $parcela) {

            $valorTotal += $parcela['fnc_parcela_valor'];

            $dadosPagador = $pagadores[$parcela['fnc_parcela_cod']];

            $sequencial++;
            $registros++;
            $segmentoP = [
                4 => $sequencial,
                8 => $dadosConvenio['fnc_convenio_agencia'],
                9 => $dadosConvenio['fnc_convenio_agencia_dv'],
                10 => $dadosConvenio['fnc_convenio_conta'],
                11 => $dadosConvenio['fnc_convenio_conta_dv'],
                13 => $parcela['fnc_parcela_nosso_numero'],
                14 => $dadosConvenio['fnc_convenio_carteira'],
                19 => $parcela['fnc_parcela_cod'],
                20 => $parcela['fnc_parcela_vencimento'],
                21 => $parcela['fnc_parcela_valor'],
                22 => $dadosConvenio['fnc_convenio_agencia'],
                23 => $dadosConvenio['fnc_convenio_agencia_dv'],
                26 => (new \DateTime($parcela['fnc_lancamento_data']))->format('Y-m-d'),
                35 => $parcela['fnc_parcela_cod'],
            ];

            $cnab->setSegmentoP($segmentoP, $lote);
            
            $sequencial++;
            $registros++;
            $segmentoQ = [
                4 => $sequencial,
                8 => ((strlen($dadosPagador['documento']) > 14) ? 2 : 1),
                9 => $dadosPagador['documento'],
                10 => $dadosPagador['nome'],
                11 => $dadosPagador['endereco'] . " N. " . $dadosPagador['endereco_numero'],
                12 => $dadosPagador['bairro'],
                13 => $dadosPagador['prefixo_cep'],
                14 => $dadosPagador['sufixo_cep'],
                15 => $dadosPagador['cidade'],
                16 => $dadosPagador['estado'],
            ];

            $cnab->setSegmentoQ($segmentoQ, $lote);
        }

        $trailerLote = [
            5 => $registros + 1,
            6 => 0,
            7 => 0,
        ];

        $cnab->setTrailerLote($trailerLote, $lote);

        $trailerArquivo = [
            6 => ($registros + 4)
        ];

        $cnab->setTrailerArquivo($trailerArquivo);

        $cnab->gerar($dadosConvenio['fnc_banco_id'], $caminhoBase, $nomeFisico, [
            'case' => 'upper',
            'obrigatorio' => ['headerArquivo', 'headerLote', 'segmentoP', 'segmentoQ', 'trailerLote', 'trailerArquivo']
        ]);
    }

}
