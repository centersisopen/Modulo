<?php

namespace Centersis\Modulos\Financeiro\Remessa\Cnab240\Gerar;

class Santander {

    public function gerar($cnab, $remessaNumero, $parcelas, $pagadores, $dadosConvenio, $caminhoBase, $nomeFisico, $objetoRemessa) {
        $headerArquivo = [
            6 => $dadosConvenio['empresa_cnpj'],
            7 => $dadosConvenio['fnc_convenio_cod_transmissao'],
            9 => $dadosConvenio['empresa_nome'],
            15 => $remessaNumero,
        ];

        if (method_exists($objetoRemessa, 'headerArquivo')) {

            $dadosGeracao = [
                'remessaNumero' => $remessaNumero,
                'dadosConvenio' => $dadosConvenio,
            ];

            $headerArquivo = $objetoRemessa->headerArquivo($headerArquivo, $dadosGeracao);
        }

        $cnab->setHeaderArquivo($headerArquivo);

        $lote = 1;

        $headerLote = [
            10 => $dadosConvenio['empresa_cnpj'],
            12 => $dadosConvenio['fnc_convenio_cod_transmissao'],
            14 => $dadosConvenio['empresa_nome'],
            17 => $remessaNumero,
        ];

        if (method_exists($objetoRemessa, 'headerLote')) {

            $dadosGeracao = [
                'remessaNumero' => $remessaNumero,
                'dadosConvenio' => $dadosConvenio,
            ];

            $headerLote = $objetoRemessa->headerLote($headerLote, $dadosGeracao);
        }

        $cnab->setHeaderLote($headerLote, $lote);

        $valorTotal = 0;
        $sequencial = 0;
        $registros = 0;

        foreach ($parcelas as $parcela) {

            $valorTotal += $parcela['fnc_parcela_valor'];

            $dadosPagador = $pagadores[$parcela['fnc_parcela_cod']];

            $sequencial++;
            $registros++;
            $segmentoP = [
                4 => $sequencial,
                8 => $dadosConvenio['fnc_convenio_agencia'],
                9 => $dadosConvenio['fnc_convenio_agencia_dv'],
                10 => $dadosConvenio['fnc_convenio_conta'],
                11 => $dadosConvenio['fnc_convenio_conta_dv'],
                12 => $dadosConvenio['fnc_convenio_conta'],
                13 => $dadosConvenio['fnc_convenio_conta_dv'],
                15 => $this->montaNossoNumeroSantander($dadosConvenio, $parcela),
                21 => $parcela['fnc_parcela_cod'],
                22 => $parcela['fnc_parcela_vencimento'],
                23 => $parcela['fnc_parcela_valor'],
                29 => (new \DateTime($parcela['fnc_lancamento_data']))->format('Y-m-d'),
                34 => $parcela['fnc_parcela_vencimento'],
                38 => $parcela['fnc_lancamento_nome'] . ' ' . $parcela['fnc_parcela_nome'],
            ];

            if (method_exists($objetoRemessa, 'segmentoP')) {

                $dadosGeracao = [
                    'sequencial' => $sequencial,
                    'parcela' => $parcela,
                    'dadosConvenio' => $dadosConvenio,
                ];

                $segmentoP = $objetoRemessa->segmentoP($segmentoP, $dadosGeracao);
            }

            $cnab->setSegmentoP($segmentoP, $lote);

            $sequencial++;
            $registros++;
            $segmentoQ = [
                4 => $sequencial,
                8 => ((strlen($dadosPagador['documento']) > 14) ? 2 : 1),
                9 => $dadosPagador['documento'],
                10 => $dadosPagador['nome'],
                11 => $dadosPagador['endereco'] . " N. " . $dadosPagador['endereco_numero'],
                12 => $dadosPagador['bairro'],
                13 => $dadosPagador['prefixo_cep'],
                14 => $dadosPagador['sufixo_cep'],
                15 => $dadosPagador['cidade'],
                16 => $dadosPagador['estado'],
                18 => $dadosConvenio['empresa_cnpj'],
                19 => $dadosConvenio['empresa_nome'],
            ];

            if (method_exists($objetoRemessa, 'segmentoQ')) {

                $dadosGeracao = [
                    'sequencial' => $sequencial,
                    'dadosPagador' => $dadosPagador,
                ];

                $segmentoQ = $objetoRemessa->segmentoQ($segmentoQ, $dadosGeracao);
            }

            $cnab->setSegmentoQ($segmentoQ, $lote);
        }

        $trailerLote = [
            5 => ($registros + 2),
        ];

        if (method_exists($objetoRemessa, 'trailerLote')) {

            $dadosGeracao = [
                'registros' => $registros,
            ];

            $trailerLote = $objetoRemessa->trailerLote($trailerLote, $dadosGeracao);
        }

        $cnab->setTrailerLote($trailerLote, $lote);

        $trailerArquivo = [
            6 => ($registros + 4)
        ];

        if (method_exists($objetoRemessa, 'trailerArquivo')) {

            $dadosGeracao = [
                'registros' => $registros,
            ];

            $trailerArquivo = $objetoRemessa->trailerArquivo($trailerArquivo, $dadosGeracao);
        }

        $cnab->setTrailerArquivo($trailerArquivo);

        $cnab->gerar($dadosConvenio['fnc_banco_id'], $caminhoBase, $nomeFisico, [
            'case' => 'upper',
            'obrigatorio' => ['headerArquivo', 'headerLote', 'segmentoP', 'segmentoQ', 'trailerLote', 'trailerArquivo']
        ]);
    }

    private function montaNossoNumeroSantander($dadosConvenio, $parcela) {
        return $parcela['fnc_parcela_nosso_numero'];
    }
}
