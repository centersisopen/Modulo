<?php

namespace Centersis\Modulos\Financeiro\Remessa\Cnab240\Layout;

class Santander {

    public function headerArquivo() {
        return [
            1 => [3, 'num'],
            2 => [4, 'num'],
            3 => [1, 'num'],
            4 => [8, 'texto'],
            5 => [1, 'num'],
            6 => [15, 'doc'],
            7 => [15, 'num'],
            8 => [25, 'texto'],
            9 => [30, 'texto'],
            10 => [30, 'texto'],
            11 => [10, 'texto'],
            12 => [1, 'num'],
            13 => [8, 'data-dmY'],
            14 => [6, 'texto'],
            15 => [6, 'num'],
            16 => [3, 'num'],
            17 => [74, 'texto'],
        ];
    }

    public function headerArquivoDefault() {
        return [
            1 => '033',
            2 => 0,
            3 => 0,
            4 => '',
            5 => 2,
            8 => '',
            10 => 'BANCO SANTANDER',
            11 => '',
            12 => '1',
            13 => date('Y-m-d'),
            14 => '',
            16 => '040',
            17 => '',
        ];
    }

    public function headerArquivoValidacao() {
        return [
        ];
    }

    public function headerLote() {
        return [
            1 => [3, 'num'],
            2 => [4, 'num'],
            3 => [1, 'num'],
            4 => [1, 'texto'],
            5 => [2, 'num'],
            6 => [2, 'texto'],
            7 => [3, 'num'],
            8 => [1, 'texto'],
            9 => [1, 'texto'],
            10 => [15, 'num'],
            11 => [20, 'texto'],
            12 => [15, 'num'],
            13 => [5, 'texto'],
            14 => [30, 'texto'],
            15 => [40, 'texto'],
            16 => [40, 'texto'],
            17 => [8, 'num'],
            18 => [8, 'data-dmY'],
            19 => [41, 'texto']
        ];
    }

    public function headerLoteDefault() {
        return [
            1 => '033',
            2 => 1,
            3 => 1,
            4 => 'R',
            5 => 1,
            6 => '',
            7 => '030',
            8 => '',
            9 => '2',
            11 => '',
            13 => '',
            15 => '',
            16 => '',
            18 => date('Y-m-d'),
            19 => '',
        ];
    }

    public function headerLoteValidacao() {
        
    }

    public function headerLoteDinamico() {
        return [];
    }

    public function segmentoP() {
        return [
            1 => [3, 'num'],
            2 => [4, 'num'],
            3 => [1, 'num'],
            4 => [5, 'num'],
            5 => [1, 'texto'],
            6 => [1, 'texto'],
            7 => [2, 'num'],
            8 => [4, 'num'],
            9 => [1, 'num'],
            10 => [9, 'num'],
            11 => [1, 'num'],
            12 => [9, 'num'],
            13 => [1, 'num'],
            14 => [2, 'texto'],
            15 => [13, 'num'],
            16 => [1, 'texto'],
            17 => [1, 'num'],
            18 => [1, 'num'],
            19 => [1, 'texto'],
            20 => [1, 'texto'],
            21 => [15, 'texto'],
            22 => [8, 'data-dmY'],
            23 => [15, 'valor'],
            24 => [4, 'num'],
            25 => [1, 'num'],
            26 => [1, 'texto'],
            27 => [2, 'texto'],
            28 => [1, 'texto'],
            29 => [8, 'data-dmY'],
            30 => [1, 'num'],
            31 => [8, 'data-dmY'],
            32 => [15, 'valor'],
            33 => [1, 'num'],
            34 => [8, 'data-dmY'],
            35 => [15, 'valor'],
            36 => [15, 'valor'],
            37 => [15, 'valor'],
            38 => [25, 'texto'],
            39 => [1, 'num'],
            40 => [2, 'num'],
            41 => [1, 'num'],
            42 => [1, 'num'],
            43 => [2, 'num'],
            44 => [2, 'num'],
            45 => [11, 'texto'],
        ];
    }

    public function segmentoPDefault() {
        return [
            1 => '033',
            2 => 1,
            3 => 3,
            5 => 'P',
            6 => '',
            7 => 1,
            14 => '',
            16 => 5,
            17 => 1,
            18 => 1,
            19 => '',
            20 => '',
            24 => 0,
            25 => 0,
            26 => '',
            27 => '02',
            28 => 'N',
            30 => 3,
            31 => 0,
            32 => 0,
            33 => 0,
            35 => 0,
            36 => 0,
            37 => 0,
            39 => 0,
            40 => 0,
            41 => 1,
            42 => 0,
            43 => 90,
            44 => 0,
            45 => ''
        ];
    }

    public function segmentoPValidacao() {
        return [
        ];
    }

    public function segmentoPDinamico() {
        return [];
    }

    public function segmentoQ() {
        return [
            1 => [3, 'num'],
            2 => [4, 'num'],
            3 => [1, 'num'],
            4 => [5, 'num'],
            5 => [1, 'texto'],
            6 => [1, 'texto'],
            7 => [2, 'num'],
            8 => [1, 'num'],
            9 => [15, 'num'],
            10 => [40, 'texto'],
            11 => [40, 'texto'],
            12 => [15, 'texto'],
            13 => [5, 'num'],
            14 => [3, 'num'],
            15 => [15, 'texto'],
            16 => [2, 'texto'],
            17 => [1, 'num'],
            18 => [15, 'num'],
            19 => [40, 'texto'],
            20 => [3, 'num'],
            21 => [3, 'num'],
            22 => [3, 'num'],
            23 => [3, 'num'],
            24 => [19, 'texto'],
        ];
    }

    public function segmentoQDefault() {
        return [
            1 => '033',
            2 => 1,
            3 => 3,
            5 => 'Q',
            6 => '',
            7 => 1,
            17 => 2,
            20 => 0,
            21 => 0,
            22 => 0,
            23 => 0,
            24 => ''
        ];
    }

    public function segmentoQValidacao() {
        return [
        ];
    }

    public function segmentoR() {
        return [
            1 => [3, 'num'],
            2 => [4, 'num'],
            3 => [1, 'num'],
            4 => [5, 'num'],
            5 => [1, 'texto'],
            6 => [1, 'texto'],
            7 => [2, 'num'],
            8 => [1, 'num'],
            9 => [8, 'data-dmY'],
            10 => [15, 'valor'],
            11 => [1, 'num'],
            12 => [8, 'data-dmY'],
            13 => [15, 'valor'],
            14 => [1, 'texto'],
            15 => [8, 'data-dmY'],
            16 => [15, 'valor'],
            17 => [10, 'texto'],
            18 => [40, 'texto'],
            19 => [40, 'texto'],
            20 => [20, 'texto'],
            21 => [8, 'data-dmY'],
            22 => [3, 'num'],
            23 => [5, 'num'],
            24 => [1, 'texto'],
            25 => [12, 'num'],
            26 => [1, 'texto'],
            27 => [1, 'texto'],
            28 => [1, 'num'],
            29 => [9, 'texto'],
        ];
    }

    public function segmentoRDefault() {
        return [
            1 => $array[0],
            2 => 0001,
            3 => 3,
            5 => 'R',
            6 => '',
            9 => 0,
            10 => 0,
            12 => 0,
            13 => 0,
            17 => '',
            19 => '',
            20 => '',
            21 => 00000000,
            22 => 000,
            23 => 00000,
            24 => '',
            25 => 00000000000000,
            26 => '',
            27 => '',
            28 => 0,
            29 => '',
        ];
    }

    public function segmentoRValidacao() {
        return [
                /* 7 => 'validaMovimentoRemessa',
                  8 => 'validaCodigo',
                  //9 => 'validaJuroseDescData',
                  10 => 'validaJurosDescValor',
                  11 => 'validaCodigo',
                  12 => 'validaJuroseDescData',
                  13 => 'validaJurosDescValor',
                  14 => 'validaCodigo',
                  //15 => 'validaJuroseDescData',
                  16 => 'validaJurosDescValor', */
        ];
    }

    public function segmentoRDinamico() {
        return [];
    }

    public function trailerLote() {
        return [
            1 => [3, 'num'],
            2 => [4, 'num'],
            3 => [1, 'num'],
            4 => [9, 'texto'],
            5 => [6, 'num'],
            6 => [217, 'texto'],
        ];
    }

    public function trailerLoteDefault() {
        return [
            1 => '033',
            2 => 1,
            3 => 5,
            4 => '',
            6 => ''
        ];
    }

    public function trailerLoteValidacao() {
        return [];
    }

    public function trailerArquivo() {
        return [
            1 => [3, 'num'],
            2 => [4, 'num'],
            3 => [1, 'num'],
            4 => [9, 'texto'],
            5 => [6, 'num'],
            6 => [6, 'num'],
            7 => [211, 'texto']
        ];
    }

    public function trailerArquivoDefault() {
        return [
            1 => '033',
            2 => 9999,
            3 => 9,
            4 => '',
            5 => 1,
            7 => '',
        ];
    }

    public function trailerArquivoValidacao() {
        return [];
    }
}
