<?php

namespace Centersis\Modulos\Financeiro\FecharMes\Sistema\Padrao;

use Centersis\Ext\Form\Form;

use Centersis\Ext\Core\Padrao\BaseForm;

class FecharMesForm extends BaseForm
{

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $formCod = null)
    {
        $form = new Form();

        $form->setAcao($acao);

        $form->config('formManu' . $formCod, 'POST')
            ->setHeader('Fechar Mês');

        $campos[] = $form->hidden('cod')
            ->setValor($form->retornaValor('cod'));

        $campos[] = $form->data('fnc_fechado_data', 'Data de Fechamento')
            ->setValor($form->retornaValor('fnc_fechado_data'));

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

}
