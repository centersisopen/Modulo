<?php

namespace Centersis\Modulos\Financeiro\FecharMes\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class FecharMesSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function filtrarSql($objForm, $colunas) {

        $qb = $this->con->qb();

        $qb->select('a.fnc_fechado_cod, a.fnc_fechado_data ')
                ->from('fnc_fechado', 'a');

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $colunas, $qb, 'a', $this->organogramaCod);

        return $qb;
    }

    public function getDadosSql($cod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_fechado', '')
                ->where($qb->expr()->eq('fnc_fechado_cod', ':fnc_fechado_cod'))
                ->setParameter('fnc_fechado_cod', $cod, \PDO::PARAM_INT);

        $this->doOrganograma($qb);

        return $qb;
    }

    public function existeFechamentoSql() {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_fechado', '');

        $this->doOrganograma($qb);

        return $qb;
    }

    public function permiteLancamentoSql($data) {
        $qb = $this->con->qb();

        $qb->select('fnc_fechado_data')
                ->from('fnc_fechado', '')
                ->where($qb->expr()->gte('fnc_fechado_data', ':fnc_fechado_data'))
                ->setParameter('fnc_fechado_data', $data, \PDO::PARAM_STR)
                ->setMaxResults(1);

        $this->doOrganograma($qb);

        return $qb;
    }

}
