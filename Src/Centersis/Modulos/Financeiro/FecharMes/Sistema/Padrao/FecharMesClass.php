<?php

namespace Centersis\Modulos\Financeiro\FecharMes\Sistema\Padrao;

use Centersis\Zion2\Tratamento\Tratamento;
use Centersis\Zion2\Exception\ValidationException;
use Centersis\Ext\Core\Padrao\BaseClass;

class FecharMesClass extends BaseClass {

    public function __construct($organogramaCod, $con, $sql) {

        parent::__construct($organogramaCod, $con, $sql);

        $this->tabela = 'fnc_fechado';
        $this->chavePrimaria = 'fnc_fechado_cod';

        $this->colunasCrud = [
            'usuario_cod',
            'fnc_fechado_data'
        ];

        $this->colunasGrid = [
            'fnc_fechado_data' => 'Data de Fechamento'
        ];

        $this->filtroDinamico = [
        ];
    }

    public function grid($grid) {

        $grid->formatarComo('fnc_fechado_data', 'Data');
        $grid->setConfiguracaoPersonalizada(false);
        $grid->setSelecaoMultipla(false);
        $grid->naoOrdenePor(['fnc_fechado_data']);
    }

    public function cadastrar($objForm) {

        $usuarioCod = $_SESSION['usuario_cod'] ?? session()->get('usuario_cod');
        
        if ($this->con->execNLinhas($this->sql->existeFechamentoSql()) > 0) {
            throw new ValidationException('Não é possível ter mais de um registro de fechamento, você pode editar o registro existente!');
        }

        $objForm->set('usuario_cod', $usuarioCod, 'Inteiro');

        parent::cadastrar($objForm);
    }

    public function alterar($objForm) {

        $usuarioCod = $_SESSION['usuario_cod'] ?? session()->get('usuario_cod');

        $objForm->set('usuario_cod', $usuarioCod, 'Inteiro');

        parent::alterar($objForm);
    }

    public function permiteLancamento($data) {
        $trata = Tratamento::instancia();

        if (empty($data)) {
            return;
        }

        $dataFechamento = $this->con->execRLinha($this->sql->permiteLancamentoSql($data));

        if ($dataFechamento) {
            throw new ValidationException('Não é possível realizar esta operação!
                <br>Data informada: ' . $trata->data()->converteData($data) .
                            '<br>Data de Fechamento: ' . $trata->data()->converteData($dataFechamento) .
                            '<br>A data informada deve ser maior que a data de fechamento.');
        }
    }

}
