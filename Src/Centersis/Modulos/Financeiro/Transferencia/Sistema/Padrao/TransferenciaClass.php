<?php

namespace Centersis\Modulos\Financeiro\Transferencia\Sistema\Padrao;

use Centersis\Financeiro\FecharMes\Sistema\FecharMesClassFactory;
use Centersis\Financeiro\Movimentacao\Sistema\ContaClassFactory;
use Centersis\Ext\Core\Padrao\BaseClass;
use Centersis\Ext\Form\Form;

class TransferenciaClass extends BaseClass {

    public function __construct($organogramaCod, $con, $sql) {
        
        parent::__construct($organogramaCod, $con, $sql);

        $this->tabela = 'fnc_transferencia';
        $this->chavePrimaria = 'fnc_transferencia_cod';

        $this->colunasCrud = [
            'usuario_cod',
            'fnc_conta_contabil_cod_origem',
            'fnc_conta_contabil_cod_destino',
            'fnc_transferencia_valor',
            'fnc_transferencia_data',
            'fnc_transferencia_motivo'
        ];

        $this->colunasGrid = [
            'conta_origem' => 'Origem',
            'conta_destino' => 'Destino',
            'fnc_transferencia_valor' => 'Valor',
            'fnc_transferencia_data' => 'Data',
            'fnc_transferencia_motivo' => 'Motivo'
        ];

        $this->filtroDinamico = [
            'fnc_transferencia_valor' => 'a',
            'fnc_transferencia_motivo' => 'a'
        ];
    }

    public function grid($grid) {
        $grid->setFormatarComo('fnc_transferencia_data', 'Data');
        $grid->setFormatarComo('fnc_transferencia_valor', 'MOEDA');
        $grid->substituaPor('fnc_transferencia_data', ['<em>não informada</em>']);
    }

    public function cadastrar($objForm) {
        
        $usuarioCod = $_SESSION['usuario_cod'] ?? session()->get('usuario_cod');
        
        $this->crudUtil->startTransaction();

        $dataTransferencia = $objForm->get('fnc_transferencia_data');

        $contaClass = (new ContaClassFactory())->instancia($this->organogramaCod);
        $fecharMesClass = (new FecharMesClassFactory())->instancia($this->organogramaCod);
        $fecharMesClass->permiteLancamento($dataTransferencia);

        $objForm->set('usuario_cod', $usuarioCod);
        $fncContaContabilCodOrigem = $objForm->get('fnc_conta_contabil_cod_origem');
        $fncContaContabilCodDestino = $objForm->get('fnc_conta_contabil_cod_destino');
        $fncContaValor = $objForm->get('fnc_transferencia_valor');

        $fncTransferenciaCod = $this->crudUtil->insert($this->organogramaCod, $this->tabela, $this->colunasCrud, $objForm);

        $contaClass->persisteLancamento($fncContaContabilCodOrigem, $fncContaValor, 'D');
        $contaClass->persisteLancamento($fncContaContabilCodDestino, $fncContaValor, 'C');

        $contaClass->confereValoresContabeis();

        $this->crudUtil->stopTransaction();

        return $fncTransferenciaCod;
    }

    public function remover($cod) {
        
        $usuarioCod = $_SESSION['usuario_cod'] ?? session()->get('usuario_cod');
        
        $objForm = new Form();
        $contaClass = (new ContaClassFactory())->instancia($this->organogramaCod);
        $fecharMesClass = (new FecharMesClassFactory())->instancia($this->organogramaCod);

        $this->crudUtil->startTransaction();

        $dados = $this->getDados($cod);

        $objForm->set('usuario_cod', $usuarioCod);
        $fncContaContabilCodOrigem = $dados['fnc_conta_contabil_cod_destino'];
        $fncContaContabilCodDestino = $dados['fnc_conta_contabil_cod_origem'];
        $fncContaValor = $dados['fnc_transferencia_valor'];
        $fncTransferenciaData = $dados['fnc_transferencia_data'];

        $fecharMesClass->permiteLancamento($fncTransferenciaData);

        $contaClass->persisteLancamento($fncContaContabilCodOrigem, $fncContaValor, 'D');
        $contaClass->persisteLancamento($fncContaContabilCodDestino, $fncContaValor, 'C');

        $contaClass->confereValoresContabeis();

        $this->crudUtil->delete($this->organogramaCod, $this->tabela, [$this->chavePrimaria => $cod]);

        $this->crudUtil->stopTransaction();
    }

}
