<?php

namespace Centersis\Modulos\Financeiro\Transferencia\Sistema\Padrao;

use Centersis\Ext\Core\Padrao\BaseSql;

class TransferenciaSql extends BaseSql {

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    public function filtrarSql($objForm, $colunas) {

        $qb = $this->con->qb();

        $qb->select('a.fnc_transferencia_cod, b.fnc_conta_contabil_nome as conta_origem, 
            c.fnc_conta_contabil_nome as conta_destino, a.fnc_transferencia_valor, 
            a.fnc_transferencia_motivo, a.fnc_transferencia_data')
                ->from('fnc_transferencia', 'a')
                ->innerJoin('a', 'fnc_conta_contabil', 'b', 'a.fnc_conta_contabil_cod_origem = b.fnc_conta_contabil_cod')
                ->innerJoin('a', 'fnc_conta_contabil', 'c', 'a.fnc_conta_contabil_cod_destino = c.fnc_conta_contabil_cod');

        $this->crudUtil->filtrar($this->filtrarClass, $objForm, $colunas, $qb, 'a', $this->organogramaCod);

        return $qb;
    }

    public function getDadosSql($cod) {
        $qb = $this->con->qb();

        $qb->select('*')
                ->from('fnc_transferencia', '')
                ->where($qb->expr()->eq('fnc_transferencia_cod', ':fnc_transferencia_cod'))
                ->setParameter('fnc_transferencia_cod', $cod, \PDO::PARAM_INT);

        $this->doOrganograma($qb);

        return $qb;
    }

}
