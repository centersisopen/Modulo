<?php

namespace Centersis\Modulos\Financeiro\Transferencia\Sistema\Padrao;

use Centersis\Ext\Form\Form;
use Centersis\Financeiro\PlanoContas\Sistema\PlanoContasFormFactory;

use Centersis\Ext\Core\Padrao\BaseForm;

class TransferenciaForm extends BaseForm
{

    public function __construct($organogramaCod, $con) {
        parent::__construct($organogramaCod, $con);
    }

    /**
     * 
     * @return \Pixel\Form\Form
     */
    public function getFormManu($acao, $formCod = null)
    {
        $form = new Form();
        $formPlano = (new PlanoContasFormFactory())->instancia($this->organogramaCod);

        $form->setAcao($acao);

        $form->config('formManu' . $formCod, 'POST')
            ->setHeader('Transferências');

        $campos[] = $form->hidden('cod')
            ->setValor($form->retornaValor('cod'));

        $campos[] = $formPlano->getFormContaContabilPorGrupo($form, '1,2')
            ->setNome('fnc_conta_contabil_cod_origem')
            ->setIdentifica('Conta de Origem')
            ->setValor($form->retornaValor('fnc_conta_contabil_cod_origem'));

        $campos[] = $formPlano->getFormContaContabilPorGrupo($form, '1,2')
            ->setNome('fnc_conta_contabil_cod_destino')
            ->setIdentifica('Conta de Destino')
            ->setValor($form->retornaValor('fnc_conta_contabil_cod_destino'));

        $campos[] = $form->float('fnc_transferencia_valor', 'Valor', true)
            ->setValor($form->retornaValor('fnc_transferencia_valor'));

        $campos[] = $form->data('fnc_transferencia_data', 'Data', true)
            ->setValor($form->retornaValor('fnc_transferencia_data'));

        $campos[] = $form->textArea('fnc_transferencia_motivo', 'Motivo', false)
            ->setLinhas(3)
            ->setValor($form->retornaValor('fnc_transferencia_motivo'));

        $campos[] = $form->botaoSalvarPadrao();

        $campos[] = $form->botaoDescartarPadrao();

        return $form->processarForm($campos);
    }

}
