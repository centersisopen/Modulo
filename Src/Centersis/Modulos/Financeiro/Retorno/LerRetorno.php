<?php

namespace Centersis\Modulos\Financeiro\Retorno;

class LerRetorno {

    protected $bancos;

    public function __construct() {

        $this->bancos = [
            '001' => 'BancoBrasil',
            '033' => 'Santander',
            '041' => 'Banrisul',
            '085' => 'Ailos',
            '104' => 'Caixa',
            '136' => 'Unicred',
            '237' => 'Bradesco',
            '341' => 'Itau',
            '422' => 'Safra',
            '748' => 'Sicredi',
            '756' => 'Sicoob',
        ];
    }

    public function lerArquivo($caminhoArquivo) {
        if (!file_exists($caminhoArquivo)) {
            throw new \Exception('Arquivo não encontrado!');
        }

        if (!is_readable($caminhoArquivo)) {
            throw new \Exception('Arquivo ser permissão para leitura!');
        }

        $arquivo = fopen($caminhoArquivo, 'r');

        $linhas = [];

        while (!feof($arquivo)) {

            $linhas[] = trim(fgets($arquivo, 9999), "\x00..\x1F");
        }

        fclose($arquivo);

        return $this->dadosDoBanco($linhas);
    }

    protected function dadosDoBanco($linhas) {

        $primeiraLinha = current($linhas);

        $tamanho = strlen($primeiraLinha);

        if ($tamanho == 240) {
            $codigoBanco = substr($primeiraLinha, 0, 3);
        } else if ($tamanho == 400) {
            $codigoBanco = substr($primeiraLinha, 76, 3);
        } else {
            throw new \Exception('Arquivo inválido! - T:' . $tamanho);
        }

        if (!key_exists($codigoBanco, $this->bancos)) {
            throw new \Exception('Código do banco não encontrado!');
        }

        $caminhoClass = 'Centersis\\Modulos\\Financeiro\\Retorno\\Cnab' . $tamanho . '\\' . $this->bancos[$codigoBanco];
        $classBancoLaitura = new $caminhoClass;

        return $classBancoLaitura->dadosLeitura($linhas);
    }
}
