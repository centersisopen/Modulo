<?php

namespace Centersis\Modulos\Financeiro\Retorno\Cnab240;

class Caixa {

    public function dadosLeitura($linhasDoArquivo) {

        $retorno = [];
        
        foreach ($linhasDoArquivo as $linha) {

            $segmento = substr($linha, 13, 1); //Segmento T ou U


            if ($segmento === 'T') {
                $nossoNumero = trim(substr($linha, 36, 20));                
            }


            if ($segmento === 'U') {
                
                $codMovimento = substr($linha, 15, 2);
                
                if($codMovimento <> '6'){
                    continue;
                }
                
                $dataOcorencia = substr(substr($linha, 137, 8), 4, 4) . '-' . substr(substr($linha, 137, 8), 2, 2) . '-' . substr(substr($linha, 137, 8), 0, 2); //Data da ocorrência

                $dataEfetivacao = substr(substr($linha, 145, 8), 4, 4) . '-' . substr(substr($linha, 145, 8), 2, 2) . '-' . substr(substr($linha, 145, 8), 0, 2); //Data da efetivação do credito
                
                $valorPago = substr($linha, 77, 13) . "." . substr($linha, 90, 2);
                
                $retorno[] = [
                    'ret_nosso_numero' => $nossoNumero,
                    'ret_valor_pago' => $valorPago + 0,
                    'ret_data_pagamento' => $dataOcorencia,
                    'ret_data_compensa' => $dataEfetivacao
                ];
            }
        }

        return $retorno;
    }

}
