<?php

namespace Centersis\Modulos\Financeiro\Retorno\Cnab240;

class Sicoob {

    public function dadosLeitura($linhasDoArquivo) {

        $retorno = [];
        
        foreach ($linhasDoArquivo as $linha) {

            $segmento = substr($linha, 13, 1); //Segmento T ou U

            if ($segmento === 'T') {
                $nossoNumero = trim(substr($linha, 37, 10));
            }

            if ($segmento === 'U') {

                $valorPago = substr($linha, 81, 9) . "." . substr($linha, 90, 2);

                $codMovimento = substr($linha, 15, 2); //Codigo de Movimento ***igual ao informado no segmento T***

                $dataOcorencia = substr(substr($linha, 137, 8), 4, 4) . '-' . substr(substr($linha, 137, 8), 2, 2) . '-' . substr(substr($linha, 137, 8), 0, 2); //Data da ocorrência

                $dataEfetivacao = substr(substr($linha, 145, 8), 4, 4) . '-' . substr(substr($linha, 145, 8), 2, 2) . '-' . substr(substr($linha, 145, 8), 0, 2); //Data da efetivação do credito


                if ($codMovimento <> '6') {
                    continue;
                }

                $retorno[] = [
                    'ret_nosso_numero' => $nossoNumero + 0,
                    'ret_valor_pago' => $valorPago + 0,
                    'ret_data_pagamento' => $dataOcorencia,
                    'ret_data_compensa' => $dataEfetivacao
                ];
            }
        }

        return $retorno;
    }

}
