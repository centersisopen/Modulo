<?php

namespace Centersis\Modulos\Financeiro\Retorno\Cnab240;

class BancoBrasil {

    public function dadosLeitura($linhasDoArquivo) {

        $retorno = [];
        
        foreach ($linhasDoArquivo as $linha) {

            $segmento = substr($linha, 13, 1); //Segmento T ou U

            $tamanhoConvenio = 0; //Identificar como resolver isso
            

            if ($segmento == 'T') {

                $codMovimento = substr($linha, 15, 2); //Codigo de Movimento (Vide Layout)

                $nossoNumeroProvisorio = trim(substr($linha, 37, 20)); //Identificacao do Titulo no Banco /Nosso Numero/ *** trunca em 11 + dv ou 17 ***
                $tamanhoNossoNumero = strlen($nossoNumeroProvisorio);
                $inicioNossoNumero = $tamanhoNossoNumero - $tamanhoConvenio;

                if ($tamanhoNossoNumero == 12) {
                    $nossoNumero = (substr($nossoNumeroProvisorio, $tamanhoConvenio, ($inicioNossoNumero - 1)));
                } elseif ($tamanhoNossoNumero == 17) {
                    $nossoNumero = (substr($nossoNumeroProvisorio, 7, ($inicioNossoNumero)));
                }
            }

            if ($segmento == 'U') {

                $codMovimento = substr($linha, 15, 2); //Codigo de Movimento ***igual ao informado no segmento T***

                $valorPago = number_format((substr($linha, 77, 13) . '.' . substr($linha, 90, 2)), 2, ',', '.'); //Valor Pago pelo Sacado

                $dataOcorencia = substr(substr($linha, 137, 8), 4, 4) . '-' . substr(substr($linha, 137, 8), 2, 2) . '-' . substr(substr($linha, 137, 8), 0, 2); //Data da ocorrência

                $dataEfetivacao = substr(substr($linha, 145, 8), 4, 4) . '-' . substr(substr($linha, 145, 8), 2, 2) . '-' . substr(substr($linha, 145, 8), 0, 2); //Data da efetivação do credito

                if ($codMovimento <> '6') {
                    continue;
                }

                $retorno[] = [
                    'ret_nosso_numero' => $nossoNumero,
                    'ret_valor_pago' => $valorPago + 0,
                    'ret_data_pagamento' => $dataOcorencia,
                    'ret_data_compensa' => $dataEfetivacao,
                ];
            }
        }

        return $retorno;
    }

}
