<?php

namespace Centersis\Modulos\Financeiro\Retorno\Cnab400;

class Unicred {

    public function dadosLeitura($linhasDoArquivo) {

        $retorno = [];
        
        foreach ($linhasDoArquivo as $linha) {

            $tipoRegistro = substr($linha, 0, 1);
            $ocorrencia = substr($linha, 108, 2);

            if ($tipoRegistro == 1 and $ocorrencia == '06') {

                $nossoNumero = substr($linha, 51, 11);
                $valorPago = substr($linha, 253, 11) . '.' . substr($linha, 264, 2);
                $dataPagamento = '20' . substr($linha, 114, 2) . '-' . substr($linha, 112, 2) . '-' . substr($linha, 110, 2);
                $dataCompensa = '20' . substr($linha, 179, 2) . '-' . substr($linha, 177, 2) . '-' . substr($linha, 175, 2);

                $retorno[] = [
                    'ret_nosso_numero' => $nossoNumero,
                    'ret_valor_pago' => $valorPago + 0,
                    'ret_data_pagamento' => $dataPagamento,
                    'ret_data_compensa' => $dataCompensa,
                ];
            }
        }

        return $retorno;
    }

}
