<?php

namespace Centersis\Modulos\Financeiro\Retorno\Cnab400;

class Sicredi {

    public function dadosLeitura($linhasDoArquivo) {

        $retorno = [];
        
        foreach ($linhasDoArquivo as $linha) {

            $tipoRegistro = substr($linha, 0, 1);
            $ocorrencia = substr($linha, 108, 2);


            if ($tipoRegistro == 1 and $ocorrencia == '06') {

                $nossoNumero = substr($linha, 47, 15);
                $valorPago = substr($linha, 253, 11).'.'.substr($linha, 264, 2);
                $dataPagamento = '20'.substr($linha, 114, 2).'-'.substr($linha, 112, 2).'-'.substr($linha, 110, 2);
                $dataCompensa = substr($linha, 328, 4).'-'.substr($linha, 332, 2).'-'.substr($linha, 334, 2);

                $retorno[] = [
                    'ret_nosso_numero' => (int) $nossoNumero,
                    'ret_valor_pago' => $valorPago + 0,
                    'ret_data_pagamento' => $dataPagamento,
                    'ret_data_compensa' => $dataCompensa,
                ];
            }
        }

        return $retorno;
    }

}
