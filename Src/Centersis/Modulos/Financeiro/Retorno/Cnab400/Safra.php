<?php

namespace Centersis\Modulos\Financeiro\Retorno\Cnab400;

class Safra {

    public function dadosLeitura($linhasDoArquivo) {

        $retorno = [];

        foreach ($linhasDoArquivo as $linha) {

            $tipoRegistro = substr($linha, 0, 1);
            $codigoMovimento = substr($linha, 108, 2);

            if ($tipoRegistro != '1' or $codigoMovimento != '06') { //é detalhe e é liquidação
                continue;
            }

            $nossoNumero = substr($linha, 62, 9);
            $valorPago = number_format((substr($linha, 253, 11) . '.' . substr($linha, 264, 2)), 2, ',', '.');
            $dataPagamento = '20' . substr($linha, 114, 2) . '-' . substr($linha, 112, 2) . '-' . substr($linha, 110, 2);
            $dataCompensacao = '20' . substr($linha, 299, 2) . '-' . substr($linha, 297, 2) . '-' . substr($linha, 295, 2);

            $retorno[] = [
                'ret_nosso_numero' => (int) $nossoNumero,
                'ret_valor_pago' => $valorPago + 0,
                'ret_data_pagamento' => $dataPagamento,
                'ret_data_compensa' => $dataCompensacao,
            ];
        }

        return $retorno;
    }

}
